<?php
return array(
	'Please_select_condition' => '请选择导出数据条件',
	'No_export_data' => '无导出数据',
	'The_export_failed' => '导出失败',
	'NOTE_EXCEL' => array(
		'手机号',
		'内容',
		'申请时间'
	),
);