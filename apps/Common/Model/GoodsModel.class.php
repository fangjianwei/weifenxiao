<?php
/**
 * ====================================
 * 这里是说明
 * ====================================
 * Author: 91336
 * Date: 2014/11/24 16:35
 * ====================================
 * File: GoodsModel.class.php
 * ====================================
 */
namespace Common\Model;
use Common\Library\Goods;
class GoodsModel extends CommonModel {


    protected $_validate = array(
        array('goods_sn','require','{%GOODS_SN_LOST}'),
        array('goods_name','require','{%GOODS_NAME_LOST}'),
        array('cat_id','require','{%CAT_ID_LOST}'),
        array('goods_sn','checkedsn','{%GOODS_SN_EXISTS}', self::MUST_VALIDATE, 'callback', self::MODEL_BOTH),
    );

    public function checkedsn($goods_sn){
        if($goods_sn == '-' && $_POST['relation_goods']) return true;
        $goods = $this
               ->where(array('goods_id' => array('neq',$_POST['goods_id']),'goods_sn' => $_POST['goods_sn'],'cat_id' => $_POST['cat_id']))
               ->count();
        $res = $goods > 0 ? false : true;
        return $res;
    }
    public function format($params){
        $channel = C('CHANNEL');
        foreach($params['rows'] as $key => $val){
            $val['cat_name'] = $channel[$val['cat_id']];
            switch($val['cat_id']){
                case 2:
                    $val['market_price'] = $val['fx_market_price'];
                    $val['shop_price'] = $val['flagship_price'];
                    break;
                case 3:
                    $val['market_price'] = $val['wp_market_price'];
                    $val['shop_price'] = $val['wp_shop_price'];
                    break;
            }
            $params['rows'][$key] = $val;
        }
        return $params;
    }

    public function filter($params) {
        $where = array();
        $this->alias(' AS g');
        if($params['keywords']){
            $map['g.goods_sn'] = array('LIKE', "%{$params['keywords']}%");
            $map['g.goods_name'] = array('LIKE', "%{$params['keywords']}%");
            $map['_logic'] = 'OR';
            $where['_complex'] = $map;
        }
        if($params['cat_id']) {
            $where['g.cat_id'] = (int)$params['cat_id'];
        }
        if (isset($params['is_package'])) {
            $where['g.is_package'] = (int)$params['is_package'];
        }
        if ($params['is_on_sale']) {        
            $where['g.is_on_sale'] = $params['is_on_sale'];
        }
        if (isset($params['relation_id'])) {
            if (empty($params['relation_id'])) {
                $params['relation_id'] = 0;
            }
            if (empty($params['goods_id'])) {
                $params['goods_id'] = 0;
            }
            $where['g.goods_id'] = array('in',$params['relation_id']);
            $this->join('LEFT JOIN __GOODS_PACKAGE__ AS p ON p.goods_id = '
                .$params['goods_id'].' AND p.relation_id =g.goods_id');
        }
        $this
            ->join('__GOODS_CATEGORY__ as c ON g.cat_id = c.id', 'LEFT')
            ->where($where);
        if (isset($params['relation_id'])) {
            $this->field('g.*,p.goods_number');
        }else{
            $this->field('g.*');
        }
      //  echo $this->select(false);exit;
        return $where;

    }

    public function getSale($gradeId, $cat_id = 0, $page_size = 5,$price_select = '') {
        $where = array(
            'cat_id' => IS_SPECIAL == 1 ? CHANNEL_ID : 1,
            'is_on_sale' => 1,
            'is_alone_sale' => 1,
            '_string' => "(is_package = 0 and goods_stock > 0) or is_package = 1"
        );
        if($cat_id) {
            $where['cat_id'] = array('IN', $cat_id);
        }
        $this->order('goods_id desc');
        $data = $this->where($where)->getPage(array('page_size'=>$page_size, 'url_demo' => U('home/goods/index/page/[page]')));
        if($data) {
            foreach($data['rows'] as $key => $row) {
                $row['agent_price'] = $this->getPrice($row['goods_id'], $gradeId, $row['shop_price']);
                $row['shop_price'] = $row[$price_select];
                //如果是套装，查出套装下的子产品最小货存（商品总库存量/商品件数=套装库存）
                if($row['is_package']) {
                    $row['goods_stock'] = get_package_stock($row['goods_id'], $row['relation_id']);
                    if(IS_SPECIAL == 1){
                        $row['goods_info'] = Goods::getPackGoods($row['relation_id'],$row['goods_id']);
                    }
                }

                if($row['goods_stock'] < 1) {
                    unset($data['rows'][$key]);
                    continue;
                }

                if (IS_MOBILE) {
                    $row['goods_name'] = mb_substr($row['goods_name'], 0, 10,'utf-8');
                }

                $data['rows'][$key] = $row;
            }
        }
        return $data;
    }

    /**
     * 返回等级商品价格
     * @param $goodsId
     * @param $gradeId
     * @return mixed
     */
    public function getPrice($goodsId, $gradeId, $shopPrice) {
        $gradePrice = $this
            ->table('__GOODS_GRADE_PRICE__')
            ->where(array('grade_id' => $gradeId, 'goods_id' => $goodsId))
            ->getField('shop_price');
        return $gradePrice ? $gradePrice : $shopPrice;
    }


}