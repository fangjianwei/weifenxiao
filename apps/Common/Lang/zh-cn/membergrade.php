<?php
return array(
	//验证
	'grade_name_lost'			=> 	'等级名称不能为空！',
	'orderby_lost'				=>	'等级排序不能为空！',
	'grade_name_is_exist' 		=>	'等级名称已存在！',
	'orderby_is_exist'			=>	'等级排序已存在！',
	'orderby_must_be_number'	=>	'等级排序必须为正整数！',
	'pay_money_must_be_number'	=>	'预付货款必须为正整数！',
);