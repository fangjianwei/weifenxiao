<?php
/**
 * 付款账号
 * User: 800015
 * Date: 2015/10/16
 * Time: 15::29
 */
namespace Common\Model;
class PayAccountModel extends CommonModel {
    /**
     * 获取列表
     * @param array $params
     * @return mixed
     */
   public function filter($params){
       if(isset($params['keywords'])){
          $where = array();
          $where= array('account_id' => $params['keywords']);
          $where['account_name'] = array('LIKE', "%{$params['keywords']}%");
          $where['_logic'] = 'OR';
          return $this->where($where);
       }
   }
}