<?php
/**
 * ====================================
 * 等级管理类
 * ====================================
 * Author: lwh
 * Date: 14-11-29 上午9:28
 * ====================================
 * File: MemberGradeController.class.php
 * ====================================
 */
namespace Manage\Controller;

use Common\Controller\ManageController;

class AgentgradeController extends ManageController {
    protected $dbTable = 'AgentGrade';

}