<?php
/**
 * 代理商类
 * User: 91336
 * Date: 2015/1/20
 * Time: 9:58
 */
namespace Common\Controller;
class AgentBase extends UCenterController {
    function _initialize(){
        parent::_initialize();
        if(!IS_MOBILE) {
            C('TMPL_ACTION_ERROR', 'agent:message');
            C('TMPL_ACTION_SUCCESS', 'agent:message');
        }

    }
}