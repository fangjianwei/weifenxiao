$(function(){
    //设定jquery同步
    $.ajaxSetup({async : false});

    //设置输入框提示
    if($.browser.msie == true && $.browser.version.slice(0,3) < 10) {
        $('input[placeholder]').each(function(){
            var input = $(this);
            $(input).val(input.attr('placeholder'));
            $(input).focus(function(){
                if (input.val() == input.attr('placeholder')) {
                    input.val('');
                }
            });
            $(input).blur(function(){
                if (input.val() == '' || input.val() == input.attr('placeholder')) {
                    input.val(input.attr('placeholder'));
                }
            });
        });
    }
});

var verify_src = '';
function verifyFresh(e) {
    e = typeof e == 'string' ? document.getElementById(e) : e;
    verify_src = verify_src == '' ? e.src : verify_src;
    e.src = verify_src + (verify_src.indexOf('?') > 0 ? '&' : '?') + 't=' + Math.random();
}

function previewImage(fileObj,imgPreviewId,divPreviewId){
    var allowExtention=".jpg,.bmp,.png,.jpeg";//允许上传文件的后缀名document.getElementById("hfAllowPicSuffix").value;
    var extention=fileObj.value.substring(fileObj.value.lastIndexOf(".")+1).toLowerCase();
    var browserVersion= window.navigator.userAgent.toUpperCase();
    if(allowExtention.indexOf(extention)>-1){
        if(fileObj.files){//兼容chrome、火狐7+、360浏览器5.5+等，应该也兼容ie10，HTML5实现预览
            if(window.FileReader){
                var reader = new FileReader();
                reader.onload = function(e){
                    document.getElementById(imgPreviewId).setAttribute("src",e.target.result);
                }
                reader.readAsDataURL(fileObj.files[0]);
            }else if(browserVersion.indexOf("SAFARI")>-1){
                alert("不支持Safari浏览器6.0以下版本的图片预览!");
            }
        }else if (browserVersion.indexOf("MSIE")>-1){//ie、360低版本预览
            if(browserVersion.indexOf("MSIE 6")>-1){//ie6
                document.getElementById(imgPreviewId).setAttribute("src",fileObj.value);
            }else{//ie[7-9]
                fileObj.select();
                if(browserVersion.indexOf("MSIE 9")>-1)
                    fileObj.blur();//不加上document.selection.createRange().text在ie9会拒绝访问
                var newPreview =document.getElementById(divPreviewId+"New");
                if(newPreview==null){
                    newPreview =document.createElement("div");
                    newPreview.setAttribute("id",divPreviewId+"New");
                    newPreview.style.width = document.getElementById(imgPreviewId).width+"px";
                    newPreview.style.height = document.getElementById(imgPreviewId).height+"px";
                    newPreview.style.border = '1px solid #a7b5bc';
                }
                newPreview.style.filter="progid:DXImageTransform.Microsoft.AlphaImageLoader(sizingMethod='scale',src='" + document.selection.createRange().text + "')";
                var tempDivPreview=document.getElementById(divPreviewId);
                tempDivPreview.parentNode.insertBefore(newPreview,tempDivPreview);
                tempDivPreview.style.display="none";
            }
        }else if(browserVersion.indexOf("FIREFOX")>-1){//firefox
            var firefoxVersion= parseFloat(browserVersion.toLowerCase().match(/firefox\/([\d.]+)/)[1]);
            if(firefoxVersion<7){//firefox7以下版本
                document.getElementById(imgPreviewId).setAttribute("src",fileObj.files[0].getAsDataURL());
            }else{//firefox7.0+
                document.getElementById(imgPreviewId).setAttribute("src",window.URL.createObjectURL(fileObj.files[0]));
            }
        }else{
            document.getElementById(imgPreviewId).setAttribute("src",fileObj.value);
        }
    }else{
        alert("仅支持"+allowExtention+"为后缀名的文件!");
        fileObj.value="";//清空选中文件
        if(browserVersion.indexOf("MSIE")>-1){
            fileObj.select();
            document.selection.clear();
        }
        fileObj.outerHTML=fileObj.outerHTML;
    }
}

function region(settings){
    try {
        if(typeof settings.element == 'undefined') return false;
        var element = $(settings.element);
        var select = element.find('select');
        select.each(function(i) {
            //i  0(省), 1(市), 2(区)
            var self = $(this);
            //查找上级ID
            var parentId = i > 0 ? select.eq(i-1).attr('selectedVal') : 1;
            var value = self.attr('selectedVal');
            if(parentId > 0) {
                set_region(parentId, self, value);
            }
            self.change(function(){
                self.nextAll('select').find('option:gt(0)').remove();
                var nextSelf = self.next('select');
                if(nextSelf.length > 0) {
                    parentId = self.val();
                    set_region(parentId, nextSelf, 0);
                }
            });
        });

    }catch (e) {
        alert('地区参数不正确');
    }
}



function set_region(parentId, self, value){
    $.ajax({
        url:'/region/query.html',
        data:{parent_id: parentId},
        dataType:'JSON',
        type:'POST',
        success:function(result){
            self.find('option:gt(0)').remove();
            $.each(result,function(k,r){
                var opt = "<option value='" + r.region_id + "' [selected] >" + r.region_name + "</option>";
                opt = opt.replace('[selected]', value == r.region_id ? 'selected' : '');
                self.append(opt);
            });
        },
        error:function(){}
    });
}

function sendCode(c, t, k) {
    $.ajaxSetup({async: false});
    var submitData = {
        sendType: t,
        token:k
    }
    if ($('#' + t).length > 0) {
        var val = $('#' + t).val();
        submitData['item'] = val;
    }
   
    $.ajax({
        url: '/' + c + '/sendcode.html',
        type: 'POST',
        data: submitData,
        dataType: 'json',
        success: function (result) {
            alert(result.info);
            return false;
        },
        error: function () {
        }
    })
    return false;
}
