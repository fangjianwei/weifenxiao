<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta name="HandheldFriendly" content="true"/>
    <meta name="keywords" content=""/>
    <meta name="description" content=""/>
    <title>订单详情</title>
    <link type="text/css" rel="stylesheet" href="/public/stage/mobile/css/reset.css?11"/>
    <link type="text/css" rel="stylesheet" href="/public/stage/mobile/css/style.css?11"/>
    <script type="text/javascript" src="/public/stage/mobile/res/jquery/jquery-2.1.3.min.js"></script>
    <link href="/public/stage/mobile/res/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>
    <link href="/public/stage/mobile/css/main.min.css" rel="stylesheet"/>
</head>
<body class="BG">
<section>
    <div class="titleBox clearfix">
        <a href="javascript:history.go(-1)" class="f-left">
            <img src="/public/stage/mobile/images/back.png"/></a>
        <span>订单详情</span>
    </div>
</section>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">
            订单信息
        </h3>
    </div>
    <div class="panel-body">
        <ul>
            <li>订单状态：<?php echo ($order["order_status_text"]); ?> - <?php echo ($order["pay_status_text"]); ?> - <?php echo ($order["shipping_status_text"]); ?></li>
            <li>订单编号：<?php echo ($order["order_sn"]); ?></li>
            <li>交易类型：<?php echo ($order["trade_type"]); ?></li>
            <li>下单时间：<?php echo ($order["create_time"]); ?></li>
            <li>商品金额：<?php echo ($order["goods_amount"]); ?></li>
        </ul>
    </div>
    <div class="panel-heading">
        <h3 class="panel-title">
            收货信息
        </h3>
    </div>
    <div class="panel-body">
        <ul>
            <li>收货人：<?php echo ($order["consignee"]); ?></li>
            <li>联系电话：<?php echo ($order["mobile"]); ?></li>
            <li>收货地址：<?php echo ($order["province_name"]); echo ($order["city_name"]); echo ($order["district_name"]); echo ($order["town"]); echo ($order["address"]); ?></li>
        </ul>
    </div>
    <div class="panel-heading">
        <h3 class="panel-title">
            订单日志
        </h3>
    </div>
    <div class="panel-body">
        <ul>
            <?php if(is_array($order_log)): $i = 0; $__LIST__ = $order_log;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?><li><?php echo ($v["create_time"]); ?>：<?php echo ($v["action_note"]); ?></li><?php endforeach; endif; else: echo "" ;endif; ?>
        </ul>
    </div>
    <div class="panel-heading">
        <h3 class="panel-title">
            商品信息
        </h3>
    </div>
    <div class="panel-body">
        <table class="table">
            <?php if(is_array($order_goods)): $i = 0; $__LIST__ = $order_goods;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?><tr>
                    <td style="vertical-align: middle">
                        <div><img src="<?php echo ($v["goods_thumb"]); ?>"></div>
                    </td>
                    <td style="vertical-align: middle">
                        <div><b><?php echo ($v["goods_name"]); ?></b></div>
                        <div class="text-muted small">价格：&yen;<?php echo ($v["goods_price"]); ?></div>
                        <div class="text-muted small">数量：<?php echo ($v["goods_number"]); ?>件</div>
                    </td>
                    <td style="vertical-align: middle">
                        <div><b>小计：<font color="red">&yen;<?php echo ($v[goods_number]*$v[goods_price]); ?></font></b></div>
                    </td>
                </tr><?php endforeach; endif; else: echo "" ;endif; ?>
        </table>
    </div>
    <div class="panel-footer">
        <div class="pull-right">商品总金额(￥<?php echo ($order["goods_amount"]); ?>)</div>
        <div class="clearfix"></div>
    </div>
</div>

<div class="hidHeight"></div>
<div class="navBottom">
    <dl class="clearfix">
        <dd class="navBottomFirst">
            <p class="navBottomItem">
                <!--<a href="stu_profile.html"><img src="/public/stage/mobile/images/tp8lz7ik.png" /></a>-->
                <?php if(defined('USER_ID')): ?><a href="<?php echo U('home/member/index');?>"><img src="<?php echo (HEADIMGURL); ?>" /></a>
                <?php else: ?>
                    <!--用户没设置头像或未获取到用户头像，显示默认头像-->
                    <a href="<?php echo U('home/member/index');?>"><img src="/public/stage/mobile/images/tp8lz7ik.png" /></a><?php endif; ?>
            </p>
        </dd>
        <dd>
            <p class="navBottomItem"><a href="<?php echo U('goods/index');?>">产品购买</a></p>
        </dd>

        <dd>
            <p class="navBottomItem"><a href="<?php echo U('member/index');?>">代理中心</a></p>
        </dd>

        <dd>
            <p class="navBottomItem"><a href="http://mp.weixin.qq.com/s?__biz=MzA4NTU1NjU2NQ==&mid=404012163&idx=1&sn=d0767f8bd157c37f25f63e70f55ff9af&scene=1&srcid=0314PVTY9tnDILY0LJunffqM#rd">客服中心</a></p>
        </dd>
    </dl>
</div>