<?php
/**
 * Created by PhpStorm.
 * User: 9009078
 * Date: 2016/5/20
 * Time: 13:53
 */
namespace Common\Model;

use Common\Library\Common;

class WithdrawTypeModel extends CommonModel {

    /**
     * 获取用户当前绑定的支付方式
     * @return array
     */
    public function get_withdraw_type(){
        $web_payment = C('WEB_PAYMENT');    //参数1:支付ID号；参数2：支付方式名称；参数3：是否支持该支付方式付款(0否1是)；参数4：是否支持该支付方式提现（0否1是）；参数5：手续费
        $where['user_id'] = 1;
        $data = M('member_withdraw_type')->where($where)->select();
        foreach($web_payment as $key => $value){
            $web_payment[$key] = explode('-',$value);   //支付方式格式转换
            foreach($data as $k => $v){
                if($v['payment_key'] == $web_payment[$key][0]){
                    $web_payment[$key]['member_withdraw'] = $v;
                }
            }
        }
        return $web_payment;
    }

    /**
     * 编辑提现方式
     * @return array
     */
    public function edit_withdraw_type($params){
        $where['user_id'] = 1;
        $where['payment_key'] = $params['payment_key'];
        $data = M('member_withdraw_type')->where($where)->find();
        if($data['payment_info']){
            $data['payment_info'] = unserialize($data['payment_info']);   //反序列化
        }
        return $data;
    }

    /**
     * 获取用户默认提现方式
     * @return array
     */
    public function get_default_withdraw_type(){
        $web_payment = C('WEB_PAYMENT');
        $where['is_default'] = array('eq'=>1);
        $where['user_id'] = array('eq'=>1);
        $data = M('member_withdraw_type')->field('withdraw_id,payment_key')->where($where)->find();
        if($data){
            $payment_key = $data['payment_key'];
        }else{
            $payment_key = 'WeChatpay';
        }
        foreach($web_payment as $key => $value){
            //获取默认提现方式信息（前提该支付可用）
            $web_payment[$key] = explode('-',$value);   //支付方式格式转换
            if($web_payment[$key][0] == $payment_key && $web_payment[$key][3] == 1){
                $data['payment_name'] = $web_payment[$key][1];
                $data['payment_fee'] = $web_payment[$key][4];
            }
        }
        return $data;
    }
}