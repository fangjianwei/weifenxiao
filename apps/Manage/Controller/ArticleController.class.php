<?php
/**
 * ====================================
 * 文章管理类
 * ====================================
 * Author: Hugo
 * Date: 14-5-20 下午9:28
 * ====================================
 * File: ArticleController.class.php
 * ====================================
 */
namespace Manage\Controller;

use Common\Controller\ManageController;

class ArticleController extends ManageController {

    /**
     * 添加或更改记录
     */
    public function save() {
        $params = I('post.');
        $params['save_type'] = $params['id'] ? 'update' : 'insert';
        $data = $this->dbModel->create($params) or $this->error($this->dbModel->getError());
        $result = false;
        switch($params['save_type']) {
            case 'insert':
                $result = $this->dbModel->add();
                break;
            case 'update':
                $data['id'] or $this->error(L('PRIMARY_KEY_LOST'));
                $result = $this->dbModel->save();
                break;
        }
        $result ? $this->success(L('save_success')) : $this->error(L('save_error'));
    }

}