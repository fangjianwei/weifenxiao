<?php
/**
 * 订单数据导出
 * User: 91336
 * Date: 2015/3/5
 * Time: 17:24
 */
namespace Common\Library\Order;
set_time_limit(1800);
ini_set('memory_limit','1000M');; //设置PHP可用内存
use Common\Model\OrderInfoModel;
class Download {
    public static function detail($where) {
        $ordeInfoModel=new OrderInfoModel();
        $order_list = M('OrderInfo')->alias(" AS o")->where($where)->field('o.order_sn, o.consignee,o.address,o.create_time,order_status,shipping_status,pay_status,o.master_id,
        (o.goods_amount+o.shipping_fee+o.insure_fee-o.discount) as order_amount,o.invoice_no,o.shipping_name,o.user_id')->select();
        $order_list = $ordeInfoModel->format(array('rows'=>$order_list));

        $data['title'] = L('ORDERS_EXCEL');

        $download = array();
        foreach($order_list['rows'] as $key=>$row){
            $agent=M('agent')->alias('a')->join('__AGENT_GRADE__ AS ag ON a.grade_id = ag.grade_id')->where(array('a.user_id'=>$row['user_id']))->field('agent_code, contact, grade_name')->find();
            $follow_user=M('master')->where(array('user_id'=>$row['master_id']))->field("concat(real_name, '-', user_name) as follow_user")->find();
            $download[]=array(
                'consignee' => $row['consignee'],
                'agent_code' => $agent['agent_code'].'-'.$agent['contact'],
                'grade_name' => $agent['grade_name'],
                'order_sn' => $row['order_sn'],
                'invoice_no' => $row['invoice_no'] ? 'NO:'.$row['invoice_no'] : '',
                'shipping_name' => $row['shipping_name'],
                'address' => $row['address'],
                'create_time' => $row['create_time'],
                'order_amount' => $row['order_amount'],
                'follow_user' => $follow_user['follow_user'],
                'status_text' => $row['status_text']
            );
        }
        $data['data'] = $download;
        if(export_data($data)){  //保存Excel
            return true;
        }else{
            return false;
        }
    }

    public static function goods($where) {
        $goodsModel = M('OrderGoods');
        $order_goods = $goodsModel
            ->alias(" AS og")
            ->join("__ORDER_INFO__ AS o ON o.order_id = og.order_id", "LEFT")
            ->join('__AGENT__ AS a ON a.user_id = o.user_id', "LEFT")
            ->where($where)
            ->field("og.*, o.order_sn, o.user_id, o.shipping_name, o.invoice_no, o.create_time, o.delivery_time,o.consignee,o.province, o.city, o.address, a.contact, o.mobile")
            ->order("og.order_id")
            ->select();
        $package = array();
        foreach($order_goods as $key => $goods) {
            if($goods['extension_code'] === 'package_buy') {
                //处理套装子商品的价格与数量
                $package_goods_price = 0;
                foreach($order_goods as $package_goods) {
                    if($goods['order_id'] != $package_goods['order_id']) continue;
                    if($package_goods['extension_code'] != 'package_goods') continue;
                    if($package_goods['parent_id'] != $goods['goods_id']) continue;

                    //统计套装中子商品的总价值
                    $package_goods_price += $package_goods['goods_number'] * $package_goods['shop_price'];
                }
                $goods['package_goods_price'] = $package_goods_price;
                $package[$goods['order_id']][$goods['goods_id']] = $goods;
            }
        }

        //查找套装
        $download = array();
        foreach($order_goods as $key => $goods) {
            //取收货地址
            $province       = D('Region')->where(array('region_id' => $goods['province']))->getField('region_name');
            $city           = D('Region')->where(array('region_id' => $goods['city']))->getField('region_name');
            $tmp = array(
                'contact'       => $goods['contact'],
                'consignee'     => $goods['consignee'],
                'shipping_name' => $goods['shipping_name'],
                'address'       => $province.$city.$goods['address'],
                'mobile'        => decrypt_phone($goods['mobile']),
                'order_sn'      => $goods['order_sn'],
                'invoice_no'    => $goods['invoice_no'],
                'delivery_time' => $goods['delivery_time'],
                'goods_sn'      => $goods['goods_sn'],
                'goods_name'    => $goods['goods_name'],
            );
            switch($goods['extension_code']) {
                case 'package_goods':
                    //得到套装信息
                    $this_package = $package[$goods['order_id']][$goods['parent_id']];
                    //计算比例
                    $scale = ($goods['goods_number'] * $goods['shop_price']) / $this_package['package_goods_price'];
                    $tmp['goods_price'] = number_format($scale * $this_package['goods_price'] / $goods['goods_number'], 2, '.', '');
                    $tmp['goods_number'] = $goods['goods_number'] * $this_package['goods_number'];
                    $tmp['total_price'] = number_format($tmp['goods_number'] * ($this_package['goods_price'] * $scale / $goods['goods_number']), 2, '.', '');
                    break;
                default:
                    $tmp['goods_number'] = $goods['goods_number'];
                    $tmp['goods_price'] = $goods['goods_price'];
                    $tmp['total_price'] = number_format($goods['goods_number'] * $goods['goods_price'], 2, '.', '');
                    break;
            }
            $download[] = $tmp;
        }


        $data['title'] = L('GOODS_EXCEL');
        $data['data'] = $download;
        if(export_data($data)){  //保存Excel
            return true;
        }else{
            return false;
        }
    }

    public static function back($where) {
        $goodsModel = M('BackGoods');
        $order_goods = $goodsModel
            ->alias(" AS og")
            ->join("__ORDER_INFO__ AS o ON o.order_id = og.order_id", "LEFT")
            ->join('__AGENT__ AS a ON a.user_id = o.user_id', "LEFT")
            ->where($where)
            ->field("og.*, o.order_sn, o.user_id, o.shipping_name, o.invoice_no, o.create_time, o.delivery_time, a.contact")
            ->order("og.order_id")
            ->select();
        $package = array();
        foreach($order_goods as $key => $goods) {
            if($goods['extension_code'] === 'package_buy') {
                //处理套装子商品的价格与数量
                $package_goods_price = 0;
                foreach($order_goods as $package_goods) {
                    if($goods['order_id'] != $package_goods['order_id']) continue;
                    if($package_goods['extension_code'] != 'package_goods') continue;
                    if($package_goods['parent_id'] != $goods['goods_id']) continue;

                    //统计套装中子商品的总价值
                    $package_goods_price += $package_goods['goods_number'] * $package_goods['shop_price'];
                }
                $goods['package_goods_price'] = $package_goods_price;
                $package[$goods['order_id']][$goods['goods_id']] = $goods;
            }
        }

        //查找套装
        $download = array();
        foreach($order_goods as $key => $goods) {
            $tmp = array(
                'contact'       => $goods['contact'],
                'shipping_name' => $goods['shipping_name'],
                'invoice_no'    => $goods['invoice_no'],
                'create_time'   => $goods['create_time'],
                'delivery_time' => $goods['delivery_time'],
                'goods_sn'      => $goods['goods_sn'],
                'goods_name'    => $goods['goods_name'],
            );
            switch($goods['extension_code']) {
                case 'package_goods':
                    //得到套装信息
                    $this_package = $package[$goods['order_id']][$goods['parent_id']];
                    //计算比例
                    $scale = ($goods['goods_number'] * $goods['shop_price']) / $this_package['package_goods_price'];
                    $tmp['goods_price'] = number_format($scale * $this_package['goods_price'] / $goods['goods_number'], 2, '.', '');
                    $tmp['goods_number'] = $goods['goods_number'] * $this_package['goods_number'];
                    $tmp['total_price'] = number_format($tmp['goods_number'] * ($this_package['goods_price'] * $scale / $goods['goods_number']), 2, '.', '');
                    break;
                default:
                    $tmp['goods_number'] = $goods['goods_number'];
                    $tmp['goods_price'] = $goods['goods_price'];
                    $tmp['total_price'] = number_format($goods['goods_number'] * $goods['goods_price'], 2, '.', '');
                    break;
            }
            $download[] = $tmp;
        }

        //print_r($download);exit;
        $data['title'] = L('GOODS_EXCEL');
        $data['data'] = $download;
        if(export_data($data)){  //保存Excel
            return true;
        }else{
            return false;
        }
    }
}