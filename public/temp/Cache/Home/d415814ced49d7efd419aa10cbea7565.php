<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="HandheldFriendly" content="true" />
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <!--begin ls 2015年8月10日-->
    <meta name="format-detection" content="telephone=no"/>
    <!--end-->
    <title>代理中心</title>
    <link type="text/css" rel="stylesheet" href="/public/stage/mobile/css/reset.css?ddd" />
    <link type="text/css" rel="stylesheet" href="/public/stage/mobile/css/style.css?dd" />
    <script type="text/javascript" src="/public/stage/mobile/res/jquery/jquery-2.1.3.min.js"></script>
    <script type="text/javascript" src="/public/stage/mobile/res/Plugs/jquery.blockUI.min.js"></script>
    <script type="text/javascript" src="/public/stage/mobile/js/common.js?11"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".warmBoxTitle").click(function () {
                indexChangeHeadImgboxClose();
            });
            $(".qutixian").click(function () {
                top.location = "/withdraw";
            });
        });
    </script>
    <style>
        .userTopConL em {
    background: #fffe34 none repeat scroll 0 0;
    color: #2ba7dd;
    display: inline-block;
    font-size: 12px;
    padding: 2px 5px;
}
    </style>
</head>
<body class="BG2">
    <form method="post" action="./my" id="form1">
<div class="aspNetHidden">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="MRQkLnx/oJzn50VoCQWZPiPF2iP3+ionpnemtvhZD0sZCCjGVgWjU6+XiRBNMpA5Hh+gB3SZHjOOB8yv7cymlcTtHfCFcahV64C9lDuUovV7o7UDYRmgmMLCCtJHD2r2WM7U2sHuvMVPbVg/mk3UkXW2oIVUX7Brty6faddMxPk=" />
</div>
<div class="aspNetHidden">
	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="F890A9BA" />
</div>
        <section>
        </section>
        <section>
            <div class="userTop">
                <div class="userTopCon">
                    <div class="userTopConL" style="text-align: center;">
                        <img src="<?php echo (HEADIMGURL); ?>" alt="pic"/><br />
                    </div>
                    <div class="userTopConR">
                        <p class="userTopConRName">
                           您好 <?php if(defined('NICKNAME')): ?>，<span><?php echo (NICKNAME); ?></span><?php endif; ?>！
                        </p>

                        <div>
                            <p class="font12"> 还不是代理？首次购买即可成为代理，占领自己的地盘获取收益。 </p>
                            <p><a href="<?php echo U('home/member/phistory');?>">立即成为代理 </a></p>
                        </div>
                    </div>
                </div>
                <div class="userTopMoney">

                    <dl class="clearfix userTopMoneyA">
                        <dd>
                            <p class="userTopMoneyTitle">预计收益</p>
                            <p class="userTopMoneyCon">0.00</p>
                        </dd>
                        <dd>
                            <p class="userTopMoneyTitle">可提现金额</p>
                            <p class="userTopMoneyCon qutixian">0.00</p>
                        </dd>
                    </dl>

                </div>
            </div>
        </section>
        <section>
            <div class="userBtm">
                <dl class="clearfix">
                    <dd>
                        <a href="<?php echo U('home/member/contacts');?>" class="userBtmCon1">
                            <img src="/public/stage/mobile/images/userBtm1.png" />
                            <p>我的代理</p>
                        </a>
                    </dd>
                    <dd>
                        <a href="<?php echo U('earnings/index');?>" class="userBtmCon2">
                            <img src="/public/stage/mobile/images/userBtm2.png" />
                            <p>我的收益</p>
                        </a>
                    </dd>
                    <dd>
                        <a href="<?php echo U('order/index');?>" class="userBtmCon3">
                            <img src="/public/stage/mobile/images/userBtm3.png" />
                            <p>我的订单</p>
                        </a>
                    </dd>
                    <dd>
                        <a href="<?php echo U('home/withdraw/withdraw');?>" class="userBtmCon4">
                            <img src="/public/stage/mobile/images/userBtm4.png" />
                            <p>去提现</p>
                        </a>
                    </dd>
                    <dd>
                            <a href="<?php echo U('home/member/prolargess');?>" class="userBtmCon5">
                                <img src="/public/stage/mobile/images/userBtm5.png" />
                                <p>我的产品</p>
                            </a>
                    </dd>
                    <dd>
                        <a href="<?php echo U('home/finance/finance_detail');?>" class="userBtmCon6">
                            <img src="/public/stage/mobile/images/userBtm6.png" />
                            <p>财务明细</p>
                        </a>
                    </dd>
                    <dd>
                        <!--2015年8月5日  ls
                    <a href="/address" class="userBtmCon7">
                        <img src="images/userBtm7.png"/>
                        <p>收货地址</p>
                    </a>
                      -->

                        <a href="<?php echo U('home/member/MyCourse');?>" class="userBtmCon7">
                            <img src="/public/stage/mobile/images/userBtm7.png" />
                            <p>我的课程</p>
                        </a>
                    </dd>
                        <dd>
                            <a href="<?php echo U('home/member/MeetList');?>" class="userBtmCon6">
                                <img src="/public/stage/mobile/images/userBtm10.png" />
                                <p>我的会议</p>
                            </a>
                        </dd>

                    <dd>
                        <a href="<?php echo U('home/member/UserSet');?>" class="userBtmCon8">
                            <img src="/public/stage/mobile/images/userBtm8.png" />
                            <p>设置</p>
                        </a>
                    </dd>

                </dl>
            </div>


            <div class="btnOut ">
                <a href="<?php echo U('home/member/phistory');?>" class="btn btnBlink">点击分享给朋友,赚佣金!</a>
                <img class="btnBlinkClick" src="/public/stage/mobile/images/click.gif" />
            </div>
        </section>
        <script type="text/javascript"></script>

        <div class="hidHeight"></div>
<div class="navBottom">
    <dl class="clearfix">
        <dd class="navBottomFirst">
            <p class="navBottomItem">
                <!--<a href="stu_profile.html"><img src="/public/stage/mobile/images/tp8lz7ik.png" /></a>-->
                <?php if(defined('USER_ID')): ?><a href="<?php echo U('home/member/index');?>"><img src="<?php echo (HEADIMGURL); ?>" /></a>
                <?php else: ?>
                    <!--用户没设置头像或未获取到用户头像，显示默认头像-->
                    <a href="<?php echo U('home/member/index');?>"><img src="/public/stage/mobile/images/tp8lz7ik.png" /></a><?php endif; ?>
            </p>
        </dd>
        <dd>
            <p class="navBottomItem"><a href="<?php echo U('goods/index');?>">产品购买</a></p>
        </dd>

        <dd>
            <p class="navBottomItem"><a href="<?php echo U('member/index');?>">代理中心</a></p>
        </dd>

        <dd>
            <p class="navBottomItem"><a href="http://mp.weixin.qq.com/s?__biz=MzA4NTU1NjU2NQ==&mid=404012163&idx=1&sn=d0767f8bd157c37f25f63e70f55ff9af&scene=1&srcid=0314PVTY9tnDILY0LJunffqM#rd">客服中心</a></p>
        </dd>
    </dl>
</div>
<!--底部公共模块-->

<script src="/public/stage/mobile/js/WeixinTools.js"></script>
        <section>
            <div class="warmRemind" id="box" style="display: none">
                <div class="warmBox">
                    <p class="warmBoxTitle clearfix">
                        <span class="f-left">
                            <img src="/public/stage/mobile/images/warmRemind.png" alt="提示" />
                            温馨提示</span>
                        <span class="f-right" id="close">
                            <img src="/public/stage/mobile/images/warmRemindClose.png" alt="关闭" /></span>
                    </p>
                    <p style="padding: 10px;">您是否重新获取微信头像？</p>
                    <input style="background: #00bfff; color: #fff" type="button" onclick="getwxheadImg();" value="确定获取" />
                    <input type="button" onclick="indexChangeHeadImgboxClose();" value="取消" />

                </div>
            </div>
        </section>

    </form>
</body>
    <script type="text/javascript">
        var isMeet = "True";
        $(function () {
            //设置菜单每格所占比例
            if (isMeet.toString().toLowerCase() == 'true') {
                $('.userBtm dd').css('width', '20%');
            } else {
                $('.userBtm dd').css('width', '25%');
            }
        })
    </script>
</html>