<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="HandheldFriendly" content="true" />
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <title>一级代理</title>
    <link type="text/css" rel="stylesheet" href="/public/stage/mobile/css/reset.css?1" />
    <link type="text/css" rel="stylesheet" href="/public/stage/mobile/css/style.css?1" />
    <style type="text/css">
        #pageMoreWrap { text-align: center; margin-bottom: 40px; }
        #pageMore { visibility: hidden; z-index: 5; position: relative; display: block; width: 300px; height: 37px; margin: 30px auto 40px; background-color: #bbbdc3; color: #51545b; font-weight: 700; line-height: 37px; text-align: center; text-decoration: none; font-size: 14px; -webkit-transition: all ease .7s; -moz-transition: all ease .7s; transition: all ease .7s; cursor: pointer; }
        .pageMorehHover:hover { background-color: #8fa4e4; color: #fff; }
    </style>
    <script type="text/javascript" src="/public/stage/mobile/res/jquery/jquery-2.1.3.min.js"></script>
    <script src="/public/stage/mobile/js/tools.min.js"></script>
    <script src="/public/stage/mobile/js/base.min.js"></script>
</head>
<body class="BG">
    <section>
        <div class="titleBox clearfix">
            <a href="javascript:history.go(-1)" class="f-left">
                <img src="/public/stage/mobile/images/back.png" /></a>
            <span>一级代理</span>
        </div>
    </section>
    <section>
        <div class="student">
            <dl class="js-appenddata">
            </dl>
        </div>
    </section>
    <section>
        <div id="pageMoreWrap" style="padding-top: 1px">
            <i class="icon-spinner icon-spin"></i>
            <a href="javascript:void(0)" id="pageMore" style="visibility: visible;">加载更多</a>
        </div>
    </section>
    
<div class="hidHeight"></div>
<div class="navBottom">
    <dl class="clearfix">
        <dd class="navBottomFirst">
            <p class="navBottomItem">
                <a href="stu_profile.html"><img src="/public/stage/mobile/UploadImage/uheaderImg/tp8lz7ik.png" /></a>
            </p>
        </dd>
        <dd>
            <p class="navBottomItem"><a href="index.html">产品购买</a></p>
        </dd>
        <dd>
            <p class="navBottomItem"><a href="my.html">代理中心</a></p>
        </dd>
        <dd>
            <p class="navBottomItem"><a href="http://mp.weixin.qq.com/s?__biz=MzA4NTU1NjU2NQ==&mid=404012163&idx=1&sn=d0767f8bd157c37f25f63e70f55ff9af&scene=1&srcid=0314PVTY9tnDILY0LJunffqM#rd">客服中心</a></p>
        </dd>
    </dl>
</div>
<script src="/public/stage/mobile/js/WeixinTools.js"></script>
    <script type="text/javascript">
        var isGetting = false;
        $(function () {
            isGetting = true;
            downloadData(1); //默认加载第一页

            var i = 2;
            $('#pageMore').click(function () {
                if (isGetting) return;
                isGetting = true;

                downloadData(i);
                i++;

            });
        });

        function downloadData(page) {
            var level = <?php echo ($level); ?>;
            $.ajax({
                type: "post",
                url: "<?php echo U('home/member/get_contacts_member');?>",
                data: {page: page, pagesize: 12, level: level},
                dataType: "json",
                success: function (result) {
                    if (result != "" && result.user) {
                        var s = "";
                        var data = result.user;
                        var total = result.totalNumber;

                        //#pageMore显示
                        if (result.length >= total) {
                            $("#pageMore").hide();
                        }
                        if (result.length <= 0) {
                            return false;
                        }
                        $.each(data, function (i, v) {
                            s += " <dd>";
                             s += "<div class='studentBoxL'>";
                             if (v.headimgurl != null) {
                             s += "<img src='" + v.headimgurl + "' />";
                             } else {
                             s += "<img src='/public/stage/mobile/images/noheadimg.jpg' />";
                             }
                             s += "</div>";
                             s += "<div class='studentBoxR'>";
                             s += "<div class='studentBoxRCon'>";
                             if (v.nickname == null || v.nickname == "") {
                             s += "<p><span>匿名</span></p>";
                             } else {
                             s += "<p><span>" + v.nickname + "</span></p>";
                             }

                             s += "<p class='studentBoxRBtm mt10'>";
                             s += "<span class='studentBoxRBtmL'>推荐人：" + v.bossnickname + "</span>";
                             s += "<span class='studentBoxRBtmR'>贡献收益：<span>￥" + v.ordermoney + "</span></span>";
                             s += "</p>";
                             s += "</div>";
                             s += "</div>";
                             s += "</dd>";
                        })
                        jQuery(".js-appenddata").append(s);
                    } else {
                        jQuery(".js-appenddata").append("");
                    }
                    isGetting = false;
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    isGetting = false;
                    appendPos.parent().append('<div class="alert alert-info no-more-pro" style="text-align:center;margin:15px -10px;" >获取更多数据失败，请稍后重试</div>');
//                    alert(errorThrown);
                }
            });
        }
            /*$.post("<?php echo U('home/member/get_contacts_member');?>",
                JSON.stringify({ page: page, pagesize: 12, level: '1' }),
                function (msg) {
                    alert(123);
                    return;
                    if (msg != "" && msg.d) {
                        var s = "";
                        var data = msg.d.ModelList;
                        var total = msg.d.Total;

                        //#pageMore显示
                        if (data.length >= total) {
                            $("#pageMore").hide();
                        }
                        if (data.length <= 0) {
                            return false;
                        }
                        $.each(data, function (i, v) {
                            s += " <dd>";
                            s += "<div class='studentBoxL'>";
                            if (v.headimgurl != null) {
                                s += "<img src='" + v.headimgurl + "' />";
                            } else {
                                s += "<img src='../images/noheadimg.jpg' />";
                            }
                            s += "</div>";
                            s += "<div class='studentBoxR'>";
                            s += "<div class='studentBoxRCon'>";
                            if (v.nickname == null || v.nickname == "") {
                                s += "<p><span>匿名</span></p>";
                            } else {
                                s += "<p><span>" + v.nickname + "</span></p>";
                            }

                            s += "<p class='studentBoxRBtm mt10'>";
                            s += "<span class='studentBoxRBtmL'>推荐人：" + v.bossnickname + "</span>";
                            s += "<span class='studentBoxRBtmR'>贡献收益：<span>￥" + v.ordermoney + "</span></span>";
                            s += "</p>";
                            s += "</div>";
                            s += "</div>";
                            s += "</dd>";
                        })
                        jQuery(".js-appenddata").append(s);
                    } else {
                        jQuery(".js-appenddata").append("");
                    }
                    isGetting = false;
                }, function () {
                    isGetting = false;
                    appendPos.parent().append('<div class="alert alert-info no-more-pro" style="text-align:center;margin:15px -10px;" >获取更多数据失败，请稍后重试</div>');

                }
            );
            }*/
    </script>
</body>
</html>