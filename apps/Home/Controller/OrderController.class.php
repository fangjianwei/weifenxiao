<?php
namespace Home\Controller;
use Common\Controller\StageController;
use Common\Model\OrderInfoModel;
class OrderController extends StageController{
    /**
     * 我的订单
     */
    public function index() {
//        $orderInfoModel = new OrderInfoModel();
//
//        $order = $orderInfoModel->find(13081);
//        $order_goods = $orderInfoModel->getOrderGoods($order['order_id']);
//        //计算商品佣金分
//        $result  = \Common\Library\Goods::getBrokerageUser($order);
//        foreach($result as $key => $val){
//            \Common\Library\Goods::brokerageEnter($val['user_id'],$val['grade'],$val['brokerage'],$val['share_goods_id']);
//        }
        //取出订单数据
        $order = D('OrderInfo')->order('create_time desc')->select();
        $order = $this->format($order);
        $this->assign('order', $order);
        $this->display();
    }

    public function brokerage($data, $count = 0){
        static $arr = array();
        if($data['share_user_id'] > 0 && $count < 3){
            $lastData = D('OrderInfo')
                     ->where(array('user_id' => $data['share_user_id'], 'share_goods_id' => $data['share_goods_id']))
                     ->find();
            if($lastData){
                $this->brokerage($arr[] = $lastData, $count += 1);
            }
        }

        return $arr;
    }

    public function show(){
        $order_id = I('get.order_id', '', 'trim');
        if(!$order_id > 0) $this->redirect('index');
        $orderInfoModel = D('OrderInfo');

        //取出订单信息
        $order = $orderInfoModel->where(array('order_id' => $order_id))->find();
        $order = $orderInfoModel->format($order);

        //判断是否为自己订单
        if($order['user_id'] != USER_ID || !$order) $this->redirect('index');
        $this->assign('order', $order);

        //取出订单日志
        $order_log = $orderInfoModel->getLog($order['order_id']);
        $this->assign('order_log', $order_log);

        //取出订单商品
        $order_goods = $orderInfoModel->getOrderGoods($order['order_id']);

        $this->assign('order_goods', $order_goods);
        $this->display();
    }

    public function format($data){
        $orderStatusText = L('order_status');
        foreach($data as $key => $val){
            if(is_array($val)){
                $val['mobile'] = decrypt_phone($val['mobile']);
                $val['order_status_text'] = $orderStatusText[$val['order_status']];
                $data[$key] = $val;
            }else{
                $data['mobile'] = decrypt_phone($data['mobile']);
                $data['order_status_text'] = $orderStatusText[$data['order_status']];
                return $data;
            }

        }
        return $data;
    }
}