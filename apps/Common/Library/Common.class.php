<?php
/**
 * ====================================
 * 公共函数类
 * ====================================
 * Author: Hugo
 * Date: 14-5-20 下午10:06
 * ====================================
 * File: Common.class.php
 * ====================================
 */

namespace Common\Library;
use Common\Library\WeChat\WePush;
use Common\Library\Sms\Sms;

class Common {
    /**
     * 组合分页信息
     * @return array
     */
    public static function pageInfo($params) {
        $orderBy = isset($params['sort']) ? trim($params['sort']) . ' ' .  trim($params['order']) : '';
        $page = isset($params['page']) && $params['page'] > 0 ? intval($params['page']) : 1;
        $pageSize = isset($params['rows']) && $params['rows'] > 0 ? intval($params['rows']) : 10;
        return array($orderBy, $page, $pageSize);
    }

    /**
     * 获取分页信息
     * @param int $pageCount
     * @param int $showNumber
     * @return array|null
     */
    public static function getPager($params) {
        //$params = array('total', 'page_size', 'show_num', 'url_demo')
        //没有数据不显示分页
        if(empty($params['total']) || empty($params['page_size'])) return null;

        $pager = array();
        $pager['page_count'] = ceil($params['total'] / $params['page_size']);
        $pager['page'] = max((int)I('page'), 1);

        $data = $_REQUEST;
        unset($data['page']);
        if($data) {
            $pager['params'] = $data;
            $data = http_build_query($data);
            $params['url_demo'] .= (strpos($params['url_demo'], '?') === false ? '?' : '&') . $data;
        }

        //设置上一页
        $pager['prev_url'] = $pager['page'] > 1 ? str_replace('[page]', $pager['page'] - 1, $params['url_demo']) : false;
        //设置下一页
        $pager['next_url'] = ($pager['page'] + 1)  <= $pager['page_count'] ? str_replace('[page]', ($pager['page'] + 1), $params['url_demo']) : false;

        $showNumber = empty($params['show_num']) ? 5 : $params['show_num'];
        $showStart = ceil($pager['page'] / $showNumber) * $showNumber - ($showNumber - 1);//设置开始页
        $showEnd = $showStart + $showNumber -1;
        $showEnd = $showEnd > $pager['page_count'] ? $pager['page_count'] : $showEnd;

        for($i = $showStart; $i <= $showEnd; $i++){
            $pager['pages'][$i]['page_url'] = str_replace('[page]', $i, $params['url_demo']);
        }
        return $pager;
    }

    /**
     * 树型结构
     * @param $data
     * @param string $selected
     * @return array|null
     */
    public static function buildTree(& $data, $selected = '', $type = '') {
        if($selected) {
            $selected = strpos($selected, ',') ? explode(',', $selected) : array($selected);
            foreach($data as $key => $row){
                $row['tree_id'] = $row['id'];
                if(in_array($row['id'], $selected)){
                    $row['checked'] = true;
                }
                $data[$key] = $row;
            }
        }

        $data = Tree::treeArray($data);
        if(in_array($type,array('select','parent'))) {
            $data = array(
                array('id' => 0, 'text' => L($type . '_NODE') , 'pid' => 0, 'children' => $data)
            );
        }

        return $data;
    }

    /**
     * 判断是否手机号码
     * @param $phone
     * @return int
     */
    public static function isMobile($phone) {
        return  preg_match("/^1[3-5,7-8]{1}[0-9]{9}$/", $phone);
    }

    /**
     * 验证邮箱地址
     * @param $string
     * @return int
     */
    public static function isEmail($string) {
        return preg_match("/^[\w\-\.]+@[\w\-\.]+(\.\w+)+$/", $string);
    }

    /**
     * 判断是否为微信号
     * @param $string
     * @return int
     */
    public static function isWeiXin($string){
        return preg_match("/^\w{1}[\w\d\-_]{5,19}$/", $string);
    }

    /**
     * 推送微信信息
     * @param $openId
     * @param $msg
     * @param string $type
     * @return bool
     */
    public static function sendWeiMsg($openId, $msg, $type = 'text') {
        if(empty($openId)) return false;
        $wePush = new WePush();
        $wePush->getAccessToken();
        dump( $wePush->sendText($openId, $msg));
        //$wePush->sendTemplateMsg($openId);
    }

    /**
     * 发送短信
     * @param $mobile   手机号 多个用英文的逗号隔开 post理论没有长度限制.推荐群发一次小于等于10000个手机号
     * @param string $sms_content 发送的内容
     * @param string $ext
     * @param string $rrid 默认空 如果空返回系统生成的标识串 如果传值保证值唯一 成功则返回传入的值
     * @param string $stime 定时时间 格式为2011-6-29 11:09:21
     * @return bool
     */
    public static function send_mobile($mobile, $sms_content = '', $ext = '', $rrid = '', $stime = '')
    {   
        //调用新短信接口
        return Sms::send_sms($mobile,get_client_ip(),$sms_content);
    }

    /**
     * 发送邮件
     * @param $toEmail  收件人地址
     * @param $MSubject  邮件主题
     * @param $MBody   邮件内容
     * @return bool
     */
    public static function send_email($toEmail, $MSubject, $MBody)
    {
        $client = new \SoapClient('http://service.respread.com/Service.asmx?WSDL');
        $sendParam = array(
            'LoginEmail' => C('mail.LoginEmail'),
            'Password' => C('mail.Password'),
            'From' => C('mail.From'),
            'FromName' => C('mail.FromName'),
            'To' => $toEmail,
            'Subject' => $MSubject,
            'Body' => $MBody.C('mail.company')
        );

        $sendResult = $client->Send($sendParam);

        if ($sendResult->SendResult == 'Sent success') {
            return true;
        } else {
            return false;
        }
    }


    /**
     * 产生验证码
     * @param $type 发送类型
     * @param int $Expires 有效时间
     * @return int
     */
    public static function setCode($type = 'mobile',$Expires = 0)
    {

        //获取上一次发送验证码时间
        if ($s_time = I('session.'.$type.'Code_time',0)) {
            $n_time = time();
            $second = $n_time - $s_time;
            //有效期过了，重新生成验证码发送
            if ($second > $Expires && $Expires != 0) {
                $code = rand(100000,999999);
                session($type.'Code',$code);
                session($type.'Code_time',time());
            }else{
                //没过期，发送之前的验证码
                $code = session($type.'Code');
            }
        }else{
            //不存在发送时间，则生成验证码
            $code = rand(100000,999999);
            session($type.'Code',$code);
            session($type.'Code_time',time());
        }
        
        return $code;
    }

    /**
     * 检测验证码
     * @param $Code
     * @param 发送类型
     * @param int $Expires 有效时间
     * @return bool
     */
    public static function checkCode($Code, $type = 'mobile', $Expires = 0)
    {
        //获取上一次发送验证码的时间
        if ($s_time = I('session.'.$type.'Code_time',0)) {
            $n_time = time();
            $second = $n_time - $s_time;
            //过了有效期，清除session
            if ($second > $Expires && $Expires != 0) {
                session($type.'Code_time',null);
                session($type.'Code',null);
            }
        }else{
            //不存在发送时间，校验不通过
            return false;
        }
        //获取session中的验证码
        if ($sessionCode = I('session.'.$type.'Code',0)) {
            if ($Code == $sessionCode) {
            //校验通过，清除session，返回true
                session($type.'Code',null);
                session($type.'Code_time',null);
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }   
    }
    /**
     * 检验是否可以再次发送验证码
     * @param $type 类型
     * @param $time 间隔时间
     * @return bool
     */
    public function canSendAgain($type='mobile',$time=60){

        if($s_time = I('session.'.$type.'Code_time',0)) {
            $n_time = time();
            if ($n_time - $s_time > $time) {
                return true;
            }
            return $s_time + $time - $n_time;
        }else{
            return true;
        }
    }

    /**
     * 生成唯一标识,用于二维码
     * @param  integer $number 生成数量
     * @return array          
     */
    public static function createUnique($number = 1){

        if (!empty($number) && (!is_int($number) || $number <= 0)) return false;
        $arr = array();
        for ($i = 0; $i < $number; $i++) {
            $arr[] = md5(md5(microtime()).rand(1, 1000000).$i.C('CRYPT_KEY'));
        }
        return $arr;
    }
}