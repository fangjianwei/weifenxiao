<?php 
/**
  * ====================================
  * 微信模板消息文件
  * ====================================
  * Author: lwh
  * Date: 15/01/08
  * ====================================
  * File: TemplateMsg.class.php
  * ====================================
  */
 
namespace Common\Library\WeChat;

class TemplateMsg {

	const COLOR = '#173177';
	//任务处理通知模板消息ID
	const MISSION_TMP = 'jx1FpvEQbUsGpSCQ0jYeMe2CziaT7_TYZtmgUII2H10';
	//未支付订单模板消息ID
	const UNPAY_ORDER_TMP = '5xgOWwov31Xg5KYYNuzWVV0QuGJAi1bpfnRcnbVlByQ';
	//会员等级变更模板消息ID
	const UPGRADE_TMP = 'V43ug3H5Vmj_7QzYj0-3IoOqEL2OUnOWFdARFl4jADo';

	/**
	 * 未支付订单消息
	 * @param int $userId  用户ID
	 * @param int $orderId 订单ID
	 */
	public static function unpayOrder($userId, $orderId){
		if (empty($userId) || empty($orderId)) return false;
		
		//查找代理商绑定的微信OPENID
		if (!$openId = self::getOpenId($userId)) {
			return false;
		}
		//查找订单信息
		$order = M('orderInfo')
		       ->where(array('user_id' => $userId, 'order_id' => $orderId))
		       ->find();
		if (empty($order)) return false;
		//模板消息参数
		$data['first']     = sprintf(L('WX_UNPAY_ORDER'), $order['consignee']);
		$data['remark']    = sprintf(L('WX_UNPAY_AMOUNT'), $order['goods_amount']+$order['shipping_fee']);
		$data['ordertape'] = $order['create_time'];
		$data['ordeID']    = $order['order_sn'];
		//模板跳转链接
		$url = U('/home/order/show', array('order_id' => $orderId), true ,true);

		return self::sendMsg($data, $openId, self::UNPAY_ORDER_TMP, self::jumpLogin($url));
	}	

	/**
	 * 任务处理通知
	 * @param  int $processId 流程ID
	 * @param  int $userId    流程申请用户ID
	 * @param  int $parentId  审核流程上级ID
	 */
	public static function mission($processId, $userId, $parentId){
		if (empty($processId) || empty($userId) || empty($parentId)){
			return false;	
		}
		//查找上级代理商绑定微信OPENID
		if (!$openId = self::getOpenId($parentId)) {
			return false;
		}
        //查找流程信息
        $process = M('process')->find($processId);
        if ($process['api_type'] == API_ORDER) {
            $type = '订单';
        }else{
            $type = '代理商';
        }
        //模板跳转链接
     	$url = U('/home/process/audit', array('process_id' => $processId), true, true);
        //查找流程发起人
        $contact = M('agent')->where(array('user_id'=>$userId))->getField('contact');
     	//模板消息参数
     	$data['first']    = sprintf(L('WX_PROCESS_APPLY'),$contact);
     	$data['keyword1'] = $process['process_title'];
     	$data['keyword2'] = $type;
     	$data['remark']   = '请抽空处理';

     	return self::sendMsg($data, $openId, self::MISSION_TMP, self::jumpLogin($url));
  
	}

	/**
	 * 会员等级变更通知
	 * @param  int $userId    会员ID
	 * @param  int $prevGrade 原等级
	 * @param  int $currGrade 现等级
	 */
	/*static function upgrade($userId, $prevGrade, $currGrade) {
		if (empty($userId) ||empty($prevGrade) || empty($currGrade)) {
			return false;
		}
		if (!$openId = self::getOpenId($userId)) {
			return false;
		}
		$data['first'] = '恭喜升级！';
		$data['grade1'] = M('member')->where(array('grade_id' => $prevGrade))->getField('grade_name');
		$data['grade2'] = M('member')->where(array('grade_id' => $currGrade))->getField('grade_name');
		$data['remark'] = '备注';
		$data['time'] = date(C('DATE_FORMAT'));
		return self::sendMsg($data, $openId, self::UPGRADE_TMP);
	}*/

	/**
	 * 跳转授权登录
	 * @param $jumpUrl
	 */
	protected static function jumpLogin($jumpUrl) {
		$wePush = new WePush();
		$url = U('/home/passport/authorize','',true,true) . '?jump_url=' . $jumpUrl;
		$callback = $wePush->getRequestCodeURL($url, null,'snsapi_base');
		return $callback;
	}

	/**
	 * 查找用户OPENID
	 * @param  int $userId 用户ID
	 * @return mixed   $openId or false
	 */
	protected static function getOpenId($userId) {
		if (empty($userId)) return false;
		$openId = M('member')->where(array('user_id' => $userId))->getField('weixin_openid');
		return $openId ? $openId : false;
	}

	/**
	 * 发送模板消息
	 * @param  array $params     模板data
	 * @param  int $openId     openid
	 * @param  string $tmeplateId 模板ID
	 * @param  string $url        url
	 * @param  string $topcolor   字体颜色
	 */
	protected static function sendMsg($params, $openId, $tmeplateId, $url = '', $topcolor = self::COLOR) {
		$data['touser']      = $openId;
		$data['template_id'] = $tmeplateId;
		$data['url'] 	     = $url;
		$data['topcolor']    = $topcolor;
		if (is_array($params)) {
			foreach ($params as $k => $v) {
				if (is_array($v)) {
					$data['data'][$k] = $v;
				} else {
					$data['data'][$k] = array('value' => $v, 'color' => self::COLOR);
				}
			}
		} else {
			return false;
		}
		$we = new WePush();
		$token = $we->getAccessToken();
		return $we->sendTemplateMsg($data);
	}

}
?>