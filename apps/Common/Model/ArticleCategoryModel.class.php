<?php
/**
 * ====================================
 * 文章类别模型
 * ====================================
 * Author: weitao
 * Date: 14-11-20 下午6:09
 * ====================================
 * File: ArticleModel.class.php
 * ====================================
 */
namespace Common\Model;
use Common\Library\Common;

class ArticleCategoryModel extends CommonModel{

    public function lists($params){
        if($params['keyword']) {
            $this->where(array(
                'title' => array('LIKE', "%{$params['keyword']}%")
            ));
        }
        $data = $this->field("*,title AS text, id AS tree_id")->order("pid ASC, orderby DESC")->select();

        Common::buildTree($data, $params['selected'], $params['type']);

        return $data ? $data : array();
    }

}