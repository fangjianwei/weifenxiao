<?php
/**
 * ====================================
 * 积分类
 * ====================================
 * Author: 9004396
 * Date: 2015/1/19 16:43
 * ====================================
 * File: Integral.class.php
 * ====================================
 */
namespace Common\Library;
use Common\Model\MemberModel;

set_lang('integral');
class Integral{


    static $errorMsg; //错误信息

    /**
     * 商品积分变动
     * @param $userId           用户ID
     * @param $integral_type    类型
     * @param int $itemId       对象ID
     * @param int $integral     积分
     * @param null $remark      备注
     * @param bool $decrease    增减
     * @return bool
     */
    public static function log($userId, $integral_type, $itemId = 0, $integral = 0, $remark = null, $decrease = true){
        if(empty($userId)) self::setError(0);
        if(empty($integral_type)) self::setError(3);
        //获取产品信息
        $logData = array(
            'user_id'       => $userId,
            'integral_type' => $integral_type,
            'item_id'       => $itemId,
            'remark'        => $remark,
            'create_time'   => date(C('DATE_FORMAT')),
        );
        if($decrease){
            $logData['integral_num'] = $integral;
        }else{
            $logData['integral_num'] = '-'.$integral;
        }
        if(M('integralLog')->add($logData)){
            $memberMode = new MemberModel();
            if($memberMode->integralFloat($userId,$integral,$decrease)){
                return true;
            }else{
                return false;
            }

        }else{
            self::setError(2);
        }
    }



    /**
     * 设置错误信息
     * @param $code
     * @return mixed
     */
    protected static function setError($code)
    {
        $msg = L('integral_error');
        self::$errorMsg = $msg[$code];
        return false;
    }

    /**
     * 返回错误信息
     * @return array
     */
    public static function getError()
    {
        return self::$errorMsg;
    }
}