<?php
/**
 * Created by PhpStorm.
 * User: 9009078
 * Date: 2016/5/23
 * Time: 13:56
 */
namespace Common\Model;

use Common\Library\Common;

class FinanceModel extends CommonModel {

    /**
     * 获取财务明细数据列表
     * @return array
     */
    public function get_finance_detail(){
        $params = I("request.");
        $startCount = ($params['page']-1) * $params['pagesize'];
        $endCount = $startCount+$params['pagesize'];
        $where['user_id'] = 1;
        if($params['type'] != 'all'){
            $type_arr = explode('-',$params['type']);
            $where['type'] = array('in',$type_arr);
        }
        $result = M('member_finance_detail')->where($where)->limit($startCount,$endCount)->order('add_time desc')->select();

        $finance_type_title = C('FINANCE_TYPE');        //获取财务明细类型名称
        foreach($result as $key=>$val){
            $result[$key]['title'] = $finance_type_title[$val['type']];
            $result[$key]['add_time_format'] = date("Y-m-d H:i:s",$val['add_time']);
            $result[$key]['confirm_time_format'] = empty($val['confirm_time'])?'':date("Y-m-d H:i:s",$val['confirm_time']);
        }
        $data['finance_data'] = $result;
        $data['length'] = count($result);
//        echo '<pre>';
//        print_r($data);
//        echo '</pre>';exit;
        return $data;
    }
}