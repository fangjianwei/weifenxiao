<?php
/**
 * ====================================
 * 购物车处理类
 * ====================================
 * Author: 91336
 * Date: 2014/12/3 15:59
 * ====================================
 * File: Cart.class.php
 * ====================================
 */
namespace Common\Library;

use Common\Model\GoodsModel;
use Common\Library\Goods;
defined('SESSION_ID') or define('SESSION_ID', session_id());
set_lang('cart');

class Cart{
    protected $errorMsg; //错误信息
    protected $userId; //当前
    protected $user;
    protected $goods; //当前商品
    protected $goodsNumber = 1; //购买数量
    protected $cartModel;

    public function __construct() {
        $this->cartModel = M('Cart');
    }

    //设置基础数据
    public function set($item, $value = '') {
        if(is_array($item)) {
            foreach($item as $key => $value) {
                $this->$key = $value;
            }
        }else{
            $this->$item = $value;
        }

        return $this;
    }

    /**
     * 读取购物车
     * @param $userId 
     * @return mixed
     * 
     */
    public function getAll() {
        //删除本人缓存的购物车商品
        $this->cartModel->where(array('session_id' => array('neq', SESSION_ID), 'user_id' => $this->userId))->delete();
        $data = $this->cartModel->alias(' AS c')
            ->join("__GOODS__ AS g ON c.goods_id = g.goods_id", "LEFT")
            ->where(array(
            'c.session_id' => SESSION_ID,
            'c.user_id' => $this->userId))
            ->field("c.*, g.goods_thumb, g.goods_stock,g.occupy,g.shipment, g.is_package, g.relation_id, g.limit_buy")
            ->select();
        //计算库存
        $data = Goods::countstock($data);
        $total_amount = 0;
        if($data) {
            foreach($data as $key => $row) {
                $total_amount += $row['goods_price'] * $row['goods_number'];
                if($row['is_package']) {
                    $row['goods_stock'] = get_package_stock($row['goods_id'], $row['relation_id']);
                    if(IS_SPECIAL == 1){
                        $row['goods_info'] = GOODS::getPackGoods($row['relation_id'],$row['goods_id']);
                    }
                }
                if (IS_MOBILE) {//手机端商品名显示部分，避免错位
                    $row['small_goods_name'] = mb_substr($row['goods_name'], 0, 15, 'utf-8');
                }
                $data[$key] = $row;
            }
        }
        return array('rows' => $data, 'total_amount' => $total_amount);
    }

    /**
     * 获取总数
     * @param int $userId
     * @return mixed
     */
    public function getCount($goods_id = 0) {
        $where = array(
            'session_id' => SESSION_ID,
            'user_id' => $this->userId
        );
        if($goods_id > 0) {
            $where['goods_id'] = $goods_id;
        }
        return $this->cartModel->where($where)
            ->sum('goods_number');
    }

    /**
     * 添加商品到购物车
     * @param $goodsId
     * @param int $goodsNumber
     * @param string $extensionCode
     * @param int $userId 
     * @return bool|mixed
     */
    public function add() {
        //判断购物车是否存在该产品，如果存在，则添加数量并返回
        $extension_code = $this->goods['is_package'] ? 'package_buy' : '';
        $where = array(
            'session_id' => SESSION_ID,
            'goods_id' => $this->goods['goods_id'],
            'user_id' => $this->userId,
            'extension_code' => $extension_code,
            'parent_id' => 0
        );
        $cart = $this->cartModel->where($where)->find();
        if($cart) {
            return $this->cartModel->where($where)->setInc('goods_number', $this->goodsNumber) ? true : $this->setError(3);
        }

        $data = array(
            'user_id'       => $this->userId,
            'session_id'    => SESSION_ID,
            'goods_id'      => $this->goods['goods_id'],
            'goods_sn'      => $this->goods['goods_sn'],
            'goods_name'    => $this->goods['goods_name'],
            'shop_price'    => $this->goods['shop_price'],
            'goods_price'   => $this->goods['goods_price'],
            'goods_number'  => $this->goodsNumber,
            'extension_code'    => $extension_code,
        );

        return $this->cartModel->add($data) ? true : $this->setError(4);
    }

    /**
     * 删除购物车商品
     * @param $cartId
     * @return bool|mixed
     */
    public function delete($cartId) {
        return $this->cartModel
            ->where(array('session_id' => SESSION_ID, 'cart_id' => $cartId))
            ->delete() ? true : $this->setError(5);
    }

    /**
     * 清空购物车
     * @return bool|mixed
     */
    public function deleteAll() {
        return $this->cartModel
            ->where(array('session_id' => SESSION_ID, 'user_id' => $this->userId))
            ->delete() ? true : $this->setError(5);
    }

    /**
     * 更改商品数量
     * @param $cartId
     * @param $cNumber 数量差值， 支持正负数，为负数量，商品数量相减
     * @return bool|mixed
     */
    public function modify($cartId) {
        $where = array('cart_id' => $cartId);
        if($this->goodsNumber > 0) {
            return $this->cartModel->where($where)->setField('goods_number', (int)$this->goodsNumber) ? true : $this->setError(7);
        }
        return $this->setError(6);
    }

    /**
     * 设置错误信息
     * @param $code
     * @return mixed
     */
    protected function setError($code) {
        $msg = L('cart_error');
        $this->errorMsg = $msg[$code];
        return false;
    }

    /**
     * 返回错误信息
     * @return array
     */
    public function getError() {
        return $this->errorMsg;
    }
}