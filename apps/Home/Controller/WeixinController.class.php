<?php
/**
 * Created by PhpStorm.
 * User: 9009078
 * Date: 2016/5/14
 * Time: 15:00
 */
namespace Home\Controller;
use Think\Controller;
use Think\Crypt;

class WeixinController extends Controller {
    protected function _initialize() {
        $this->db=new \Common\Model\FrontmemberModel();
        $this->mycurl = new \Common\Library\Curl\curl();
    }

    /**
     * 网页获取用户信息---openid
     */
    public function get_user_code(){
        $params = I("request.");
        Vendor('phxcrypt.phxcrypt');
        $myCrypt = new \phxCrypt;
//        $return_url = empty($params['return_url'])?'home/member/index':str_replace('-','/',$params['return_url']);
        $return_url = empty($params['return_url'])?'home/member/index':$myCrypt->isDecrypted($params['return_url']);
        $url = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=".C('WECHAT_APPID')."&secret=".C('WECHAT_APPSECRET')."&code=".$params['code']."&grant_type=authorization_code";
        $code_json = $this->mycurl->get($url);
        $code_arr = json_decode($code_json,true);
        $data = $this->db->check_user_exist($code_arr['openid']);   //查看member表是否存在此用户记录
//        if(!$data){
//            $data_userinfo = $this->get_userinfo($code_arr);    //获取用户详细信息
//            if(!$data_userinfo['errcode']){
//                $data = $this->db->insert_user($data_userinfo,USER_SOURCE_WEBSQ);         //新增用户数据
//            }else{
//                //获取用户信息接口调用失败，提示相关信息
//            }
//        }
        $this->set_login_session($data);    //设置用户登录session信息
        $this->redirect($return_url);   //返回会员中心
    }

    /**
     * 网页获取用户信息---详细信息
     */
    public function get_userinfo($user_info_arr){
        $url = "https://api.weixin.qq.com/sns/userinfo?access_token=".$user_info_arr['access_token']."&openid=".$user_info_arr['openid']."&lang=zh_CN";
        $data = $this->mycurl->get($url);
        return json_decode($data,true);
    }

    /**
     * 设置用户登录session信息
     * @param bool $write
     * @return bool
     */
    public function set_login_session($data){
        Vendor('phxcrypt.phxcrypt');
        $myCrypt = new \phxCrypt;

        $data_encrypt = Crypt::encrypt(serialize($data), C('CRYPT_KEY'));   //加密登录信息
        session(C('MEMBER_SESSION_KEY'),$data_encrypt);     //保存session
        define('USER_ID', $data['user_id']);
        define('OPENID', $data['openid']);
        define('NICKNAME', $data['nickname']);
        define('HEADIMGURL', $data['headimgurl']);
        define('TRUENAME', $data['truename']);
        define('MOBILE', $myCrypt->isDecrypted($data['mobile']));
    }


    /**
     * 微信初始化
     */

    public function weixinInit(){
        new \Common\Library\WeChat\WeReceive();
    }
}