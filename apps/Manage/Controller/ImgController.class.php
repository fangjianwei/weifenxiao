<?php
/**
 * ====================================
 * 这里是说明
 * ====================================
 * Author: 91336
 * Date: 2014/11/24 16:34
 * ====================================
 * File: GoodsController.class.php
 * ====================================
 */
namespace Manage\Controller;
use Common\Controller\ManageController;

class ImgController extends ManageController {
    public function water($userId){
       
        if(empty($userId))return;
        $agent = M()->table('__AGENT__')->alias(' AS a')->join('__AGENT_GRADE__ AS g ON a.grade_id = g.grade_id', 'left')->where(array('a.user_id'=>$userId))->field('contact,agent_code,wecha_name,grade_name,id_number,a.create_time,startdate,enddate')->find();
        //把身份证后5位替换成星号
        $agent['id_number'] = substr_replace($agent['id_number'],'*****',13);
        $time = isset($agent['startdate']) ? strtotime($agent['startdate']) : strtotime($agent['create_time']);
        $dq_time = isset($agent['enddate']) ? strtotime($agent['enddate']) : strtotime($agent['create_time'])+180*24*3600;
        $image = new \Think\Image();
        $image->open(APP_ROOT.'public/shouquan/template/template.jpg')->text($agent['agent_code'],APP_ROOT.'public/shouquan/template/JDJHCU.TTF',45,'#000000',array(1840,850));
        $image->text($agent['contact'],APP_ROOT.'public/shouquan/template/JDJHCU.TTF',50,'#000000',array(600,1600));
        $image->text($agent['wecha_name'],APP_ROOT.'public/shouquan/template/JDJHCU.TTF',50,'#000000',array(1650,1600));
        $image->text($agent['grade_name'],APP_ROOT.'public/shouquan/template/JDJHCU.TTF',50,'#000000',array(1750,1740));
        $image->text($agent['id_number'],APP_ROOT.'public/shouquan/template/JDJHCU.TTF',40,'#000000',array(1100,2320));
        $image->text(date('Y', $time),APP_ROOT.'public/shouquan/template/JDJHCU.TTF',30,'#000000',array(999,2450));
        $image->text(date('m', $time),APP_ROOT.'public/shouquan/template/JDJHCU.TTF',30,'#000000',array(1230,2450));
        $image->text(date('d', $time),APP_ROOT.'public/shouquan/template/JDJHCU.TTF',30,'#000000',array(1390,2450));
        $image->text(date('Y', $dq_time),APP_ROOT.'public/shouquan/template/JDJHCU.TTF',30,'#000000',array(1610,2450));
        $image->text(date('m', $dq_time),APP_ROOT.'public/shouquan/template/JDJHCU.TTF',30,'#000000',array(1810,2450));
        $image->text(date('d', $dq_time),APP_ROOT.'public/shouquan/template/JDJHCU.TTF',30,'#000000',array(1980,2450));
        $fileName='public/upload/'.$agent['agent_code'].'.jpg';
        $image->save(APP_ROOT.$fileName);
        return is_file(APP_ROOT.$fileName) ? '/'.$fileName : false;
    }
}