<?php
/**
 * Created by PhpStorm.
 * User: 9009078
 * Date: 2016/5/4
 * Time: 14:50
 */
namespace Home\Controller;
use Common\Model\FrontmemberModel;
use Common\Library\Sms;
use Common\Controller\StageController;
use Think\Crypt;

class MemberController extends StageController {
    public function _initialize(){
        parent::_initialize();
        $this->db=new FrontmemberModel();
        $this->mycurl = new \Common\Library\Curl\curl();
    }

    public function _before_index()  {

    }

    /**
     * 清除登陆信息，方便测试
     */
    public function test(){
        session(null);
        echo '清除缓存';
        exit;
    }

    /**
     * 会员中心
     */
    public function index() {
        //如果用户没登陆，则通过网页授权获取用户信息，并登陆
        /*if(!defined('USER_ID')){
            $redirect_uri = urlencode('http://'.$_SERVER["SERVER_NAME"].'/home/member/get_user_code');
            $url = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=".WECHAT_APPID."&redirect_uri=".$redirect_uri."&response_type=code&scope=snsapi_userinfo&state=111#wechat_redirect";
            header("Location: $url");
            exit;
        }*/
        $this->display();
    }

    /**
     * 我的代理
     */
    public function contacts() {
        $data = $this->db->get_contacts_info();
        $this->assign('data',$data);
        $this->display();
    }

    /**
     * 获取代理数据列表，加载分页数据
     */
    public function get_contacts_member(){
        $data = $this->db->get_contacts_member();
        echo json_encode($data);
    }

    /**
     * 代理列表（一级、二级、三级）
     */
    public function step() {
        $params = I("request.");
        $this->assign('level',$params['level']);
        $this->display();
    }

    /**
     * 设置
     */
    public function UserSet() {
        $this->display();
    }

    /**
     * 修改资料
     */
    public function stu_profile() {
        $data = $this->db->stu_profile();
        $this->assign('data',$data);
        $this->display();
    }

    /**
     * 修改资料-数据处理
     */
    public function stu_profile_action() {
        $flag = $this->db->stu_profile_action();
        echo json_encode($flag);
    }

    /**
     * 绑定手机号
     */
    public function bound() {
        $data = $this->db->bound();
        $this->assign('data',$data);
        $this->display();
    }

    /**
     * 绑定手机号-数据处理
     */
    public function bound_action() {
        $flag = $this->db->bound_action();
        echo json_encode($flag);
    }

    /**
     * 网页获取用户信息---openid
     */
    public function get_user_code(){
        $params = I("request.");
        $url = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=".WECHAT_APPID."&secret=".WECHAT_APPSECRET."&code=".$params['code']."&grant_type=authorization_code";
        $code_json = $this->mycurl->get($url);
        $code_arr = json_decode($code_json,true);
        $data = $this->db->check_user_exist($code_arr['openid']);   //查看member表是否存在此用户记录
        if(!$data){
            $data_userinfo = $this->get_userinfo($code_arr);    //获取用户详细信息
            if(!$data_userinfo['errcode']){
                $data = $this->db->insert_user($data_userinfo,USER_SOURCE_WEBSQ);         //新增用户数据
            }else{
                //获取用户信息接口调用失败，提示相关信息
            }
        }
        $this->set_login_session($data);    //设置用户登录session信息
        $this->redirect('home/member/index');   //返回会员中心
    }

    /**
     * 网页获取用户信息---详细信息
     */
    public function get_userinfo($user_info_arr){
        $url = "https://api.weixin.qq.com/sns/userinfo?access_token=".$user_info_arr['access_token']."&openid=".$user_info_arr['openid']."&lang=zh_CN";
        $data = $this->mycurl->get($url);
        return json_decode($data,true);
    }

}