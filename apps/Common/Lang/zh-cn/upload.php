<?php
return array(
    'PLEASE_LOGIN'                  => '请先登录！',
    'CREATE_DIRECTORY_FAILED'       => '创建目录失败！',

    'UPDATE_AVATAR_SUCCESS' 		=> '更新头像成功！',
    'UPDATE_AVATAR_FAIL'			=> '更新头像失败！',
    'NO_FILE_UPLOAD'				=> '没有文件上传！',

);