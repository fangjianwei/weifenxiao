<?php
/**
 * ====================================
 * 会员中心公共类
 * ====================================
 * Author: 91336
 * Date: 2014/11/21 14:20
 * ====================================
 * File: UCenterController.class.php
 * ====================================
 */
namespace Common\Controller;

use Common\Library\WeChat\WePush;

class UCenterController extends StageController {
    function _initialize(){
        parent::_initialize();
        defined('SESSION_ID') or define('SESSION_ID', session_id());
        //验证登录
        if(!defined('USER_ID')) {
            redirect(U('passport/login'));
        }
    }

}