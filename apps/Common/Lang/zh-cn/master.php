<?php
return array(
    'user_name_lost'            => '请输入登录账号',
    'real_name_lost'            => '请输入真实姓名',
    'channel_id_lost'           => '请选择渠道',
    'password_error'            => '请输入6 ～ 32位密码',
    'confirm_password_error'    => '确认密码不正确',
    'user_name_exists'          => '登录账号已存在',
    'must_select_man'           => '请选择需要操作的人员！一次只能选择一位。',
    'must_input_money'          => '请填写充值金额！',
    'recharge'                  => '充值',
    'account_not_existence'     => '付款账户不存在',
    'account_locked'            => '付款账户已被停用',
);