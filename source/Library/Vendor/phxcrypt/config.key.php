<?php 
/** * 
 * phxAlg   3DES算法, 64bit分组密码, 即8位字符为一组, 密文长度64bit, 即8位二进制字符,16位十六进制字符---3des,gost,blowfish,twofish
 * phxKey   196bit, 24位字符密钥 
 * 以上两项定义一旦提交服务器后不允许修改,一旦修改将导致已加密的信息无法解密
 * cut_length   从后面截取的指定长度
 * */

$phx_key = array("phxAlg"=>"twofish","phxKey"=>"8031e6ac94b4a75db83c85c23e209143","cut_length"=>5);
?>