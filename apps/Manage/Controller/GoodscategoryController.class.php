<?php
/**
 * ====================================
 * 商品分类
 * ====================================
 * Author: 91336
 * Date: 2014/11/24 16:02
 * ====================================
 * File: GoodsCategoryController.class.php
 * ====================================
 */
namespace Manage\Controller;
use Common\Controller\ManageController;

class GoodscategoryController extends ManageController {
    protected $dbTable = 'GoodsCategory';
}