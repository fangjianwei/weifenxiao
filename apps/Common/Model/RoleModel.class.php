<?php
/**
 * ====================================
 * 角色模型
 * ====================================
 * Author: Hugo
 * Date: 14-5-20 下午9:28
 * ====================================
 * File: RoleModel.class.php
 * ====================================
 */
namespace Common\Model;

use Common\Library\Common;

class RoleModel extends CommonModel
{
    protected $_auto = array(
        array('create_time', 'date', self::MODEL_INSERT, 'function', 'Y-m-d H:i:s'),
        array('update_time', 'date', self::MODEL_UPDATE, 'function', 'Y-m-d H:i:s'),
    );

    protected $_validate = array(
        array('text','require','{%role_name_lost}'),
        array('text','','{%role_name_exists}', self::MUST_VALIDATE, 'unique', self::MODEL_BOTH),
        array('pid', 'validateEq', '{%parent_error}', self::EXISTS_VALIDATE, 'callback', self::MODEL_UPDATE)
    );

    /**
     * 判断父级ID正确性
     * @param $pid
     * @param $id
     * @return bool
     */
    protected function validateEq($pid){
        return $pid != I('post.id');
    }

    protected function _after_delete($data, $options){
        M('RoleMenu')->where("role_id = %d", $data['id'])->delete();
    }

    public function lists($params) {
        $data = $this->field('*, id as role_id')->order("pid ASC, orderby DESC")->select();
        Common::buildTree($data, $params['selected'], $params['type']);
        return $data;
    }

    /**
     * 设置角色菜单
     * @param $role_id
     * @param $menu_list
     * @return bool
     */
    public function setMenu($roleId, $menuList){
        $model = M('RoleMenu');
        $model->where("role_id = %d", $roleId)->delete();
        $menuList = preg_replace("/\s+/",'',$menuList);
        if(empty($menuList)) return false;
        $menuList = explode(',', $menuList);
        $data = array();
        foreach($menuList as $menu_id){
            $data[] = array('menu_id' => $menu_id, 'role_id' => $roleId);
        }
        $model->addAll($data);
        return true;
    }

    /**
     * 读取详细
     * @param $id
     * @return mixed
     */
    public function getInfo($id) {
        $data = $this
            ->alias(' AS r')
            ->join('__ROLE_MENU__ AS m ON r.id = m.role_id', 'LEFT')
            ->field('r.*, GROUP_CONCAT(m.menu_id) as menu_list, r.id AS mid')
            ->group('r.id')
            ->where("r.id = $id")
            ->find();
        return $data;
    }
}