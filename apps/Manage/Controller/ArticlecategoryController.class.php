<?php
/**
 * ====================================
 * 文章分类管理
 * ====================================
 * Author: Hugo
 * Date: 14-5-20 下午9:28
 * ====================================
 * File: CategoryController.class.php
 * ====================================
 */
namespace Manage\Controller;
use Common\Controller\ManageController;


class ArticlecategoryController extends ManageController{
    protected $dbTable = 'ArticleCategory';
    //删除文章类型
    public function delete(){
        $items = explode(',', I('request.items'));
        foreach($items as $item_id){
            $this->dbModel->where("pid = '%d' or id = '%d'", $item_id, $item_id)->delete();
        }
        $this->success();
    }
}