<?php
namespace Home\Controller;
use Common\Controller\StageController;
class EarningsController extends StageController{
    protected $dbTable = 'Brokerage';

    public function index(){
        //取出收益
        $brokerage = $this->dbModel->where(array('user_id' => USER_ID, 'locked' => 0))->select();

        $data = array();

        for($i = 1;$i <= C('BROKERAGE'); $i++){
            $data['profit_'.$i] = $data['scale_'.$i] = 0;
        }

        $moneyTotal = 0;
        foreach($brokerage as $val){
            $moneyTotal += $val['money'];
        }

        foreach($brokerage as $val){
            $data['profit_'.$val['grade']] = $val['money'];
            $data['scale_'.$val['grade']]  = sprintf('%.2f' ,$val['money'] / $moneyTotal * 100);
        }

        $this->assign('data', $data);
        $this->display();
    }
}