<?php
/**
 * ====================================
 * 支付宝接口
 * ====================================
 * Author: 91336
 * Date: 2014/12/15 11:51
 * ====================================
 * File: Alipay.class.php
 * ====================================
 */
namespace Common\Library\Payment;
class Alipay {

    protected $notify_url = '';
    protected $return_url = '';

    //支付类型，1即时到账
    protected $payment_type = '1';

    //签名方式 不需修改
    protected $sign_type = 'MD5';

    //字符编码格式 目前支持 gbk 或 utf-8
    protected $input_charset = 'utf-8';

    //ca证书路径地址，用于curl中ssl校验
    //请保证cacert.pem文件在当前文件夹目录中
    protected $cacert = '';

    //访问模式,根据自己的服务器是否支持ssl访问，若支持请选择https；若不支持请选择http
    protected $transport = 'http';

    /**
     *支付宝网关地址（新）
     */
    protected $alipay_gateway_uri = 'https://mapi.alipay.com/gateway.do?';

    //支付宝沙箱测试地址
    protected $alipay_sandbox_uri = "https://openapi.alipaydev.com/gateway.do?";

    public function __construct($config = array()) {
        isset($config['notify_url']) ? $this->notify_url = $config['notify_url'] : '';
        isset($config['transport']) ? $this->transport = $config['transport'] : '';
        isset($config['return_url']) ? $this->return_url = $config['return_url'] : '';
        $this->cacert = dirname(__FILE__) . '/cacert.pem';
    }

    //生成支付连接
    public function getPayUri() {
        $params = func_get_args();
        $params = empty($params) ? I('request.') : $params;

        //构造要请求的参数数组，无需改动
        $parameter = array(
            "service" => "create_direct_pay_by_user",
            "partner" => C('ALIPAY_PARTNER'),
            "payment_type"	=> $this->payment_type,
            "notify_url"	=> $this->notify_url,
            "return_url"	=> $this->return_url,
            "seller_email"	=> C('ALIPAY_SELLER'),
            "out_trade_no"	=> $params['out_trade_no'],
            "subject"	=> $params['subject'],
            "total_fee"	=> $params['total_fee'],
            "body"	=> $params['body'],
            "show_url"	=> $params['show_url'],
            "_input_charset"	=> trim(strtolower($this->input_charset))
        );
        //过滤空参数
        $parameter = $this->filter($parameter);

        //排序参数
        ksort($parameter);
        reset($parameter);

        //签名结果与签名方式加入请求提交参数组中
        $parameter['sign'] = $this->sign($parameter);
        $parameter['sign_type'] = strtoupper(trim($this->sign_type));

        return $this->alipay_gateway_uri . http_build_query($parameter);
    }

    /**
     * 支付返回
     */
    public function respond() {

    }

    //异步通知操作
    public function notify() {

    }

    /**
     * 创建签名
     * @param $params
     * @return string
     */
    protected function sign($params) {
        $params = http_build_query($params);
        //如果存在转义字符，那么去掉转义
        if(get_magic_quotes_gpc()){
            $params = stripslashes($params);
        }
        $sign = "";
        switch (strtoupper(trim($this->sign_type))) {
            case "MD5" :
                $sign = md5($params, C('ALIPAY_KEY'));
                break;
        }
        return $sign;
    }

    /**
     * 除去数组中的空值和签名参数
     * @param $params 签名参数组
     * return 去掉空值与签名参数后的新签名参数组
     */
    protected function filter($params) {
        $para_filter = array();
        while (list ($key, $val) = each ($params)) {
            if($key == "sign" || $key == "sign_type" || $val == "")
                continue;
            else
                $para_filter[$key] = $params[$key];
        }
        return $para_filter;
    }

    /**
     * 远程获取数据，POST模式
     * 注意：
     * 1.使用Crul需要修改服务器中php.ini文件的设置，找到php_curl.dll去掉前面的";"就行了
     * 2.文件夹中cacert.pem是SSL证书请保证其路径有效，目前默认路径是：getcwd().'\\cacert.pem'
     * @param $url 指定URL完整路径地址
     * @param $cacert_url 指定当前工作目录绝对路径
     * @param $para 请求的数据
     * @param $input_charset 编码格式。默认值：空值
     * return 远程输出的数据
     */
    protected function post($url, $params, $input_charset = '') {

        if (trim($input_charset) != '') {
            $url = $url . "_input_charset=" . $input_charset;
        }
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);//SSL证书认证
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);//严格认证
        curl_setopt($curl, CURLOPT_CAINFO, $this->cacert);//证书地址
        curl_setopt($curl, CURLOPT_HEADER, 0 ); // 过滤HTTP头
        curl_setopt($curl,CURLOPT_RETURNTRANSFER, 1);// 显示输出结果
        curl_setopt($curl,CURLOPT_POST,true); // post传输数据
        curl_setopt($curl,CURLOPT_POSTFIELDS, $params);// post传输数据
        $responseText = curl_exec($curl);
        //var_dump( curl_error($curl) );//如果执行curl过程中出现异常，可打开此开关，以便查看异常内容
        curl_close($curl);

        return $responseText;
    }

    /**
     * 远程获取数据，GET模式
     * 注意：
     * 1.使用Crul需要修改服务器中php.ini文件的设置，找到php_curl.dll去掉前面的";"就行了
     * 2.文件夹中cacert.pem是SSL证书请保证其路径有效，目前默认路径是：getcwd().'\\cacert.pem'
     * @param $url 指定URL完整路径地址
     * @param $cacert_url 指定当前工作目录绝对路径
     * return 远程输出的数据
     */
    protected function get($url) {
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, 0 ); // 过滤HTTP头
        curl_setopt($curl,CURLOPT_RETURNTRANSFER, 1);// 显示输出结果
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);//SSL证书认证
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);//严格认证
        curl_setopt($curl, CURLOPT_CAINFO,$this->cacert);//证书地址
        $responseText = curl_exec($curl);
        //var_dump( curl_error($curl) );//如果执行curl过程中出现异常，可打开此开关，以便查看异常内容
        curl_close($curl);

        return $responseText;
    }
}