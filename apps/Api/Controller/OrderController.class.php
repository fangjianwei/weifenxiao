<?php
/**
 * ====================================
 * 这里是说明
 * ====================================
 * Author: 91336
 * Date: 2014/12/25 11:20
 * ====================================
 * File: OrderController.class.php
 * ====================================
 */
namespace Api\Controller;
use Common\Library\Config;
use Common\Model\AppAccountModel;
use Common\Model\BackInfoModel;
use Common\Model\OrderInfoModel;
use Common\Model\RegionModel;
use Think\Controller\RpcController;
use Common\Library\Order\Operate;
use Common\Library\Manage\Master;

class OrderController extends RpcController {

    /**
     * 登录验证
     * @param $app_key  应用标识KEY
     * @param $app_secret 应用通讯密钥
     */
    public function clientLogin($app_key, $app_secret) {
        $appModel = new AppAccountModel();
        $appInfo = $appModel->where(array(
            'app_key' => trim($app_key),
            'app_secret' => trim($app_secret),
            'locked' => 0
            ))
            ->find();
        if($appInfo) {
            session('login_app', $app_key);
            return true;
        }else {
            return false;
        }
    }

    /**
     * 获取退货单
     * @param $order_sn
     * @return array|int|mixed
     */
    public function getBackOrder($order_sn) {
        if(!$this->_session()) return -1;
        $backModel = new BackInfoModel();
        $where = array(
            'order_sn' => $order_sn,
            'back_status' => BK_AUDIT
        );

        $back = $backModel->field("order_sn, order_id, back_sn,shipping_name,back_invoice_no,back_status,remark,create_time")->where($where)->find();
        if(!$back) return array();

        $back['goods_list'] = M('BackGoods')->where(array('order_id' => $back['order_id']))->select();

        return $back;
    }

    public function orderBack($order_sn, $goods_list) {
        if(!$this->_session()) return -1;
        $goods_list = json_decode($goods_list, true);
        if(empty($goods_list)) return -2; //商品为空

        $backModel = new BackInfoModel();
        $back = $backModel->where("order_sn = '%s' AND back_status = " . BK_AUDIT, $order_sn)->find();
        if(!$back) return -3; //没有找到相关退货单

        $order_id = $back['order_id'];
        $backGoodsModel = M('BackGoods');
        $back_goods = $backGoodsModel->where("order_id = '%d'", $order_id)->select();

        $package = $package_goods = $alone_goods = array();
        foreach($back_goods as $key => $goods) {
            switch($goods['extension_code']) {
                case 'package_buy':
                    $package[$goods['goods_id']] = $goods;
                    break;
                case 'package_goods':
                    $package_goods[$goods['goods_sn']] = $goods;
                    break;
                default:
                    $alone_goods[$goods['goods_sn']] = $goods;
                    break;
            }
        }

        //处理套装商品
        foreach($package_goods as $goods_sn => $goods) {
            $goods_number = 0;
            if(isset($goods_list[$goods_sn])) {
                $real_goods_number = $goods_list[$goods_sn];
                if($real_goods_number < 1) continue;
                $package_info = $package[$goods['parent_id']];
                $goods_number = min($package_info['goods_number'] * $goods['goods_number'], $real_goods_number);
                $package_goods[$goods_sn]['real_goods_number'] = $goods_number;
                $goods_list[$goods_sn] -= $goods_number;
            }
            $package[$goods['parent_id']]['real_goods_number_arr'][] = floor($goods_number / $goods['goods_number']);
        }

        //处理单品
        foreach($alone_goods as $goods_sn => $goods) {
            if(!isset($goods_list[$goods_sn])) continue;
            $real_goods_number = $goods_list[$goods_sn];
            if($real_goods_number < 1) continue;
            $alone_goods[$goods_sn]['real_goods_number'] = $real_goods_number;
        }

        //处理套装
        foreach($package as $goods_id => $goods) {
            $goods['real_goods_number'] = min($goods['real_goods_number_arr']);
            unset($goods['real_goods_number_arr']);
            $package[$goods_id] = $goods;
        }

        $back_goods = array_merge($package, $package_goods, $alone_goods);

        if($back_goods) {
            //处理单品数量
            foreach($back_goods as $key => $goods) {
                $backGoodsModel->where("rec_id = '%d'", $goods['rec_id'])->save($goods);
            }
            $backModel->where("order_sn = '%s' AND back_status = " . BK_AUDIT, $order_sn)->save(array(
                'back_status' => BK_RECEIVED,
                'back_time'   => date(C('DATE_FORMAT'))
            ));

            //记录日志
            $backModel->addLog($back['order_id'], '仓库返回退货商品', BK_RECEIVED, 9);
        }
        return 1;
    }

    /**
     * 查询订单配货中总数
     * @param string $where
     * @return bool
     */
    public function getShippingOrderCount($where = '') {
        if(!$this->_session()) return -1;
        $orderModel = new OrderInfoModel();
        $orderModel->alias(' AS o');
        $orderModel->where($this->_where($where));
        return (int)$orderModel->count();
    }

    /**
     * 订单查询接口
     * @param $page 页码
     * @param $pageSize 每页数量
     * @param string $where  查询条件
     * @param string $orderBy 排序
     * @return array|bool
     */
    public function getShippingOrder($page, $pageSize, $where = '', $orderBy = '') {
        if(!$this->_session()) return -1;
        $page = intval($page) < 1 ? 1 : $page;
        $pageSize = intval($pageSize) < 10 ? 10 : $pageSize;
        $orderModel = new OrderInfoModel();
        $orderModel->alias(' AS o');
        $orderModel->where($this->_where($where));

        if($orderBy) $orderModel->order($orderBy);
        else $orderModel->order("o.picking_time ASC");

        $data = $orderModel->page($page)->limit($pageSize)->select();
        if(!$data) return array();
        $orderGoodsModel = M('OrderGoods');
        $regionModel = new RegionModel();
        foreach($data as $key => $row) {
            $regionId = array($row['province'], $row['city'], $row['district']);
            $region = $regionModel->where(array('region_id' => array('IN', $regionId)))->getField('region_id, region_name');
            $address = '';
            foreach($regionId as $id) {
                $address .= $region[$id];
            }
            $row['site_id'] = $row['channel_id'] ? $row['channel_id'] : 79;
            $row['warehouse'] = 1;
            $row['address'] = $address . $row['address'];
            $row['goods_list'] = $orderGoodsModel->field('*, goods_number as real_goods_number')->where(array('order_id' => $row['order_id']))->select();
            $row['user_no'] = M('agent')->where(array('user_id'=>$row['user_id']))->getField('user_no');
            $data[$key] = $row;
        }

        return $data;
    }

    /**
     * 更新订单状态
     * @param $orderId
     * @param $shippingStatus
     * @param string $invoiceNo
     * @param string $addNote
     * @param int $postTime
     * @return int
     */
    public function setOrderStatus($orderSn, $shippingId, $shippingStatus, $invoiceNo = '', $addNote = '', $postTime = 0) {
        if(!$this->_session()) return -1;
        $orderModel = new OrderInfoModel();
        $postTime = empty($postTime) ? date(C('DATE_FORMAT')) : $postTime;
        $order = $orderModel->where("order_sn = '$orderSn'")->find();
        if(!$order) return -5;
        if($order['order_status'] != OS_AUDIT_PASS) return -2;
        if(in_array($order['shipping_status'], array(SS_DEPOSTUNUSUAL, SS_RECEIVED))) return -3;
//        if($shippingId != $order['shipping_id']) return -4;
        $orderModel->where(array('order_id' => $order['order_id']));
        $data = array(
            'update_time' => $postTime,
            'shipping_status' => $shippingStatus,
            'shipping_id' => $shippingId,
            'shipping_name' => M('shipping')->where(array('shipping_id'=>$shippingId))->getField('shipping_name')
        );
        $addNote = empty($addNote) ? get_lang('shipping_status', $shippingStatus) : $addNote;
        if(empty($invoiceNo)) {
            $data['invoice_no'] = $order['invoice_no'];
        }else {
            $data['invoice_no'] = $invoiceNo;
            if($order['invoice_no'] && $order['invoice_no'] != $invoiceNo) {
                $addNote = "重新打单，快递单号：$invoiceNo";
            }else {
                $addNote = "已打单，快递单号：$invoiceNo";
            }
        }
        switch($shippingStatus) {
            case SS_EXPRESSED: //已打单
                //更新打单时间
                $data['print_time'] = $postTime;
                break;
            // //已发货
            case SS_SHIPPED:
                $data['delivery_time'] = $postTime;
                //增加商品发货数
                $order_goods = D('OrderGoods')
                             ->field('goods_id,goods_number,extension_code,parent_id,rec_id')
                             ->where(array('order_id' => $order['order_id'],'extension_code' => array('neq','package_buy')))
                             ->select();
                //判断是单品还是套装
                foreach($order_goods as $val){
                    //套装
                    if($val['extension_code'] == 'package_goods' && $val['parent_id'] > 0){
                        $number = D('OrderGoods')
                                ->join('AS a left join __ORDER_GOODS__ AS b on a.parent_id=b.goods_id')
                                ->where(array('a.rec_id' => $val['rec_id'],'a.order_id' => $order['order_id'],'b.order_id' => $order['order_id'],'a.parent_id' =>$val['parent_id']))
                                ->getField('a.goods_number*b.goods_number');
                        //更新商品发货数
                       $res    = D('Goods')
                               ->where(array('goods_id' => $val['goods_id']))
                               ->setInc('shipment',$number);
                    }else{
                       //单品
                        $res   = D('Goods')
                               ->where(array('goods_id' => $val['goods_id']))
                               ->setInc('shipment',$val['goods_number']);
                    }

                }
                break;
            case SS_DEPOSTUNUSUAL:
                $data['abnormal_time'] = $postTime;
                $addNote = "仓库返回异常！";
                break;
            case SS_SPlIT:
                $data['split_time'] = $postTime;
                $addNote = "商品拆单！";
                break;
        }

        if($orderModel->save($data)) {
            //记录订单日志
            $orderModel->addLog($order['order_id'], $addNote, $order['order_status'], $order['shipping_status'], $order['pay_status'], 9);
            if($shippingStatus==SS_DEPOSTUNUSUAL){
                $operateObj = new Operate($order, false);
                $operateObj->free_stock();
                //解除相应的冻结金额
                Master::unBalance($order['master_id'], $order['user_id'], $order['surplus'], '仓库返回异常,取消订单返款，SN：' . $order['order_sn'], USER_ID);
            }
            return 1;
        }else {
            return -6;
        }

    }

    /**
     * 退货
     * @param $orderId
     * @return int
     */
    public function orderReturn($orderSn) {
        if(!$this->_session()) return -1;
        $orderModel = new OrderInfoModel();
        $postTime = date(C('DATE_FORMAT'));
        $order = $orderModel->where("order_sn = '$orderSn'")->find();
        if(!$order) return -1;
        if($order['shipping_status'] != SS_SHIPPED) return -2;

        $data = array(
            'order_status' => OS_RETURNED,
            'returns_time' => $postTime
        );
        if($orderModel->where(array('order_id' => $order['order_id']))->save($data)) {
            //记录订单日志
            $orderModel->addLog($order['order_id'], get_lang('order_status', $data['order_status']), $order['order_status'], $order['shipping_status'], $order['pay_status']);
            return 1;
        }
        return -1;
    }

    /**
     * 折单商品详情接口
     * @param $orderSn
     * @param $params
     * @return int
     */
    public function setOrderSplit($orderSn, $params){
        $params=json_decode($params);
        if(empty($orderSn) || empty($params))return -2;
        if(!$this->_session()) return -1;
        $orderModel = new OrderInfoModel();
        $orderId = $orderModel->where(array('order_sn'=>$orderSn))->getField('order_id');
        $orderGoodsModel=M('order_goods');

        $error=0;
        $data=array();
        foreach($params as $key=>$item){
            $goodsList=explode(',', $item->goods_id);
            $goodsNum=explode(',', $item->qty);
            foreach($goodsList as $k=>$row){
                if($goodsNum[$k]<0){
                    $error=1;
                    break;
                }

                $goods=array();
                $where=array('goods_id'=>$row, 'order_id'=>$orderId, 'parent_id'=>0);

                $goods=M('order_goods')->where($where)->field("order_id,goods_id,goods_name,goods_sn,goods_number,shop_price,goods_price,extension_code,
                    parent_id,is_gift,'{$item->invoice_no}' as invoice_no")->find();

                if(empty($goods)){
                    $error=1;
                    break;
                }
                $goods['goods_number']=$goodsNum[$k];
                $data[]=$goods;
            }
        }

        if($error==1){
            return 0;
        }
        $orderGoodsModel->startTrans();

        $orderGoodsModel->where(array('order_id'=>$orderId,'parent_id'=>0))->delete();
        $orderGoodsModel->addAll($data);
        if($orderGoodsModel->getError()){
            $orderGoodsModel->rollback();
            return -1;
        } else{
            $orderGoodsModel->commit();
            return 1;
        }
    }

    /**
     * 处理订单查询条件
     * @param string $where
     * @return array|string
     */
    private function _where($where = '') {
        if($where) {
            return $where;
        }else {
            return array(
                'o.order_status' => OS_AUDIT_PASS,
                'o.shipping_status' => SS_PREPARING,
                'o.pay_status' => PS_PAYED
            );
        }
    }

    //取得登录身份
    private function _session() {
        return session('login_app');
    }


}