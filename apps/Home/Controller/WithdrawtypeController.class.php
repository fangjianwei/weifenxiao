<?php
/**
 * Created by PhpStorm.
 * User: 9009078
 * Date: 2016/5/20
 * Time: 13:51
 */
namespace Home\Controller;
use Common\Library\Sms;
use Common\Controller\StageController;
use Common\Model\WithdrawTypeModel;

class WithdrawtypeController extends StageController
{

    public function _initialize()
    {
        parent::_initialize();
        $this->db = new WithdrawTypeModel();
    }

    /**
     * 其他提现方式
     */
    public function select_apply_type() {
        $this->display();
    }

    /**
     * 管理提现方式
     * return array  参数1:支付ID号；参数2：支付方式名称；参数3：是否支持该支付方式付款(0否1是)；参数4：是否支持该支付方式提现（0否1是）；参数5：手续费
     */
    public function withtype_config() {
        $web_payment = $this->db->get_withdraw_type();    //当前用户绑定的支付方式
        $this->assign('web_payment',$web_payment);
        $this->display();
    }

    /**
     * 编辑提现方式
     */
    public function edit_withdraw_type(){
        $params = I("request.");
        $data = $this->db->edit_withdraw_type($params);
        if($params['payment_key'] == 'WeChatpay'){
            $tpl = '';
        }elseif($params['payment_key'] == 'Alipay'){
            $tpl = 'Withdrawtype/edit_zfb';
        }elseif($params['payment_key'] == 'Unionpay'){
            $tpl = 'Withdrawtype/edit_card';
        }

        $this->assign('data',$data);
        $this->display($tpl);
    }

    /**
     * 编辑支付宝
     */
    public function editzfb() {
        $this->display();
    }

    /**
     * 编辑银行卡
     */
    public function editcard() {
        $this->display();
    }
}