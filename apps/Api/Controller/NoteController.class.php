<?php
/**
 * Created by PhpStorm.
 * User: 91336
 * Date: 2015/5/26
 * Time: 11:45
 */
namespace Api\Controller;
use Common\Model\AppAccountModel;
use Common\Model\NoteModel;
use Think\Controller\RpcController;

class NoteController extends RpcController {
    /**
     * 登录验证
     * @param $app_key  应用标识KEY
     * @param $app_secret 应用通讯密钥
     */
    public function clientLogin($app_key, $app_secret) {
        $appModel = new AppAccountModel();
        $appInfo = $appModel->where(array(
            'app_key' => trim($app_key),
            'app_secret' => trim($app_secret),
            'locked' => 0
        ))
            ->find();
        if($appInfo) {
            session('login_app', $app_key);
            return true;
        }else {
            return false;
        }
    }

    public function gird($limit = 100) {
        if(!$this->_session()) return -1;
        $dbModel = new NoteModel();
        $where = array(
            'create_time' => array('gt', '2015-05-26'),
            'have_sync' => 0
        );
        $data = $dbModel->where($where)->limit($limit)->field('mobile')->select();
        return $data;
    }

    public function setSync($mobile) {
        $dbModel = new NoteModel();
        $where = array(
            'have_sync' => 0,
            'mobile' => array('IN', $mobile)
        );
        return $dbModel->where($where)->save(array('have_sync' => 1));
    }

    //取得登录身份
    private function _session() {
        return session('login_app');
    }
}