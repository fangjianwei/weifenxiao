<?php
/**
 * Created by PhpStorm.
 * User: 9009078
 * Date: 2016/5/5
 * Time: 14:43
 */
namespace Common\Model;

use Common\Library\Common;

class FrontmemberModel extends CommonModel {

    /**
     * 绑定手机号
     * @return array
     */
    public function bound(){
        $where['user_id'] = 1;
        $data = M('member')->field('mobile,truename')->where($where)->find();
        Vendor('phxcrypt.phxcrypt');
        $myCrypt = new \phxCrypt;
        $data['mobile'] = $myCrypt->isDecrypted($data['mobile']);    //手机号码加密
        return $data;
    }

    /**
     * 绑定手机号--数据处理
     * @return array
     */
    public function bound_action(){
        $params = I("request.");
        if($params['truename']) {
            $data['truename'] = trim($params['truename']);
        }else{
            return BOUND_TRUENAME_EMPTY;   //姓名为空
        }
        if($params['mobile']) {
            if(!preg_match("/^0?(13[0-9]|15[0123456789]|17[0123456789]|18[0123456789]|14[57])[0-9]{8}$/",$params['mobile'])){
                return BOUND_MOBILE_ERR;   //手机号格式有误
            }
            Vendor('phxcrypt.phxcrypt');
            $myCrypt = new \phxCrypt;
            $data['mobile'] = $myCrypt->phxEncrypt(trim($params['mobile']));    //手机号码加密
            $flag = $this->chech_mobile_exist($data['mobile']);
            if($flag){
                return BOUND_MOBILE_EXIST;   //手机号已注册
            }
        }else{
            return BOUND_MOBILE_EMPTY;   //手机号为空
        }
        M('member')->where("user_id = 1")->save($data);
        return BOUND_SUC;
    }

    /**
     * 绑定手机号--数据处理
     * @return array
     */
    public function stu_profile_action(){
        $params = I("request.");
        if($params['truename']) {
            $data['truename'] = trim($params['truename']);
        }else{
            return BOUND_TRUENAME_EMPTY;   //姓名为空
        }
        if($params['mobile']) {
            if(!preg_match("/^0?(13[0-9]|15[0123456789]|17[0123456789]|18[0123456789]|14[57])[0-9]{8}$/",$params['mobile'])){
                return BOUND_MOBILE_ERR;   //手机号格式有误
            }
            Vendor('phxcrypt.phxcrypt');
            $myCrypt = new \phxCrypt;
            $data['mobile'] = $myCrypt->phxEncrypt(trim($params['mobile']));    //手机号码加密
            $flag = $this->chech_mobile_exist($data['mobile']);
            if($flag){
                return BOUND_MOBILE_EXIST;   //手机号已注册
            }
        }else{
            return BOUND_MOBILE_EMPTY;   //手机号为空
        }
        if($params['wechat']) {
            $data['wechat'] = trim($params['wechat']);
        }else{
            return BOUND_WECHAT_EMPTY;   //微信号为空
        }
        M('member')->where("user_id = 1")->save($data);
        return BOUND_SUC;
    }

    /**
     * 验证手机号是否存在
     * @return array
     */
    public function chech_mobile_exist($mobile){
        $user_id = 1;
        $where['mobile'] = array('eq',$mobile);
        $where['user_id'] = array('neq',$user_id);
        $flag = M('member')->where($where)->count();
        return $flag;
    }

    /**
     * 个人资料
     * @return array
     */
    public function stu_profile(){
        $where['user_id'] = 1;
        $data = M('member')->field('mobile,truename,wechat')->where($where)->find();
        Vendor('phxcrypt.phxcrypt');
        $myCrypt = new \phxCrypt;
        $data['mobile'] = $myCrypt->isDecrypted($data['mobile']);    //手机号码解密
        return $data;
    }

    /**
     * 查看member表是否存在此用户记录
     * @return array
     */
    public function check_user_exist($openid){
        $where['openid'] = $openid;
        $data = M('member')->where($where)->find();
        return $data;
    }

    /**
     * 新增用户数据
     * @param $data_userinfo 用户信息
     * @param $source 用户来源
     * @return array
     */
    public function insert_user($data_userinfo,$source){
        $data['openid'] = $data_userinfo['openid'];
        $data['nickname'] = $data_userinfo['nickname'];
        $data['sex'] = $data_userinfo['sex'];
        $data['province'] = $data_userinfo['province'];
        $data['city'] = $data_userinfo['city'];
        $data['country'] = $data_userinfo['country'];
        $data['headimgurl'] = $data_userinfo['headimgurl'];
        $data['source'] = $source;
        $data['add_time'] = time();  //用户数据创建时间
        $user_id = M('member')->add($data);

        $data_user = M('member')->where("user_id = '$user_id'")->find();
        return $data_user;
    }

    /**
     * 获取我的代理概述信息
     * @return array
     */
    public function get_contacts_info(){
        $user_id = 1;
        $where['user_id'] = $user_id;
        $pid = M('member')->where("user_id = '$user_id'")->getField('pid');     //上级ID
        if($pid){
            $data_parent = M('member')->field('truename')->where("user_id = '$pid'")->find();
            $data['parent_truename'] = empty($data_parent['truename'])?'匿名':$data_parent['truename'];    //上级代理昵称
        }else{
            $data['parent_truename'] = '无';
        }
        $data_step = $this->get_member_step($where);
        $data['step1_nums'] = count($data_step[0]);  //一级代理人数
        $data['step2_nums'] = count($data_step[1]);  //二级代理人数
        $data['step3_nums'] = count($data_step[2]);  //三级代理人数
        return $data;
    }

    /**
     * 获取各级代理数据
     * @return array   说明：$data[0]存放一级代理user_id,$data[1]存放二级代理user_id
     */
    public function get_member_step($user_id){
        global $data;
        $where['pid'] = array('in',$user_id);
        $data_user_id = M('member')->field('user_id')->where($where)->select();
        foreach($data_user_id as $k => $v){
            $arr_user_id[] = $v['user_id'];
        }
        if($arr_user_id){
            $data[] = $arr_user_id;
            $this->get_member_step($arr_user_id);
        }
        return $data;
    }

    /**
     * 获取代理数据列表
     * @return array
     */
    public function get_contacts_member(){
        $params = I("request.");
        $where['user_id'] = 1;
        $data_step = $this->get_member_step($where);
        $data_step_level = $data_step[($params['level']-1)];
        $startCount = ($params['page']-1) * $params['pagesize'];
        for($i=$startCount;$i<($startCount+$params['pagesize']);$i++){
            if($data_step_level[$i]){
                $data_page[] = $data_step_level[$i];
            }
        }
        $data['totalNumber'] = M('member')->where(array('user_id'=>array('in',$data_step_level)))->count(); //获得记录总数
        $data['totalPage']=ceil($data['totalNumber']/$params['pagesize']); //计算出总页数
        $map['user_id'] = array('in',$data_page);
        $result = M('member')->field('user_id,truename,nickname,headimgurl,pid')->where($map)->select();
        foreach($result as $k=>$v){
            $pid = $v['pid'];
            $data_parent = M('member')->field('truename')->where("user_id = '$pid'")->find();
            $result[$k]['bossnickname'] = empty($data_parent['truename'])?'匿名':$data_parent['truename'];    //上级代理昵称
            $result[$k]['ordermoney'] = 100;  //贡献收益
        }
        $data['user'] = $result;    //当前分页数据
        $data['length'] = $startCount+$params['pagesize'];
        return $data;
    }
}