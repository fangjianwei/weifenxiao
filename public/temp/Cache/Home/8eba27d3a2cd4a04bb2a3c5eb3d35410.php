<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta name="HandheldFriendly" content="true" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<title>提现</title>
<link type="text/css" rel="stylesheet" href="/public/stage/mobile/css/reset.css?11" />
<link type="text/css" rel="stylesheet" href="/public/stage/mobile/css/style.css?11" />
<!--<link href="/public/stage/mobile/res/font-awesome/css/font-awesome.min.css" rel="stylesheet" />-->
</head>
<body class="BG">
<section>
    <div class="titleBox clearfix">
        <a href="javascript:history.go(-1)" class="f-left">
            <img src="/public/stage/mobile/images/back.png" /></a>
        <span>提现</span>
        <a href="<?php echo U('home/member/finance');?>" class="f-right">提现记录</a>
    </div>
</section>
<section>
<form method="post" action="<?php echo U('home/member/withdraw_action');?>" id="form1">
    <div class="getMoney mt10">
        <dl>
            <dd>
                <div class="getMoneyBox">
                    <div class="getMoneyBoxL">
                        <p>提现到微信</p>
                    </div>

                    <div class="getMoneyBoxR">
                        <div class="getMoneyBoxRHid">

                            <p class="textIn">
                                <?php if(defined('TRUENAME')): ?><input type="text" name="truename" value="<?php echo (TRUENAME); ?>" readonly="readonly">
                                <?php else: ?>
                                    <input type="text" name="truename" value="" readonly="readonly"><?php endif; ?>
                                <a href="<?php echo U('home/member/SelectApplyType');?>" style="float: right;">其他提现方式<i class="fa fa-credit-card"></i></a>
                            </p>
                        </div>
                    </div>  
                </div>
                <p class="getMoneyRemind">可提现金额：<span class="js-confirm"><?php echo ($allow_withdraw_money); ?></span> 元 &nbsp; 单笔最低提现金额3.00元, 单笔最高提现金额10000.00元</p>
                <?php if(!empty($data_member_default_withdraw[payment_fee])): ?><p class="getMoneyRemind">手续费：<?php echo ($data_member_default_withdraw["payment_fee"]); ?>%</p>
                <?php else: ?>
                    <p class="getMoneyRemind">手续费：无手续费</p><?php endif; ?>
            </dd>
            <dd>
                <div class="getMoneyBox">
                    <div class="getMoneyBoxL">
                        <p>手机号码</p>
                    </div>
                    <div class="getMoneyBoxR">
                        <p class="textIn">
                            <?php if(defined('MOBILE')): ?><input type="text" name="mobile" value="<?php echo (MOBILE); ?>" readonly="readonly">
                                <?php else: ?>
                                <input type="text" name="mobile" value="" readonly="readonly"><?php endif; ?>
                            <!--<a href="javascript:void (0);" onclick="get_code_sms();" style="float: right;">获取验证码</a>-->
                            <a href="javascript:void (0);" id="send_code_sms" onclick="get_code_sms();" style="float: right;">获取验证码</a>
                        </p>
                    </div>
                </div>
            </dd>
            <dd>
                <div class="getMoneyBox">
                    <div class="getMoneyBoxL">
                        <p>短信验证码</p>
                    </div>
                    <div class="getMoneyBoxR">
                        <p class="textIn">
                            <input type="text" name="code_sms" placeholder="请输入短信验证码" />
                        </p>
                    </div>
                </div>
                <!--本次提现预计<span>2个工作日</span>内到账-->
            </dd>
            <dd>
                <div class="getMoneyBox">
                    <div class="getMoneyBoxL">
                        <p>提现金额</p>
                    </div>
                    <div class="getMoneyBoxR">
                        <p class="textIn">
                            <input type="text" id="blance" name="blance" placeholder="请输入提现金额" />
                        </p>
                    </div>
                </div>
                <p class="getMoneyRemind">本次提现预计2个工作日内到账</p>
                <!--本次提现预计<span>2个工作日</span>内到账-->
            </dd>
        </dl>
        <div class="btnOut mt10">
            <input type="button" value="确定提现" onclick="return submitinput();" id="Button1" class="btn" />
        </div>
    </div>
        </form>
    </section>

<!--底部公共模块-->

<script src="/public/stage/mobile/js/WeixinTools.js"></script>
</body>
<script src="/public/stage/mobile/js/jquery2-1-3.min.js "></script>
<script src="/public/stage/mobile/res/Plugs/jquery.blockUI.min.js"></script>
<script src="/public/stage/mobile/js/common.js"></script>
<script type="text/javascript">
    //回车提交表单事件
    document.onkeydown = function (e) {
        var theEvent = window.event || e;
        var code = theEvent.keyCode || theEvent.which;
        if (code == 13) {
            $("#Button1").click();
        }
    }

    var send_code_lock = false;
    var waiting = 10;
    function get_code_sms(){
        var mobile = $("input[name='mobile']").val();
        if(mobile == '' || !mobile.match(/^0?(13[0-9]|15[0123456789]|17[0123456789]|18[0123456789]|14[57])[0-9]{8}$/)){
            alert('手机号码格式有误！');
            return;
        }
        if (send_code_lock) {return;}
        $.ajax({
            type: "post",
            url: "<?php echo U('home/withdraw/send_codesms');?>",
            data:
            {
                mobile : mobile
            },
            dataType: "json",
            success: function (result) {
                send_code_lock = true;
                if(result.errno == 0){
                    usite.alert.text("发送成功，请注意查收验证码");
                    $('#send_code_sms').html("重新发送 "+waiting);
                    $("#send_code_sms").removeAttr("onclick");
                    switch_send_status();
                }else{
                    usite.alert.text(result.msg);
                }
                return;
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert(errorThrown);
            }
        });
    }


    function switch_send_status(){
        if (waiting == 0) {
            $("#send_code_sms").attr("onclick","get_code_sms();");
            $('#send_code_sms').html("获取验证码");
            waiting = 10;
            send_code_lock = false;
        } else {
            $('#send_code_sms').html("重新发送 "+waiting);
            waiting--;
            setTimeout(function() {
                switch_send_status()
            },
            1000)
        }
    }

    var lock = false;   //防止重复提交
    function submitinput(){
        var flag = check();
        if(flag == true){
            if (lock) {return;}
            lock = true;
            $.ajax({
                type: "post",
                url: "<?php echo U('home/withdraw/withdraw_action');?>",
                data:
                {
                    blance : $.trim($("#blance").val())
                },
                dataType: "json",
                success: function (result) {
                    lock = false;
                    if(result == <?php echo (USER_WITHDRAW_SUC); ?>){
                        usite.alert.text("提交成功，本次提现预计2个工作日内到账");
                        setTimeout("location.href = \"<?php echo U('home/member/index');?>\"",3000)
                    }else if(result == <?php echo (USER_WITHDRAW_BLANCE_EMPTY); ?>){
                        usite.alert.text("提取金额为空");
                    }else if(result == <?php echo (USER_WITHDRAW_BLANCE_ERR); ?>){
                        usite.alert.text("提现金额格式有误");
                    }else if(result == <?php echo (USER_WITHDRAW_MONEY_NOTENOUGH); ?>){
                        usite.alert.text("您的可提现金额不足");
                    }else{
                        location.href = "<?php echo U('home/member/index');?>";
                    }
                    return false;
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert(errorThrown);
                }
            });
        }else{
            return false;
        }
    }

    function check() {
        var n = $("#blance").val();
        n=n.trim();
        if(n=="")
        {
            usite.alert.text("请输入提现金额");
            return false;
        }
        if (isNaN(n)) {
            usite.alert.text("请输入正确提现金额");
            return false;
        }
        if (n<0) {
            usite.alert.text("不能提取负数");
            return false;
        }
        if(n.toString().split(".").length==2){
            if(n.toString().split(".")[1].length>2)
            {
                usite.alert.text("提现金额的精度不能小于分");
                return false;
            }
        }
        var money = parseFloat(n);
        if (money > parseFloat($.trim($(".js-confirm").text()))) {
            usite.alert.text("您的可提现金额不足");
            return false;
        }
        return true;
       /* var chargeType= 0;
        var fixedCharge = null;
        var maxCharge = 0.00;
        var minCharge = 0.00;
        var maxMoney = 10000.00;
        var minMoney = 3.00;
        var rate = 0;
        if( money<minMoney || money>maxMoney)
        {
            usite.alert.text("提现金额不在提现限制范围");
            return false;
        }
        var charge = money * <?php echo ($data_member_default_withdraw["payment_fee"]); ?> / 100;
        var message = "提现金额为"+money+"元，手续费为："+charge+"元，实际到账："+(money-charge)+"元";
        var r = confirm(message);
        return r;*/

    }

</script>
<script></script>
</html>