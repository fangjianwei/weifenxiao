<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>后台管理系统</title>
    <script type="text/javascript" src="/public/static/base.js"></script>
    <script type="text/javascript" src="/public/cpanel/js/easyui.js"></script>
</head>
<body>
<div class="message">
    <?php if(isset($message)): ?><h1>:)</h1>
        <p class="success"><?php echo($message); ?></p>
        <?php else: ?>
        <p class="error"><font>:(</font><?php echo($error); ?></p><?php endif; ?>
    <br><br>
</div>
</body>
</html>