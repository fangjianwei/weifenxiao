<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="HandheldFriendly" content="true" />
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <title>财务明细</title>
    <link type="text/css" rel="stylesheet" href="/public/stage/mobile/css/reset.css?1" />
    <link type="text/css" rel="stylesheet" href="/public/stage/mobile/css/style.css?1" />
    <script type="text/javascript" src="/public/stage/mobile/res/jquery/jquery-2.1.3.min.js"></script>
    <script src="/public/stage/mobile/js/tools.min.js"></script>
    <script src="/public/stage/mobile/js/base.min.js"></script>
</head>
<body class="BG">
    <section>
        <div class="titleBox clearfix">
            <a href="javascript:history.go(-1)" class="f-left">
                <img src="/public/stage/mobile/images/back.png" /></a>
            <span>财务明细</span>
        </div>
    </section>
    <section>
        <div class="financeNav clearfix js-tab">
            <span class="financeNavNow js-toggle" data-t="all">全部</span>
            <span class="js-toggle" data-t="1-2">收益</span>
            <span class="js-toggle" data-t="3-4-5">提现</span>
            <span class="js-toggle" data-t="">其他</span>
        </div>
    </section>
    <section>
        <dl class="financeList financeList0 ">
            <dd>
                <ul class="js-appenddata">
                </ul>
            </dd>
        </dl>
    </section>

    <div class="hidHeight"></div>
<div class="navBottom">
    <dl class="clearfix">
        <dd class="navBottomFirst">
            <p class="navBottomItem">
                <!--<a href="stu_profile.html"><img src="/public/stage/mobile/images/tp8lz7ik.png" /></a>-->
                <?php if(defined('USER_ID')): ?><a href="<?php echo U('home/member/index');?>"><img src="<?php echo (HEADIMGURL); ?>" /></a>
                <?php else: ?>
                    <!--用户没设置头像或未获取到用户头像，显示默认头像-->
                    <a href="<?php echo U('home/member/index');?>"><img src="/public/stage/mobile/images/tp8lz7ik.png" /></a><?php endif; ?>
            </p>
        </dd>
        <dd>
            <p class="navBottomItem"><a href="<?php echo U('goods/index');?>">产品购买</a></p>
        </dd>

        <dd>
            <p class="navBottomItem"><a href="<?php echo U('member/index');?>">代理中心</a></p>
        </dd>

        <dd>
            <p class="navBottomItem"><a href="http://mp.weixin.qq.com/s?__biz=MzA4NTU1NjU2NQ==&mid=404012163&idx=1&sn=d0767f8bd157c37f25f63e70f55ff9af&scene=1&srcid=0314PVTY9tnDILY0LJunffqM#rd">客服中心</a></p>
        </dd>
    </dl>
</div>
<!--底部公共模块-->

<script src="/public/stage/mobile/js/WeixinTools.js"></script>
    <script type="text/javascript">
        var type = '' == "" ? "all" : '';
        var pagesize = 10;
        var t = true;
        $(function () {
            var i = 2;
            $(".js-tab span").removeClass("financeNavNow");
            $(".js-tab span[data-t='" + type + "']").addClass("financeNavNow");
            $(window).bind("scroll", function (event) {
                var top = document.documentElement.scrollTop + document.body.scrollTop;
                var textheight = $(document).height();
                if (textheight - top - $(window).height() <= 5) {
                    downloadData(i, type);
                    i++;
                }
            });
            //选项卡切换
            $(".js-tab span").each(function () {
                $(this).click(function () {
                    $(".js-tab span").removeClass("financeNavNow");
                    $(this).addClass("financeNavNow");
                    if (type != $(this).attr("data-t")) {
                        i = 1;
                        t = true;
                        type = $(this).attr("data-t");
                        jQuery(".js-appenddata").html("");
                        downloadData(i, type);
                        i++;
                    }
                })
            });
        });

        function GetState(s){
            var state = "";
            switch (s) {
                case 0:
                    state = "未入账收益";
                    break;
                case 1:
                    state = "已入账收益";
                    break;
                case 41:
                    state = "待退款";
                    break;
                case 42:
                    state = "退款成功";
                    break;
                case 43:
                    state = "退款失败";
                    break;
                case 31:
                    state = "待打款";
                    break;
                case 32:
                    state = "提现成功";
                    break;
                case 33:
                    state = "提现失败";
                    break;
                default:
                    state="";
                    break;
            }
            return state;
        }
        downloadData(1, type); //默认加载第一页
        function downloadData(page, type) {
            if (t) {
                /*wx.action.post('/Service/AjaxService.svc/FinancedetailList',
                    JSON.stringify({ weid: 'tp8lz7ik', page: page, pagesize: pagesize, type: type }),
                    function(msg) {*/
                $.ajax({
                    type: "post",
                    url: "/home/finance/get_finance_detail",
                    data: {page: page, pagesize: pagesize, type: type},
                    dataType: "json",
                    success: function (result) {
//                        alert(data);
                        var data = result.finance_data;
//                        alert(result.length);
                        if (result.length > 0) {
                            var s = "";
                            $.each(data, function(i, v) {
                                s += "<li>";
                                s += "<a href='#" + v.finance_id + "' class='clearfix'>";
                                s += "   <div class='f-left financeTitle'>";
                                s += "       <p>" + v.title + "</p>";
                                s += "    <p class='financeTime'>";
                                s += "        <span>" + v.add_time_format + "</span>";
                                s += "    </p>";
                                s += " </div>";
                                s += " <div class='f-right financeNumber'>";
                                s += "    <p>金额：<span>" + v.money + "</span></p>";
//                                s += "    <p>";
//                                s += v.State == "1" || v.State == "32" || v.State == "40"
//                                    ? "<span class='financeStepA'>" + GetState(v.State) + "</span>"
//                                    : "<span class='financeStepB'>" + GetState(v.State) + "</span>";
//                                s += "    </p>";
                                s += "</div>";
                                s += "   </a>";
                                s += "</li>";
                            });
                            jQuery(".js-appenddata").append(s);
                            if (result.length < pagesize) {
                                t = false;
                            }
                        } else {
                            t = false;
                        }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
//                        isGetting = false;
//                        appendPos.parent().append('<div class="alert alert-info no-more-pro" style="text-align:center;margin:15px -10px;" >获取更多数据失败，请稍后重试</div>');
//                    alert(errorThrown);
                    }
                });
            }
        }
    </script>
</body>
</html>