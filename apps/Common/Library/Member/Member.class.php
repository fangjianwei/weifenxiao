<?php
/**
 * ====================================
 * 这里是说明
 * ====================================
 * Author: 91336
 * Date: 2014/11/21 14:22
 * ====================================
 * File: Member.class.php
 * ====================================
 */

namespace Common\Library\Member;
use Common\Model\AgentModel;
use Think\Crypt;
set_lang('agent');

class Member {
    /**
     * 获取登录信息
     * @param $key
     * @return bool
     */
    public static function loginInfo($key) {
        static $data;
        if(isset($data[$key])) return $data[$key];
        $data = session(C('MEMBER_SESSION_KEY'));
        $isCookie = false;
        if(empty($data)) {
            $data = cookie(C('MEMBER_SESSION_KEY'));
            $isCookie = true;
        }
        if(empty($data)) return false;
        $data = unserialize(Crypt::decrypt($data, C('CRYPT_KEY')));

        if($isCookie && !$data['cookie_auto_login']) {
            cookie(C('MEMBER_SESSION_KEY'), null);
            return false;
        }

        return isset($data[$key]) ? $data[$key] : false;
    }

    /**
     * 判断是否允许下单
     * @return mixed  不允许返回false，允许返回代理资料
     */
    public static function allowOrder($user_id = 0) {
        $user_id or $user_id = USER_ID;
        $agentModel = new AgentModel();
        $agent = $agentModel
            ->where(array('user_id' => $user_id, 'agent_type' => AG_AUTH))
            ->find();
        //未授权，不允许下单
        if(!$agent) return false;

        //判断等级是否允许下单
        $gradeId = M('AgentGrade')->where(array('grade_id' => $agent['grade_id'], 'allow_order' => 1))->getField('grade_id');
        if(!$gradeId) return false;

        //查询顶级级别ID
        $grade_id = M('AgentGrade')->order('orderby asc')->getField('grade_id');

        //判断是否有上级，如果没有则判断是否为顶级代理
        if(empty($agent['parent_id'])) {
            if($grade_id == $agent['grade_id']) $agent['have_top'] = true;
        }else {
            $parent = get_top_agent($agent['parent_id']);
            if($parent['grade_id'] == $grade_id) $agent['have_top'] = true;
        }

        return $agent;
    }
}