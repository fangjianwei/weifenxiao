<?php
/**
 * Created by PhpStorm.
 * User: 91336
 * Date: 2015/1/7
 * Time: 13:59
 */
namespace Common\Library;
class Code {

    /**
     * 创建验证码
     * @param int $openId
     * @param int $userId
     * @return mixed
     */
    public static function create($openId = 0, $userId = 0) {
        $code = rand(100000,999999);
        $codeModel = M('Code');
        $today = date(C('DATE_FORMAT'));
        $expireTime = date(C('DATE_FORMAT'), strtotime('+10 minute'));

        $data = array(
            'code' => $code,
            'openid' => $openId,
            'user_id' => $userId,
            'create_time' => $today,
            'expire_time' => $expireTime
        );

        if($codeModel->add($data)) {
            return $code;
        }

        return false;
    }

    /**
     * 检查验证码
     * @param $code
     * @param string $field
     * @return mixed
     */
    public static function check($code, $field = '*') {
        $codeModel = M('Code');
        $today = date(C('DATE_FORMAT'));

        $where = array(
            'code' => $code,
            'expire_time' => array('gt', $today),
            'used' => 0
        );
        $data = $codeModel->where($where)->field($field)->find();
        return $data;
    }

    /**
     * 使用验证码
     * @param $code
     * @return mixed
     */
    public static function used($code) {
        $codeModel = M('Code');
        return $codeModel->where("code = '{$code}'")->setField('used', 1);
    }
}