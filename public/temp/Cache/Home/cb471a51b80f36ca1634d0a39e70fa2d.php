<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta name="HandheldFriendly" content="true" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<title>我的代言</title>
<link type="text/css" rel="stylesheet" href="/public/stage/mobile/css/reset.css" />
<link type="text/css" rel="stylesheet" href="/public/stage/mobile/css/style.css" />
<script type="text/javascript" src="/public/stage/mobile/res/jquery/jquery-2.1.3.min.js"></script>
<link href="/public/stage/mobile/res/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<script src="/public/stage/mobile/js/WeixinTools.js"></script>
<script src="/public/stage/mobile/res/bootstrap/js/bootstrap.min.js"></script>
</head>
<body class="BG">
    <section>
        <div class="productList">
            <!--此页面代码需要优化-->
            <p style="margin: 0 0px 0 10px; line-height: 40px">生成代理推广二维码</p>
            <?php if(is_array($data)): foreach($data as $key=>$goods): ?><dl  style="margin-bottom:0px">
                <dd>
                    <div class="productListCon clearfix">
                        <div class="f-left">
                            <img class="productListImg" src="<?php echo ($goods["goods_thumb"]); ?>" />
                        </div>
                        <div class="f-left productListConR">
                            <span><?php echo ($goods["goods_name"]); ?></span><br />
                            <span class="productListConTopR">￥<?php echo ($goods["shop_price"]); ?></span><br />
                        </div>
                    </div>
                    <div class="clearfix">
                        <a class="f-left productListConLink" href="<?php echo U('buy',array('goods_id'=>$goods['goods_id']));?>">立即购买<img src="images/productListMore.png" /></a>
                    </div>
                    
                </dd>
            </dl><?php endforeach; endif; ?>
            <p style="margin: 0 0px 0 10px; line-height: 40px">购买以下任意产品成为代理</p>
            <dl></dl>
        </div>
    </section>
</body>
</html>