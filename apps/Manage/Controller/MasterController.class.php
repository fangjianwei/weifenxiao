<?php
/**
 * ====================================
 * 管理员管理
 * ====================================
 * Author: Hugo
 * Date: 14-5-20 下午9:58
 * ====================================
 * File: MasterController.class.php
 * ====================================
 */

namespace Manage\Controller;
use Common\Controller\ManageController;
use Common\Library\Manage\Master;
use Common\Model\ConsumeLogModel;
use Common\Model\MasterModel;

class MasterController extends ManageController{
    protected $allowAction = array('combo');
    public function info() {
        $masterModel = new MasterModel();
        $this->info = $masterModel->find(USER_ID);
        $this->display();
    }

    public function combo() {
        $keyword = trim(I('keyword'));
        $data = $this->dbModel->filter(array('keyword' => $keyword))->select();
        if($data) {
            foreach($data as $key => $row) {
                $data[$key] = array(
                    'user_id' => $row['user_id'],
                    'user_name' => $row['user_name'],
                    'real_name' => $row['real_name'] . '-' . $row['user_name']
                );
            }
        }
        $this->ajaxReturn($data);
    }

    //充值
    public function recharge() {
        if(IS_AJAX && IS_POST) {
            $params = I('post.');
            //验证充值账户是否存在
            $account_exists = M('PayAccount')->field('account_id,locked')->where(array('account_id' => (int)$params['account_id']))->find();
            if(!$account_exists) $this->error(L('account_not_existence'));
            if($account_exists['locked'] == 1) $this->error(L('ACCOUNT_LOCKED'));
            if(empty($params['user_id'])) $this->error(L('must_select_man'));
            if(empty($params['money'])) $this->error(L('must_input_money'));
            if(Master::recharge($params['user_id'], $params['money'],$params['account_id'],$params['content'], USER_ID)) {
                $this->success(L('SAVE_SUCCESS'));
            }else {
                $this->error(L('OPERATE_ERROR'));
            }
            exit;
        }
        //查询充值账户参数
        $account = M('PayAccount')->where(array('locked' => 0))->select();
        $this->assign('account', $account);
        $this->display();
    }

    public function consume() {
        $user_id = (int)$_GET['user_id'];
        if($user_id && !power('consume-all')) $this->error('您没有权限查看他人消费历史');
        $user_id or $user_id = USER_ID;
        if(IS_AJAX) {
            $logModel = new ConsumeLogModel();
            $logModel->where(
                array(
                    'user_id' => $user_id
                )
            );
            $logModel->order('create_time DESC');
            $data = $logModel->field('create_time, money, content, log_id, master_id, agent_id, account_id')->lists(I('request.'));
            if($data) {
                foreach($data['rows'] as $key => $row) {
                    if($row['master_id']) {
                        $master = D('Master')->where("user_id = '%d'", $row['master_id'])->field("CONCAT(real_name, '-', user_name) AS master_name")->find();
                        $row['master_name'] = $master['master_name'];
                    }
                    if($row['agent_id']) {
                        $agent = D('Agent')->where("user_id = '%d'", $row['agent_id'])->field("CONCAT(contact, '-', user_no) AS agent_name")->find();
                        $row['agent_name'] = $agent['agent_name'];
                    }
                    //区分付款账户
                    $payaccount = M('PayAccount')->where("account_id = '%d'", $row['account_id'])->field('account_name')->find();
                    $row['money'] =isset($payaccount['account_name'])? $row['money'].'&nbsp;<font color=red>['.$payaccount['account_name'].']</font>' : $row['money']. '<font color=red>[账户已被停用]</font>';
                    
                    $data['rows'][$key] = $row;
                }
            }
            $this->ajaxReturn($data);
            exit;
        }
        //账户信息
        $balanceModel = M('Balance');
        $accountdata = $balanceModel->alias('AS b')->join('__PAY_ACCOUNT__ AS p ON b.account_id=p.account_id','LEFT')->where(array('user_id' => $user_id, 'locked' => 0))->field("balance,p.account_name")->select();
        $this->assign('accountdata',$accountdata);
        $this->display();
    }

    //获取渠道参数
    public function getChannel(){
        $channel = C('CHANNEL');
        $newdata = array();
        foreach($channel as $key => $val){
            $new['id']   = $key;
            $new['text'] = $val;
            $newdata[] = $new;
        }
        $this->ajaxReturn($newdata);
    }
}