<?php
/**
 * ====================================
 * 微信接口
 * ====================================
 * Author: 91336
 * Date: 2014/12/8 10:11
 * ====================================
 * File: WchatController.class.php
 * ====================================
 */
namespace Api\Controller;
use Common\Library\Code;
use Common\Library\Config;
use Common\Library\WeChat\WeReceive;
use Common\Model\AgentModel;
use Common\Model\MemberModel;
use Think\Controller;

class WeixinController extends Controller {
    protected function _initialize() {
        Config::init(); //初始化数据库中的配置
    }

    public function receive() {

        /* 加载微信SDK */
        $wechat = new WeReceive(C('WEI_XIN_TOKEN'));

        /* 获取请求信息 */
        $data = $wechat->request();

        if($data && is_array($data)){
            $content = ''; //回复内容，回复不同类型消息，内容的格式有所不同
            $type    = ''; //回复消息的类型
            //接收事件推送
            switch($data['MsgType']) {
                case WeReceive::MSG_TYPE_EVENT:
                    switch($data['Event']) {
                        case WeReceive::MSG_EVENT_SUBSCRIBE: //未关注时
                            $sceneId = str_replace('qrscene_', '', $data['EventKey']);
                            if(empty($sceneId)) { //如果不是扫描二维码回复欢迎语
                                $content = format_weixin_content(C('AUTO_REPLY_MSG'));
                                $type = WeReceive::MSG_TYPE_TEXT;
                                break;
                            }
                        case WeReceive::MSG_EVENT_SCAN: //已关注时
                            if(empty($sceneId)) {
                                $sceneId = $data['EventKey'];
                            }
                            switch($sceneId) {
                                case '9596': //关注发送验证码绑定微信号
                                    if($code = Code::create($data['FromUserName'])) {
                                        $content = sprintf(L('SEND_CODE_CONTENT'), $code);
                                        $type = WeReceive::MSG_TYPE_TEXT;
                                    }
                                    break;
                            }
                            break;
                        case WeReceive::MSG_EVENT_UNSUBSCRIBE:
                            //去除微信号绑定
                            $agentModel = new MemberModel();
                            $agentModel->where(array('weixin_openid' => $data['FromUserName']))
                                ->setField('weixin_openid', '');
                            break;
                    }
                    break;
            }


            /* 响应当前请求(自动回复) */
            $wechat->response($content, $type);
        }
    }
}
