<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>后台管理系统</title>
    <script type="text/javascript" src="/public/static/base.js"></script>
    <script type="text/javascript" src="/public/cpanel/js/easyui.js"></script>
</head>
<body>
<div id="toolbar" class="form-panel">
    <table>
        <tr>
            <td>
                <a class="easyui-linkbutton" iconCls="icon-cancel" plain="true" onclick="delItem(this, 1)">停用</a>
                <a class="easyui-linkbutton" iconCls="icon-ok" plain="true" onclick="delItem(this, 0)">启用</a>
                <a class="datagrid-btn-separator"></a>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="openWindow(this, {})">添加</a>
            </td>
            <th>配置类型：</th>
            <td>
                <select name="search_type" class="easyui-combobox" style="width: 100px;">
                    <option value="-1">全部</option>
                    <?php if(is_array(C("CONFIG_TYPE_LIST"))): $i = 0; $__LIST__ = C("CONFIG_TYPE_LIST");if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$type): $mod = ($i % 2 );++$i;?><option value="<?php echo ($key); ?>"><?php echo ($type); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                </select>
            </td>
            <th>配置分组：</th>
            <td>
                <select name="search_group" class="easyui-combobox" style="width: 100px;">
                    <option value="-1">全部</option>
                    <option value="0">不分组</option>
                    <?php if(is_array(C("CONFIG_GROUP_LIST"))): $i = 0; $__LIST__ = C("CONFIG_GROUP_LIST");if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$type): $mod = ($i % 2 );++$i;?><option value="<?php echo ($key); ?>"><?php echo ($type); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                </select>
            </td>
            <th>关键词：</th>
            <td>
                <input name="keywords" class="easyui-textbox" data-options="buttonText:'查询',prompt:'请输入关键词',onClickButton: function(){searchItem()}">
            </td>
        </tr>
    </table>
</div>
<table id="grid">
    <thead>
        <tr>
            <th data-options="field:'ck',checkbox:true"></th>
            <th data-options="field:'name',width:120">标识</th>
            <th data-options="field:'title',width:50">标题</th>
            <th data-options="field:'group_name',width:50">分组</th>
            <th data-options="field:'type_name',width:50">类型</th>
            <th data-options="field:'create_time',width:50">添加时间</th>
            <th data-options="field:'update_time',width:60, align:'center'">更新时间</th>
            <th data-options="field:'locked',width:50,formatter: function(value){
                if(value == '1') return '<font color=red>停用</font>';
                return '启用';
            }">状态</th>
        </tr>
    </thead>
</table>
<script type="text/javascript">
    var grid = $('#grid');
    grid.datagrid({
        url: "<?php echo U();?>",
        fit: true,
        toolbar: '#toolbar',
        border: false,
        pagination: true,
        fitColumns: true,
        singleSelect:true,
        rownumbers:true,
        selectOnCheck:false,
        checkOnSelect: false,
        onDblClickRow: function(index, row){
            openWindow('#grid', row);
        }
    });

    function openWindow(element, row) {
        blur(element);
        var opts = {
            href: '<?php echo U("form");?>',
            width: 600,
            height: 500,
            data: row,
            handler: function(result){
                grid.datagrid('reload');
            }
        };
        easyui.dialog(opts);
    }

    var searchItem = function(element){
        blur(element);
        var keywords = $.trim($('[name="keywords"]').val());
        var type = $('[name="search_type"]').val();
        var group = $('[name="search_group"]').val();
        grid.datagrid({
            queryParams: {
                keywords: keywords,
                type: type,
                group: group
            }
        });
    };

    var formSubmit = function(element){
        blur(element);
        easyui.submit('toolbar-form', function(result){
            try{
                if(!result.status){
                    easyui.alert(result.info);
                    return false;
                }
                easyui.formReset('toolbar-form');
                grid.datagrid('reload');
            }catch (e){}
        })
    };

    var delItem = function(element, locked){
        blur(element);
        var rows = grid.datagrid('getChecked');
        var items = [];
        for(var i = 0; i < rows.length; i++){
            items.push(rows[i].id);
        }
        items = items.join(',');
        if(empty(items)){
            easyui.alert('请选择需要操作的选项！');
            return false;
        }
        easyui.confirm('请再次确认是否需要继续进行您的操作!', function(){
            easyui.ajaxCall('<?php echo U("lock");?>', {items: items, locked: locked}, function(result){
                try{
                    if(!result.status){
                        easyui.alert(result.info);
                    }
                    grid.datagrid('reload');
                }catch (e){}
            });
        });
    };
</script>
</body>
</html>