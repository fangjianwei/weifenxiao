<?php
namespace Manage\Controller;
use Common\Controller\ManageController;
use Common\Library\Manage\Master;
use Common\Model\ConsumeLogModel;
use Common\Model\MasterModel;

class FrontaccountController extends ManageController{

    //充值
    public function recharge() {
        if(IS_AJAX && IS_POST) {
            $params = I('post.');
            //验证充值账户是否存在
            $account_id = 1;
            $account_exists = M('PayAccount')->field('account_id,locked')->where(array('account_id' => $account_id))->find();
            if(!$account_exists) $this->error(L('account_not_existence'));
            if($account_exists['locked'] == 1) $this->error(L('ACCOUNT_LOCKED'));
            if(empty($params['user_id'])) $this->error(L('must_select_man'));
            if(empty($params['money'])) $this->error(L('must_input_money'));
            if(Master::recharge($params['user_id'], $params['money'],$account_id,$params['content'], USER_ID)) {
                $this->success(L('SAVE_SUCCESS'));
            }else {
                $this->error(L('OPERATE_ERROR'));
            }
            exit;
        }
        //查询充值账户参数
        $account = M('PayAccount')->where(array('locked' => 0))->select();
        $this->assign('account', $account);
        $this->display();
    }

        public function consume() {
        $user_id = (int)$_GET['user_id'];
        if($user_id && !power('frontaccount-consume')) $this->error('您没有权限查看他人消费历史');
        $user_id or $user_id = USER_ID;
        if(IS_AJAX) {
            $logModel = new ConsumeLogModel();
            $logModel->where(
                array(
                    'user_id' => $user_id
                )
            );
            $logModel->order('create_time DESC');
            $data = $logModel->field('create_time, money, content, log_id, master_id, agent_id, account_id')->lists(I('request.'));
            if($data) {
                foreach($data['rows'] as $key => $row) {
                    if($row['master_id']) {
                        $master = D('Master')->where("user_id = '%d'", $row['master_id'])->field("CONCAT(real_name, '-', user_name) AS master_name")->find();
                        $row['master_name'] = $master['master_name'];
                    }
                    if($row['agent_id']) {
                        $agent = D('Member')->where("user_id = '%d'", $row['agent_id'])->field("CONCAT(user_name, '-', special_number) AS agent_name")->find();
                        $row['agent_name'] = $agent['agent_name'];
                    }
                    //区分付款账户
                    $payaccount = M('PayAccount')->where("account_id = '%d'", $row['account_id'])->field('account_name')->find();
                    $row['money'] =isset($payaccount['account_name'])? $row['money'].'&nbsp;<font color=red>['.$payaccount['account_name'].']</font>' : $row['money']. '<font color=red>[账户已被停用]</font>';

                    $data['rows'][$key] = $row;
                }
            }
            $this->ajaxReturn($data);
            exit;
        }
        //账户信息
        $balanceModel = M('Balance');
        $accountdata = $balanceModel->alias('AS b')->join('__PAY_ACCOUNT__ AS p ON b.account_id=p.account_id','LEFT')->where(array('user_id' => $user_id, 'locked' => 0))->field("balance,p.account_name")->select();
        $this->assign('accountdata',$accountdata);
        $this->display();
    }

    public function save(){
        $data = I('post.','','trim');
        $data['is_special'] = 1;
        $success = false;
        $manage_user = array(
            'user_name'     => $data['special_number'],
            'real_name'     => $data['user_name'],
            'sex'           => $data['sex'],
            'locked'        => $data['locked'],
            'role_id'       => $data['role_id'],
            'create_time'   => date('Y-m-d H:i:s'),
            'update_time'   => date('Y-m-d H:i:s'),
            'channel_id'    => $data['channel_id'],

        );
        if($this->dbModel->create($data)){
            if($data[$this->dbModel->getPk()] > 0){
                $success = $this->dbModel->save();
                if($success){
                    unset($manage_user['create_time']);
                    if($data['password']) $manage_user['password'] = md5(md5($data['password']) . C('CRYPT_KEY'));
                    M('Master')->where(array('front_id' => $data[$this->dbModel->getPk()]))->save($manage_user);//修改后台账号
                }
            }else{
                $success = $this->dbModel->add();
                if($success){
                    unset($manage_user['update_time']);
                    $manage_user['password'] = md5(md5($data['password']) . C('CRYPT_KEY'));
                    $manage_user['front_id'] = $success;
                    M('Master')->add($manage_user); //保存到后台
                }
            }
        }
        $success ? $this->success('保存成功！') : $this->error($this->dbModel->getError());
    }

    /**
     * 个人密码修改
     */
    public function  privatekey(){
        $userInfo = M('Master')->field("concat(user_name,'-',real_name) as name,password,front_id")->where(array('user_id' => USER_ID))->find();
        if(IS_POST){
            $data = I('post.');
            if(empty($data['oldPass'])) $this->error('原密码不能为空！');
            if(empty($data['newPass'])) $this->error('新密码不能为空！');
            if(strlen($data['newPass']) < 6) $this->error('密码长度至少为6位！');
            if($data['newPass'] != $data['newPassconfirm']) $this->error('两次密码不正确！');
            if(md5(md5($data['oldPass']) . C('CRYPT_KEY')) != $userInfo['password']) $this->error('原密码不正确！');
            $arr = array(
                'password'      => md5(md5($data['newPass']) . C('CRYPT_KEY')),
                'update_time'   => date('Y-m-d H:i:s'),
            );
            $result = M('Master')->where(array('user_id' => USER_ID))->save($arr);
            if($userInfo['front_id'] > 0 && $result){
                $front_arr = array(
                    'password'      => md5($data['newPass'] . C('CRYPT_KEY')),
                    'update_time'   => date('Y-m-d H:i:s'),
                );
                M('Member')->where(array('user_id' => $userInfo['front_id']))->save($front_arr);
            }
            $result ? $this->success('修改密码成功！') : $this->error('修改密码失败，请稍后重试！');
        }
        $this->assign('info',$userInfo);
        $this->display();
    }

    //获取渠道参数
    public function getChannel(){
        $channel = C('CHANNEL');
        $newdata = array();
        foreach($channel as $key => $val){
            $new['id']   = $key;
            $new['text'] = $val;
            $newdata[] = $new;
        }
        $this->ajaxReturn($newdata);
    }
}