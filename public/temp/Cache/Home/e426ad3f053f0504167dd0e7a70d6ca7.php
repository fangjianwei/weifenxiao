<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta name="HandheldFriendly" content="true"/>
    <meta name="keywords" content=""/>
    <meta name="description" content=""/>
    <title>我的订单</title>
    <link type="text/css" rel="stylesheet" href="/public/stage/mobile/css/reset.css?11"/>
    <link type="text/css" rel="stylesheet" href="/public/stage/mobile/css/style.css?11"/>
    <script type="text/javascript" src="/public/stage/mobile/res/jquery/jquery-2.1.3.min.js"></script>
    <link href="/public/stage/mobile/res/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>
    <link href="/public/stage/mobile/css/main.min.css" rel="stylesheet"/>
</head>
<body class="BG">
<section>
    <div class="titleBox clearfix">
        <a href="javascript:history.go(-1)" class="f-left">
            <img src="/public/stage/mobile/images/back.png"/></a>
        <span>我的订单</span>
    </div>
</section>
<div class="container mt20">
    <div class="row">
        <div class="col-xs-12">
            <div class="btn-group btn-group-justified" role="group" style="padding-bottom: 10px">
                <div class="btn-group" role="group">
                    <a class="btn <?php if($_GET['type']!= 1): ?>btn-info<?php else: ?>btn-default<?php endif; ?>"
                       href="<?php echo U('');?>">商品订单</a>
                </div>
                <div class="btn-group" role="group">
                    <a class="btn <?php if($_GET['type']== 1): ?>btn-info<?php else: ?>btn-default<?php endif; ?>"
                       href="<?php echo U('',array('type' => 1));?>">学员订单</a>
                </div>
            </div>
            <div>
                <?php if($order == false): ?><div class="alert alert-warning" style="margin-bottom: 0;"><b>暂无符合条件的订单</b></div>
                    <?php else: ?>
                    <?php if(is_array($order)): $i = 0; $__LIST__ = $order;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?><a href="<?php echo U('show',array('order_id' => $v[order_id]));?>" class="list-group-item">
                            <ul>
                                <li><?php echo ($v["order_sn"]); ?></li>
                                <li>下单时间：<?php echo ($v["create_time"]); ?></li>
                                <li>订单金额：<b style="color:red;">&yen;<?php echo ($v["goods_amount"]); ?></b></li>
                                <li><b class="text-info pull-right"><?php echo ($v["order_status_text"]); ?></b></li>
                            </ul>
                        </a><?php endforeach; endif; else: echo "" ;endif; endif; ?>
            </div>

        </div>
    </div>
</div>
<script src="/public/stage/mobile/res/jquery/jquery-2.1.3.min.js"></script>
<script src="/public/stage/mobile/res/bootstrap/js/bootstrap.min.js"></script>
<script src="/public/stage/mobile/js/tools.min.js"></script>
<script src="/public/stage/mobile/js/base.min.js"></script>
<script type="text/javascript">

    $(document).ready(function () {
        $('.cancel_order').click(function () {
            if (!confirm('您将撤销此未付款订单，是否确认？')) return;
            var jsonObj = {orderId: $(this).attr('data-orderid')};
            wx.alert.load();
            wx.action.post(
                    "/Service/AjaxService.svc/CancelOrder",
                    JSON.stringify(jsonObj),
                    function (msg) {
                        var res = eval("(" + msg.d + ")");
                        if (res) {
                            wx.alert.close();
                            wx.alert.text_go_url("成功撤销订单！", window.location.href);
                        }
                        else {
                            wx.alert.close();
                            wx.alert.text_autoclose("撤销订单失败，请刷新页面后重试");
                        }
                    },
                    function () {
                        wx.alert.close();
                        wx.alert.text_autoclose("撤销订单失败，请刷新页面后重试");
                    });
        });

        $('.end-order').click(function () {
            if (!confirm('您已收到货物并确认完成交易，是否确定？')) return;
            var jsonObj = {orderId: $(this).attr('data-orderid')};
            wx.alert.load();
            wx.action.post(
                    "/Service/AjaxService.svc/EndOrder",
                    JSON.stringify(jsonObj),
                    function (msg) {
                        var res = eval("(" + msg.d + ")");
                        if (res) {
                            wx.alert.close();
                            wx.alert.text_go_url("成功完成交易！", window.location.href);
                        }
                        else {
                            wx.alert.close();
                            wx.alert.text_autoclose("完成交易失败，请刷新页面后重试");
                        }
                    },
                    function () {
                        wx.alert.close();
                        wx.alert.text_autoclose("完成交易失败，请刷新页面后重试");
                    });
        });
    });
</script>

<div class="hidHeight"></div>
<div class="navBottom">
    <dl class="clearfix">
        <dd class="navBottomFirst">
            <p class="navBottomItem">
                <!--<a href="stu_profile.html"><img src="/public/stage/mobile/images/tp8lz7ik.png" /></a>-->
                <?php if(defined('USER_ID')): ?><a href="<?php echo U('home/member/index');?>"><img src="<?php echo (HEADIMGURL); ?>" /></a>
                <?php else: ?>
                    <!--用户没设置头像或未获取到用户头像，显示默认头像-->
                    <a href="<?php echo U('home/member/index');?>"><img src="/public/stage/mobile/images/tp8lz7ik.png" /></a><?php endif; ?>
            </p>
        </dd>
        <dd>
            <p class="navBottomItem"><a href="<?php echo U('goods/index');?>">产品购买</a></p>
        </dd>

        <dd>
            <p class="navBottomItem"><a href="<?php echo U('member/index');?>">代理中心</a></p>
        </dd>

        <dd>
            <p class="navBottomItem"><a href="http://mp.weixin.qq.com/s?__biz=MzA4NTU1NjU2NQ==&mid=404012163&idx=1&sn=d0767f8bd157c37f25f63e70f55ff9af&scene=1&srcid=0314PVTY9tnDILY0LJunffqM#rd">客服中心</a></p>
        </dd>
    </dl>
</div>

<script src="/public/stage/mobile/js/WeixinTools.js"></script>
</body>
</html>