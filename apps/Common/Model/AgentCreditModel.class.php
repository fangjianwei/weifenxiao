<?php
/**
 * Created by PhpStorm.
 * User: 91336
 * Date: 2015/5/5
 * Time: 9:49
 */
namespace Common\Model;
class AgentCreditModel extends CommonModel {
    protected $_validate = array(
        array('text', 'require', '{%text_lost}'),
        array('text', '','{%text_is_exist}',self::MUST_VALIDATE,'unique'),
    );

    public function filter($params) {
        $where = array();
        if($params['keywords']) {
            $where['text'] = array('LIKE', "%{$params['keywords']}%");
        }

        return $this->where($where);
    }
}