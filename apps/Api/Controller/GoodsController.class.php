<?php
namespace Api\Controller;
use Think\Controller\RpcController;
use Common\Model\StockLogModel;
class GoodsController extends RpcController{
    /**
     * 库存操作
     * @param json $stock
     * return boolean
     */
    public function stockControl($stock){
        $stock = json_decode($stock, true);
        $success = false;
        $goodsModel = M('Goods');
        $stocklogmodel = new StockLogModel();
        foreach($stock as $val){
            //重新计算占用库存
            $occupy = $goodsModel->where(array('goods_sn' => $val['goods_sn']))->getField('(occupy-shipment)');
            $data = array(
                'goods_stock'   => $val['number'],
                'occupy'        => trim($occupy,'-'),
                'shipment'      => 0,
            );
            $goodsdata = $goodsModel->field('goods_name,goods_stock')->where(array('goods_sn' => $val['goods_sn']))->find();
            $res = $goodsModel->where(array('goods_sn' => $val['goods_sn']))->save($data);
            if($res){
                $updatelog = array(
                    'user_id' => '接口同步',
                    'content' => "修改了商品库存！货号：{$val['goods_sn']}，商品名：{$goodsdata['goods_name']}，原来库存：{$goodsdata['goods_stock']}，更改为：{$val['number']}",
                );
                $stocklogmodel->create($updatelog);
                $stocklogmodel->add();
                $success = true;
            }
        }
        return $success;
    }
}