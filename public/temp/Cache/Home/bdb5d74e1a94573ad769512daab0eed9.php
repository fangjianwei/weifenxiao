<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta name="format-detection" content="telephone=no">
<title>瓷肌抑黑焕白套装</title>
<link rel="apple-touch-icon-precomposed" href="common/images/apple-touch-icon.png">
<link rel="stylesheet" href="/public/stage/mobile/css/detail.css">
<script src="/public/stage/mobile/js/jquery2-1-3.min.js"></script>
</head>
<body>
<div class="main">
	<header class="header">
		<div class="fixed-full">
			<!-- <a class="hd-btn" href="javascript:history.back();">
				<i class="font icon-prev"></i>
			</a> -->
			<div id="detail-thd" class="detail-thd clf js-tabs-hd">
				<span class="cur"><b>详情</b></span>
				<span><b>参数</b></span>
				<span class="no_br"><b>评价</b></span>
			</div>
			<!-- <a  href="/" class="hd-btn">
				<i class="font icon-homePage"></i>
			</a> -->
		</div>
	</header>
	<section class="container">
		<div class="detail-tbd js-tabs">
			<div class="tab1 cur" >
				<div id="banner">
					<img src="<?php echo ($goods["original_img"]); ?>" alt="">
				</div>
				<div class="detail-fav">
					<div class="detail-tz">
						<div class="detail-tzs">
							<strong class="price">￥<?php echo ($goods["market_price"]); ?></strong>&nbsp;
							<!--<b class="favorable">手机专享价</b><del class="org-price">价格￥814元</del><br/>-->
							<h2><?php echo ($goods["goods_name"]); ?></h2>
						</div>
						<!-- <span onclick="good_like(459,this)"><i class="font icon-fav "></i><a>23001</a>人喜欢</span> -->
						<a href="<?php echo U('buy');?>" class="go_carts on_ord" alt=""><i class="font icon-order"></i>立即购买</a>
					</div>
				</div>
        						<!--<div class="youhui">-->
					<!--<span>本期优惠</span>优惠大放送<a href="/act-20160520.html">点击进入优惠专场></a>-->
				<!--</div>-->
        		        		<div style="border-top: 1px solid #e1e1e1;">
					<img src="/public/stage/mobile/images_temp/action.jpg" alt="">
				</div>
				<div class="youhui2 clf">
					<p><b>减</b>在线付减10元 <b>免</b>200包邮<b>!</b>新旧包装均正品</p>
					<span><i class="font icon-roundCheck"></i>官网正品</span>
					<span><i class="font icon-roundCheck"></i>1对1咨询</span>
					<span><i class="font icon-roundCheck"></i>7天退货</span>
					<span><i class="font icon-roundCheck"></i>闪电发货</span>
				</div>

				<!--包含商品模块-->
				$desc



				<div id="RaD" class="detail-box">
					<div class="cj-hd hide">生产研发实力</div>
					<div class="RaD hide">
						<img src="/public/stage/mobile/images_temp/RaD_01.jpg" alt="" />
						<dl>
							<dt>1. 生产合作伙伴——韩国科丝美诗</dt><br />
							<dd>
								<p class="th-p">卓越研发能力</p>
								<p>•21项国际研发专利，1266项韩国政府功效型化妆品许可批文</p>
							</dd>
							<dd>
								<p class="th-p">强大生产实力</p>
								<p>•23年生产研发经验，4大生产基地，辐射亚洲</p>
							</dd>
							<dd>
								<p class="th-p">优异品质认可</p>
								<p>•2项大奖：国内化妆品蓝玫大奖，2012年度品质大奖。</p>
							</dd>
						</dl>
						<img  src="/public/stage/mobile/images_temp/RaD_02.jpg">
						<dl>
							<dt>2.医疗研发背景——韩国4EVER医疗集团</dt>
							<dd>
								<p class="th-p">韩国尖端医疗网络集团</p>
								<p>•韩国20多加分院，200大型阶段协诊系统，连续3年获评顾客满意度大奖</p>
							</dd>
							<dd>
								<p class="th-p">韩国模特协会公证指定医院</p>
								<p>•韩国、日本、印尼等亚洲29个国家模特大赛金牌评委</p>
							</dd>
							<dd>
								<p class="th-p">优秀研发专家团队</p>
								<p>•汇集首尔大学、延世大学专家40余位，2012时事获评最强医疗阵容</p>
							</dd>
							<dd>
								<p class="th-p">一站式医美服务体系</p>
								<p>•6大护理中心，27项特色美容护理，提供一站式医美服务</p>
							</dd>
						</dl>
					</div>
				</div>
			</div>

			<div class="tab2">
								<div class="detail-box2 prod-parms" >
					<dl>
					   						<dt class="detail-hd">套装组成</dt>
						<dd class="clf">
                            							<p><span>清本源五谷亮白精华霜（V1.0）</span>50g<b>1件</b></p>
                            							<p><span>清本源卸妆洗颜乳（V1.0）</span>100g<b>1件</b></p>
                            							<p><span>清本源角质调整膜（V1.0）</span>60g<b>1件</b></p>
                            							<p><span>净白无瑕五谷焕彩面膜（V1.0）</span>200g<b>1件</b></p>
                            							<p><span>清本源五谷亮白肌底液（V1.0）</span>30ml <b>1件</b></p>
                            							<p><span>美主人美白祛斑面膜（V1.0）</span>25g<b>1件</b></p>
                            						</dd>
											</dl>
                    					<dl>
						<dt class="detail-hd">核心功效</dt>
						<dd class="clf">
							<p>针对“黑色素型”肤质特点，以皮肤科医学原理研制产品配方，融入创新馨肤白肌底净透科技，深入肌底击破黑色素，并调整肌肤新陈代谢，将击破的黑色素排出体外。同时为肌肤补充水分和营养，全面改善肌肤暗沉、肤色不均、黑黄等现象，重塑亮白润泽肌肤。</p>
						</dd>
					</dl>
                                        					<dl>
						<dt class="detail-hd">适用肤质</dt>
						<dd class="clf">
							<p>天生肤色黑、受紫外线或电脑辐射过多，肤色暗沉粗糙，急需美白人群。</p>
						</dd>
					</dl>
                                        					<dl>
						<dt class="detail-hd">主要成分</dt>
						<dd class="clf">
							<p>五谷提取物、山茶花、Symwhite馨肤白、光果甘草、牛油果等。</p>
						</dd>
					</dl>
                    				</div>			</div>


			<div class="detail-box tab3" id="comment_detail" >
				<div class="com_tabs">
				    <span class="cur">全部</span>
				    <span >有图（7）</span>
				   <!--  <p class="font icon-pullDown"></p> -->
				</div>
				<p style="text-align:center;padding:1em 0;font-size:1.2em;color:#ff4e00;">暂无评论!</p>
				<ul class="detail-bd experience">
				    <li>
				        <p>杨***n：</p>
				        <p>收到货打开包装看了，赠送了很多礼品，物流非常快，客服非常热情，很好！</p>
				        <img src="/public/stage/mobile/images_temp/459_G_1460326122632.jpg" alt="">
				        <img src="/public/stage/mobile/images_temp/459_G_1460326122632.jpg" alt="">
				        <div class="clf">
				            <span  onclick="do_like(<?php echo ($one_comment["id"]); ?>,this)"><i class="font icon-fav {if isset($like_data.$one_comment.id) && $like_data.$one_comment.id == 1 }on {/if}"></i>有用（<a>285</a>）</span>
				            <b></b>
				        </div>
				        <div class="day_add">
				            <p class="titem">天后追评</p>
				            <p></p>
				        </div>
				    </li>
				</ul>
			</div>

		</div>
                    <script src="/templates/common/script/goods.js?v=2.0"></script>
                    <form action="/tobuy.php" name="orderfrm" id="orderfrm" autocomplete="off" method="post">
					<div class="order-box">
						<div class="cj-hd">在线订购，包邮到你家</div>						<dl class="order-item">
                            <input type="hidden" name="goods_id" value="459">
							<dt><a href="javascript::"><img src="/public/stage/mobile/images_temp/459_G_1460326122632.jpg" alt=""></a></dt>
							<dd>
								<p><?php echo ($goods["goods_name"]); ?></p>
								<strong class="price" id="shop_price" data="<?php echo ($goods["market_price"]); ?>">￥<?php echo ($goods["market_price"]); ?></strong>
								<!--<b class="favorable">手机专享价</b>-->
								<!--<del class="org-price">价格￥814元</del>-->
								<div class="order-qty">
									<span class="s" id="reduce_num">-</span>
									<div><input type="text" name="goods_num" value="1"></div>
									<span class="w" id="add_num">+</span>
								</div>
							</dd>
						</dl>
                                                                        						<div class="order-total">合计：￥<b><?php echo ($goods["market_price"]); ?></b></div>
						<div class="detail-box2">
							<div class="detail-hd">配送信息</div>
							<ul class="order-info">
								<li>
									<i class="font icon-consignee"></i><input type="text" name="userName" value="<?php echo ($address["consignee"]); ?>"  placeholder="收货人">
									<span>*</span>
								</li>
								<li>
									<i class="font icon-phone"></i><input type="text" name="userPhone" value="<?php echo ($address["mobile"]); ?>" placeholder="联系电话(填写后请认真检查)">
                                    <span>*</span>
								</li>
								<li>
									<i class="font icon-address"></i><input type="text" name="txtadd" value="<?php echo ($address["address"]); ?>" placeholder="详细地址">
                                    <span>*</span>
								</li>
								<!--<li>-->
								    <!--<i class="font icon-price-tags"></i><input type="text" placeholder="优惠券码" name="bonus_sn"><span></span>-->
                                <!--</li>-->
							</ul>
							<div class="pay-mode clf payment">
								<p>支付方式</p>
								<div class="pay_chic">
									<label class="checked"><input class="checkbox" type="radio" checked="true" value="4" name="pay">支付宝支付
										<!--<span>减10元</span>-->
									</label>
									<label><input class="checkbox" type="radio" value="7" name="pay">微信支付
										<!--<span>减10元</span>-->
									</label>
									<label><input class="checkbox" type="radio" value="8" name="pay">网银支付
										<!--<span>减10元</span>-->
									</label>

									<label><input class="checkbox" type="radio" value="1" name="pay">货到付款</label>
								</div>
                                <input type="hidden" name="act" value="done">
                                 <script>document.write('<script src="http:\/\/'+window.location.host+'\/get_prevent_refresh.php?t='+Math.random()+'"><\/script>');</script>
                                <input type="hidden" name="parent_url" value="">
							</div>
						</div>
						<div class="pay-submit">
							<p>总计  ￥<b class="total_price" id="originalPrice"><?php echo ($goods["market_price"]); ?>
							</b>
								<!--/  优惠<span class="hl">-￥<b id="preferentialPrice">10.00</b>-->
								</span>  /  运费￥<b id="shipping_fee">0.00</b></p>
							<strong class="price">实付：￥<b id="need_pay"><?php echo ($goods["market_price"]); ?></b></strong>
							<a class="btn" href="javascript:void(0);" onclick="return checkForm();">确 定 提 交</a>
						</div>						
					</div>
                    </form>
                    <script>
                    $(function(){
							$("#add_num").on('click', function () {
								var goods_num = $(":input[name=goods_num]").val()
								$(":input[name=goods_num]").val(parseInt(goods_num)+1)
							})

							$("#reduce_num").on('click', function () {
								var goods_num = $(":input[name=goods_num]").val()
								if(goods_num>0){
									$(":input[name=goods_num]").val(parseInt(goods_num)-1)
								}

							})
//                        $('.icon-roundClose').on('click', function(){
//                            $(this).parent().html('');
//                        });
//                        var span_in =$('.order-info li span').html();
//    					var $username_input = $("input[name=userName]");
//    					var $userPhone_input = $("input[name=userPhone]");
//    					var $address_input = $("input[name=txtadd]");
//                        $('.order-info li span').click(function(){
//                        	var noNum = $(this).parent('li').index();
//                            $(this).html(span_in);
//                            $(this).parent('li').removeClass('cur');
//                            $('.order-info02 li').eq(noNum).children('span').html(span_in);
//                            $('.order-info02 li').eq(noNum).removeClass('cur');
//                            $username_input.attr('placeholder','收货人');
//                            $userPhone_input.attr('placeholder','联系电话(填写后请认真检查)');
//                            $address_input.attr('placeholder','详细地址');
//                            $(this).siblings('input').focus();
//            				$(this).siblings('span').html('*');
//            				$(this).parent('li').removeClass('cur');
//                        });
                        // $(".js-tabs-hd").children().click(function(){
                        // 	$(this).addClass("cur").siblings().removeClass("cur");
                        // 	$(this).closest(".js-tabs").find(".js-tabs-bd").children().hide().eq($(this).index()).show();
                        // });
//                        $('a').click(function(){
//                                 if($('input[name="userName"]').val() != '' || $('input[name="userPhone"]').val() != '' || $('input[name="txtadd"]').val() != '' ){
//                                     if($(this).parent().hasClass('logo') && $(this).parent().parent().hasClass('fixed-full') ){
//                                       return true;
//                                     }
//                                     if($(this).hasClass('btn') && $(this).parent().hasClass('pay-submit')){
//                                       return true;
//                                     }
//                                     if($(this).parent().attr('id')=='detail-thd' && $(this).parent().hasClass('detail-thd') && $(this).parent().hasClass('clf') && $(this).parent().hasClass('clf') && $(this).parent().hasClass('width-full')){
//                                        return true;
//                                      }
//                                     if($(this).attr('href') == '#orderfrm'){
//                                        return true;
//                                     }
//                                     if($(this).attr('href').match('tel:') == 'tel:'){
//                                        return true;
//                                     }
//                                     if($(this).hasClass('btn-praise')){
//                                        return true;
//                                     }
//                                     // return confirm('确定要放弃提交订单,前往其他页面吗?');
//                                 }
//                        });
                    });

						function checkForm(){
							var goods_num = $(":input[name=goods_num]").val()
							if(goods_num<1){
								alert("商品数量不能少于一个");
								return ;
							}
						}

                    </script>
<footer class="footer">
	<a class="footer-tel" href="tel:4001020398" onclick="Call_Buttom()">客服热线 400-1020-398<b></b></a>
	<p>ICP备案号：赣ICP备12007816号-23</p>
	<p><a href="javascript:;" style="color:#a9a9a9;" >版权所有：江西瓷肌电子商务有限公司</a></p>
</footer>
	</section>
    <script>
    //详情页头部选项卡
    $(function(){
    	$(".js-tabs-hd span").click(function(){
	    	$(this).addClass("cur").siblings().removeClass("cur");
	    	$(this).parent().parent().parent().siblings('.container').children(".js-tabs").children().eq($(this).index()).addClass("cur").siblings().removeClass("cur");
	    });
    })
//    function addOne(obj){
	//        $(obj).parent().find('input[type="text"]').val();
	//    }
	//    $.post('/comments.php?act=show_comment','comment_goods=459',function(data){
	//        $('#comment_detail').html(data);
	//    });
    //获取评论标签数据
//	function getType(type_id){
//		$.post('/comments.php?act=show_comment',{comment:'comment',type_id:type_id,comment_goods:459},function(msg){
//			$('#comment_detail').html(msg);
//			$('#'+type_id).addClass('cur').siblings().removeClass('cur'); //标签高亮样式
//		});
//	}
	//评论点赞js
//	function good_like(id,obj){
//	    $.post('/act_goods.php?act=like_goods','id='+id,function(data){
//			if(data.msg == 'too_many_click'){
//			   	alert('您对些产品操作太频繁了');
//			   	return;
//			}
//	       	if(data.msg == 'do_like'){
//	           	$(obj).children('i').addClass('on');
//	           	$(obj).children('a').html(parseInt($(obj).children('a').html())+1);
//	        }
//	       	if(data.msg == 'dis_like'){
//	           	$(obj).children('i').removeClass('on');
//	           	$(obj).children('a').html(parseInt($(obj).children('a').html())-1);
//	       	}
//	    },'json');
//	}
    </script>
</div>
</body>
</html>