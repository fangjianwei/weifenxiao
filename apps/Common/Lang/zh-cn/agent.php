<?php
return array(
    'contact_lost'      => '请填写联系人',
    'sex_lost'          => '请选择性别',
    'mobile_lost'       => '请填写手机号码',
    'mobile_error'      => '手机号码不正确',
    'wecha_name_lost'   => '请填写微信号',
    'email_lost'        => '请填写邮箱地址',
    'email_error'       => '邮箱格式不正确',
    'province_lost'     => '请选择省份',
    'city_lost'         => '请选择市区',
    'district_lost'     => '请选择区域',
    'address_lost'      => '请填写详细地址',
    'grade_id_lost'     => '请选择需要申请的代理级别',
    'advantage_lost'    => '请填写自身优势',
    'mobile_exists'     => '手机号码已存在',
    'wecha_name_exists' => '微信号已存在',
    'email_exists'      => '邮箱地址已存在',
    'SAVE_AGENT_SUCCESS'=> '提交成功！微商专员将会主动与您联系，请保持手机畅通！',
    'not_allow_save'    => '已注销代理不允许修改信息',
    'id_number_exists'  => '身份证号码已存在',
    'account_not_existence'=> '付款账户不存在',
    'account_locked'    => '付款账户已被停用',
    'account_not_select'=> '请选择付款账户',

    'agent_code_lost'   => '授权编码错误',

    'process_nothing_lost'      => '存在未完成的流程，不能重新申请',


    'agent_apply'       => '代理商申请',
    'agent_remove'      => '代理商注销',
    'agent_update'      => '代理商资料修改',
    'agent_upgrade'     => '代理商升级',

    'option_grade'      => '请选择升级的等级',
    'id_number_error'   => '请输入正确的身份证号码',
    'header_path_error' => '请上传手持身份证头部照',




    'yes'               => '审核通过',
    'no'                => '拒绝',

    'update_error_lost' => '数据没有更新',
    'update_fall_lost'  => '数据更新失败',
    'upgrade_fall_lost' => '升级失败',


    'MEMBER_GRADE'              => array(
        'GRADE_NAME'            => '等级名称',
        'ORDERBY'               => '等级排序',
        'CONSUME_MAX'           => '消费上限',
        'CONSUME_MIN'           => '消费下限'
    ),
    'AGENT_TITLE'               => array(
        0                       =>'ID',
        1                       =>'联系人',
        2                       =>'性别',
        3                       =>'微信号',
        4                       =>'电子邮箱',
        5                       =>'手机号码',
        6                       =>'申请代理级别',
        7                       =>'省',
        8                       =>'市',
        /* 8                       =>'区',*/
        9                      	=>'详细地址',
        10                      =>'申请时间',
        11                      =>'自身优势',
        12						=> '状态',
    ),
    'add_agent'					=> '添加代理商',
    'edit_agent'				=> '编辑代理商',
    'delete_agent'				=> '删除代理商',

    //代理商状态
    'agent_type'              => array(
        AG_NEW                  => '意向代理',
        AG_AUTH                 => '已授权代理',
        AG_CANCEL               => '已注销代理',
    ),
    'data_from'                 => array(
        0                       => '注册',
        1                       => '咨询'
    ),

    //提交申请
    'apply_error'               => array(
        '1'                     => '未选择申请的代理商等级',
        '2'                     => '未填写代理商信息',
        '3'                     => '未绑定代理商微信号',
    ),


    'agent_error'               => '你的代理商状态%s,不能执行注销',
    'detail_exist'              => '存在未处理流程，不能执行注销',
    'operating_success'         => '操作成功',


    'transfer_grade_level'      => '转移的代理商等级不能比该代理商低级和同级',
    'transfer_parent_same'      => '该代理商已在此团队中。',
    'transfer_parent_agent'     => '转移的团队代理商未授权或已注销',
    'transfer_agent_note'       => '代理商：%s 转移到代理商： %s 下级',

    'allocation_master_exist'   => '该员工不存在',
    'allocation_all_agent'      => '所选代理商都在此员工管理，无需分配',
    'allocation_agent_note'     => '分配给: %s ',


    'agent_info'                => '代理商资料',
    'agent_allnext'             => '下级代理',
);