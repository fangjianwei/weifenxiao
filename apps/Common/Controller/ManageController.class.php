<?php
/**
 * ====================================
 * 公共处理类
 * ====================================
 * Author: Hugo
 * Date: 14-5-20 下午9:58
 * ====================================
 * File: SystemController.class.php
 * ====================================
 */
namespace Common\Controller;
use Common\Library\Common;
use Common\Library\Config;
use Common\Library\Manage\Master;
use Common\Model\MenuModel;
use Think\Controller;

abstract class ManageController extends Controller
{
    protected $dbModel;//当前数据模型
    protected $allowAction = array(); //允许不验证权限的操作
    protected $template = ''; //当前操作模板
    protected $dbTable = ''; //设置当前操作表

    protected function _initialize() {
        Config::init(); //初始化系统配置
        $this->dbModel = D(empty($this->dbTable) ? CONTROLLER_NAME : $this->dbTable);
        //TODO 验证权限
        if(login('user_id')) {
            if(false == $this->access()) $this->error(L('_NOT_ACCESS_'));

            defined('USER_ID') or define('USER_ID', login('user_id'));
            defined('USER_NAME') or define('USER_NAME', login('user_name'));
            defined('REAL_NAME') or define('REAL_NAME', login('real_name'));
        } else {
            if(IS_AJAX && IS_POST) {
                $this->error('已掉线，请重新登录');
            }else {
                echo '<script>parent.window.location.href="' . U('passport/index') . '"</script>';
                exit;
            }
        }
    }

    /**
     * 验证操作权限
     * 设计思路:
     * 1、每个控制器中都可以设置不需要验证的操作;
     * 2、带get的方法不需要验证
     * 3、如果当前菜单的ID在管理员所拥有的菜单ID中则验证通过;
     * @return bool
     */
    final protected function access($action = ACTION_NAME) {
        //如果是超级管理员不需要验证
        if(login('is_admin')) return true;

        $action = strtolower($action);
        $controller = strtolower(CONTROLLER_NAME);
        //如果在允许访问列表内，不需要验证权限
        if(!empty($this->allowAction) && ($this->allowAction == '*' || in_array($action, $this->allowAction))) return true;
        //如果未分配菜单，则不允许访问
        if(is_null(login('menu'))) return false;
        //验证当前操作是否有权限
        return power(array($controller.'-'.$action, $controller.'-*'));
    }

    /**
     * 查列表
     */
    public function index() {
        if(IS_AJAX) {
            $params = I('request.');
            //先判断Model层是否存在
            if(method_exists($this->dbModel, 'lists')) {
                if(method_exists($this->dbModel, 'filter')){
                    $this->dbModel->filter($params);
                }
                $data = $this->dbModel->lists($params);
                if(method_exists($this->dbModel, 'format')) {
                    $data = $this->dbModel->format($data);
                }
            }
            //判断公共方法lists是否存在
            elseif(method_exists($this, 'lists')) {
                $data = $this->lists($params);
            }
            //默认输出树结构
            else {
                if($params) {
                    $fields = $this->dbModel->getDbFields();
                    $where = array();
                    foreach($params as $key => $val) {
                        if(in_array($key, $fields)) {
                            if(is_numeric($val)) {
                                $where[$key] = $val;
                            }else{
                                $where[$key] = array('like', "%$val%");
                            }
                        }
                    }
                    $this->dbModel->where($where);
                }
                $data = $this->dbModel->select();
                Common::buildTree($data, $params['selected'], $params['type']);
            }
//            echo $this->dbModel->getLastSql();
//            exit;
            if(I('get.test') == 'test'){
                dump($this->dbModel->getLastSql());exit;
            }
            $this->ajaxReturn($data);
            exit;
        }
        $this->display($this->template);
    }

    /**
     * 添加或更改记录
     */
    public function save() {
        $params = I('post.');
        $params['save_type'] or $this->error(L('SAVE_TYPE_LOST'));
        $data = $this->dbModel->create($params) or $this->error($this->dbModel->getError());
        $result = false;
        $pk = $this->dbModel->getPk();
        switch($params['save_type']) {
            case 'insert':
                $result = $this->dbModel->add();
                $params[$pk] = $this->dbModel->getLastInsID();
                break;
            case 'update':
                $data[$pk] or $this->error(L('PRIMARY_KEY_LOST'));
                $result = $this->dbModel->save();
                break;
        }
        if($result) {
            //调用保存后需要处理的方法
            if(method_exists($this, '_after_save')) {
                $this->_after_save($params);
            }
            $this->success(L('save_success'));
        }else {
            $this->error(L('save_error'));
        }
    }

    /**
     * 删除记录
     * @param $itemid
     */
    public function delete() {
        $itemId = I('request.itemid');
        $itemId or $this->error(L('item_lost'));
        $where = array();
        $pk = $this->dbModel->getPk();
        if(strpos($itemId, ',') !== false) {
            $where[$pk] = array('IN', $itemId);
        }else {
            $where[$pk] = $itemId;
        }
        if($this->dbModel->where($where)->delete()) {
            if(method_exists($this, '_after_delete')) {
                $this->_after_delete($itemId);
            }
            $this->success(L('drop_success'));
        }else {
            $this->error(L('drop_error'));
        }
    }

    //编辑或添加表单页面
    public function form() {
        $pk = $this->dbModel->getPk();
        $params = I("request.");
        if(isset($params[$pk])) {
            if(method_exists($this->dbModel, 'getInfo')) {
                $this->info = $this->dbModel->getInfo((int)$params[$pk]);
            }else{
                $this->info = $this->dbModel->find((int)$params[$pk]);
            }
            $this->json_info = empty($this->info) ? null : json_encode($this->info);
        }
        $this->display();
    }

    //设置状态
    public function lock() {
        $items = I('post.items');
        $locked = I('post.locked');
        $where = array(
            $this->dbModel->getPk() => array('in', $items)
        );
        if($this->dbModel->where($where)->setField('locked', $locked)) {
            if(method_exists($this, '_after_lock')) {
                $this->_after_lock($items);
            }
            $this->success();
        }else {
            $this->error(L('save_error'));
        }
    }
    
    /**
     * 防止越级访问
     * @params1 array $master_id 要验证的管理员
     * @params2 string $module 模块名
     * @params3 string $action 方法名
     */
    public function modulepri($master_id, $module = '', $action = 'index' ){
        if($module == 'order'){
            $is_mass = D('OrderInfo')->where(array('master_id' => $master_id))->getField('is_mass');
            if($is_mass == 1) return true;
        }
        if(login('is_admin')) return true;
        $master = D('Master')->field('role_id')->where(array('user_id' => USER_ID))->find();
        $data = M('RoleMenu')
        ->join('AS rm LEFT JOIN biz_menu AS m on rm.menu_id=m.id')
        ->where("rm.role_id={$master['role_id']} and pid > 0")->select();
        $newdata = array();
        foreach($data as $key => $val){
            $newdata[$key] = strtolower($val['module'].'-'.$val['action']);
        }
        if(in_array(strtolower($module.'-'.$action),$newdata)) return true;
        //权限验证
        if($master_id != USER_ID) $this->error('链接地址错误');
    }
    
    
    
    
    
}