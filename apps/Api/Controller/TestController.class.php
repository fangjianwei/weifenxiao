<?php
namespace Api\Controller;
use Think\Controller;
use Common\Model\GoodsModel;
class TestController extends Controller{
    public function test(){
        vendor('phpRPC.phprpc_client');
        $client = new \PHPRPC_Client('http://l.ciji.com/api/order');
        $result = $client->clientLogin('bizEQRG1LC37DM9TATC','BUFBAJWBWZ1TINRQOU1O9VIK9AQO3QV7LFZ');
        $data = $client->getShippingOrder(0,20);
        echo "<pre/>";
        dump($data);
    }

    public function importgoods(){
        header('Content-type:text/html;charset=utf-8');
        $goods = D('Goods')->where(array('is_package' => 0,'cat_id' => 1))->select();
        //获取代理等级
        $agent_grade = D('AgentGrade')->order('grade_id desc')->select();
        $data = array();
        $data['title'] = array(
            '商品编码',
            '商品名称',
            '零售价',
        );
        foreach($agent_grade as $key => $val){
            if($val['grade_name'] == '取消授权') continue;
            array_push($data['title'],$val['grade_name']);
        }
        $GoodsModel = new GoodsModel();
       foreach($goods as $val){
           $v['goods_sn'] = trim($val['goods_sn']);
           $v['goods_name'] = trim($val['goods_name']);
           $v['market_price'] = trim($val['market_price']);
           foreach($agent_grade as $grade){
               if($grade['grade_name'] == '取消授权') continue;
                $v[$grade['grade_id']] = $GoodsModel->getPrice($val['goods_id'],$grade['grade_id'],$val['shop_price']);
           }
           $data['data'][] = $v;
       }
        export_data($data);
    }
}