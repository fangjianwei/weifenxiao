<?php
/**
 * Created by PhpStorm.
 * User: 9009078
 * Date: 2016/5/19
 * Time: 11:39
 */
namespace Common\Library\Sms;

class CodeSms
{


    /**
     * 发送短信
     * @param $mobile               手机号
     * @return array                返回值Array ( [errno] => 0 [msg] => 发送成功)  errno=0为成功，非0则失败
     */

    public static function send_sms($mobile,$content){
        $client = new \SoapClient(C('CODESMS_URL'));
        $parameters=array(
            'in0' => C('CODESMS_USER'),   //用户名
            'in1' => C('CODESMS_PASS'),   //密码
            'in2' => C('CODESMS_PROXY'),  //代理商编号
            'in3' => $mobile,             //手机号码
            'in4' => $content, //短信内容
            'in5' => '',                  //扩展号
            'in6' => '',                  //序列号
        );
        $result = $client->sendNormMsg($parameters);
        $data = $result->out;
        if($data == 1){
            $arr['errno'] = 0;
            $arr['msg'] = '发送成功';
        }else{
            $data = explode(':',$data);
            $arr['errno'] = $data[0];
            $arr['msg']   = $data[1];
        }
        return $arr;
    }

}