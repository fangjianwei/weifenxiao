<?php
/**
 * ====================================
 * 角色管理
 * ====================================
 * Author: Hugo
 * Date: 14-5-20 下午9:58
 * ====================================
 * File: RoleController.class.php
 * ====================================
 */
namespace Manage\Controller;
use Common\Controller\ManageController;

class RoleController extends ManageController {
    protected $allowAction = array('index');

    public function power() {
        if(IS_AJAX && IS_POST) {
            $params = I('post.');
            $this->dbModel->setMenu((int)$params['role_id'], $params['menu_list']);
            $this->success(L('save_success'));
            exit;
        }
        $role_id = intval(I('role_id'));
        $this->info = $this->dbModel->getInfo($role_id);
        $this->display();
    }
    /**
     * 删除角色后，
     */
    public function delete(){
        $items = explode(',', I('request.items'));
        foreach($items as $itemid){
            if($this->dbModel->where("pid = '%d'", $itemid)->count() > 0){
                $message = L('delete_error');
            }else{
                $this->dbModel->delete($itemid);
            }
        }
        if(empty($message)){
            $this->success();
        }else{
            $this->error($message);
        }
    }

    //获取渠道审核流程
    public function getflow(){
        $data = C('CHANNEL_FLOW');
        $newdata = array();
        foreach($data as $key => $val){
            $new['id']   = $key;
            $new['text'] = $val;
            $newdata[] = $new;
        }
        $this->ajaxReturn($newdata);
    }
}