<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title><?php echo (L("meta_title")); ?></title>
    <link rel="stylesheet" type="text/css" href="/public/cpanel/css/login.css" />
    <script type="text/javascript" src="/public/static/base.js"></script>
    <script type="text/javascript" src="/public/cpanel/js/easyui.js"></script>
</head>
<body>
<div class="login-panel">
    <div class="login-logo"></div>
    <form id="login-form" method="post" data-url="<?php echo U('login');?>">
        <div class="login-label">
            <input class="easyui-textbox" name="login_name" style="width:100%; height:40px;padding:12px" data-options="prompt:'请输入用户名', required:true, missingMessage: '请输入用户名', iconCls:'icon-man',iconWidth:38">
        </div>
        <div class="login-label">
            <input class="easyui-textbox" type="password" id="login_password" name="login_password" style="width:100%;height:40px;padding:12px" data-options="prompt:'请输入登录密码', required:true, missingMessage: '请输入登录密码',iconCls:'icon-lock',iconWidth:38">
        </div>
        <div class="login-label">
            <a class="easyui-linkbutton" style="width:100%; height: 40px;" onclick="userLogin()">
                <span style="font-size:14px;">登录</span>
            </a>
        </div>
    </form>
</div>
<script type="text/javascript">
    $(function(){
        $('#login-form').keydown(function(e){
            if(e.keyCode=='13'){
                if(e.target.type='password'){
                    userLogin();
                }
            }
        })
    })
    function userLogin(){
        var formObj = $('#login-form');
        easyui.submit(formObj, function(result){
            if(!result.status){
                alert(result.info);
                return false;
            }
            window.location.href = '<?php echo U("/manage");?>';
        });
        return false;
    }
</script>
</body>
</html>