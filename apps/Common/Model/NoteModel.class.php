<?php
/**
 * 留言模型
 * User: 91336
 * Date: 2015/3/9
 * Time: 17:12
 */
namespace Common\Model;
use Common\Library\Common;

class NoteModel extends CommonModel {
	
	public function filter($params)
	{
		$where = array();
		!empty($params['begindate']) && $params['begindate'] = substr($params['begindate'], 0,10).' 00:00:00';
		!empty($params['enddate']) && $params['enddate'] = substr($params['enddate'], 0, 10).' 23:59:59';
		if (!empty($params['begindate']) && !empty($params['enddate'])){
			$where['create_time'] = array('between',$params['begindate'].','.$params['enddate']);
		} else {
			!empty($params['begindate']) && $where['create_time'] = array('egt',$params['begindate']);
			!empty($params['enddate']) && $where['create_time'] = array('elt',$params['enddate']);
		}
		!empty($params['mobile']) && $where['mobile'] = array('eq',$params['mobile']);

		$this->where($where);
		return $where;
	}

	public function format($data) {
		foreach ($data['rows'] as $key => $row) {
            if($row['mobile']) $row['mobile'] = Common::isMobile($row['mobile']) ? $row['mobile']: decrypt_phone($row['mobile']);
			$data['rows'][$key] = $row;
		}
		return $data;
	}
}