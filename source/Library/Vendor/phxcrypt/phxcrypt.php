<?php

class phxCrypt
{
	protected $phxMod = "ecb";// 电码本模式, 不需要iv,此项不能修改

	public function __construct()
	{
        $phx_key = array("phxAlg"=>"twofish","phxKey"=>"8031e6ac94b4a75db83c85c23e209143","cut_length"=>5);
		$this->phx_key = $phx_key;		
	}
	protected function hex2bin($data)
	{
		$len = strlen($data);
		return pack("H".$len, $data);
	}
	//加密函数
	public function phxEncrypt($plaintext, $iscut=false,$no_encrypt=false)
	{
		$plaintext = trim($plaintext);
		if(empty($plaintext)) return false;
		//判断是否已加密连续达23位字符以上内容并且不是纯数字，不进行加密
		$reg = "|^[0-9a-zA-Z]{23,}$|i";							
		if (preg_match($reg, $plaintext) == 0 && strpos('A'.$plaintext,'±') == 0)
		{				
			if (function_exists('phxencrypt')) return phxencrypt($plaintext);//使用新的加密方式
		}		
		
		if (strpos('A'.$plaintext,'±') == 1)	return $plaintext;//如开头带此符号的不执行加密
		$str = '';
		if ($iscut)
		{
			$cut_str = $this->back_cut_str($plaintext,$this->phx_key["cut_length"]);//截取后面指定长度内容值
			$str = str_replace($cut_str,'',$plaintext).'±';
			$plaintext = $cut_str;
		}
		$td = @mcrypt_module_open($this->phx_key["phxAlg"], '', $this->phxMod, '');
		$iv = @mcrypt_create_iv (mcrypt_enc_get_iv_size($td), MCRYPT_RAND);
		@mcrypt_generic_init($td, $this->phx_key["phxKey"], $iv);
		$cipher = @mcrypt_generic($td, $plaintext);
		@mcrypt_generic_deinit($td);
		@mcrypt_module_close($td);
		$hexCipher = bin2hex($cipher);

		return '±'.$str.$hexCipher;
	}
	//解密函数
	protected function phxDecrypt($hexCipher)
	{
		if (empty($hexCipher)) return $hexCipher;
		$cipher=$this->hex2bin($hexCipher);
		$plaintext=@mcrypt_decrypt($this->phx_key["phxAlg"],$this->phx_key["phxKey"],$cipher,$this->phxMod);
		return trim($plaintext);
	}

	/*
	从后面起截取汉字函数，只支持Utf-8
	back_cut_str(字符串, 截取长度);
	开始长度默认为 0
	*/
	protected function back_cut_str($string,$sublen=5)
	{
		$sublen = intval($sublen);
		$string= str_replace('&nbsp;','',$string);
		$string = preg_replace( "@<(.*?)>@is", "", $string );
		$string= str_replace('  ','',$string);
		$pa="/[\x01-\x7f]|[\xc2-\xdf][\x80-\xbf]|\xe0[\xa0-\xbf][\x80-\xbf]|[\xe1-\xef][\x80-\xbf][\x80-\xbf]|\xf0[\x90-\xbf][\x80-\xbf][\x80-\xbf]|[\xf1-\xf7][\x80-\xbf][\x80-\xbf][\x80-\xbf]/";
		preg_match_all($pa,$string,$t_string);
		return join('',array_slice($t_string[0], $sublen * -1));
	}
	//判断是否加密
	public function isEncrypted($str)
	{
		if ( is_numeric($str) && strlen($str) < 32) return false;
		$reg = "|^[0-9a-fA-F]{8,}$|i";
		$ret = preg_match($reg, $str);

		return $ret;
	}
	

	//判断字符串是否已加密，如加密执行解密操作is_substr判断是否前台加密
	public function isDecrypted($str,$is_substr = false)
	{
		if ($this->isEncrypted($str))
		{
			if (function_exists('phxdecrypt'))
			{
				return phxdecrypt($str);//解密后输出
			}			
		}
		
		
		$not_str = '';//不需要解密的内容			
		$str_array = explode('±',$str);		
		
		$decrypt_str = $str_array[1];//需要解密的内容	
		if ($str_array[2])//此项不为空则为只加密内容部分,需重新定义
		{
			$not_str = $str_array[1];
			$decrypt_str = $str_array[2];
		}
		
		if ($this->isEncrypted($decrypt_str))
		{
			if ($is_substr)
			{
				$decrypt_str = substr($decrypt_str,0,-2);
			}
			return $not_str . $this->phxDecrypt($decrypt_str);//解密后输出
		}
		return $str;
	}
}
?>