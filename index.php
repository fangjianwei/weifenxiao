<?php

// 应用入口文件

// 应用入口文件
ini_set("display_errors", "On");
error_reporting(E_ALL | E_STRICT);

// 检测PHP环境

if(version_compare(PHP_VERSION,'5.3.0','<'))  die('require PHP > 5.3.0 !');

define('APP_ROOT', str_replace('\\', '/', dirname(__FILE__)) . '/');// 设置项目根目�?

define('APP_PATH', APP_ROOT . 'apps/');// 定义应用目录

define('APP_DEBUG', true);// 开启开发者模�?

define('RUNTIME_PATH', APP_ROOT . 'public/temp/'); //设置临时文件路径

define('THINK_PATH', APP_ROOT .'source/');// 绑定Thinkphp目录

define('_PHP_FILE_',    rtrim($_SERVER['SCRIPT_NAME'],'/'));// 定义当前文件名，以防fcgi模式下发生错�?

require THINK_PATH . 'ThinkPHP.php'; // 引入ThinkPHP入口文件