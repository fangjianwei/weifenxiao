<?php
/**
 * ====================================
 * 后台代理商管理类
 * ====================================
 * Author: lwh
 * Date: 14-11-21 上午9:28
 * ====================================
 * File: AgentController.class.php
 * ====================================
 */
namespace Manage\Controller;

use Common\Controller\ManageController;
use Common\Library\Manage\Master;
use Common\Model\AgentCreditModel;
use Common\Model\AgentModel;
use Common\Model\MasterModel;
use Common\Model\AgentGradeModel;
use Common\Model\MemberModel;
use Common\Library\Common;


class AgentController extends ManageController{
    protected $info;
    protected $allowAction = array('show_qrcode', 'show_team');

    public function index(){
        if(IS_POST) {
            $params = I('request.', '', 'trim');
            $params['agent_type'] = $params['agent_type'] > -1 ? $params['agent_type'] : array(AG_AUTH, AG_CANCEL);
            $this->dbModel->filter($params);
            $data = $this->dbModel->lists($params);
            $data = $this->dbModel->format($data);
            $this->ajaxReturn($data);
            exit;
        }
        $gradeModel = new AgentGradeModel();
        $grade_lists = $gradeModel->field('grade_id, grade_name')->order('orderby')->select();
        $this->assign('grade_lists', $grade_lists);
        $this->display();
    }

    public function newlist(){
        if(IS_POST) {
            $params = I('request.', '', 'trim');
            $params['agent_type'] = AG_NEW;
            $this->dbModel->filter($params);
            $data = $this->dbModel->lists($params);
            $data = $this->dbModel->format($data);
            $this->ajaxReturn($data);
            exit;
        }
        $gradeModel = new AgentGradeModel();
        $grade_lists = $gradeModel->field('grade_id, grade_name')->order('orderby')->select();
        $this->assign('grade_lists', $grade_lists);
        $this->display();
    }

    public function newalone(){
        if(IS_POST) {
            $params = I('request.', '', 'trim');
            $params['agent_type'] = AG_NEW;
            $params['alone'] = true;
            $this->dbModel->filter($params);
            $data = $this->dbModel->lists($params);
            $data = $this->dbModel->format($data);
            $this->ajaxReturn($data);
            exit;
        }
        $gradeModel = new AgentGradeModel();
        $grade_lists = $gradeModel->field('grade_id, grade_name')->order('orderby')->select();
        $this->assign('grade_lists', $grade_lists);
        $this->display();
    }

    //查看全部代理
    public function alone(){
        if(IS_POST) {
            $params = I('request.', '', 'trim');
            $params['alone'] = true;
            $this->dbModel->filter($params);
            $data = $this->dbModel->lists($params);
            $data = $this->dbModel->format($data);
            $this->ajaxReturn($data);
            exit;
        }
        $gradeModel = new AgentGradeModel();
        $grade_lists = $gradeModel->field('grade_id, grade_name')->order('orderby')->select();
        $this->assign('grade_lists', $grade_lists);
        $this->display();
    }

    public function add() {
        if(IS_POST) {
            $params = I('post.', '', 'trim');
            $member = array(
                'email' => $params['email'],
                'mobile' => $params['mobile'],
                'user_name' => $params['contact'],
                'sex' => $params['sex'],
                'password' => generate_code(8)
            );
            $memberModel = new MemberModel();
            if($memberModel->create($member)) {
                $user_id = $memberModel->add();

                $agent = array(
                    'email' => $params['email'],
                    'mobile' => $params['mobile'],
                    'contact' => $params['contact'],
                    'sex' => $params['sex'],
                    'wecha_name' => $params['wecha_name'],
                    'grade_id' => $params['grade_id'],
                    'data_from' => 1,
                    'user_id' => $user_id,
                    'add_master' => USER_ID,
                    'user_no' => get_user_no()
                );
                if($this->dbModel->create($agent)) {
                    $this->dbModel->add();
                    $this->dbModel->addAgentLog($user_id, USER_ID, '添加意向资料');
                }else {
                    $memberModel->delete($user_id);
                    $this->error($this->dbModel->getError());
                }
            }else {
                $this->error($memberModel->getError());
            }
            $this->success(L('SAVE_SUCCESS'));
            exit;
        }
        $gradeModel = new AgentGradeModel();
        $grade_lists = $gradeModel->field('grade_id, grade_name')->order('orderby')->select();
        $this->assign('grade_lists', $grade_lists);
        $this->display();
    }

    public function save() {
        $params = I('post.','','trim');
        $user_id = (int)$params['user_id'];
        $agent = $this->dbModel->find($user_id);
        //权限验证
        $this->modulepri($agent['follow_master'], 'agent');
        if(!$agent) $this->error('代理不存在！');
        if (!empty($params['agent_code']) && $agent['agent_type'] == AG_NEW) {
            $params['agent_type'] = AG_AUTH;
        }
        //判断用户填写授权时间字段是否为空
        if(empty($params['startdate'])){
            unset($params['startdate']); //删除字段
        }
        if(empty($params['enddate'])){
            unset($params['enddate']); //删除字段
        }
        $params['buy_cat_id'] = join(',', $params['buy_cat_id']);
        $parent = array();
        //如果等级改变后，清除下级代理与上级代理
        if($params['grade_id'] != $agent['grade_id'] && $agent['parent_id'] > 0) {
            $data['parent_id'] = 0;
            $parent = $this->dbModel->find($agent['parent_id']);
        }
        $data = $this->dbModel->create($params) or $this->error($this->dbModel->getError());
        $result = $this->dbModel->save();
        if($result) {
            //调用保存后需要处理的方法
            $this->dbModel->addAgentLog($params['user_id'], USER_ID, L('agent_update'));
            if($parent) {
                $this->dbModel->addAgentLog($params['user_id'], USER_ID, '等级改变，清除上级团队关系，原上级代理编号：' . $parent['user_no']);
            }
            //清除下级代理
            if($params['grade_id'] != $agent['grade_id']) {
                $children = $this->dbModel->where("parent_id = '%d'", $user_id)->select();
                if ($children) {
                    foreach ($children as $key => $row) {
                        $this->dbModel->addAgentLog($row['user_id'], USER_ID, '上级代理等级改变，清除团队关系，原上级代理编号：' . $parent['user_no']);
                    }
                    $this->dbModel->where("parent_id = '%d'", $user_id)->save(array('parent_id' => 0));
                }
            }
            if(empty($params['warrant_thumb'])){
                if($warrant_thumb=A('img')->water($params['user_id'])){
                    M('agent')->where(array('user_id'=>$params['user_id']))->save(array('warrant_thumb'=>$warrant_thumb));
                }
            }
            if(empty($params['agent_code'])){
                $agent_code = $this->dbModel->createCode($user_id);
                $this->dbModel->save(array('user_id' => $params['user_id'],'agent_code' => $agent_code));
            }
            $this->success(L('save_success'));
        }else {
            $this->error(L('save_error'));
        }
    }


    /**
     * 代理转化
     */
    public function turn() {
        $gradeModel = new AgentGradeModel();
        $params = I('request.','','trim');
        $user_id = (int)$params['user_id'];
        $agent = $this->dbModel->find($user_id);
        //权限验证
        $this->modulepri($agent['follow_master'], 'agent');
        if($agent['agent_type'] != AG_NEW) $this->error('该资料无法进行转化，原因可能是该资料已转化或已取消！');
        if(IS_POST) {
            if(empty($params['agent_code'])) {
                $params['agent_code'] = $this->dbModel->createCode($user_id);
            }
            $params['buy_cat_id'] = join(',', $params['buy_cat_id']);
            $params['agent_type'] = AG_AUTH;
            //判断代理级别是否需要上传证照
            $grade = $gradeModel->find($params['grade_id']);
            if($grade['must_upload'] && empty($params['header_thumb'])) $this->error('请上传手持身份证照');
            $data = $this->dbModel->create($params) or $this->error($this->dbModel->getError());
            $result = $this->dbModel->save();
            if($result) {
                //调用保存后需要处理的方法
                $this->dbModel->addAgentLog($user_id, USER_ID, '意向代理转化');
                if(empty($params['warrant_thumb'])){
                    if($warrant_thumb=A('img')->water($params['user_id'])){
                        M('agent')->where(array('user_id'=>$params['user_id']))->save(array('warrant_thumb'=>$warrant_thumb));
                    }
                }
                $this->success(L('save_success'));
            }else {
                $this->error(L('save_error'));
            }
            exit;
        }
        $agent['mobile'] = decrypt_phone($agent['mobile']);
        $this->assign('agent', $agent);
        //查询等级
        $this->assign('grade_lists', $gradeModel->field('grade_id, grade_name')->order('orderby')->select());
        $this->display();
    }

    public function show() {
        $user_id = (int)I('user_id');
        $agent = $this->dbModel->getInfo($user_id);
        //权限验证
        $this->modulepri($agent['follow_master'], 'agent');
        //获取申请时间
        $time=strtotime($agent['create_time']);
        //默认180天到期
        $dq_time=$time+180*24*3600;
        $arrtime=array('time'=>$time,'dq_time'=>$dq_time);
        $this->assign('arrtime',$arrtime);
        $this->assign('agent', $agent);

        if($agent['parent_id']) {
            $parent = $this->dbModel->getInfo($agent['parent_id']);
            $this->assign('parent', $parent);
        }

        //查询信誉等级
        $creditModel = new AgentCreditModel();
        $this->assign('credit_list', $creditModel->select());

        //查询代理等级
        $gradeModel = new AgentGradeModel();
        $this->assign('grade_lists', $gradeModel->field('grade_id, grade_name')->order('orderby')->select());
        //查日志
        $this->assign('log', $this->dbModel->getLog($user_id));
        $template = power('agent-save') ? 'edit' : 'show';
        $this->display($template);
    }

    //显示授权查询二维码
    public function show_qrcode() {
        $user_id = (int)I('user_id');
        $agent = $this->dbModel->find($user_id);
        import('Common/Library/Phpqrcode');
        $url = U('/search/agent', '', true, true) . '?tag=' . $agent['agent_code'];
        \QRcode::png($url, false, 'L', 10, 2);
    }

    //显示团队组成
    public function parent_list() {
        $user_id = (int)I('user_id');
        $parent = get_parent_agent($user_id);
        $agent = $this->dbModel->find($user_id);
        $gradeModel = new AgentGradeModel();
        $order_by = $gradeModel->where("grade_id = '%d'", $agent['grade_id'])->getField('orderby');
        $grade_list = $gradeModel->where("orderby <= %d", $order_by)->field('grade_id, grade_name')->order('orderby desc')->select();
        foreach($grade_list as $key => $row) {
            if($row['grade_id'] == $agent['grade_id']) {
                $row['agent'] = $agent;
            }elseif($parent) {
                foreach($parent as $vo) {
                    if($vo['grade_id'] == $row['grade_id']) {
                        $row['agent'] = $vo;
                        break;
                    }
                }
            }
            $grade_list[$key] = $row;
        }
        $this->assign('grade_list', $grade_list);
        $this->display();
    }

    public function children_list() {
        if(IS_POST) {
            $params = I('request.', '', 'trim');
            $params['agent_type'] = AG_AUTH;
            $this->dbModel->filter($params);
            $data = $this->dbModel->lists($params);
            $data = $this->dbModel->format($data);
            $this->ajaxReturn($data);
            exit;
        }
        $this->display();
    }

    //导出代理商信息
    public function download(){
        $data = I('get.');
        //过滤数据
        $this->dbModel->filter($data);
        $info = $this->dbModel->downList();

        $title = L('AGENT_TITLE');
        $fileName = 'Agent-' . date("Y-m-d");
        $this->dbModel->download($info, $title, $fileName);
    }

    /**
     * 代理商分配
     */
    public function allocation() {
        $user_name = trim(I('post.user_name'));
        $user_list = explode(',', I('post.user_list'));
        //获取员工ID
        $masterModel = new MasterModel();
        $master = $masterModel->where(array('user_name' => $user_name))->find();
        if (empty($master)) $this->error(L('allocation_master_exist'));


        //检测员工是否存在代理管理权限
        $logic = Master::getMenu($master['role_id']);
        if (is_null($logic) || !in_array('agent-manage', $logic)) $this->error(L('_THIS_NOT_ACCESS_'));

        //过滤重复分配
        $agent = $this->dbModel
            ->field("user_id, follow_master, contact, parent_id, user_no")
            ->where(array('user_id' => array('IN', $user_list)))
            ->select();

        $error_parent = array();
        foreach ($agent as $k => $v) {
            if ($v['follow_master'] == $master['user_id']) {
                unset($agent[$k]);
                continue;
            }
            //判断所选代理是否存在上一级，如果存在则判断上一级代理跟进人是否为当前所选接收人
            if($v['parent_id']) {
                $parent_follow_master = $this->dbModel->where("user_id = %d", $v['parent_id'])->getField('follow_master');
                if($parent_follow_master != $master['user_id']) {
                    $error_parent[] = $v['user_no'];
                    unset($agent[$k]);
                    continue;
                }
            }
        }

        //判断是否有需要更新的代理
        if (empty($agent)) {
            $message = L('allocation_all_agent') . (empty($error_parent) ? '' : "\n上级跟进人不一致的代理编号：\n" . join("\n", $error_parent));
            $this->error($message);
        }

        foreach ($agent as $v) {
            $data = array(
                'user_id' => $v['user_id'],
                'follow_master' => $master['user_id'],
                'update_time' => date(C('DATE_FORMAT'))
            );
            $result = $this->dbModel->save($data);
            if ($result) {
                $note = sprintf(L('allocation_agent_note'), $master['real_name'] . '-' . $user_name);
                $this->dbModel->addAgentLog($v['user_id'], USER_ID, $note);
            }
        }
        $message = L('operating_success') . (empty($error_parent) ? '' : "\n上级跟进人不一致的代理编号：\n" . join("\n", $error_parent));
        $this->success($message);
    }

    public function transfer() {
        if(IS_POST) {
            $params = I('post.');
            $user_id = (int)$params['user_id'];
            $parent_id = (int)$params['parent_id'];
            if($user_id == $parent_id) $this->error('待转移代理不能与上级代理一样！');

            //获取代理级别
            $agentModel = new AgentModel();
            $data = $agentModel->where(array('user_id' => array('IN', array($user_id, $parent_id))))->getField('user_id, grade_id');

            //判断是否为同一级代理
            $grade_id = array_unique($data);
            if(count($grade_id) < 2) $this->error('同一级别的代理不能互相转移！');

            //判断等级高低是否符合要求
            $orderby = M('AgentGrade')->where(array('grade_id' => array('IN', $grade_id)))->getField('grade_id, orderby');
            $parent_grade_id = M('AgentGrade')->where(array("orderby" => array('LT', $orderby[$grade_id[$user_id]])))->order("orderby desc")->getField('grade_id');
            if($parent_grade_id != $grade_id[$parent_id]) $this->error('不能跨级别转移代理！');

            $update = array(
                'user_id' => $user_id,
                'parent_id' => $parent_id,
                'update_time' => date(C('DATE_FORMAT'))
            );
            if($agentModel->save($update)) {
                $this->dbModel->addAgentLog($user_id, USER_ID, '转移代理团队');
                $this->success(L('operating_success'));
            }else {
                $this->error(L('OPERATE_ERROR'));
            }
            exit;
        }
        if(!power('agent-index')) {
            $this->assign('search_url', U('agent/alone'));
        }else {
            $this->assign('search_url', U('agent/index'));
        }
        $this->display();
    }

    public function reset() {
        $user_list = trim(I('user_list'));
        if(empty($user_list)) $this->error('请选择需要操作的代理！');
        $update = array(
            'agent_type' => AG_AUTH,
            'update_time' => date(C('DATE_FORMAT'))
        );
        $user_list = explode(',', $user_list);
        $result = false;
        foreach($user_list as $user_id) {
            $where = array(
                'user_id' => $user_id
            );
            if($this->dbModel->where($where)->save($update)) {
                $this->dbModel->addAgentLog($user_id, USER_ID, '重新授权');
                $result = true;
            }
        }

        if($result) {
            $this->success(L('operating_success'));
        }else {
            $this->error(L('OPERATE_ERROR'));
        }
    }

    public function remove() {
        $user_list = trim(I('user_list'));
        if(empty($user_list)) $this->error('请选择需要操作的代理！');
        $update = array(
            'agent_type' => AG_CANCEL,
            'update_time' => date(C('DATE_FORMAT'))
        );

        $user_list = explode(',', $user_list);
        $result = false;
        foreach($user_list as $user_id) {
            $where = array(
                'user_id' => $user_id
            );
            $warrant_thumb=$this->dbModel->where($where)->getField('warrant_thumb');
            file_exists(APP_PATH.'..'.$warrant_thumb) && @unlink(APP_PATH.'..'.$warrant_thumb);
            $update['warrant_thumb']='';
            if($this->dbModel->where($where)->save($update)) {
                $this->dbModel->addAgentLog($user_id, USER_ID, '注销授权');
                $result = true;
            }
        }

        if($result) {
            $this->success(L('operating_success'));
        }else {
            $this->error(L('OPERATE_ERROR'));
        }
    }

    public function modifyPass(){
        $user_id = intval(I('user_id'));
        if(empty($user_id)) $this->error('请选择需要操作的代理！');
        if(IS_POST) {
            if(I('password')!=I('confirm_password')){
                $this->error(L('confirm_password_error'));
            }
            if(M('member')->where(array('user_id'=>$user_id))->save(array('password'=>md5(I('password').C('CRYPT_KEY'))))){
                $this->success(L('save_success'));
            }else{
                $this->error(L('save_error'));
            }
        }else{
            $member=$this->dbModel->where(array('user_id'=>$user_id))->field('user_id,contact,mobile')->find();
            $member['mobile']=$member['mobile']? decrypt_phone($member['mobile']) : "";
            $this->assign('member_info', $member);
            $this->display();
        }
    }

    public function modifyFollow(){
        $user_id = intval(I('user_id'));
        $agentlogmodel = D('AgentLog');
        if(empty($user_id)) $this->error('请选择需要操作的代理！');
        $agent = $this->dbModel->find($user_id);
        if(!$agent) $this->error('代理不存在！');
        if(IS_POST){
            //添加日志记录
           $master_data  = D('Master')->field('real_name')->where(array('user_id' => I('follow_master'),'front_locked' => 0,'role_id' => 10))->find();
           if(!$master_data) $this->error('跟进人不存在');
           if($this->dbModel->where(array('user_id' => $user_id))->save(array('follow_master' => I('follow_master')))){
               $action_note = '原先跟进人为:'.I('real_name').'--更改为:'.$master_data['real_name'];
               $this->dbModel->addAgentLog($user_id,USER_ID,$action_note);
               $this->success(L('save_success'));
           }else{
               $this->error(L('save_error'));
           }
        }else{
            $member = $this->dbModel->where(array('a.user_id' => $user_id))->field('a.user_id,a.contact,b.user_id m_user,b.real_name')
            ->join(' a left join biz_master b on a.follow_master=b.user_id')->find();
            //查找所有跟进人
            $follow_master = D('Master')->field('user_id,real_name')->where(array('role_id' => 10, 'front_locked' => 0))->select();
            $this->assign('follow_master', $follow_master);
            $this->assign('member_info', $member);
            $this->display();
        }
       
    }
}