<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="HandheldFriendly" content="true" />
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <title> 我的下级代理</title>
    <link type="text/css" rel="stylesheet" href="/public/stage/mobile/css/reset.css?1" />
    <link type="text/css" rel="stylesheet" href="/public/stage/mobile/css/style.css?1" />
    <script type="text/javascript" src="/public/stage/mobile/res/jquery/jquery-2.1.3.min.js"></script>
</head>
<body class="BG">
    <section>
        <div class="titleBox clearfix">
            <a href="javascript:history.go(-1)" class="f-left">
                <img src="/public/stage/mobile/images/back.png" /></a>
            <span> 我的下级代理</span>
        </div>
    </section>
    <section>
        <p class="myStudentTitle"> 我的上级代理：<span><?php echo ($data["parent_truename"]); ?></span></p>
        <div class="myStudent">
            <dl>
                <dd>
                    <a href="<?php echo U('home/member/step/level/1');?>" class="clearfix">
                        <div class="f-left">
                            <p class="myStudentName myStudentNameA">
                                一级代理
                                <img src="/public/stage/mobile/images/myStudent1.png" />
                            </p>
                        </div>
                        <div class="f-right myStudentNumber myStudentNumberA">
                            <p>
                                <span><?php echo ($data["step1_nums"]); ?></span>
                                <img src="/public/stage/mobile/images/open.png" />
                            </p>
                        </div>
                    </a>
                </dd>
                <dd>
                    <a href="<?php echo U('home/member/step/level/2');?>" class="clearfix">
                        <div class="f-left">
                            <p class="myStudentName myStudentNameB">
                                二级代理
                                <img src="/public/stage/mobile/images/myStudent2.png" />
                                <img src="/public/stage/mobile/images/myStudent2.png" />
                            </p>
                        </div>
                        <div class="f-right myStudentNumber myStudentNumberB">
                            <p>
                                <span><?php echo ($data["step2_nums"]); ?></span>
                                <img src="/public/stage/mobile/images/open.png" />
                            </p>
                        </div>
                    </a>
                </dd>
                <dd>
                    <a href="<?php echo U('home/member/step/level/3');?>" class="clearfix">
                        <div class="f-left">
                            <p class="myStudentName myStudentNameC">
                                三级代理
                                <img src="/public/stage/mobile/images/myStudent3.png" />
                                <img src="/public/stage/mobile/images/myStudent3.png" />
                                <img src="/public/stage/mobile/images/myStudent3.png" />
                            </p>
                        </div>
                        <div class="f-right myStudentNumber myStudentNumberC">
                            <p>
                                <span><?php echo ($data["step3_nums"]); ?></span>
                                <img src="/public/stage/mobile/images/open.png" />
                            </p>
                        </div>
                    </a>
                </dd>
                
            </dl>
        </div>
    </section>
<div class="hidHeight"></div>
<div class="navBottom">
    <dl class="clearfix">
        <dd class="navBottomFirst">
            <p class="navBottomItem">
                <a href="stu_profile.html"><img src="/public/stage/mobile/UploadImage/uheaderImg/tp8lz7ik.png" /></a>
            </p>
        </dd>
        <dd>
            <p class="navBottomItem"><a href="index.html">产品购买</a></p>
        </dd>
    
        <dd>
            <p class="navBottomItem"><a href="my.html">代理中心</a></p>
        </dd>
    
        <dd>
            <p class="navBottomItem"><a href="http://mp.weixin.qq.com/s?__biz=MzA4NTU1NjU2NQ==&mid=404012163&idx=1&sn=d0767f8bd157c37f25f63e70f55ff9af&scene=1&srcid=0314PVTY9tnDILY0LJunffqM#rd">客服中心</a></p>
        </dd>
    </dl>
</div>
<script src="js/WeixinTools.js"></script>
</body>
</html>