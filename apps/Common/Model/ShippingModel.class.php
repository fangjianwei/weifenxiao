<?php
/**
 * ====================================
 * 这里是说明
 * ====================================
 * Author: 91336
 * Date: 2014/12/16 9:24
 * ====================================
 * File: ShippingModel.class.php
 * ====================================
 */
namespace Common\Model;
class ShippingModel extends CommonModel {
    
    /*
     * 获取列表
     * @param array $params
     * @return mixed
     */
    public function filter($params){
        if(isset($params['keywords'])){
            $where = array();
            $where['shipping_id'] = $params['keywords'];
            $where['shipping_name'] = array('LIKE', "%{$params['keywords']}%");
            $where['shipping_code'] = array('LIKE', "%{$params['keywords']}%");
            $where['_logic'] = 'OR';
            return $this->where($where);
        }
    }
}