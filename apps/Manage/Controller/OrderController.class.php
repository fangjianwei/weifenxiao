<?php
/**
 * ====================================
 * 订单管理
 * ====================================
 * Author: 91336
 * Date: 2014/12/17 15:54
 * ====================================
 * File: OrderController.class.php
 * ====================================
 */
namespace Manage\Controller;
use Common\Controller\ManageController;
use Common\Library\Common;
use Common\Library\Order\Download;
use Common\Model\MasterModel;
use Common\Library\Order\Operate;
use Common\Model\PaymentModel;
use Common\Model\ShippingModel;
use Common\Library\Manage\Master;
use Common\Library\Goods;
class OrderController extends ManageController {
    protected $dbTable = 'OrderInfo';

    public function _before_index() {
        $paymentModel = new PaymentModel();
        $this->assign('payment', $paymentModel->where('locked = 0')->select());
        $this->assign('order_status', L('order_status'));
        $this->assign('shipping_status', L('shipping_status'));
        $this->assign('pay_status', L('pay_status'));
    }

    //我的订单列表
    public function alone(){
        if(IS_POST) {
            $params = I('request.');
            $params['alone'] = true;
            $this->dbModel->filter($params);
            $data = $this->dbModel->lists($params);
            $data = $this->dbModel->format($data);
            $this->ajaxReturn($data);
            exit;
        }
        $this->assign('order_status', L('order_status'));
        $this->assign('shipping_status', L('shipping_status'));
        $this->assign('pay_status', L('pay_status'));
        $this->display();
    }

    public function show() {
        $order_id = intval(I('order_id'));
        if(!$order_id) $this->error(L('ORDER_NOT_FOUND'));
        $order = $this->dbModel->getRow($order_id);
        //进行权限验证
        $this->modulepri($order['master_id'],'order');
        $channel = C('CHANNEL');
        $order['channel_name'] = $channel[$order['channel_id']];
        $order['role_name'] = D('Role')->where(array('id' => $order['role_id']))->getField('text');
        $this->assign('order', $order);
        $regionModel = D('Region');
        $pro = $regionModel->field('region_name')->where(array('region_id'=>$order['province']))->find();
        $cit = $regionModel->field('region_name')->where(array('region_id'=>$order['city']))->find();
        $dis = $regionModel->field('region_name')->where(array('region_id'=>$order['district']))->find();
        $this->assign('province',$pro);
        $this->assign('city',$cit);
        $this->assign('district',$dis);
        //获取支付日志
        $this->assign('pay', $this->dbModel->getPayLog($order_id));
        //获取订单商品
        $orderGoods = $this->dbModel->getOrderGoods($order_id);
        if($order['is_mass'] == 1){
            foreach($orderGoods as $key => $row){
                if($row['is_package']){
                    $row['goods_info'] = Goods::getPackGoods($row['relation_id'],$row['goods_id']);
                }
                $orderGoods[$key] = $row;
            }
            $this->assign('balance_show',Master::checkChannel($order['is_mass'],$order['channel_id']));
        }
        $this->assign('order_goods', $orderGoods);

        //获取订单日志
        $this->assign('order_log', $this->dbModel->getLog($order_id,$order['is_mass']));
        $operateObj = new Operate($order, false);
        $operate = $operateObj->getOperate();
        //p($operate);
        $this->assign('operate', $operate);

        $shippingModel        = new ShippingModel();
        $shipping             = $shippingModel->select();
        $this->assign('shipping', $shipping);

        //查找支付方式
        $paymentModel = new PaymentModel();
        $payment = $paymentModel->where("locked = 0")->select();
        $this->assign('payment', $payment);
        
        //查找付款账户
        $account_name = M('PayAccount')->field('account_name')->where(array('account_id' => $order['account_id']))->find();
        $this->assign('account_name', $account_name);

        $this->assign('payshow',Master::checkChannel($order['is_mass'],$order['channel_id']));
        $this->display();
    }

    //大批量订单列表
    public function mass(){
        if(IS_POST) {
            $params = I('request.');
            $params['is_mass'] = 1;
            $this->dbModel->filter($params);
            $data = $this->dbModel->lists($params);
            $data = $this->dbModel->format($data);
            $this->ajaxReturn($data);
            exit;
        }
        $this->assign('order_status', L('order_status'));
        $this->assign('shipping_status', L('shipping_status'));
        $this->assign('pay_status', L('pay_status'));
        $this->display();
    }

    public function download() {

        if(IS_POST) {
            $params = I('post.');
            if($params['account_id'] > -1 ) $where['o.account_id'] = (int)$params['account_id'];
            if($params['order_status'] > -1) $where['o.order_status'] = (int)$params['order_status'];
            if($params['shipping_status'] > -1) $where['o.shipping_status'] = (int)$params['shipping_status'];
            if($params['pay_status'] > -1) $where['o.pay_status'] = (int)$params['pay_status'];
            if($params['order_sn']) $where['o.order_sn'] = strip_tags($params['order_sn']);
            if($params['email']) $where['o.email'] = strip_tags($params['email']);
            if($params['consignee']) $where['o.consignee'] = strip_tags($params['consignee']);
            if($params['mobile']) $where['o.mobile'] = crypt_phone(decrypt_phone(strip_tags($params['mobile'])));
            if($params['province']) $where['o.province'] = (int)$params['province'];
            if($params['city']) $where['o.city'] = (int)$params['city'];
            if($params['district']) $where['o.district'] = (int)$params['district'];
            if($params['shipping_id'] > -1) $where['o.shipping_id'] = (int)$params['shipping_id'];
            if($params['invoice_no']) $where['o.invoice_no'] = strip_tags($params['invoice_no']);
            if($params['pay_id'] > -1) $where['o.pay_id'] = (int)$params['pay_id'];
            if($params['master_id']>-1)$where['o.master_id'] = (int)$params['master_id'];
            if($params['channel_id']>0)$where['o.channel_id'] = (int)$params['channel_id'];
            if($params['is_mass'] == 1){
                $where['o.is_mass'] = (int)$params['is_mass'];
            }elseif($params['is_mass'] == 2){
                $where['o.is_mass'] = array('neq',1);
            }
            $where['o.create_time'] = array(array('EGT', date('Y-m-d')), array('LT', date('Y-m-d 23:59:59')));
            if($params['create_time']['start']) $where['o.create_time'][0] = array('EGT', $params['create_time']['start']);
            if($params['create_time']['end']) $where['o.create_time'][1] = array('LT', $params['create_time']['end']);


            if($params['confirm_time']['start']) $where['o.confirm_time'] = array('EGT', $params['confirm_time']['start']);
            if($params['confirm_time']['end']) $where['o.confirm_time'] = array('LT', $params['confirm_time']['end']);
            if($params['confirm_time']['start'] && $params['confirm_time']['end']) $where['o.confirm_time'] = array(array('EGT', $params['confirm_time']['start']),array('LT', $params['confirm_time']['end']));

            if($params['update_time']['start']) $where['o.update_time'] = array('EGT', $params['update_time']['start']);
            if($params['update_time']['end']) $where['o.update_time'] = array('LT', $params['update_time']['end']);
            if($params['update_time']['start'] && $params['update_time']['end']) $where['o.update_time'] = array(array('EGT', $params['update_time']['start']),array('LT', $params['update_time']['end']));

            if($params['pay_time']['start']) $where['o.pay_time'] = array('EGT', $params['pay_time']['start']);
            if($params['pay_time']['end']) $where['o.pay_time'] = array('LT', $params['pay_time']['end']);
            if($params['pay_time']['start'] && $params['pay_time']['end']) $where['o.pay_time'] = array(array('EGT', $params['pay_time']['start']),array('LT', $params['pay_time']['end']));

            if($params['picking_time']['start']) $where['o.picking_time'] = array('EGT', $params['picking_time']['start']);
            if($params['picking_time']['end']) $where['o.picking_time'] = array('LT', $params['picking_time']['end']);
            if($params['picking_time']['start'] && $params['picking_time']['end']) $where['o.picking_time'] = array(array('EGT', $params['picking_time']['start']),array('LT', $params['picking_time']['end']));

            if($params['print_time']['start']) $where['o.print_time'] = array('EGT', $params['print_time']['start']);
            if($params['print_time']['end']) $where['o.print_time'] = array('LT', $params['print_time']['end']);
            if($params['print_time']['start'] && $params['print_time']['end']) $where['o.print_time'] = array(array('EGT', $params['print_time']['start']),array('LT', $params['print_time']['end']));

            if($params['delivery_time']['start']) $where['o.delivery_time'] = array('EGT', $params['delivery_time']['start']);
            if($params['delivery_time']['end']) $where['o.delivery_time'] = array('LT', $params['delivery_time']['end']);
            if($params['delivery_time']['start'] && $params['delivery_time']['end']) $where['o.delivery_time'] = array(array('EGT', $params['delivery_time']['start']),array('LT', $params['delivery_time']['end']));

            if($params['receive_time']['start']) $where['o.receive_time'] = array('EGT', $params['receive_time']['start']);
            if($params['receive_time']['end']) $where['o.receive_time'] = array('LT', $params['receive_time']['end']);
            if($params['receive_time']['start'] && $params['receive_time']['end']) $where['o.receive_time'] = array(array('EGT', $params['receive_time']['start']), array('LT', $params['receive_time']['end']));

            if($params['returns_time']['start']) $where['o.returns_time'] = array('EGT', $params['returns_time']['start']);
            if($params['returns_time']['end']) $where['o.returns_time'] = array('LT', $params['returns_time']['end']);
            if($params['returns_time']['start'] && $params['returns_time']['end']) $where['o.returns_time'] = array();


            if($params['cancel_time']['start']) $where['o.cancel_time'] = array('EGT', $params['cancel_time']['start']);
            if($params['cancel_time']['end']) $where['o.cancel_time'] = array('LT', $params['cancel_time']['end']);
            if($params['cancel_time']['start'] && $params['cancel_time']['end']) $where['o.cancel_time'] = array(array('EGT', $params['cancel_time']['start']),array('LT', $params['cancel_time']['end']));

            if($params['abnormal_time']['start']) $where['o.abnormal_time'] = array('EGT', $params['abnormal_time']['start']);
            if($params['abnormal_time']['end']) $where['o.abnormal_time'] = array('LT', $params['abnormal_time']['end']);
            if($params['abnormal_time']['start'] && $params['abnormal_time']['end']) $where['o.abnormal_time'] = array(array('EGT', $params['abnormal_time']['start']),array('LT', $params['abnormal_time']['end']));
            switch($params['download_type']) {
                case 1:
                    Download::detail($where);
                    break;
                case 2:
                    Download::goods($where);
                    break;
                case 3:
                    Download::back($where);
                    break;
            }

            exit;
        }

        $shippingModel        = new ShippingModel();
        $shipping             = $shippingModel->select();
        $this->assign('shipping', $shipping);

        //查找支付方式
        $paymentModel = new PaymentModel();
        $payment = $paymentModel->select();
        $this->assign('payment', $payment);
        //查找付款账户
        $payaccountModel = M('PayAccount');
        $payaccount = $payaccountModel->where(array('locked' => 0))->select();
        $this->assign('payaccount',$payaccount);
        $this->assign('order_status', L('order_status'));
        $this->assign('shipping_status', L('shipping_status'));
        $this->assign('pay_status', L('pay_status'));
        $this->assign('master_id', M('master')->where(array('role_id'=>'10'))->field('user_id,user_name')->select());

        $this->display();
    }

    public function modify_consignee() {
        $order_id = intval(I('order_id'));
        $order = $this->dbModel->getRow($order_id);
        $operateObj = new Operate($order, false);
        if(!$operateObj->checkOperate('modify_consignee')) {
            $this->error(sprintf(L('CANNOT_OPERATE'), '修改收货信息'));
        }
        if(IS_POST and IS_AJAX) {
            $params = I('post.');
            if(empty($params['consignee'])) $this->error('请填写收货人！');
            if(!Common::isMobile($params['mobile'])) $this->error('请填写正确的联系电话！');
            if(empty($params['district'])) $this->error('请选择地区！');
            if(empty($params['address'])) $this->error('请填写详细地址！');

            $data = array(
                'consignee' => trim($params['consignee']),
                'mobile'    => crypt_phone($params['mobile']),
                'email'     => trim($params['email']),
                'province'  => $params['province'],
                'city'      => $params['city'],
                'district'  => $params['district'],
                'address'   => trim($params['address']),
                'update_time' => date(C('DATE_FORMAT')),
                'order_id'  => $order_id
            );

            if($this->dbModel->save($data)) {
                $this->dbModel->addLog(
                    $order_id,
                    '修改收货信息',
                    $order['order_status'],
                    $order['shipping_status'],
                    $order['pay_status'],
                    USER_ID
                );
                $this->success('已成功修改收货信息！');
            }else {
                $this->success('修改失败，请重试！');
            }
            exit;
        }
        $this->assign('order', $order);
        $this->display();
    }

    public function modify_money() {
        $order_id = intval(I('order_id'));
        $order = $this->dbModel->getRow($order_id);
        $operateObj = new Operate($order, false);
        if(!$operateObj->checkOperate('modify_money')) {
            $this->error(sprintf(L('CANNOT_OPERATE'), '修改金额'));
        }
        if(IS_POST and IS_AJAX) {
            $params = I('post.');
            $order_amount = census_order_amount(array(
                'goods_amount' => $order['goods_amount'],
                'shipping_fee' => $params['shipping_fee'],
                'insure_fee'   => $order['insure_fee'],
                'extra_money'  => $params['extra_money'],
                'discount'     => $params['discount'],
                'surplus'      => $order['surplus'],
                'paid'         => $order['paid']
            ));
            if($order_amount < 0) $this->error('折扣超出范围，请重新计算后提交！');

            $data = array(
                'order_amount' => $order_amount,
                'shipping_fee' => $params['shipping_fee'],
                'discount'     => $params['discount'],
                'update_time'  => date(C('DATE_FORMAT')),
                'order_id'     => $order_id,
                'extra_money'  => $params['extra_money']
            );

            if($this->dbModel->save($data)) {
                $this->dbModel->addLog(
                    $order_id,
                    '修改订单金额',
                    $order['order_status'],
                    $order['shipping_status'],
                    $order['pay_status'],
                    USER_ID
                );
                $this->success('已成功修改订单金额！');
            }else {
                $this->success('修改失败，请重试！');
            }
            exit;
        }
        $this->assign('order', $order);
        $this->display();
    }

    public function modify_goods() {
        $order_id = intval(I('order_id'));
        $order = $this->dbModel->getRow($order_id);
        $operateObj = new Operate($order, false);
        if(!$operateObj->checkOperate('modify_goods')) {
            $this->error(sprintf(L('CANNOT_OPERATE'), '修改商品信息'));
        }
        redirect(U('buy/index', array('order_id' => $order_id, 'user_id' => $order['user_id'])));
    }

    public function set_payment() {
        $payId = (int)I('pay_id');
        $orderId = (int)I('order_id');

        $operateObj = new Operate($orderId, null, 0, USER_ID);
        if(!$operateObj->checkOperate('pay')) {
            $this->error(sprintf(L('CANNOT_OPERATE'), L('SET_PAYMENT')));
        }

        //检查支付方式是否正确
        $paymentModel = new PaymentModel();
        if($payName = $paymentModel->where(array('locked' => 0, 'pay_id' => $payId))->getField('pay_name')) {
            if($this->dbModel->save(array('order_id' => $orderId, 'pay_id' => $payId, 'pay_name' => $payName)) !== false) {
                $this->success(L('SET_PAYMENT_SUCCESS'));
            }
            $this->error(L('SET_PAYMENT_FAIL'));
        }
        $this->error(L('payment_not_exists'));
    }

    public function do_operate() {
        $order_id = (int)I('order_id');
        $operate = trim(I('operate'));
        $remark = trim(I('remark'));
        $order = $this->dbModel->getRow($order_id);
        //进行权限验证
        $this->modulepri($order['master_id'],'order');
        $operateObj = new Operate($order, false);
        $result = $operateObj->doOperate($operate, $remark);
        if($result) $this->success(sprintf(L('OPERATE_SUCCESS'), L($operate)));
        $this->error($operateObj->getError());
    }

    //获取渠道参数
    public function getChannel(){
        $channel = C('CHANNEL');
        $newdata = array();
        foreach($channel as $key => $val){
            $new['id']   = $key;
            $new['text'] = $val;
            $newdata[] = $new;
        }
        $this->ajaxReturn($newdata);
    }
}	