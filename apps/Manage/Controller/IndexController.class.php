<?php
/**
 * ====================================
 * 后台首页管理
 * ====================================
 * Author: Hugo
 * Date: 14-5-20 下午9:58
 * ====================================
 * File: IndexController.class.php
 * ====================================
 */
namespace Manage\Controller;
use Common\Controller\ManageController;
use Think\Cache;

class IndexController extends ManageController
{
    protected $allowAction = array('index', 'main');

    public function _before_index()  {
        layout(false);
        $isAdmin = login('is_admin');
        if(!$isAdmin){
            $menuId = array_keys(login('menu'));
        }else {
            $menuId = 0;
        }

        if(!is_null($menuId)) {
            $this->menuList = D('Menu')->filter(
                array(
                    'display' => true,
                    'menu_id' => $menuId
                )
            )->lists();
        }

        $this->realName = REAL_NAME;
    }

    //工作台内容
    public function main() {
        echo '欢迎';
    }

    public function clear() {
        deldir(TEMP_PATH);
        deldir(DATA_PATH);
        deldir(CACHE_PATH);
        $this->success('缓存清除成功！');
    }
}