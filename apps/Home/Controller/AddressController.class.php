<?php
/**
 * Created by PhpStorm.
 * User: 9009078
 * Date: 2016/5/20
 * Time: 15:23
 */
namespace Home\Controller;
use Common\Controller\StageController;
use Common\Model\AddressModel;

class AddressController extends StageController
{
    public function _initialize()
    {
        parent::_initialize();
        $this->db = new AddressModel();
    }

    /**
     * 管理收货地址
     */
    public function address() {
        $data = $this->db->address_list();
        $this->assign('data',$data);
        $this->display();
    }

    /**
     * 新增/编辑收货地址
     */
    public function add_address() {
        $data = $this->db->address_info();
        $this->assign('data',$data);
        $this->display();
    }

    /**
     * 新增/编辑收货地址-数据处理
     */
    public function add_address_action() {
        $flag = $this->db->add_address();
        echo json_encode($flag);
    }

    /**
     * 删除收货地址
     */
    public function delete_address(){
        $data = $this->db->delete_address();
        echo json_encode($data);
    }

    /**
     * 加载省市区数据
     */
    public function load_address(){
        $data = $this->db->load_address();
        echo json_encode($data);
    }
}