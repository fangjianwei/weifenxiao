<?php
/**
 * ====================================
 * 管理员公共函数类
 * ====================================
 * Author: Hugo
 * Date: 14-5-20 下午9:28
 * ====================================
 * File: Tree.class.php
 * ====================================
 */
namespace Common\Library\Manage;

use Common\Model\ConsumeLogModel;
use Common\Model\MasterModel;

class Master{
    /**
     * 管理操作日志接口
     * @param $note
     * @return bool
     */
    public static function log($userId, $note){
        $userId = empty($userId) ? login('user_id') : $userId;
        if(empty($userId)) return false;
        $db = M('MasterLog');
        $db->data(array(
            'user_id' => $userId,
            'module_name' => MODULE_NAME,
            'controller_name' => CONTROLLER_NAME,
            'action_name' => ACTION_NAME,
            'note' => $note,
            'create_time' => date(C('DATE_FORMAT'))
        ));
        $result = $db->add();
        return $result ? true : false;
    }

    /**
     * 获取管理员菜单
     * @param int $user_id
     * @return array|null
     */
    public static function getMenu($role_id = 0) {
        //读取管理员权限菜单
        $data = M('Menu')
            ->alias(' AS m')
            ->join("__ROLE_MENU__ AS rm ON m.id = rm.menu_id", "LEFT")
            ->where(array("rm.role_id" => $role_id))
            ->select();
        if(empty($data)) return null;

        $menu = array();
        foreach($data as $row){
            $menu[$row['menu_id']] = $row['module'] . '-' . $row['action'];
        }
        return $menu;
    }

    /**
     * 检查余额
     * @param $user_id
     * @param $money
     * @return bool|string
     */
    public static function checkBalance($user_id, $money,$account_id) {
        if(empty($user_id)) return false;
        //读取会员余额
        $balanceModel = M('Balance');
        $where = array('user_id' => $user_id,'account_id' => $account_id);
        $data = $balanceModel
            ->where($where)
            ->field('balance')
            ->find();
        if($data['balance'] < $money) return L('insufficient_balance'); //余额不足
        return true;
    }

    /**
     * 还原余额
     * @param $user_id
     * @param $money
     * @return bool|string
     */
    public static function unBalance($user_id, $agent_id, $money, $content, $account_id, $master_id = 0) {
        if(M('consume_log')->where(array('user_id'=>$user_id, 'money'=>$money, 'agent_id'=>$agent_id, 'content'=>$content, 'master_id'=>$master_id))->getField('log_id')){
            return false;
        }
        $balanceModel = M('Balance');
        $where = array('user_id' => $user_id,'account_id' => $account_id);
        if($balanceModel->where($where)->setInc('balance', $money)) {
            $data = array(
                'money' => $money,
                'user_id' => $user_id,
                'content' => $content,
                'agent_id' => $agent_id,
                'master_id' => $master_id,
                'account_id' => $account_id,
            );
            $logModel = new ConsumeLogModel();
            $logModel->create($data);
            $logModel->add();
            return true;
        }
        return false;
    }

    /**
     * 扣除余额
     * @param $user_id
     * @param $money
     * @param $content
     * @return bool
     */
    public static function useBalance($user_id, $agent_id, $money, $content,$account_id,$master_id = 0) {
        $balanceModel = M('Balance');
        $masterModel = new MasterModel();
        $where = array('user_id' => $user_id,'account_id' => $account_id);
        if($balanceModel->where($where)->setDec('balance', $money)) {
            $data = array(
                'money' => -$money,
                'user_id' => $user_id,
                'content' => $content,
                'agent_id' => $agent_id,
                'master_id' => $master_id,
                'account_id' => $account_id,
            );
            $logModel = new ConsumeLogModel();
            $logModel->create($data);
            $logModel->add();
            return true;
        }
        return false;
    }

    /**
     * 充值
     * @param $user_id
     * @param $money
     * @param string $content
     * @return bool
     */
    static function recharge($user_id, $money,$account_id, $content = '', $master_id = 0) {
        $balanceModel = M('Balance');
        $logModel = new ConsumeLogModel();
        $content = empty($content) ? L('recharge') : $content;

        $data = array(
            'money' => $money,
            'user_id' => $user_id,
            'content' => $content,
            'master_id' => $master_id,
            'account_id' => $account_id,
        );
        $logModel->create($data);
        if($logModel->add()) {
            //判断是否存在该账户
            $checkaccount=$balanceModel->field('user_id')->where("user_id = %d and account_id = %d", array($user_id,$account_id))->find();
            if($checkaccount){
                //存在该账户，更改用户余额
                $balanceModel->where("user_id = %d and account_id = %d", array($user_id,$account_id))->setInc('balance', $money);
            }else{
                //不存在该账户，进行插入数据
                $accountdata=array(
                    'user_id' => $user_id,
                    'account_id' => $account_id,
                    'balance'   => $money,
                ); 
                $balanceModel->create($accountdata);
                $balanceModel->add();
                
            }
            return true;
        }
        return false;
    }

    /**
     * 检查渠道是否需要进行款项交易
     * @param $is_mass
     * @param $channel_id
     * @return boolean
     */
    public static function checkChannel($is_mass,$channel_id){
        if(!$is_mass || !$channel_id) return true;
        if(in_array($channel_id,C('NOT_PAY_CHANNEL')) && $is_mass == 1){
            return false;
        }elseif(!in_array($channel_id,C('NOT_PAY_CHANNEL')) && $is_mass == 1){
            return true;
        }else{
            return true;
        }
    }

    /**
     * 判断是否需要判断渠道的审核流程
     */
    public static function judgeChannel(){
        $_login = unserialize(session('login_session'));
        $flow = D('Role')->where(array('id' => $_login['role_id']))->getField('flow');
        //查询上一级的渠道
        $masterModel = D('Master');
        $masterModel
          ->alias('AS m')
          ->join('LEFT JOIN __ROLE__ AS r on m.role_id=r.id')
          ->where(array('flow' => array('lt',$flow)))
          ->order('flow desc');
        if(in_array($flow,C('JUDGE_CHANNEL'))){
            $masterModel->where(array('m.channel_id' => $_login['channel_id']));
            $prevFlow = $masterModel->getField('flow');
            $prevFlow = $prevFlow ? $prevFlow : 1;
            return array('prevFlow' => $prevFlow,'flow'=>$flow,'result' => true,'login' => $_login);
        }else{
            $prevFlow = $masterModel->getField('flow');
            $prevFlow = $prevFlow ? $prevFlow : 1;
            return array('prevFlow' => $prevFlow,'flow' => $flow,'result' => false,'login' => $_login);
        }
    }

    /**
     * 判断是否有大批量下单权限
     */
    public static function buyFlow($flow){
        if(!$flow) return false;
        if(in_array($flow,C('BUY_FLOW'))){
            return true;
        }else{
            return false;
        }
    }
}