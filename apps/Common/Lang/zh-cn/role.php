<?php
return array(
    'role_name_lost'            => '请填写角色名称！',
    'role_name_exists'          => '角色已存在，请重新输入！',
    'drop_master_before'        => '请先移除角色下的管理员',
    'drop_success'              => '成功删除角色',
    'delete_error'              => '部分角色因为存在下级角色而删除失败，请先删除下级角色',
);