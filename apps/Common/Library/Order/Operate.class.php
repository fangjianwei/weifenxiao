<?php

/* 
 * 订单操作类
 */
namespace Common\Library\Order;
use Common\Library\Manage\Master;
use Common\Library\Member\Member;
use Common\Library\WeChat\TemplateMsg;
use Common\Model\GoodsModel;
use \Common\Model\OrderInfoModel;
use Common\Library\Process;
use Common\Model\PaymentModel;
use Think\Upload;
set_lang('order');

class Operate {
    protected $order;
    protected $orderModel;
    protected $message;
    protected $stage; //是否前台操作
    protected $addNote = '';
    protected $operateType = '';
    protected $today;

    //操作组合
    protected $operateArr = array(
        'confirm' => array(
            'text' => '提交审核',
            'power' => array('order-confirm'),
            'status' => array('1-0-2' => 'manage', '4-0-2' => 'manage'),
            'alone' => true,  //只允许操作自己的订单
        ),
        'unconfirm' => array(
            'text' => '退回审核',
            'power' => array('order-confirm'),
            'status' => array('2-0-2' => 'manage'),
            'alone' => true
        ),
        'audit_pass' => array(
            'text' => '配货',
            'power' => array('order-audit'),
            'status' => array('2-0-2' => 'manage')
        ),
        'audit_fail' => array(
            'text' => '审核退回',
            'power' => array('order-audit'),
            'status' => array('2-0-2' => 'manage')
        ),
        'cancel' => array(
            'text' => '取消订单',
            'power' => array('order-cancel'),
            'status' => array(
                '1-0-2' => 'both',
                '2-0-2' => 'manage',
                '5-0-2' => array(//数组只支持manage, stage，不支持both
                    'manage' => array('order-shipping', 'order-audit'),
                ),
            ),
        ),
        'picking' => array(
            'text' => '同意配货',
            'power' => array('order-shipping'),
            'status' => array('5-0-2' => 'manage')
        ),
        'receive' => array(
            'text' => '确认收货',
            'power' => array('order-receive'),
            'status' => array('5-1-2' => 'both')
        ),
        'back' => array(
            'text' => '创建退货单',
            'virtual' => true,  //虚拟按钮，不显示
            'power' => array('back-form'),
            'status' => array('5-2-2' => 'manage')
        ),
        'addnote' => array(
            'text' => '备注',
            'status' => array('*' => 'manage'),
            'power' => array('order-addnote'),
        ),
    );


    public function __construct($order, $stage = true) {
        $this->stage = $stage;
        $this->orderModel = new OrderInfoModel();
        $this->today = date(C('DATE_FORMAT'));
        $this->order = $order;
    }

    /**
     * 大批量发货获取操作
     * @return mixed
     */
    public function getMassOperate(){
        if($this->stage == true) return array();
        $flow_count = count(C('CHANNEL_FLOW'));
        $judgechannel = Master::judgeChannel();
        if($judgechannel['result']){
            $channel_result = $this->order['channel_id'] == $judgechannel['login']['channel_id'] ? true : false;
        }else{
            $channel_result = true;
        }
        $result = $this->order['order_status'] != OS_CANCEL //订单不能为取消状态
                && power('order-mass_audit')  //是否有大批量审核权限
                && $this->order['audit_status'] < $flow_count   //订单审核状态不超过最大角色审核状态
                && $channel_result
                && $this->order['init_status']  < $judgechannel['flow'] //订单初始状态小于当前角色审核状态
                && $this->order['audit_status'] == $judgechannel['prevFlow']; //订单审核状态等于上一级角色审核状态
        if($result){
            $menu = array(
                'mass' => array(
                    'text' => '审核通过',
                    'power' => array('order-mass'),
                    'status' => array('5-3-2' => 'manage')
                ),
                'cancel' => $this->operateArr['cancel'],
            );
        }
        $menu['addnote'] = $this->operateArr['addnote'];
        return $menu;
    }
    
    /**
     * 获取操作
     * @param $orderId
     * @return null
     */
    public function getOperate(){
        if($this->order['is_mass'] == 1) return $this->getMassOperate(); //大批量发货
        $status = join('-', array($this->order['order_status'], $this->order['shipping_status'], $this->order['pay_status']));
        $operateKey = array();
        foreach($this->operateArr as $operate => $operateInfo) {
            $operateStatus = $operateInfo['status'];
            $status = isset($operateStatus['*']) ? '*' : $status;
            if(!in_array($status, array_keys($operateInfo['status']))) continue;
            $position = $operateInfo['status'][$status]; //得到当前状态下的操作位置（前台或后台）
            if($this->stage) { //如果是前台操作
                if($position === 'manage') continue;
                if(is_array($position) && !$position['stage']) continue;
            }else {
                //操作属于前台，跳过
                if($position === 'stage') continue;
                if(!empty($operateInfo['power'])) {
                    $hasPower = false;
                    foreach($operateInfo['power'] as $power) {
                        if(power($power)) {
                            $hasPower = true;
                            break;
                        }
                    }
                    if(!$hasPower) continue;
                }

                if(is_array($position)) {
                    if(!$position['manage']) continue;
                    $hasPower = false;
                    foreach($position['manage'] as $power) {
                        if(power($power)) {
                            $hasPower = true;
                            break;
                        }
                    }
                    if(!$hasPower) continue;
                }

                //如果限制了只能操作自己的订单
                if($operateInfo['alone']) {
                    //非所属订单直接跳过，不允许处理
                    if($this->order['master_id'] !== USER_ID) continue;
                }
            }
            $operateKey[$operate] = $operateInfo;
        }
        return $operateKey;
    }

    /**
     * 判断是否可以进行确认、支付、取消等操作
     * @param int    $orderId    订单ID
     * @param string $oprateType 操作类型
     * @return bool
     */
    public function checkOperate($operateType = ''){
        $operate = $this->getOperate();
        if($operate && in_array($operateType, array_keys($operate))){
            return true;
        }
        return false;
    }



    /**
     * 处理订单
     * @param $orderId
     * @param $operateType
     * @param $userId 下单会员ID
     * @return bool
     */
    public function doOperate($operateType, $addNote = '') {
        if($this->checkOperate($operateType)) {
            if(method_exists($this, $operateType)) {
                $this->addNote = $addNote;
                $this->operateType = $operateType;
                if(call_user_func(array($this, $operateType))) {
                    return true;
                }

            }
        }
        $this->message or $this->message = sprintf(L('CANNOT_OPERATE'), L($operateType));
        return false;
    }
    
    /**
     * 保存订单信息
     * @param type $data
     * @return type
     */
    protected function save($data) {
        $data['order_id'] = $this->order['order_id'];
        $data['update_time'] = $this->today;
        return $this->orderModel->save($data);
    }

    /**
     * 提交审核
     * @return bool
     */
    private function confirm() {
        $data = array(
            'order_status' => OS_WAIT_AUDIT,
            'confirm_time' => $this->today
        );
        if($this->save($data) === false) {return false;}
        $this->recordLog();
        return true;
    }

    /**
     * 退回审核
     * @return bool
     */
    private function unconfirm() {
        $data = array(
            'order_status' => OS_CANCEL,
            'confirm_time' => null,
            'pay_status' => PS_REFUND
        );
        if($this->save($data) === false) {return false;}
        $this->free_stock();
        //解除相应的冻结金额
        Master::unBalance($this->order['master_id'], $this->order['user_id'], $this->order['surplus'], '审核退回订单返款，SN：' . $this->order['order_sn'], $this->order['account_id'], USER_ID);
        $this->recordLog();
        return true;
    }

    /**
     * 审核通过
     * @return bool
     */
    private function audit_pass() {
        $data = array(
            'order_status' => OS_AUDIT_PASS,
            'audit_time' => $this->today,
        );
        if($this->save($data) === false) {return false;}
        $this->recordLog();
        return true;
    }

    /**
     * 配货
     * @return bool
     */
    private function picking() {
        $post = I('post.');
        //默认发新邦物流
        $post['shipping_id'] = 44;
        $post['shipping_fee']=0;
        if($post['shipping_fee'] > 0) {
            //判断余额是否足够
            $result = Master::checkBalance($this->order['master_id'], $post['shipping_fee'],$this->order['account_id']);
            if($result !== true) {
                $this->message = $result;
                return false;
            }
        }

        $shipping_name = D('Shipping')
            ->where("shipping_id = '%d'", (int)$post['shipping_id'])
            ->getField('shipping_name');

        $data = array(
            'shipping_status' => SS_PREPARING,
            'picking_time' => $this->today,
            'shipping_id' => (int)$post['shipping_id'],
            'shipping_fee' => $post['shipping_fee'],
            'surplus' => array('exp','surplus+' . $post['shipping_fee']),
            'shipping_name' => $shipping_name
        );
        if($this->save($data) === false) {return false;}
        if($post['shipping_fee'] > 0) {
            //冻结余额
            Master::useBalance($this->order['master_id'], $this->order['user_id'], $post['shipping_fee'], '扣除运费，SN：' . $this->order['order_sn'],$this->order['account_id'], USER_ID);
        }
        $this->recordLog();
        return true;
    }

    /**
     * 审核退回
     * @return bool
     */
    private function audit_fail() {
        $data = array(
            'order_status' => OS_AUDIT_BACK,
            'audit_time' => $this->today,
            'pay_status' => PS_REFUND
        );
        if($this->save($data) === false) {return false;}
        $this->free_stock();
        //解除相应的冻结金额
        Master::unBalance($this->order['master_id'], $this->order['user_id'], $this->order['surplus'], '审核退回订单返款，SN：' . $this->order['order_sn'], $this->order['account_id'], USER_ID);
        $this->recordLog();
        return true;
    }
    
    /**
     * 签收
     * @return boolean
     */
    private function receive() {
        $data = array(
            'receive_time' => $this->today,
            'shipping_status' => SS_RECEIVED
        );
        if($this->save($data) === false) {return false;}
        $this->recordLog();
        return true;
    }

    /**
     * 大批量发货通过审核
     * $return boolean
     */
    private function mass(){
        $flow_count = count(C('CHANNEL_FLOW'));
        $judgechannel = Master::judgeChannel();
        if($this->order['audit_status'] < $flow_count-1){
            $data = array(
               'audit_status' => $judgechannel['flow']
            );
            if($this->save($data) === false) {return false;}
            $_role = D('Role')->getInfo($judgechannel['login']['role_id']);
            $this->addNote = trim($this->addNote) != '' ? "({$_role['text']})，备注：".(trim($this->addNote)) : "({$_role['text']})";
            $this->recordLog();
            return true;
        }
        $post = I('post.');
        //默认发新邦物流
        $post['shipping_id'] = 44;
        $post['shipping_fee']=0;
        if($post['shipping_fee'] > 0) {
            //判断余额是否足够
            $result = Master::checkBalance($this->order['master_id'], $post['shipping_fee'],$this->order['account_id']);
            if($result !== true) {
                $this->message = $result;
                return false;
            }
        }

        $shipping_name = D('Shipping')
            ->where("shipping_id = '%d'", (int)$post['shipping_id'])
            ->getField('shipping_name');

        $data = array(
            'audit_status' => $judgechannel['flow'],
            'order_status' => OS_AUDIT_PASS,
            'audit_time' => $this->today,
            'shipping_status' => SS_PREPARING,
            'picking_time' => $this->today,
            'shipping_id' => (int)$post['shipping_id'],
            'shipping_fee' => $post['shipping_fee'],
            'surplus' => array('exp','surplus+' . $post['shipping_fee']),
            'shipping_name' => $shipping_name
        );
        if($this->save($data) === false) {return false;}
        if($post['shipping_fee'] > 0) {
            //冻结余额
            Master::useBalance($this->order['master_id'], $this->order['user_id'], $post['shipping_fee'], '扣除运费，SN：' . $this->order['order_sn'],$this->order['account_id'], USER_ID);
        }
        $_role = D('Role')->getInfo($judgechannel['login']['role_id']);
        $this->addNote = trim($this->addNote) != '' ? "({$_role['text']})，备注：".(trim($this->addNote)) : "({$_role['text']})";
        $this->recordLog();
        return true;
    }

    /**
     * 取消订单
     * @return boolean
     */
    private function cancel() {
        $data = array(
            'cancel_time' => $this->today,
            'order_status' => OS_CANCEL,
            'pay_status' => PS_REFUND
        );
        if($this->save($data) === false) {return false;}
        $this->free_stock();
        //解除相应的冻结金额
        if(Master::checkChannel($this->order['is_mass'],$this->order['channel_id'])){
            Master::unBalance($this->order['master_id'], $this->order['user_id'], $this->order['surplus'], '取消订单返款，SN：' . $this->order['order_sn'], $this->order['account_id'], USER_ID);
        }
        $this->recordLog();
        return true;
    }

    /**
     * 添加备注
     * @return bool
     */
    public function addnote() {
        if(trim($this->addNote) == '') {
            $this->message = '请填写备注！';
            return false;
        }
        $this->recordLog();
        return true;
    }

    //释放库存占用
    public  function free_stock() {
        //释放库存占用
        $goodsModel = new GoodsModel();
        $orderGoods = M('OrderGoods')->where(array('order_id' => $this->order['order_id']))->select();
        if($orderGoods) {
            foreach($orderGoods as $key => $goods) {
                //查询是否子产品，子产品直接跳过
                if($goods['parent_id']) continue;
                $has_small = false;
                foreach($orderGoods as $small) {
                    if($small['parent_id'] == $goods['goods_id']) {
                        $goodsModel->where(array('goods_id' => $small['goods_id']))->setDec('occupy', $goods['goods_number'] * $small['goods_number']);
                        $has_small = true;
                    }
                }
                if(!$has_small) {
                    $goodsModel->where(array('goods_id' => $goods['goods_id']))->setDec('occupy', $goods['goods_number']);
                }
            }
        }
    }
    
    /**
     * 记录操作日志
     * @param type $addNote
     */
    public  function recordLog() {
        $order = $this->orderModel
            ->field('order_status,shipping_status,pay_status, user_id,account_id')
            ->find($this->order['order_id']);
        $this->orderModel->addLog(
            $this->order['order_id'],
            L($this->operateType) . ($this->addNote ? "，" . $this->addNote : ''),
            $order['order_status'],
            $order['shipping_status'],
            $order['pay_status'],
            $order['account_id'],
            $order['user_id'],
            $this->stage ? 0 : USER_ID
        );
    }

    /**
     * 返回错误信息
     * @return type
     */
    public function getError() {
        return $this->message;
    }
}

