<?php
namespace Common\Model;

class FrontaccountModel extends CommonModel
{
    protected $tableName = 'member';
    protected $_auto = array(
        array('create_time', 'date', self::MODEL_INSERT, 'function', 'Y-m-d H:i:s'),
        array('update_time', 'date', self::MODEL_UPDATE, 'function', 'Y-m-d H:i:s'),
        array('password', 'cryptPassword', self::MODEL_BOTH, 'callback'),
    );

    protected $_validate = array(
        array('special_number','require','{%special_number_lost}'),
        array('real_name','require','{%real_name_lost}'),
        array('role_id','role','{%role_id_lost}',self::MUST_VALIDATE,'callback'),
        array('password','require','{%password_lost}',self::MUST_VALIDATE,'',self::MODEL_INSERT),
        array('confirm_password','require','{%confirm_password_lost}',self::MUST_VALIDATE,'',self::MODEL_INSERT),
        array('special_number','checkaccount','{%special_number_exists}', self::MUST_VALIDATE, 'callback', self::MODEL_BOTH),
        array('confirm_password','password','{%confirm_password_error}', self::EXISTS_VALIDATE, 'confirm'), // 验证确认密码是否和密码一致
        array('password', '6,32', '{%password_error}', self::VALUE_VALIDATE, 'length', self::MODEL_BOTH),
    );

    public function checkaccount($account){
        $data = I('post.','','trim');
        $mobile = $this
                ->where(array('mobile' =>  crypt_phone($account),'user_id' => array('neq',$data[$this->getPk()])))
                ->count();
        $email  = $this
                ->where(array('email' => $account,'user_id' => array('neq',$data[$this->getPk()])))
                ->count();
        $special_number = $this
                ->where(array('special_number' => $account,'user_id' => array('neq',$data[$this->getPk()])))
                ->count();
        //后台账号判断
        $where['user_name'] = $data['special_number'];
        if($data[$this->getPk()]) $where['front_id'] = array('neq',$data[$this->getPk()]);
        $manage_user = M('Master')
                     ->where($where)
                     ->count();
        if($mobile > 0 || $email>0 || $special_number >0 || $manage_user){
            return false;
        }else{
            return true;
        }
    }
    public function role($data){
        return (bool)$data;
    }

    /**
     * 加密登录密码
     * @param $password
     * @return string
     */
    public function cryptPassword($password)
    {
        if(empty($password)) return false;
        return md5($password . C('CRYPT_KEY'));
    }

    public function format($params){
        $channel = C('CHANNEL');
        foreach($params['rows'] as $key => $val){
            $val['mobile'] = decrypt_phone($val['mobile']);
            $val['channel_name'] = $channel[$val['channel_id']];
            $val['role_name'] = D('Role')->where(array('id' => $val['role_id']))->getField('text');
            $params['rows'][$key] = $val;
        }
        return $params;
    }

    /**
     * 获取列表
     * @param array $params
     * @return mixed
     */
    public function filter($params) {
        $where = array();
        if($params['keyword']){
            $whe['special_number'] = array('LIKE', "%{$params['keyword']}%");
            $whe['user_name'] = array('LIKE', "%{$params['keyword']}%");
            $whe['_logic'] = 'OR';
            $where = array(
                'is_special' => 1,
                '_complex' => $whe
            );
        }else{
            $where['is_special'] = 1;
        }

        return $this->where($where);
    }
}