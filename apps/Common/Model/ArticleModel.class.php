<?php
/**
 * ====================================
 * 文章模型
 * ====================================
 * Author: Hugo
 * Date: 14-5-20 下午9:28
 * ====================================
 * File: ArticleModel.class.php
 * ====================================
 */
namespace Common\Model;

class ArticleModel extends CommonModel
{
    protected $_validate = array(
        array('title', 'require', '{%text_lost}'),
        array('cat_id', 'require', '{%cat_id_lost}'),

    );

    public function filter($params = array())
    {
        $this->alias(" AS a")
            ->join('__ARTICLE_CATEGORY__ as ac ON ac.id = a.cat_id', 'LEFT')
            ->field('a.*,ac.title as cat_name')
            ->order('a.create_time DESC');
        $where = array();
        if ($params['keywords']) {
            $where['a.title'] = array('LIKE', "%{$params['keywords']}%");
        }
        if ($params['cat_id']) {
            $where['a.cat_id'] = (int)$params['cat_id'];
        }
        $this->where($where);
        return $where;
    }


}