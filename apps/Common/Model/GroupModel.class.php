<?php
namespace Common\Model;
use Common\Library\Common;

class GroupModel extends CommonModel {
    protected $_validate = array(
        array('text','require','{%role_name_lost}'),
        array('text','','{%role_name_exists}', self::MUST_VALIDATE, 'unique', self::MODEL_BOTH),
        array('pid', 'validateEq', '{%parent_error}', self::EXISTS_VALIDATE, 'callback', self::MODEL_UPDATE)
    );

    /**
     * 判断父级ID正确性
     * @param $pid
     * @param $id
     * @return bool
     */
    protected function validateEq($pid){
        return $pid != I('post.id');
    }

    public function lists($params) {
        if($params['keyword']) {
            $this->where(array(
                'text' => array('LIKE', "%{$params['keyword']}%")
            ));
        }
        $data = $this
            ->field('*,id as group_id')
            ->order("pid ASC, orderby DESC")
            ->select();

        Common::buildTree($data, $params['selected'], $params['type']);
        return $data;
    }

}