<?php

/**
 * 得到新订单号
 * @return  string
 */
function get_order_sn($type='') {
    /* 选择一个随机的方案 */
    mt_srand((double) microtime() * 1000000);
    $order_sn = $type.date('Ymd') . str_pad(mt_rand(1, 99999), 5, '0', STR_PAD_LEFT);
    $add_time = strtotime(date("Y-m-d")) - 604800;
    $row = M('OrderInfo')->where("add_time > $add_time AND order_sn = '$order_sn'")->count();
    if($row) {
        return get_order_sn($type);
    }
    return $order_sn;

}

/**
 * 得到新退货单号
 * @return  string
 */
function get_back_sn($type='') {
    /* 选择一个随机的方案 */
    mt_srand((double) microtime() * 1000000);
    $back_sn = $type.date('Ymd') . str_pad(mt_rand(1, 99999), 5, '0', STR_PAD_LEFT);
    $row = M('BackInfo')->where("back_sn = '$back_sn'")->count();
    if($row) {
        return get_back_sn($type);
    }
    return $back_sn;
}

/**
 * 生成客户编号
 * @return string
 */
function get_user_no() {
    $no = 'C' . date('ymdhis') . generate_code(5);
    $row = M('Agent')->where("user_no = '$no'")->count();
    if($row) {
        return get_user_no();
    }
    return $no;
}

function generate_code($length = 4) {
    $min = pow(10 , ($length - 1));
    $max = pow(10, $length) - 1;
    return mt_rand($min, $max);
}

/**
 * 统计订单应付金额
 * @param $order
 * @return mixed
 */
function census_order_amount($order) {
    return $order['goods_amount'] + $order['shipping_fee'] + $order['insure_fee'] + $order['extra_money'] - $order['discount'] - $order['surplus'] - $order['paid'];
}

/**
 * 得到地区名称
 * @param $region_id
 * @return mixed
 */
function get_region($region_id){
    $region_id = empty($region_id) ? 0 : $region_id;
    return M('region')->where(array('region_id'=>$region_id))->getField('region_name');
}

/**
 * 积分类型处理
 * @param $type
 * @param int $item_id
 * @return array
 */
function get_integral_item($type, $item_id = 0) {
    $thumb = array(INTEGRAL_SIGN => '/public/stage/mobile/images/user/sign_icon.jpg', INTEGRAL_REGISTER =>'/public/stage/mobile/images/user/reg_icon.jpg', 'default' => '/public/images/order/product.jpg');
    $return = array('title' => '', 'thumb' => '');
    switch($type) {
        case INTEGRAL_SIGN:
            $return['thumb'] = $thumb[INTEGRAL_SIGN];
            break;
        case INTEGRAL_GOODS:
            $return['thumb'] = $thumb['default'];
            if($item_id) {
                $goodsInfo = M('goods')->where(array('goods_id' => $item_id))->find();
                $return = array(
                    'title' => $goodsInfo['goods_name'],
                    'thumb' => $goodsInfo['goods_thumb']
                );
            }
            break;
        case INTEGRAL_REGISTER:
            $return['thumb'] = $thumb[INTEGRAL_REGISTER];
            break;
    }
    return $return;
}

/**
 * 获取会员头像
 * @param  INT $userId 用户ID
 * @return 会员头像
 */
function getAvatar($userId) {
    if (empty($userId)) return false;
    return M('member')->where('user_id = %d', $userId)->getField('header_image');
}

/**
 * 代理中心导航标题
 * @param  $controller 控制器
 * @param  $action     动作
 * @param  $param      参数
 * @return 组装好的标题             
 */
function getTitle($controller, $action = '', $param = array()) {
    $title = '';
    if (empty($action)) {
        return $title;
    }
    $actions = explode('/', $action);
    foreach ($actions as $k => $v) {
        if (!empty($param) && isset($param[$v])) {
            $href = U($controller.'/'.$v, $param[$v]);
        }else{
            $href = U($controller.'/'.$v);
        }
        $title .= '<li><a href="'.$href.'">'.L($controller.'_'.$v).'</a></li>';
    }
    return $title;
}

/**
 * 获取套装的库存
 * @param $goods_id
 * @param $relation_id
 * @return mixed
 */
function get_package_stock($goods_id, $relation_id) {
    $goodsModel = D('Goods');
    //查询套装内容的单品数量
    $data = $goodsModel->alias(' AS g')
        ->join("__GOODS_PACKAGE__ AS p ON p.relation_id = g.goods_id", "LEFT")
        ->field('g.goods_id,floor((g.goods_stock-g.occupy)/p.goods_number) as total,p.goods_number')
        ->where("p.goods_id = '%d' AND p.relation_id IN (%s)", $goods_id, $relation_id)
        ->select();
    //查询购物车数量
    $cart = D('Cart')
          ->where(array('goods_id' => array('IN',$relation_id),'session_id' => SESSION_ID))
          ->select();
    //查询购物车的其他套装
    $other_package = D('Cart')
                   ->field('relation_id,c.goods_number as cart_number,p.goods_number')
                   ->join('AS c left join __GOODS_PACKAGE__ AS p on c.goods_id=p.goods_id')
                   ->where(array('session_id' => SESSION_ID,'c.goods_id' => array('neq',$goods_id),'extension_code' => 'package_buy'))
                   ->select();

    foreach($data as $val){
        foreach($cart as $v){
           if($v['goods_id'] == $val['goods_id']){
               //计算剩余的套装数量
               $val['total'] = $val['total']-floor($v['goods_number']/$val['goods_number']);
           }
        }

        foreach($other_package as $other){
            if($other['relation_id'] == $val['goods_id']){
                //用其他套装在购物车的数量*其他套装内单品数量/自己套装内单品数量，就是要减掉的套装数量
                $val['total'] = $val['total']-floor($other['cart_number']*$other['goods_number']/$val['goods_number']);
            }
        }
        $min[] = $val['total'];
   }
    $data=min($min);
   // dump($data);
    return (int)$data;
}

/**
 * 获取顶级代理
 * @param $user_id
 * @return mixed
 */
function get_top_agent($user_id) {
    static $agentModel;
    if(!$agentModel) {
        $agentModel = new \Common\Model\AgentModel();
    }
    $parent = $agentModel->find($user_id);
    if($parent['parent_id']) {
        return get_top_agent($parent['parent_id']);
    }
    return $parent;
}

/**
 * 获取所有上级代理
 * @param $user_id
 * @return mixed
 */
function get_parent_agent($user_id) {
    static $data = array(), $agentModel;
    if(!$agentModel) {
        $agentModel = new \Common\Model\AgentModel();
    }
    $agent = $agentModel->field('parent_id')->find($user_id);
    if($agent['parent_id']) {
        $data[$agent['parent_id']] = $agentModel->find($agent['parent_id']);
        get_parent_agent($agent['parent_id']);
    }
    return $data;
}