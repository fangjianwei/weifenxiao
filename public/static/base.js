var __CreateJSPath = function (js) {
    var scripts = document.getElementsByTagName("script");
    var path = "";
    for (var i = 0, l = scripts.length; i < l; i++) {
        var src = scripts[i].src;
        if (src.indexOf(js) != -1) {
            var ss = src.split(js);
            path = ss[0];
            break;
        }
    }
    var href = location.href;
    href = href.split("#")[0];
    href = href.split("?")[0];
    var ss = href.split("/");
    ss.length = ss.length - 1;
    href = ss.join("/");
    if (path.indexOf("https:") == -1 && path.indexOf("http:") == -1 && path.indexOf("file:") == -1 && path.indexOf("\/") != 0) {
        path = href + "/" + path;
    }
    return path;
}

function closeandopen(url){
    var browserName = navigator.appName;
    if (browserName == "Microsoft Internet Explorer") {
        window.opener = null;
        window.open('','_self');
        window.close();
        var myWindow = window.open(url, 'myWindow','resizable=no,location=no,menubar=no,status=yes');
        myWindow.focus();
        myWindow.moveTo(0, 0);
        myWindow.resizeTo(screen.availWidth, screen.availHeight);
        return false;
    }
    window.location.href = url;
}

/**
 * 判断字符是否为空
 * @param string
 * @returns {boolean}
 */
function empty(string){
    if(typeof(string) == 'undefined') return true;
    if(string == '') return true;
    if(string == null) return true;
    return false;
}

/**
 * 计算对象的长度
 * @param o
 * @returns {*}
 */
function count(o){
    var t = typeof o;
    if(t == 'string'){
        return o.length;
    }else if(t == 'object'){
        var n = 0;
        for(var i in o){
            n++;
        }
        return n;
    }
    return false;
}

/**
 * 判断函数是否存在
 * @param function_name
 * @returns {boolean}
 */
function function_exists(function_name){
    try {
        if (typeof(eval(function_name)) == "function") {
            return true;
        }
    } catch(e) {}
    return false;
}

/**
 * 创建完整URL
 * @param url
 * @param query
 * @returns {*}
 */
function http_build_query(url, query){
    if($.isEmptyObject(query)) return url;
    query = $.param(query);
    if(url.indexOf('?') > -1) {
        url += '&';
    }else{
        url += '?';
    }
    return url + query;
}

/**
 * 设置对象焦点
 * @param element
 */
function blur(element){
    $(element).blur();
}

/**
 * 判断searchText是否与targetText匹配
 * @param searchText 检索的文本
 * @param targetText 目标文本
 * @return true-检索的文本与目标文本匹配；否则为false.
 */
function isMatch(searchText, targetText) {
    return $.trim(targetText) != "" && targetText.indexOf(searchText) != -1;
}