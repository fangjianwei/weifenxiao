<?php
/**
 * ====================================
 * 接口公共类
 * ====================================
 * Author: 91336
 * Date: 2014/12/8 10:13
 * ====================================
 * File: ApiController.class.php
 * ====================================
 */
namespace Common\Controller;
use Common\Library\Config;
use Think\Controller;

class ApiController extends Controller {
    protected $dbModel;//当前数据模型
    protected function _initialize() {
        Config::init(); //初始化数据库中的配置
    }


}