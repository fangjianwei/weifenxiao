<?php
namespace Home\Controller;

use Common\Controller\StageController;

class IndexController extends StageController {
    /**
     * 商品分类
     */
    public function index() {
        $category = M("goods_category")->where(array("pid"=>0))->select();
        $cat_id = I("get.cat_id");
        $where = $cat_id || $cat_id === "0"?"cat_id={$cat_id}":"";
        if($cat_id === "0"){
            $cat_name = "未分类";
        }else if ($cat_id>0){
            $cat_name = M("goods_category")->where(array("id"=>$cat_id))->getField("text");
        }else{
            $cat_name = "全部产品";
        }
        $goods = M("goods")->where($where)->limit(10)->select();
        $this->assign("cat_name",$cat_name);
        $this->assign("goods",$goods);
        $this->assign("category",$category);
        $this->display();
    }
}