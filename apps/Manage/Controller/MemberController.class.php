<?php
/**
 * Created by PhpStorm.
 * User: 9009078
 * Date: 2016/5/10
 * Time: 15:40
 */
namespace Manage\Controller;
use Common\Controller\ManageController;
use Common\Library\Manage\Master;
use Common\Model\ConsumeLogModel;
use Common\Model\MasterModel;

class MemberController extends ManageController{
    protected $dbTable = 'member';

    /**
     * 编辑或添加表单页面
     */
    public function form() {
        $pk = $this->dbModel->getPk();
        $params = I("request.");
        if(isset($params[$pk])) {
            if(method_exists($this->dbModel, 'getInfo')) {
                $data = $this->dbModel->getInfo((int)$params[$pk]);
            }else{
                $data = $this->dbModel->find((int)$params[$pk]);
            }
        }
        $this->assign('info',$data);
        $this->display();
    }

    /**
     * 查看用户列表
     */
    public function get_user_list(){
        $params = I("request.");
        $default_data = array(0=>array('user_id'=>0,'name'=>'请选择上级代理'));
        $data = $this->dbModel->get_user_list($params);
        $data = empty($data)?$default_data:array_merge($default_data,$data);
        $this->ajaxReturn($data);
    }

    /**
     * 保存用户资料
     */
    public function save(){
        $data = $this->dbModel->save();
        $this->ajaxReturn($data);
    }

    /**
     * 查看用户收货地址
     */
    public function address_list(){
        $params = I("request.");
        print_r($params);exit;
    }

    /**
     * 查看用户财务明细
     */
    public function finance(){
        $params = I("request.");
        print_r($params);exit;
    }

}