<?php
/**
 * ====================================
 * 菜单管理
 * ====================================
 * Author: Hugo
 * Date: 14-5-20 下午9:58
 * ====================================
 * File: MenuController.class.php
 * ====================================
 */

namespace Manage\Controller;
use Common\Controller\ManageController;

class MenuController extends ManageController {

    public function power() {
        if(IS_AJAX && IS_POST) {
            $params = I('post.');
            $this->dbModel->setRole((int)$params['menu_id'], $params['role_list']);
            $this->success(L('save_success'));
            exit;
        }
        $menu_id = intval(I('menu_id'));
        $this->info = $this->dbModel->getInfo($menu_id);
        $this->display();
    }

    //删除菜单
    public function delete() {
        $items = explode(',', I('request.items'));
        foreach($items as $item_id){
            $this->dbModel->where("pid = '%d' or id = '%d'", $item_id, $item_id)->delete();
        }
        $this->success();
    }
}