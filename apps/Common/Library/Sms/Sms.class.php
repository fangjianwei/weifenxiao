<?php
/**
 * ====================================
 * Author: 9004396
 * Date: 2015/1/8 16:57
 * ====================================
 * File: Sms.php
 * ====================================
 */

namespace Common\Library\Sms;


class Sms{
    
/**
 * 发送短信
 * @param $mobile               手机号 多个用英文的逗号隔开 post理论没有长度限制.推荐群发一次小于等于10000个手机号
 * @param string $ip            客户端IP
 * @param string $sms_content   发送的内容
 * @param string $ext
 * @param string $rrid          默认空 如果空返回系统生成的标识串 如果传值保证值唯一 成功则返回传入的值
 * @param string $stime         定时时间 格式为2011-6-29 11:09:21
 * @return array|mixed          返回值Array ( [error] => M000000 [message] => 发送成功 [data] => Array () ) 
 * 错误代码     array('M000000'=>'发送成功','M000001'=>'标识错误','M000002'=>'手机号码不存在',
 *              'M000003'=>'手机号码不正确','M000004'=>'操作过于频繁',
 *              'M000005'=>'请不要恶意操作','M000006'=>'发送失败','M000007'=>'记录失败',);
 */
    public static function send_sms($mobile, $ip = '', $sms_content = '', $ext = '', $rrid = '', $stime = ''){
        if(empty($mobile)) return false;
        $data = array(
            'mobile'        => $mobile,
            'ip'            => $ip,
            'source'        => $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'],
            'sms_content'   => $sms_content,
            'ext'           => $ext,
            'rrid'          => $rrid,
            'stime'         => $stime,
            'flag'          => 'API_CHINASKIN-002'

        );
        $ret = Curl::getApiResponse(URL,$data);
        return $ret;
    }

}







