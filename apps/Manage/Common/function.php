<?php
/**
 * 获取角色名称
 * @param $roleId
 * @return mixed
 */
function get_role_name($roleId) {
    $roleModel = M('Role');
    return $roleModel->where("id = '%d'", $roleId)->getField('text');
}

/**
 * 获取组别名称
 * @param $groupId
 * @return mixed
 */
function get_group_name($groupId) {
    $dbModel = M('Group');
    return $dbModel->where("id = '%d'", $groupId)->getField('text');
}

/**
 * 获取登录信息
 * @param $key
 * @return bool
 */
function login($key) {
    static $data;
    if(isset($data[$key])) return $data[$key];
    $data = session('login_session');
    if(empty($data)) return false;
    $data = unserialize($data);
    return isset($data[$key]) ? $data[$key] : false;
}

/**
 * 验证逻辑权限
 * @param $item
 * @return bool
 */
function power($item, $show_error = false) {
    static $menu = array();
    if(empty($menu)) {
        $menu = login('menu');
    }
    if(login('is_admin')) return true;
    $result = false;
    if(is_array($item)) {
        foreach($item as $key => $sitem) {
            if(in_array(strtolower($sitem), $menu)) {
                $result = true;
                break;
            }
        }
    }else {
        $result = in_array(strtolower($item), $menu);
    }
    if(!$result && $show_error) {
        if(IS_AJAX) {
            $data['info']   =   L('_NOT_ACCESS_');
            $data['status'] =   0;
            exit(json_encode($data));
        }else {
            $view = \Think\Think::instance('Think\View');
            $view->assign('msgTitle', L('_OPERATION_FAIL_'));
            $view->assign('status', 0);   // 状态
            $view->assign('error', L('_NOT_ACCESS_'));// 提示信息
            $view->display(C('TMPL_ACTION_ERROR'));
        }
        exit;
    }
    return $result;
}