<?php
/**
 * ====================================
 * 配置管理
 * ====================================
 * Author: Hugo
 * Date: 14-5-20 下午9:58
 * ====================================
 * File: SettingController.class.php
 * ====================================
 */
namespace Manage\Controller;
use Common\Controller\ManageController;

class ConfigController extends ManageController{
    public function group() {
        $list = $this->dbModel->where(array('locked' => 0))
            ->field('id,name,title,extra,value,remark,type, group')
            ->order('orderby')
            ->select();
        if($list) {
            $config = array();
            foreach($list as $row){
                $config[$row['group']][] = $row;
            }
            $this->assign('list', $config);
        }
        $this->display();
    }

    public function group_save($config) {
        if($config && is_array($config)){
            foreach ($config as $name => $value) {
                $map = array('name' => $name);
                $this->dbModel->where($map)->setField('value', $value);
            }
        }
        S('DB_CONFIG_DATA',null);
        $this->success(L('SAVE_SUCCESS'));
    }
}