<?php
return array(
    'GOODS_SN_LOST'     => '货号必填！',
    'GOODS_NAME_LOST'   => '商品名称必填！',
    'CAT_ID_LOST'       => '请选择分类！',
    'GOODS_SN_EXISTS'   => '货号已存在！',

    'MARKET_PRICE_LOST'      => '市场价格必填',
    'SHOP_PRICE_LOST'        => '本店价格必填',
    'GRADE_PRICE_LOST'       => '代理价格必填',

    'CANNOT_BE_PACKAGE'      => '该商品不能作为套装！',
    'RELATION_GOODS_LOST'    => '套装关联商品不能为空！',

    'GOODS_LOST'		=> '请选择生成二维码的商品！',
    'GEN_NUMBER_LOST'	=> '请填写二维码数量！',
    'CANNOT_OPEN_FILE'  => '无法打开二维码文件！',
    'CANNOT_WRITE_FILE'	=> '无法写入二维码文件！',

    //页头导航标题
    'goods_index'       => '商品列表',
);