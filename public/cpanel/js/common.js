/**
 * 创建按钮
 * @param attributes
 * @returns {string}
 */
function button(attributes)
{
    var button_string = '<span title="[title]" class="images-btn [icon]" onclick="[method]([value], this)">[title]</span>';
    var out_string = [];
    for(var i = 0; i < attributes.length; i++){
        var item = attributes[i];
        var tmp_string = button_string
            .replace(/\[title\]/g, item.title)
            .replace(/\[icon\]/g, item.icon)
            .replace(/\[method\]/, item.method)
            .replace(/\[value\]/, item.value);
        out_string.push(tmp_string);
    }
    return out_string.join(' ');
}

/**
 * 设置对象不可用
 * @param element
 */
function disable(element){
    $(element).linkbutton('disable');
}

/**
 * 设置对象可用
 * @param element
 */
function enable(element){
    $(element).linkbutton('enable');
}

/**
 * 返回是或否
 * @param value
 * @returns {string}
 */
function displayYesNo(value){
    return value == 1 ? '<font color="red">是</font>' : '否';
}

/**
 * 显示性别
 * @param value
 * @returns {*}
 */
function displaySex(value){
    var sexData = [{sexname: '男',sex: '1'},{sexname: '女',sex: '2'}];
    for(var i = 0; i < sexData.length; i++){
        if (sexData[i].sex == value) return sexData[i].sexname;
    }
    return value;
}

function date_formatter(date) {
    var y = date.getFullYear();
    var m = date.getMonth() + 1;
    var d = date.getDate();
    return y + '-' + (m < 10 ? ('0' + m) : m) + '-'
        + (d < 10 ? ('0' + d) : d);
}

function date_parser(s) {
    if (!s)
        return new Date();
    var ss = (s.split('-'));
    var y = parseInt(ss[0], 10);
    var m = parseInt(ss[1], 10);
    var d = parseInt(ss[2], 10);
    if (!isNaN(y) && !isNaN(m) && !isNaN(d)) {
        return new Date(y, m - 1, d);
    } else {
        return new Date();
    }
}

/**
 * 创建随机密钥
 * @param prefix
 * @returns {string}
 */
function createKey(prefix, n) {
    var chars = ['0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
    var res = "";
    for(var i = 0; i < n ; i ++) {
        var id = Math.ceil(Math.random()*35);
        res += chars[id];
    }
    if(typeof prefix == 'string') {
        res = prefix + res;
    }
    return res;
}

/**
 * 初始化图片上传
 * @param setObj
 */
function initUploadImage(setObj,id) {
    var uploadUeId = 'uploadUe' + Math.random();
    var platObj = $('<script type="text/plain" id="'+uploadUeId+'"></script>');
    if($(document.body).find(platObj).length <= 0) {
        $(document.body).append(platObj);
    }
    var uploadUe = UE.getEditor(uploadUeId);
    uploadUe.ready(function () {
        uploadUe.hide();
    });
    uploadUe.addListener('beforeInsertImage', function (t, args) {
        $(setObj).textbox('setValue', args[0].src);
        if(function_exists('updateImage')){
            updateImage(args[0].src,id);
        }
    });
    $(setObj).textbox({
        onClickButton: function() {
            var dlg = uploadUe.getDialog("insertimage");
            dlg.render();
            dlg.open();
        }
    });
}

function region(provinceObj, cityObj, districtObj) {
    var url = '/home/region/query.html';
    provinceObj.combobox({
        url: http_build_query(url, {parent_id: 1}),
        valueField: 'region_id',
        textField: 'region_name',
        onChange: function(selValue){
            cityObj.combobox('setValue', '');
            cityObj.combobox('reload', http_build_query(url, {parent_id: selValue}));
        }
    });
    cityObj.combobox({
        valueField: 'region_id',
        textField: 'region_name',
        onChange: function(selValue){
            districtObj.combobox('setValue', '');
            districtObj.combobox('loadData', []);
            if(selValue > 0)
                districtObj.combobox('reload', http_build_query(url, {parent_id: selValue}));
        }
    });
    if(provinceObj.val() > 0) {
        cityObj.combobox({
            url: http_build_query(url, {parent_id: provinceObj.val()})
        })
    }
    districtObj.combobox({
        valueField: 'region_id',
        textField: 'region_name'
    });
    if(cityObj.val() > 0) {
        districtObj.combobox({
            url: http_build_query(url, {parent_id: cityObj.val()})
        })
    }
}
