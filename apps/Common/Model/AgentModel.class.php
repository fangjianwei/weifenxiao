<?php
/**
 * ====================================
 * 代理商模型
 * ====================================
 * Author: lwh
 * Date: 14-11-24 上午9:28
 * ====================================
 * File: AgentModel.class.php
 * ====================================
 */
namespace Common\Model;

use Common\Library\Common;

class AgentModel extends CommonModel
{

    protected $_auto = array(
        array('update_time', 'date', self::MODEL_UPDATE, 'function', 'Y-m-d H:i:s'),
        array('mobile', 'crypt_phone', self::MODEL_BOTH, 'function'),
    );

    protected $_validate = array(
        array('contact', 'require', '{%contact_lost}', self::MUST_VALIDATE),
        array('grade_id', 'require', '{%grade_id_lost}', self::EXISTS_VALIDATE),
        array('grade_id', 'require', '{%grade_id_lost}', self::EXISTS_VALIDATE),
        array('sex', 'require', '{%sex_lost}', self::MUST_VALIDATE),
        array('mobile', 'require', '{%mobile_lost}', self::MUST_VALIDATE),
        array('email', 'require', '{%email_lost}', self::MUST_VALIDATE),
        array('province', 'require', '{%province_lost}', self::EXISTS_VALIDATE),
        array('city', 'require', '{%city_lost}', self::EXISTS_VALIDATE),
        array('district', 'require', '{%district_lost}', self::EXISTS_VALIDATE),
        array('wecha_name', 'require', '{%wecha_name_lost}', self::MUST_VALIDATE),
        array('id_number', 'require', '{%id_number_error}', self::EXISTS_VALIDATE),
        array('address', 'require', '{%address_lost}', self::EXISTS_VALIDATE),
        array('advantage', 'require', '{%advantage_lost}'),
        array('mobile', 'isMobile', '{%mobile_error}', self::MUST_VALIDATE, 'callback', self::MODEL_BOTH),
        array('email', 'email', '{%email_error}', self::MUST_VALIDATE, 'regex', self::MODEL_BOTH),
        array('email', '', '{%email_exists}', self::MUST_VALIDATE, 'unique', self::MODEL_BOTH),
        array('wecha_name', '', '{%wecha_name_exists}', self::MUST_VALIDATE, 'unique', self::MODEL_BOTH),
        array('id_number', '', '{%id_number_exists}', self::EXISTS_VALIDATE, 'unique', self::MODEL_BOTH),
        array('mobile', 'existsMobile', '{%mobile_exists}', self::MUST_VALIDATE, 'callback', self::MODEL_BOTH),
    );

    /**
     * 判断 是否手机号码
     * @param $mobile
     * @return bool
     */
    public function isMobile($mobile) {
        if (strpos($mobile, '*') !== false) return true;
        if (Common::isMobile($mobile)) return true;
        return false;
    }

    /**
     * 判断电话号码是否存在
     * @param $mobile
     * @return bool
     */
    public function existsMobile($mobile) {
        $cryptMobile = crypt_phone($mobile);
        return $this->where('mobile =' . $cryptMobile . ' AND user_id <> ' . USER_ID)->find() ? false : true;
    }

    public function filter($params = array()) {
        $this->alias(' AS a')
            ->join('LEFT JOIN __AGENT_GRADE__ as mg on a.grade_id=mg.grade_id')
            ->join('LEFT JOIN __MASTER__ as m on a.follow_master=m.user_id')
            ->field('a.user_id, a.user_no, a.contact, a.wecha_name, a.sex, a.agent_code, a.email,a.mobile, a.create_time, a.agent_type, a.data_from, a.data_type, mg.grade_name, concat_ws("-", m.real_name, m.user_name) as real_name')
            ->order('a.create_time desc');

        $where = array();

        if ($params['alone']) {
            $where['a.follow_master'] = USER_ID;
        } else{
            if($params['follow_master'] > 0) {
                $where['a.follow_master'] = $params['follow_master']== 1 ? 0 : array('neq', 0);
            }
        }

        if(isset($params['agent_type'])) {
            if(is_array($params['agent_type'])) {
                $where['a.agent_type'] = array('IN', $params['agent_type']);
            }elseif($params['agent_type'] > -1) {
                $where['a.agent_type'] = (int)$params['agent_type'];
            }
        }


        $params['grade_id'] <= 0        or $where['a.grade_id'] = (int)$params['grade_id'];
        //通过身份证查询
        empty($params['wecha_name'])    or $where['a.wecha_name']  = $params['wecha_name'];
        empty($params['id_number'])     or $where['a.id_number'] = $params['id_number'];
        empty($params['user_no'])       or $where['a.user_no'] = $params['user_no'];
        empty($params['contact'])       or $where['a.contact'] = array('LIKE', "%{$params['contact']}%");
        empty($params['email'])         or $where['a.email'] = trim($params['email']);
        empty($params['mobile'])        or $where['a.mobile'] = Common::isMobile($params['mobile']) ? crypt_phone($params['mobile']) : $params['mobile'];
        empty($params['agent_code'])    or $where['a.agent_code'] = strip_tags(trim($params['agent_code']));
        !isset($params['parent_id'])    or $where['a.parent_id'] = (int)$params['parent_id'];
        $order = 'a.create_time desc';
        if($params['sort']) {
            $order = 'a.' . $params['sort'] . ' ' . $params['order'];
        }

        return $this->order($order)->where($where);
    }


    public function format($data) {
        set_lang('agent');
        $data_from = L('data_from');
        $agent_type = L('agent_type');
        foreach ($data['rows'] as $key => $row) {
            $row['mobile'] = decrypt_phone($row['mobile']);
            $row['data_from_text'] = $data_from[$row['data_from']];
            $row['agent_type_text'] = $agent_type[$row['agent_type']];
            $data['rows'][$key] = $row;
        }
        return $data;
    }

    //查找需要下载的数据
    public function downList()
    {
        //统计总记录数
        $options = $this->options;

        //排序并获取记录
        $options['order'] = empty($options['order']) ? 'a.create_time desc' : $options['order'];
        $this->options = $options;
        $rows = $this->select();
        return $rows;
    }


    /**
     * excel下载
     * @param array $data 下载的数据
     * @param array $title 标题
     * @param string $fileName 文件名
     * @return array
     */
    public function download($data = array(), $title = array(), $fileName = "report")
    {
        header("Content-Type: application/vnd.ms-execl");
        header("Content-Disposition: attachment; filename=" . $fileName . ".xls");
        header("Pragma: no-cache");
        header("Expires: 0");
        if (!empty($title)) {
            foreach ($title as $k => $v) {
                $title[$k] = iconv("UTF-8", "GB2312", $v);
            }
            $title = implode("\t", $title);
            echo "$title\n";
        }
        if (!empty($data)) {
            foreach ($data as $key => $val) {
                foreach ($val as $ck => $cv) {
                    $data[$key][$ck] = iconv("UTF-8", "GB2312", $cv);
                }
                $data[$key] = implode("\t", $data[$key]);
            }
            echo implode("\n", $data);
        }
    }

    public function getInfo($user_id) {
        $info = $this->alias('a')
            ->join('LEFT JOIN __AGENT_GRADE__ AS m ON m.grade_id = a.grade_id')
            ->field('a.*,m.grade_name')
            ->find($user_id);

        if($info) {
            $where = array(
                'region_id' => array('in', array(
                    $info['province'],
                    $info['city'],
                    $info['district']
                ))
            );
            $region = $this->table('__REGION__')->where($where)->getField('region_id, region_name');
            $info['province_name'] = $region[$info['province']];
            $info['city_name'] = $region[$info['city']];
            $info['district_name'] = $region[$info['district']];

            $info['mobile'] = decrypt_phone($info['mobile']);
        }

        return $info;

    }

    /**
     * 生成授权码
     * @param $user_id
     * @return null|string
     */
    public function createCode($user_id)
    {
        $data = $this->find($user_id);
        if (empty($data)) return null;
        if($data['grade_id']) {

        }
        $grade_code = $this->table('__AGENT_GRADE__')->where(array('grade_id' => $data['grade_id']))->getField('grade_code');
        return $grade_code . sprintf('%06d', $user_id);
    }

    /**
     * 管理员操作代理商日志
     * @param $userId
     * @param $adminId
     * @param $note
     * @return bool
     */
    public function addAgentLog($userId, $adminId, $note){
        $db = M('AgentLog');
        $db->data(array(
            'user_id' => $userId,
            'master_id' => $adminId,
            'action_note' => $note,
            'create_time' => date(C('DATE_FORMAT'))
        ));
        $result = $db->add();
        return $result ? true : false;
    }

    /**
     * 获取订单日志
     * @param $orderId
     * @return mixed
     */
    public function getLog($userId) {
        $data = $this->table('__AGENT_LOG__')
            ->where(array('user_id' => $userId))
            ->order('create_time desc')
            ->select();

        $masterModel = new MasterModel();
        if($data) {
            foreach($data as $key => $row) {
                $row['master_name'] = empty($row['master_id']) ? L('CUSTOM') : $masterModel->where(array('user_id' => $row['master_id']))->getField('real_name');
                $data[$key] = $row;
            }
        }
        return $data;
    }

    /**
     * 重置上级ID及集合
     * @param $patent_id
     * @param $user_id
     */
//    public function updateParentId($user_id,$parent_id){
//        if(empty($user_id) || empty($parent_id)) return false;
//        $supArr = $this->getSuperiors($parent_id);
//        $parentSet = implode(',',$supArr);
//        if($parentSet){
//            $data['parent_set'] = $parent_id.','.$parentSet;
//        }else{
//            $data['parent_set'] = $parent_id;
//        }
//        $data['parent_id'] = $parent_id;
//        if($this->where(array('user_id' => $user_id)) -> save($data) ===  false){
//            return false;
//        }else{
//            return true;
//        }
//    }

    /**
     * 获取上级代理
     * @param $user_id
     * @return array|bool
     */
//    public function getSuperiors($parent_id){
//        static $parentArr = array();
//        if(empty($parent_id)) return false;
//        $supId = $this->where(array('user_id' => $parent_id,'status'=>array('NOT IN ',array(AG_CANCEL,AG_UN_AUTH))))->getField('parent_id');
//        if($supId){
//            $parentArr[] = $supId;
//            $this->getSuperiors($supId);
//        }
//        return $parentArr;
//    }

    /**
     * 检测是否属于下级
     * @param $itemId
     * @param int $user_id
     * @return bool
     */
    public function checkChildAgent($itemId,$user_id = USER_ID){
        if(empty($itemId)) return false;
        if($itemId == $user_id){
            return true;
        }
        $parentId = $this->where(array('user_id' => $itemId,))->getField('parent_id');
        if($parentId == $user_id){
            return true;
        }else{
            return false;
        }
    }

}