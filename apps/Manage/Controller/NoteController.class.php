<?php
/**
 * 留言管理
 * User: 91336
 * Date: 2015/3/9
 * Time: 17:11
 */
namespace Manage\Controller;

use Common\Controller\ManageController;
use Common\Library\Common;

class NoteController extends ManageController {

	public function download()
	{
		if (IS_POST) {
			
			$type = I('post.export_type',0,'intval');
			if (!in_array($type, array(1,2,3))) {
				$this->error(L('Please_select_condition'));
			}		
			
			$params = I('post.');
			
			$where = array();
			
			switch ($type) {
				case 1:					
					$where['create_time'] = array('between',date('Y-m-d 00:00:00,Y-m-d 23:59:59'));
					break;
				case 2:
					break;
				default:
					!empty($params['begindate']) && $params['begindate'] = substr($params['begindate'], 0,10).' 00:00:00';
					!empty($params['enddate']) && $params['enddate'] = substr($params['enddate'], 0, 10).' 23:59:59';
				    if (!empty($params['begindate']) && !empty($params['enddate'])){
			        	$where['create_time'] = array('between',$params['begindate'].','.$params['enddate']);   
			        } else {
			            !empty($params['begindate']) && $where['create_time'] = array('egt',$params['begindate']);
			            !empty($params['enddate']) && $where['create_time'] = array('elt',$params['enddate']);
			        }
					!empty($params['mobile']) && $where['mobile'] = array('eq',$params['mobile']);
			}
			
			$list = M('Note')->field('mobile,note,create_time')->where($where)->select();
			if (empty($list)) {
				$this->error(L('No_export_data'));
			}
			
			$data['title'] = L('NOTE_EXCEL');
			$data['data'] = $list;
			if(export_data($data)){  //保存Excel
				exit();
			}else{
				$this->error(L('The_export_failed'));
			}			
		}
	}

	public function reset_phone() {
		$list = M('Note')->field('mobile,id')->select();
		if($list) {
			foreach($list as $row) {
				if(Common::isMobile($row['mobile'])) {
					M('Note')->save(array('id' => $row['id'], 'mobile' => crypt_phone($row['mobile'])));
				}
			}
		}
	}

	public function reset_phone2() {
		$list = M('Member')->field('mobile,user_id')->where("user_id <= 4290")->select();
		if($list) {
			foreach($list as $row) {
				$mobile = $row['mobile'];
				if(Common::isMobile($mobile)) {
					$mobile = crypt_phone($mobile);
				}else {
					$mobile = decrypt($mobile);
					$mobile = crypt_phone($mobile);
				}
				M('Member')->save(array('user_id' => $row['user_id'], 'mobile' => $mobile));
			}
		}
	}
}