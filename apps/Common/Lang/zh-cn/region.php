<?php
return array(
    'region_name_lost'          => '请输入地区名称！',
    'region_name_exists'        => '地区名称已存在！',
    'add_region'                => '添加地区：',
    'edit_region'               => '编辑地区：',
    'delete_region'             => '删除了地区：',
     //页头导航标题
    'region_index'              => '收货地址',
    'region_edit'               => '地址编辑',
    //提示
    'edit_error'                => '链接地址错误，请检查！',
    'contact_error'             => '请填写收货人！',
    'mobile_errors'             => '请填写正确的手机号码！',
    'pcd_error'                 => '请填写完整的省市区！',
    'address_error'             => '请填写详细地址！',
    'save_success'              => '恭喜您保存成功！',
    'save_error'                => '保存失败，内容未更新或网络原因！',
    'delete_lost'               => '请选择要删除的地址！',
    'delete_success'            => '删除地址成功！',
    'delete_error'              => '删除地址失败，请勿非法操作！',
    'edit_illegality'           => '请勿非法提交数据！',

);