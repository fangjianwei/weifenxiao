<?php
/**
 * Created by PhpStorm.
 * User: 9009078
 * Date: 2016/5/23
 * Time: 13:55
 */
namespace Home\Controller;
use Common\Controller\StageController;
use Common\Model\FinanceModel;

class FinanceController extends StageController
{
    public function _initialize()
    {
        parent::_initialize();
        $this->db = new FinanceModel();
    }

    /**
     * 财务明细-全部
     */
    public function finance_detail(){
        $this->display();
    }

    /**
     * 财务明细-数据异步加载
     */
    public function get_finance_detail(){
        $data = $this->db->get_finance_detail();
//        $data = 123;
        echo json_encode($data);
    }

}