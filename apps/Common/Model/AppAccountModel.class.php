<?php
/**
 * ====================================
 * 这里是说明
 * ====================================
 * Author: 91336
 * Date: 2014/12/25 13:26
 * ====================================
 * File: AppAccountModel.class.php
 * ====================================
 */
namespace Common\Model;
class AppAccountModel extends CommonModel {
    
    /*
     * 获取列表
     * @param array $params
     * @return mixed
     */
    public function filter($params){
        if(isset($params['keywords'])){
            $where = array();
            $where['app_id'] = $params['keywords'];
            $where['app_name'] = array('LIKE', "%{$params['keywords']}%");
            $where['app_key'] = array('LIKE', "%{$params['keywords']}%");
            $where['app_secret'] = array('LIKE', "%{$params['keywords']}%");
            $where['_logic'] = "OR";
            return $this->where($where);
        }
    }

}