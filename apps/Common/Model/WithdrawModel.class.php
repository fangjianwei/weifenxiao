<?php
/**
 * Created by PhpStorm.
 * User: 9009078
 * Date: 2016/5/20
 * Time: 15:04
 */
namespace Common\Model;

use Common\Library\Common;
use Common\Model\WithdrawTypeModel;

class WithdrawModel extends CommonModel {

    /**
     * 新增提现记录，并调用提现接口
     * @return array
     */
    public function withdraw_action(){
        $this->db_withdraw_type = new WithdrawTypeModel();

        $params = I("request.");
        $allow_withdraw_money = 100;    //可提现金额
        $data_member_default_withdraw = $this->db_withdraw_type->get_default_withdraw_type();   //获取用户默认提现方式
        $payment_fee = $data_member_default_withdraw['payment_fee'];
        if(!$params['blance']){
            return USER_WITHDRAW_BLANCE_EMPTY;      //提现金额为空
        }elseif(!preg_match("/^(([0-9]+\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\.[0-9]+)|([0-9]*[1-9][0-9]*))$/",$params['blance'])){
            return USER_WITHDRAW_BLANCE_ERR;      //提现金额格式有误
        }elseif(($params['blance']+$params['blance']*$payment_fee/100)>$allow_withdraw_money){
            return USER_WITHDRAW_MONEY_NOTENOUGH;      //提现金额不足
        }
        //调用微信企业付款给用户接口，并记录结果到数据库member_withdraw_log
        //目前没支付接口，此处代码暂时无法实现，

        return USER_WITHDRAW_SUC;
    }
}