<?php
/**
 * Created by PhpStorm.
 * User: 9009078
 * Date: 2016/5/20
 * Time: 15:01
 */
namespace Home\Controller;
use Common\Library\Sms;
use Common\Controller\StageController;
use Common\Model\WithdrawModel;
use Common\Model\WithdrawTypeModel;

class WithdrawController extends StageController
{
    public function _initialize()
    {
        parent::_initialize();
        $this->db_withdraw = new WithdrawModel();
        $this->db_withdraw_type = new WithdrawTypeModel();
    }

    /**
     * 去提现
     */
    public function withdraw() {
        /*if(!defined('TRUENAME') || !defined('MOBILE')){
            $redirect_uri = 'http://'.$_SERVER["SERVER_NAME"].'/home/member/bound';
            header("Content-type: text/html; charset=utf-8");
            echo '<script>alert("请先完善好个人信息");location.href="'.$redirect_uri.'"</script>';
            exit;
        }*/
        $allow_withdraw_money = 100;    //可提现金额
        $data_member_default_withdraw = $this->db_withdraw_type->get_default_withdraw_type();   //获取用户默认支付方式
        $this->assign('allow_withdraw_money',$allow_withdraw_money);
        $this->assign('data_member_default_withdraw',$data_member_default_withdraw);
        $this->display();
    }

    public function send_codesms(){
        $params = I("request.");
        $content = rand(100000,999999); //验证码
        $CodeSms = new Sms\CodeSms();
        $result = $CodeSms->send_sms($params['mobile'],$content);
        echo json_encode($result);
    }

    /**
     * 去提现--数据处理
     */
    public function withdraw_action() {
        $data = $this->db_withdraw->withdraw_action();
        echo json_encode($data);
    }

}