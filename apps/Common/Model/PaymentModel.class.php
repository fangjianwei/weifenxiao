<?php
/**
 * 支付方式
 * User: 91336
 * Date: 2015/1/5
 * Time: 16:27
 */
namespace Common\Model;
class PaymentModel extends CommonModel {
    
    /*
     * 获取列表
     * @param array $params
     * @return mixed
     */
    public function filter($params){
        if(isset($params['keywords'])){
           $where = array();
            $where['pay_id'] = $params['keywords'];
            $where['pay_code'] = array('LIKE', "%{$params['keywords']}%");
            $where['pay_name'] = array('LIKE', "%{$params['keywords']}%");
            $where['_logic'] = 'OR';
            return $this->where($where);
        }
    }
}