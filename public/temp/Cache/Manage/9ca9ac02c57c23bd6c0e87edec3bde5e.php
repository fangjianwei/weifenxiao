<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>后台管理系统</title>
    <script type="text/javascript" src="/public/static/base.js"></script>
    <script type="text/javascript" src="/public/cpanel/js/easyui.js"></script>
</head>
<body>
<div id="toolbar">
    <table class="form-panel">
        <tr>
            <td width="100%">
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="openWindow(this, '')">添加</a>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-ok" plain="true" onclick="lockItem(this, 1)">上架</a>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-no" plain="true" onclick="lockItem(this, 0)">下架</a>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="onEdit(this)">编辑</a>
                <input name="gen_number" id="gen_number" class="easyui-textbox" data-options="buttonText:'生成二维码',prompt:'请输入数量',onClickButton: function(){genQrcode()}">
                <?php if(power('goods-realityOccupt')): ?><a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-reload" plain="true" onclick="onSync()">同步占用库存</a><?php endif; ?>
                <?php if(power('goods-stocklog')): ?><a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-search" plain="true" onclick="onControl()">库存操作记录</a><?php endif; ?>
                <?php if(power('goods-implode')): ?><form name="upload" action ="<?php echo U('upload');?>" method="post" enctype="multipart/form-data" target="upload">
                        产品导入：<input class="easyui-filebox" name="goods_data" buttonText="选择文件">
                        <input id="type" name="type"  class="easyui-combobox" data-options="valueField:'id',textField:'text',url:'<?php echo U('getChannel');?>'" />
                        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-undo" plain="true" onclick="onImplode(this)">导入</a>
                   </form><?php endif; ?>
                <iframe style="display: none;" name="upload"></iframe>
               <!--  <input name="gen_number" id="gen_number" class="easyui-textbox" data-options="prompt:'输入生成数量', icons: [{
               iconCls:'icon-clear',
               handler: function(e){
               $(e.data.target).textbox('clear');
               }
               }]">
               <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="genQrcode()">生成二维码</a> -->
            </td>
            <td>
                <input id="cat_id"  class="easyui-combobox" data-options="valueField:'id',textField:'text',url:'<?php echo U('getChannel');?>',prompt:'请选择渠道...'" />
            </td>
            <td>
                <input id="keyword" class="easyui-textbox" data-options="buttonText:'SEARCH',prompt:'请输入关键词...',onClickButton: function(){onSearch();}" style="width:250px;">
            </td>
        </tr>
    </table>
</div>
<script type="text/javascript" src="/public/cpanel/js/easyui/datagrid-cellediting.js"></script>
<table id="grid">
    <thead>
    <tr>
        <th data-options="field:'ck',checkbox:true">查看</th>
        <th data-options="field:'goods_id',width:50" >商品ID</th>
        <th data-options="field:'cat_name',width:80">所属分类</th>
        <th data-options="field:'is_package',width:40,formatter:function(value){if(value == '0') return '否';return '<font color=red>是</font>';}">是否套装</th>
        <th data-options="field:'goods_name',width:120">商品名称</th>
        <th data-options="field:'goods_sn',width:50,editor:'text'">货号</th>
        <th data-options="field:'market_price',width:50">市场价</th>
        <th data-options="field:'shop_price',width:50">本店售价</th>
        <th data-options="field:'goods_stock',width:50,editor:'text'">库存</th>
        <th data-options="field:'occupy',width:50">占用库存</th>
        <th data-options="field:'shipment',width:50">发货数</th>
        <th data-options="field:'limit_buy',width:50">起购数量</th>
        <th data-options="field:'update_time',width:60, align:'center'">更新时间</th>
        <th data-options="field:'is_on_sale',width:50,formatter: function(value){
                if(value == '0') return '<font color=red>下架</font>';
                return '上架';
            }" sortable="true">状态</th>
    </tr>
    </thead>
</table>
<script type="text/javascript">
    var grid = $('#grid');
    grid.datagrid({
        url: "<?php echo U();?>",
        fit: true,
        toolbar: '#toolbar',
        border: false,
        pagination: true,
        fitColumns: true,
        singleSelect:true,
        rownumbers:true,
        selectOnCheck:false,
        checkOnSelect: false,
        onAfterEdit:function(rowIndex, rowData, changes){
            if(typeof(changes.goods_sn) == 'undefined' && typeof(changes.goods_stock)=='undefined'){
                return false;
            }

            easyui.ajaxCall('<?php echo U("save");?>', {goods_id:rowData.goods_id, goods_sn:changes.goods_sn, goods_stock: changes.goods_stock},function(result){
                try{
                    if(result.info){
                        alert(result.info);
                    }
                }catch (e){}
            });
        }
    });
  	grid.datagrid('enableCellEditing').datagrid('gotoCell');
  	
    function openWindow(element, row){
        blur(element);
        parent.openTab(null, '产品表单'+(row ? ' :' + row.goods_name : ''), http_build_query("<?php echo U('form');?>", {id: row.goods_id}), true);
    }
	
    function onEdit(element){
    	blur(element);
    	var rows = grid.datagrid('getChecked');
    	item = rows[0];
        if(empty(item)){
            easyui.alert('请勾选需要编辑的商品！');
            return false;
        }
        openWindow('#grid',item);
    }


    function lockItem(element, saled){
        blur(element);
        var rows = grid.datagrid('getChecked');
        var items = [];
        for(var i = 0; i < rows.length; i++){
            items.push(rows[i].goods_id);
        }
        items = items.join(',');
        if(empty(items)){
            easyui.alert('请选择需要操作的选项！');
            return false;
        }
        easyui.confirm('请再次确认是否需要继续进行您的操作!', function(){
            easyui.ajaxCall('<?php echo U("sale");?>', {items: items, sale : saled}, function(result){
                try{
                    if(!result.status){
                        easyui.alert(result.info);
                    }
                    grid.datagrid('reload');
                }catch (e){}
            });
        });
    }

    function onSearch(element){
        blur(element);
        grid.datagrid({
            queryParams: {
                keywords: $('#keyword').val(),
                cat_id: $('#cat_id').combobox('getValue')
            }
        });
    }

    function setStatus(element, locked){
        blur(element);
        var rows = grid.datagrid('getChecked');
        var items = [];
        for(var i = 0; i < rows.length; i++){
            items.push(rows[i].id);
        }
        items = items.join(',');
        if(empty(items)){
            easyui.alert('请选择需要操作的选项！');
            return false;
        }
        easyui.confirm('请再次确认是否需要继续进行您的操作!', function(){
            easyui.ajaxCall('<?php echo U("lock");?>', {items: items, locked: locked}, function(result){
                try{
                    if(!result.status){
                        easyui.alert(result.info);
                    }
                    grid.datagrid('reload');
                }catch (e){}
            });
        });
    }

    function genQrcode(){
        var rows = grid.datagrid('getChecked');
        if (rows.length > 1) {
            easyui.alert('一次只能生成一个商品的二维码！');
            return false;
        }
        if (rows.length == 0) {
            easyui.alert('请选择需要操作的选项！');
            return false;
        };
        var gen_number = $('#gen_number').val();

        if (isNaN(gen_number) || gen_number <= 0 || gen_number == '') {
            easyui.alert('请填写生成二维码的数量！');
            return false;
        };
        var href = "<?php echo U('qrcode');?>"+'?'+'goods_id='+rows[0].goods_id+'&gen_number='+gen_number;
        location.href = href;
    }

    function onSync(){
       $.messager.progress({
            title:'请等待...',
            msg:'正在同步中...',
            text:'',
            interval:300
        });
            $.post('<?php echo U("realityOccupt");?>', function(result){
                try{
                    if(!result.status){
                        $.messager.progress('close');
                        easyui.alert(result.info);

                    }
                    grid.datagrid('reload');
                }catch (e){}
            });
    }

    function onControl(){
        var user = grid.datagrid('getSelected');
        var opts = {
            href: http_build_query('<?php echo U("stocklog");?>'),
            title: '库存操作记录',
            width: 1200,
            height: 500
        };
        easyui.dialog(opts);
    }

    function onImplode(element){
        blur(element);
        var file = $('input[name=goods_data]').val();
        var type = $('#type').combobox('getValue');
        if(file == ''){
            easyui.alert('请选择文件!');
            return false;
        }
        if(type == ''){
            easyui.alert('请选择渠道类型！');
            return false;
        }
        $('form[name=upload]').submit();
    }
    function meserror(message){
        easyui.alert(message);
    }
</script>
</body>
</html>