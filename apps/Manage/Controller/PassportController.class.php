<?php
/**
 * ====================================
 * 登录管理
 * ====================================
 * Author: Hugo
 * Date: 14-5-20 下午9:58
 * ====================================
 * File: PassportController.class.php
 * ====================================
 */
namespace Manage\Controller;
use Common\Library\Config;
use Common\Library\Manage\Master;
use Common\Model\MasterModel;
use Common\Model\MenuModel;
use Think\Controller;

class PassportController extends Controller
{
    public function _initialize() {
        Config::init();
    }

    //登录首页
    public function index() {
        layout(false);
        $this->display('Index:login');
    }

    public function login() {
        $params = I('post.');
        $params['login_name'] or $this->error(L('login_name_lost'));
        $params['login_password'] or $this->error(L('login_password_lost'));

        $masterModel = new MasterModel();
        //验证表单
        $masterModel->autoCheckToken($params) or $this->error(L('_TOKEN_ERROR_'));
        $where['user_name'] = strip_tags($params['login_name']);
        $data = $masterModel->where($where)->find();

        //管理员不存在
        $data or $this->error(L('user_not_exists'));

        //密码不正确
        $data['password'] == md5(md5($params['login_password']) . C('CRYPT_KEY')) or $this->error(L('password_error'));
        unset($data['password']);
        //已被锁定
        !$data['locked'] or $this->error(L('not_allow_login'));

        $data['menu'] = Master::getMenu($data['role_id']);//读取菜单权限

        $data['login_time'] = date(C('DATE_FORMAT'));
        session('login_session', serialize($data));

        $update = array(
            'session_key' => md5(session_id() . $data['login_time']),
            'last_login_time' => $data['now_login_time'],
            'now_login_time' => $data['login_time'],
            'last_login_ip' => $data['now_login_ip'],
            'now_login_ip' => get_client_ip()
        );
        $masterModel->data($update)->where("user_id = %d", intval($data['user_id']))->save();
        Master::log($data['user_id'], L('have_login'));
        $this->success();
    }

    /**
     * 退出登录
     */
    public function logout() {
        session('login_session', null);
        session_destroy();
        redirect(U('manage/index/index'));
    }
}