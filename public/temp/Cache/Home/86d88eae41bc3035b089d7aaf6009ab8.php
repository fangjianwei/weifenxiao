<?php if (!defined('THINK_PATH')) exit();?><div>
    <div class="detail-box"><div class="cj-hd">测1测 • 你是哪种类型暗沉肌？</div></div>
    <div class="w100"><img src="/public/stage/mobile/images_temp/img_01.jpg" alt="" /></div>
    <div class="w100"><img src="/public/stage/mobile/images_temp/img_02.jpg" alt="" /></div>
    <div class="w100">
        <ul>
            <li class="w_li"><span class="wli_tit">1</span>大油田"，有痘痘、粉刺伴随  —  <span class="wli_aft">油浊黑</span></li>
            <li class="w_li"><span class="wli_tit">2</span>超越30岁，细纹、干纹陆续浮现  —  <span class="wli_aft">熟龄黑</span></li>
            <li class="w_li"><span class="wli_tit">3</span>家族遗传，天生皮肤黑色素偏多  —  <span class="wli_aft">遗传黑</span></li>
            <li class="w_li"><span class="wli_tit">4</span>日晒、辐射导致的色素沉着  —  <span class="wli_aft">色素黑</span></li>
            <li class="w_li"><span class="wli_tit">5</span>色斑、过敏、污染等化学物损伤  —  <span class="wli_aft">受损黑</span></li>
            <li class="w_li"><span class="wli_tit">6</span>气血不足，代谢缓慢，病理性的暗沉  —  <span class="wli_aft">病理黑</span></li>
        </ul>
    </div>
    <div class="detail-box"><div class="cj-hd">颜值界大咖 李治廷为韩国瓷肌代言</div></div>
    <div class="w100" style="overflow:hidden;">
        <p class="dk_L fl"><img src="/public/stage/mobile/images_temp/img_03.jpg" alt="" /></p>
        <p class="dk_R fl">
            作为艺人，熬夜是常有的事，面对镜头，必须保持皮肤的白暂、干净、柔嫩。要出色地胜任工作，就必须把皮肤健康问题放在首位。韩国瓷肌抑黑焕白套装，安全温和、持久有效，改善“暗黄肌”效果显著。
        </p>
    </div>

    <div class="detail-box"><div class="cj-hd">PK • 超越传统美白产品功效！</div></div>
    <div class="w100"><img src="/public/stage/mobile/images_temp/img_07.jpg" alt="" /></div>
    <div class="w100"><img src="/public/stage/mobile/images_temp/img_08.jpg" alt="" /></div>

    <div class="detail-box"><div class="cj-hd">科技 • 真正让肌肤层层白！</div></div>
    <div class="w100 kj" >
        <dl>
            <dt class="w_li"><span class="wli_tit"><b>1</b></span><span class="wli_aft">Symwhite肌底净透科技，从肌底赶走暗黄</span></dt>
            <dd><p>抑制黑色素的效果，是曲酸的210倍，熊果苷的32倍。能有效改善肤色不均，降低紫外线照射肌肤引起的皮肤着色，让肌肤嫩白透亮。</p></dd>
        </dl>
        <ul>
            <li class="fir"><img src="/public/stage/mobile/images_temp/kj01_01.jpg"><br/></li>
            <li class="sec"><span>改善暗沉</span></li>
            <li class="thr"><img src="/public/stage/mobile/images_temp/kj01_02.jpg"><br/></li>
        </ul>
    </div>
    <div class="w100 kj">
        <dl>
            <dt class="w_li"><span class="wli_tit"><b>2</b></span><span class="wli_aft">提升美白度、透亮度、弹滑度：肌底净透科技</span></dt>
            <dd><p>美白因子遍布表皮、肌理、肌底、有效瓦解色素，赶走肌肤暗黄，令肌肤润白透亮。</p></dd>
        </dl>
        <ul>
            <li class="fir"><img src="/public/stage/mobile/images_temp/kj02_01.jpg"><br/></li>
            <li class="sec"><span>改善色素</span></li>
            <li class="thr"><img src="/public/stage/mobile/images_temp/kj02_02.jpg"><br/></li>
        </ul>
    </div>
    <div class="w100 kj">
        <dl>
            <dt class="w_li"><span class="wli_tit"><b>3</b></span><span class="wli_aft">隔绝黑色素形成源头，持久白皙：H2H超渗透术</span></dt>
            <dd><p>核心成分微囊深入渗透层层肌肤结构中，稳定起效。持续美白，长效抑黑，可与"美白针"媲美。</p></dd>
        </dl>
        <ul>
            <li class="fir"><img src="/public/stage/mobile/images_temp/kj03_01.jpg"><br/></li>
            <li class="sec"><span>改善粗糙</span></li>
            <li class="thr"><img src="/public/stage/mobile/images_temp/kj03_02.jpg"><br/></li>
        </ul>
    </div>
    <div class="detail-box"><div class="cj-hd">热卖 • 抑黑焕白套装销量领航！</div></div>
    <div class="rm_01">
        <ul>
            <li>•  用户信任，<span style="color:red">韩国瓷肌销量第一</span>产品</li>
            <li>•  <span style="color:red">热销847,346套</span>，用户好评保障</li>
            <li>•  中华保险<span style="color:red">品质承保</span>，安全、安心</li>
            <li>•  馨肤白美白成分，安全<span style="color:red">高效口碑赞</span></li>
        </ul>
    </div>
    <!-- 咨询和在线订购 -->
    <div class="q_onchat" >
        <p style="">美容老师1对1教你解决肌肤问题</p>
        <a href="" class="on_call" >
            <img src="/public/stage/mobile/images_temp/consult_tel.png" alt="">
        </a>
        <a href="#orderfrm" class="on_ord" >
            <img src="/public/stage/mobile/images_temp/on_zx.png" alt="">
        </a>
    </div>
    <!-- 咨询和在线订购 -->
    <div class="detail-box"><div class="cj-hd">体验 • 从未感受过的美白感觉</div></div>
    <div class="w100"><img src="/public/stage/mobile/images_temp/ty_01.jpg" alt="" /></div>
    <div class="w100"><img src="/public/stage/mobile/images_temp/ty_02.jpg" alt="" /></div>
    <div class="w100"><img src="/public/stage/mobile/images_temp/ty_03.jpg" alt="" /></div>

    <div class="detail-box"><div class="cj-hd">惊喜 • 这是值得等待的4个过程</div></div>
    <div class="w100"><img src="/public/stage/mobile/images_temp/qj_01.jpg" alt="" /></div>
    <div class="w100">
        <ul style="padding:0.2em 0.8em 0.6em;color:#000;">
            <li class="w_li"><span class="wli_tit"><b>阶段一</b></span>让你的肌肤喝饱水</li>
            <li class="w_in">缺水粗糙首先被解决，你会感到自己的皮肤水润起来，触感柔嫩，气色红润。</li>
            <li class="w_li"><span class="wli_tit"><b>阶段二</b></span>肌底开始净白全脸</li>
            <li class="w_in">黄气、暗沉等色素沉淀逐渐澄净，真正由内而外晶莹剔透，饱满肌细胞，淡化色斑。</li>
            <li class="w_li"><span class="wli_tit"><b>阶段三</b></span>肌理去黄深度水润</li>
            <li class="w_in">肌肤水嫩嫩，美白渗透肌底，让肌肤逐步如白瓷般细致，焕发出年轻肌肤的光泽。</li>
            <li class="w_li"><span class="wli_tit"><b>阶段四</b></span>表皮焕亮收紧毛孔</li>
            <li class="w_in">暗黄遁形，黑色素难以再度形成，整体气色明显改善，肌肤宛若新生。</li>
        </ul>
    </div>
    <div class="detail-box"><div class="cj-hd">天然 • 植物成分安全美白</div></div>
    <div class="w100"><img src="/public/stage/mobile/images_temp/tr_01.jpg" alt="" /></div>
    <div class="detail-box"><div class="cj-hd">5步 • 修护去黄，持久美白</div></div>
    <div class="w100 steps" style="padding-top:1em;">
        <p class="wb_L"><img src="/public/stage/mobile/images_temp/wubu_01.jpg" alt="" /></p>
        <p class="wb_R">
            <em class="wb_li"><span class="wli_tit">STEP 1</span><span class="redwb">&nbsp;清&nbsp;</span>卸妆洗颜乳</em>
            <em>彻底卸妆+深入清洁同步进行，彩妆污垢都洗净，毛孔自由呼吸，真透爽~</em>
        </p>
        <div class="clear"></div>
    </div>
    <div class="w100 steps">
        <p class="wb_L"><img src="/public/stage/mobile/images_temp/wubu_02.jpg" alt="" /></p>
        <p class="wb_R">
            <em class="wb_li"><span class="wli_tit">STEP 2</span><span class="redwb">&nbsp;透&nbsp;</span>角质调整膜</em>
            <em>透明的啫喱搓搓搓，脸上的废旧角质就被搓掉了。看得到的效果，搓完皮肤嫩嫩的！</em>
        </p>
        <div class="clear"></div>
    </div>
    <div class="w100 steps">
        <p class="wb_L"><img src="/public/stage/mobile/images_temp/wubu_03.jpg" alt="" /></p>
        <p class="wb_R">
            <em class="wb_li"><span class="wli_tit">STEP 3</span><span class="redwb">&nbsp;水&nbsp;</span>亮白肌底液</em>
            <em>一触碰到皮肤，肌底液就像被毛孔"拉"进去一样，一下就被吸光光，不黏，快从肌底白润起来吧~</em>
        </p>
        <div class="clear"></div>
    </div>
    <div class="w100 steps">
        <p class="wb_L"><img src="/public/stage/mobile/images_temp/wubu_05.jpg" alt="" /></p>
        <p class="wb_R ">
            <em class="wb_li"><span class="wli_tit">STEP 4</span><span class="redwb">&nbsp;白&nbsp;</span>五谷焕彩膜</em>
            <em>每周使用2-3次，面膜质感像雪糕那样稠密，锁住水感透白，不让美白白费功夫。</em>
        </p>
        <div class="clear"></div>
    </div>
    <div class="w100 steps">
        <p class="wb_L"><img src="/public/stage/mobile/images_temp/wubu_04.jpg" alt="" /></p>
        <p class="wb_R">
            <em class="wb_li"><span class="wli_tit">STEP 5</span><span class="redwb">&nbsp;亮&nbsp;</span>亮白精华霜</em>
            <em>只是"白"肯定不够！还要水感，通透，白里透红。精华霜都可以帮到你哦，淡淡的香味，饱含五谷精华呢！</em>
        </p>
        <div class="clear"></div>
    </div>
    <!-- 咨询和在线订购 -->
    <div class="q_onchat" >
        <p style="">美容老师1对1教你解决肌肤问题</p>
        <a href="" class="on_call" >
            <img src="/public/stage/mobile/images_temp/consult_tel.png" alt="">
        </a>
        <a href="#orderfrm" class="on_ord" >
            <img src="/public/stage/mobile/images_temp/on_zx.png" alt="">
        </a>
    </div>
    <!-- 咨询和在线订购 -->
    <div class="detail-box"><div class="cj-hd">1套 • 她们都变白啦！</div></div>
    <div class="w100 w85 w603">
        <ul style="padding-top:0.8em;">
            <li>
                <span><img src="/public/stage/mobile/images_temp/case_01.jpg"></span>
                <div style="padding:0 0.8em 0.5em;">
                    <h2 style="height:2em;line-height:2em;font-weight: normal;color:#db0742;font-size:18px;">天生黑皮肤，真的能白！</h2>
                    <p class="deta">
                        BOBO&nbsp;&nbsp;&nbsp;23岁&nbsp;&nbsp;&nbsp;遗传暗沉，毫无光泽
                    </p>
                    <p style="color:#666;padding-top:0.4em; font-size:14px;">
                        毕业工作两年了，每天面对着电脑，而且还经常性加班。皮肤真的经不住折腾，变得又干有暗沉，一下子像老了好几岁。朋友介绍用了韩国瓷肌的产品，坚持用了一套，效果非常明显，我又是青春无敌小美女了！
                    </p>
                </div>
            </li>
            <li>
                <span><img src="/public/stage/mobile/images_temp/case_02.jpg"></span>
                <div style="padding:0 0.8em 0.5em;">
                    <h2 style="height:2em;line-height:2em;font-weight: normal;color:#db0742;font-size:18px;">整个脸都均匀亮白，效果真心赞~</h2>
                    <p class="deta">雅燃&nbsp;&nbsp;&nbsp;27岁&nbsp;&nbsp;&nbsp;皮肤很干、黑黄黯哑</p>
                    <p style="color:#666;padding-top:0.4em; font-size:14px;">
                        经常带妆、熬夜，慢慢脸上某些循环不好的部位有些微色沉。听姐推荐的一款韩国瓷肌美白去黄套装，迫不及待就试用了。还记得第一次用完之后，好像给面部补充足了营养，感觉皮肤又滑又嫩，补足了水分，一点不紧绷不油腻。现在肤色也均匀亮白，自拍都像打了光，嘻嘻~
                    </p>
                </div>
            </li>
            <li>
                <span><img src="/public/stage/mobile/images_temp/case_03.jpg"></span>
                <div style="padding:0 0.8em 0.5em;">
                    <h2 style="height:2em;line-height:2em;font-weight: normal;color:#db0742;font-size:18px;">效果很实在，全家用起来</h2>
                    <p class="deta">二月雪&nbsp;&nbsp;&nbsp;32岁&nbsp;&nbsp;&nbsp;肤色泛黄，晦暗粗糙</p>
                    <p style="color:#666;padding-top:0.4em; font-size:14px;">
                        现在每周使用两次面膜，搭配着套装使用，肌肤比之前红润了！其实18岁后，护肤真的越早越好。我用的是韩国瓷肌抑黑焕白套装，洁面乳、肌底液、调整膜、面膜、精华霜，搭配很合理，一套就搞定了！效果也实在，才用几天皮肤就亮泽通透不少，之后黄气便逐渐淡去，现在全家都在用啦。
                    </p>
                </div>
            </li>
        </ul>
    </div>				</div>