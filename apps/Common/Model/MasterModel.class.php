<?php
/**
 * ====================================
 * 管理员模型
 * ====================================
 * Author: Hugo
 * Date: 14-5-20 下午9:28
 * ====================================
 * File: MasterModel.class.php
 * ====================================
 */
namespace Common\Model;

class MasterModel extends CommonModel
{
    protected $_auto = array(
        array('create_time', 'date', self::MODEL_INSERT, 'function', 'Y-m-d H:i:s'),
        array('update_time', 'date', self::MODEL_UPDATE, 'function', 'Y-m-d H:i:s'),
        array('password', 'cryptPassword', self::MODEL_BOTH, 'callback'),
    );

    protected $_validate = array(
        array('user_name','require','{%user_name_lost}'),
        array('real_name','require','{%real_name_lost}'),
        array('user_name','','{%user_name_exists}', self::MUST_VALIDATE, 'unique', self::MODEL_BOTH),
        array('confirm_password','password','{%confirm_password_error}', self::EXISTS_VALIDATE, 'confirm'), // 验证确认密码是否和密码一致
        array('password', '6,32', '{%password_error}', self::VALUE_VALIDATE, 'length', self::MODEL_BOTH),
    );


    /**
     * 加密登录密码
     * @param $password
     * @return string
     */
    public function cryptPassword($password)
    {
        if(empty($password)) return false;
        return md5(md5($password) . C('CRYPT_KEY'));
    }

    /**
     * 获取列表
     * @param array $params
     * @return mixed
     */
    public function filter($params) {
        $where = array();
        if($params['keyword']){
            $where['a.user_name'] = array('LIKE', "%{$params['keyword']}%");
            $where['a.real_name'] = array('LIKE', "%{$params['keyword']}%");
            $where['_logic'] = 'OR';
        }

        if($params['role_id']) {
            $where['a.role_id'] = intval($params['role_id']);
        }

        if($params['group_id']) {
            $where['a.group_id'] = intval($params['group_id']);
        }

        $this->alias(' AS a')
            ->join("__GROUP__ AS g ON a.group_id = g.id", 'left')
            ->join("__ROLE__ AS r ON a.role_id = r.id", 'left')
            ->field('a.user_id, a.user_name, a.real_name, a.sex, a.locked, a.is_admin, a.group_id, a.role_id, a.create_time, a.update_time, g.text as group_name, r.text as role_name');

        return $this->where($where);
    }

    public function format($params){
        $channel = C('CHANNEL');
        foreach($params['rows'] as $key => $val){
            $val['channel_name'] = $channel[$val['channel_id']];
            $params['rows'][$key] = $val;
        }
        return $params;
    }
}