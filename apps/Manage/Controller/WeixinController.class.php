<?php
/**
 * 微信客户端管理
 * User: 91336
 * Date: 2015/1/8
 * Time: 9:41
 */
namespace Manage\Controller;
use Common\Controller\ManageController;
use Common\Model\ConfigModel;

class WeixinController extends ManageController {
    public function reply() {
        if(IS_POST) {
            $content = trim(I('post.content'));
            $configModel = new ConfigModel();
            if($configModel->where(array('name' => 'AUTO_REPLY_MSG'))->setField('value', $content)) {
                S('DB_CONFIG_DATA',null);
                $this->success(L('SAVE_SUCCESS'));
            }
            $this->error(L('SAVE_ERROR'));
        }
        $this->assign('content', C('AUTO_REPLY_MSG'));
        $this->display();
    }
}