<?php
/**
 * 退货订单处理
 * User: 91336
 * Date: 2015/3/19
 * Time: 17:25
 */
namespace Common\Library\Order;
use Common\Model\BackInfoModel;
use Common\Model\OrderInfoModel;

set_lang('order');
class BackOperate {
    protected $backInfo;
    protected $orderModel;
    protected $backModel;
    protected $message;
    protected $addNote = '';
    protected $operateType = '';
    protected $today;

    //操作组合
    protected $operateArr = array(
        'audit_pass' => array(
            'text' => '审核通过',
            'power' => array('back-audit'),
            'status' => array(1)
        ),
        'audit_fail' => array(
            'text' => '审核退回',
            'power' => array('back-audit'),
            'status' => array(1)
        ),
        'cancel' => array(
            'text' => '取消退货',
            'power' => array('back-cancel'),
            'status' => array(1)
        ),
        'edit' => array(
            'text' => '修改',
            'power' => array('back-edit'),
            'status' => array(BK_WAIT_AUDIT, BK_AUDIT_FAIL, BK_CANCEL),
            'virtual' => true
        )
    );


    public function __construct($backInfo) {
        $this->orderModel = new OrderInfoModel();
        $this->backModel = new BackInfoModel();
        $this->today = date(C('DATE_FORMAT'));
        $this->backInfo = $backInfo;
    }

    /**
     * 获取操作
     * @param $orderId
     * @return null
     */
    public function getOperate(){
        $status = (int)$this->backInfo['back_status'];
        $operateKey = array();
        foreach($this->operateArr as $operate => $operateInfo) {
            $operateStatus = $operateInfo['status'];
            $status = in_array('*', $operateStatus) ? '*' : $status;
            if(!in_array($status, $operateStatus)) continue;

            if(!empty($operateInfo['power'])) {
                $hasPower = false;
                foreach($operateInfo['power'] as $power) {
                    if(power($power)) {
                        $hasPower = true;
                        break;
                    }
                }
                if(!$hasPower) {
                    continue;
                }
            }
            $operateKey[$operate] = $operateInfo;
        }
        return $operateKey;
    }

    /**
     * 判断是否可以进行确认、支付、取消等操作
     * @param int    $orderId    订单ID
     * @param string $oprateType 操作类型
     * @return bool
     */
    public function checkOperate($operateType = ''){
        $operate = $this->getOperate();
        if($operate && in_array($operateType, array_keys($operate))){
            return true;
        }
        return false;
    }

    /**
     * 处理订单
     * @param $orderId
     * @param $operateType
     * @param $userId 下单会员ID
     * @return bool
     */
    public function doOperate($operateType, $addNote = '') {
        if($this->checkOperate($operateType)) {
            if(method_exists($this, $operateType)) {
                $this->addNote = $addNote;
                $this->operateType = $operateType;
                if(call_user_func(array($this, $operateType))) {
                    return true;
                }
            }
        }
        $this->message or $this->message = sprintf(L('CANNOT_OPERATE'), L($operateType));
        return false;
    }

    /**
     * 审核通过
     * @return bool
     */
    private function audit_pass() {
        $data = array(
            'back_status' => BK_AUDIT,
            'audit_time' => $this->today,
        );
        if($this->save($data) === false) {return false;}
        $this->recordLog();
        return true;
    }

    /**
     * 审核通过
     * @return bool
     */
    private function audit_fail() {
        $data = array(
            'back_status' => BK_AUDIT_FAIL,
            'audit_time' => $this->today,
        );
        if($this->save($data) === false) {return false;}
        $this->recordLog();
        return true;
    }

    /**
     * 取消退货
     * @return bool
     */
    private function cancel() {
        $data = array(
            'back_status' => BK_CANCEL,
            'audit_time' => $this->today,
        );
        if($this->save($data) === false) {return false;}
        $this->recordLog();
        return true;
    }


    /**
     * 保存订单信息
     * @param type $data
     * @return type
     */
    protected function save($data) {
        $data['order_id'] = $this->backInfo['order_id'];
        $data['update_time'] = $this->today;
        return $this->backModel->save($data);
    }

    /**
     * 记录操作日志
     * @param type $addNote
     */
    private function recordLog() {
        $order = $this->backModel
            ->field('back_status')
            ->find($this->backInfo['order_id']);

        $this->backModel->addLog(
            $this->backInfo['order_id'],
            L($this->operateType) . ($this->addNote ? "，" . $this->addNote : ''),
            $order['back_status'],
            USER_ID
        );
    }

    /**
     * 返回错误信息
     * @return type
     */
    public function getError() {
        return $this->message;
    }
}