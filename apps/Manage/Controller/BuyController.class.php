<?php 
/**
 * ====================================
 * 后台下单
 * ====================================
 * Author: lwh
 * Date: 14/12/25
 * ====================================
 * File: BuyController.class.php
 * ====================================
 */

namespace Manage\Controller;
use Common\Controller\ManageController;
use Common\Library\Cart;
use Common\Library\Member\Member;
use Common\Model\AgentModel;
use Common\Model\GoodsModel;
use Common\Model\OrderInfoModel;

class BuyController extends ManageController {
	
	public function index() {

		$userId = I('user_id',0);
    	if (empty($userId)) $this->error(L('AGENT_LOST'));

        $agentModel = new AgentModel();
        $agent = $agentModel
            ->where(array('user_id' => $userId, 'status' => AG_AUTH))
            ->find();
        //判断是否可以下单
        if(!Member::allowOrder($agent)) $this->error(L('ONLY_TOP_AGENT_ADD'));

    	$this->assign("info",$agent);

        //读取商品列表
        $goodsModel = new GoodsModel();
        $page_size = 20;
        $goods_list = $goodsModel->getSale($agent,$page_size);
        $this->assign('goods_list', $goods_list['rows']);
    
        //读取快递信息
        $shippingInfo = M('shipping')->field('shipping_id,shipping_name')->select();
        $this->assign("shipping_info",$shippingInfo);

        $data = Cart::getAll($userId);
        $this->assign('cart_list', $data['rows']);
        $this->assign('total_amount', $data['total_amount']);
        $this->display();
    }
    //添加购物车
    public function addtocart() {

        $post = I('post.');
        if(empty($post['user_id'])) $this->error(L('AGENT_LOST'));
        if(Cart::add((int)$post['goods_id'], (int)$post['goods_number'],'',$post['user_id'])) {
            $this->success(L('add_successful'));
        }else {
            $this->error(Cart::getError());
        }
    }

    //删除购物车
    public function deletecart() {
        $post = I('post.');
        if(Cart::delete((int)$post['cart_id'])) {
            $this->success();
        }else {
            $this->error(Cart::getError());
        }
    }

    //修改数量
    public function modifycart() {
        $post = I('post.');
        if(Cart::modify((int)$post['cart_id'], (int)$post['goods_number'])) {
            $this->success();
        }else {
            $this->error(Cart::getError());
        }
    }

    //保存订单
    public function saveorder() {
    	$userId = I('post.user_id',0);
    	$shippingId = (int)I('post.shipping_id',0);
    	$shippingName = I('post.shipping_name','');
    	$shippingFee = (int)I('post.shipping_fee',0);

    	if (empty($userId)) $this->error(L('AGENT_LOST'));
    	if (empty($shippingId) || empty($shippingName)){
    		$this->error(L('SHIPPING_NAME_LOST'));	
    	}
    	if ($shippingFee < 0) $this->error(L('FEE_MUST_GT_ZERO'));
    	
        //查找代理商信息
        $agentModel = new AgentModel();
        $agentInfo = $agentModel->where(array('user_id' => $userId, 'status' => AG_AUTH))->find();
        //验证是否为代理商
       	
        if(!$agentInfo) $this->error(L('ONLY_AGENT_ADD'));

        //检查购物车
        $cart = Cart::getAll($userId);
        if(!$cart['rows']) $this->error(L('CART_NOT_GOODS'));

        //写入订单 
        //后台下单，订单状态为审核通过、待付款，同时写入快递公司和运费信息
        $order = array(
            'order_sn'      => get_order_sn('WS'),
            'user_id'       => $userId,
            'order_status'  => OS_AUDIT_PASS,
            'pay_status'	=> PS_WAIT_PAY,
            'shipping_id'	=> $shipping_id,
            'shipping_name'	=> $shipping_name,
            'shipping_fee'	=> $shipping_fee,
            'consignee'     => $agentInfo['contact'],
            'country'       => 1,
            'province'      => (int)$agentInfo['province'],
            'city'          => (int)$agentInfo['city'],
            'district'      => (int)$agentInfo['district'],
            'address'       => $agentInfo['address'],
            'mobile'        => $agentInfo['mobile'],
            'email'         => $agentInfo['email'],
            'order_amount'  => $cart['total_amount'],
            'goods_amount'  => $cart['total_amount'],
            'create_time'   => date(C('DATE_FORMAT'))
        );
        $orderModel = new OrderInfoModel();
        $orderId = $orderModel->add($order);

        if($orderId) {
            //写入订单商品
            $orderGoodsModel = M('OrderGoods');
            $affectRows = 0;
            $orderGoods = array();
            foreach($cart['rows'] as $goods) {
                unset($goods['session_id'], $goods['cart_id'], $goods['user_id']);
                $goods['order_id'] = $orderId;
                $orderGoods[] = $goods;
            }
            if($recId = $orderGoodsModel->addAll($orderGoods)) {
                //写入订单日志
                $orderModel->addLog($orderId,L('SYSTEM').L('SAVE_ORDER'),$order['order_status'], $order['shipping_status'], $order['pay_status'], USER_ID);
                    
                //写入套装详细商品
                $goodsModel = new GoodsModel();
                for ($i=$recId; $i < ($recId + count($orderGoods)); $i++) { 
                    $relation = $orderGoodsModel
                    ->alias('o')
                    ->join('LEFT JOIN __GOODS__ AS g ON g.goods_id = o.goods_id')
                    ->where(array('o.rec_id'=>$i))
                    ->field('o.goods_id,o.goods_number,g.is_package,g.relation_id')
                    ->find();
                    if ($relation['is_package'] && $relation['relation_id']) {
                        $relationGoods = $goodsModel->alias('g')
                                       ->join('LEFT JOIN __GOODS_PACKAGE__ AS p '.
                                              'ON p.relation_id = g.goods_id and '.
                                              'p.goods_id = '.$relation['goods_id'])
                                       ->field('g.goods_id,g.goods_name,g.goods_sn,g.shop_price,'.
                                               'p.goods_number')
                                       ->where(array('g.goods_id'=>array('in',$relation['relation_id'])))
                                       ->select();
                        foreach ($relationGoods as $k => $v) {
                            $v['parent_id'] = $recId;
                            $v['order_id'] = $orderId;
                            $v['goods_number'] = $v['goods_number'] * $relation['goods_number'];
                            $relationGoods[$k] = $v;
                        }
                        $orderGoodsModel->addAll($relationGoods);
                    }
                }
                //清除购物车
                Cart::deleteAll($userId);
                $this->success(L('SAVE_ORDER_SUCCESS'));
                exit;
            }else {
                $orderModel->delete($orderId);
            }
        }
        $this->error(L('SAVE_ORDER_FAIL'));
    }

}



 ?>