<?php
/**
 * ====================================
 * 菜单模型
 * ====================================
 * Author: Hugo
 * Date: 14-5-20 下午9:28
 * ====================================
 * File: MenuModel.class.php
 * ====================================
 */
namespace Common\Model;
use Common\Library\Common;

class MenuModel extends CommonModel
{
    protected $_auto = array(
        array('create_time', 'date', self::MODEL_INSERT, 'function', 'Y-m-d H:i:s'),
        array('update_time', 'date', self::MODEL_UPDATE, 'function', 'Y-m-d H:i:s'),
        array('module', 'strtolower', self::MODEL_BOTH, 'function'),
        array('action', 'strtolower', self::MODEL_BOTH, 'function'),
    );

    protected $_validate = array(
        array('text','require','{%menu_name_lost}'),
        array('name','require','{%class_name_lost}'),
        array('pid', 'validateEq', '{%parent_error}', self::EXISTS_VALIDATE, 'callback', self::MODEL_UPDATE)
    );

    /**
     * 判断父级ID正确性
     * @param $pid
     * @param $id
     * @return bool
     */
    protected function validateEq($pid){
        return $pid != I('post.id');
    }

    public function filter($params) {
        $where = array();
        if($params['text']) {
            $where['text'] = array('like', "%{$params['text']}%");
        }

        is_null($params['pid']) || $where['pid'] = $params['pid'];
        is_null($params['display']) || $where['display'] = $params['display'];
        empty($params['menu_id']) || $where['id'] = array('in', $params['menu_id']);

        $this->where($where);
        return $this;
    }

    /**
     * 读取树型数据
     * @return mixed
     */
    public function lists($params) {

        $data = $this->field("*, id AS tree_id")->order("pid ASC, orderby DESC")->select();

        if($data){
            foreach($data as $key => $row){
                $query = str_replace(',', '&', str_replace(array(' ', '，'), array('', ','), $row['params']));
                parse_str($query, $query);
                $query['menuid'] = $row['id'];
                $action = $row['action'] == '*' ? C('DEFAULT_ACTION') : $row['action'];
                $row['href'] = U($row['module'] . '/' . $action, $query);
                $row['power'] = "power('{$row['module']}-{$row['action']}')";
                $data[$key] = $row;
            }
            Common::buildTree($data, $params['selected'], $params['type']);
        }
        return $data ? $data : array();
    }

    /**
     * 获取验证菜单
     * @return mixed
     */
    public function accessMenu($menuId = array()){
        if(empty($menuId)) return array();
        $where = array(
            'id' => array('in', $menuId)
        );
        $accessMenu = array();
        $data = $this->where($where)->field("id, module, action")->select();
        if($data){
            foreach($data as $row){
                $action = empty($row['action']) ? C('DEFAULT_ACTION') : $row['action'];
                $name = md5(strtolower(U($row['module'] . '/' . $action)));
                $accessMenu[$name] = $row;
            }
        }
        return $accessMenu;
    }

    /**
     * 设置角色菜单
     * @param $role_id
     * @param $menu_list
     * @return bool
     */
    public function setRole($pk_id, $item_list){
        $model = M('RoleMenu');
        $model->where("menu_id = %d", $pk_id)->delete();
        $item_list = preg_replace("/\s+/",'',$item_list);
        if(empty($item_list)) return false;
        $item_list = explode(',', $item_list);
        $data = array();
        foreach($item_list as $item_id){
            $data[] = array('menu_id' => $pk_id, 'role_id' => $item_id);
        }
        $model->addAll($data);
        return true;
    }

    /**
     * 读取详细
     * @param $id
     * @return mixed
     */
    public function getInfo($id) {
        $data = $this
            ->alias(' AS r')
            ->join('__ROLE_MENU__ AS m ON r.id = m.menu_id', 'LEFT')
            ->field('r.*, GROUP_CONCAT(m.role_id) as role_list, r.id AS mid')
            ->group('r.id')
            ->where("r.id = $id")
            ->find();
        return $data;
    }
}