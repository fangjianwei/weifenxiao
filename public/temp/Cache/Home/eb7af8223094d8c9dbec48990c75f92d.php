<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta name="HandheldFriendly" content="true" />
<meta name="keywords" />
<meta name="description" />
<title>设置</title>
<link type="text/css" rel="stylesheet" href="/public/stage/mobile/css/reset.css" />
<link type="text/css" rel="stylesheet" href="/public/stage/mobile/css/style.css" />
</head>
<body>
    <form method="post" action="UserSet.aspx" id="form1">
<div class="aspNetHidden">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="h3FB4CeuYKdfZeeCGvpItC8abzcwof5nsvzgkTu9hogHPBcdQr2NcTrLpZs0oMcnKKSn/yBJgq4Pa1K6w0CJhgsczWEr2AOCYzTeiODi5OT/Sksov0n9K/AVn9dTfEt7" />
</div>
<div class="aspNetHidden">
	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="50B348D9" />
</div>
 <body class="BG">
    <section>
        <div class="titleBox clearfix">
            <a href="javascript:history.go(-1)" class="f-left"><img src="/public/stage/mobile/images/back.png"/></a>
            <span>设置</span>
        </div>
    </section>
    <section>
        <div class="setList mt15">
            <a class="setListCon clearfix" href="<?php echo U('home/address/address');?>">
                <span class="f-left setListConL">管理收货地址</span>
                <span class="f-right setListConR"><img src="/public/stage/mobile/images/open.png"/></span>
            </a>
        </div>
        <div class="setList mt15">
                <a class="setListCon clearfix" href="<?php echo U('home/member/stu_profile');?>"/>
                <span class="f-left setListConL">修改资料</span>
                <span class="f-right setListConR"><img src="/public/stage/mobile/images/open.png"/></span>
            </a>
        </div>
          <div class="setList mt15">
            <a class="setListCon clearfix" href="<?php echo U('home/withdrawtype/withtype_config');?>">
                <span class="f-left setListConL">提现方式管理</span>
                <span class="f-right setListConR"><img src="/public/stage/mobile/images/open.png"/></span>
            </a>
        </div>
    </section>
<div class="hidHeight"></div>
<div class="navBottom">
    <dl class="clearfix">
        <dd class="navBottomFirst">
            <p class="navBottomItem">
                <a href="stu_profile.html"><img src="/public/stage/mobile/UploadImage/uheaderImg/tp8lz7ik.png" /></a>
            </p>
        </dd>
        <dd>
            <p class="navBottomItem"><a href="index.html">产品购买</a></p>
        </dd>
    
        <dd>
            <p class="navBottomItem"><a href="my.html">代理中心</a></p>
        </dd>
    
        <dd>
            <p class="navBottomItem"><a href="http://mp.weixin.qq.com/s?__biz=MzA4NTU1NjU2NQ==&mid=404012163&idx=1&sn=d0767f8bd157c37f25f63e70f55ff9af&scene=1&srcid=0314PVTY9tnDILY0LJunffqM#rd">客服中心</a></p>
        </dd>
    </dl>
</div>
<script src="/public/stage/mobile/js/WeixinTools.js"></script>
</form>
</body>
</html>