<?php
/**
 * ====================================
 * 推荐位模型
 * ====================================
 * Author: Hugo
 * Date: 14-5-20 下午9:28
 * ====================================
 * File: PositionModel.class.php
 * ====================================
 */
namespace Common\Model;

class PositionModel extends CommonModel {
    protected $_validate = array(
        array('title','require','{%title_lost}'),
        array('code','require','{%code_lost}'),
        array('table_name','require','{%table_name_lost}'),
    );
}