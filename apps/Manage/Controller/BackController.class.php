<?php
/**
 * 退货管理
 * User: 91336
 * Date: 2015/3/19
 * Time: 15:29
 */
namespace Manage\Controller;
use Common\Controller\ManageController;
use Common\Library\Order\BackOperate;
use Common\Library\Order\Operate;
use Common\Model\BackInfoModel;

set_lang('order');

class BackController extends ManageController {
    protected $dbTable = 'BackInfo';

    public function show() {
        $order_id = (int)I('order_id');
        $back_info = $this->dbModel->find($order_id);
        if(!$back_info) $this->error(L('ORDER_NOT_FOUND'));
        $back_info['status_text'] = get_lang('back_status', $back_info['back_status']);
        $this->assign('back_info', $back_info);

        //获取订单商品
        $this->assign('back_goods', M('BackGoods')->where("order_id = '%d'", $order_id)->select());

        //获取订单日志
        $this->assign('back_log', $this->dbModel->getLog($order_id));

        $operateObj = new BackOperate($back_info);
        $operate = $operateObj->getOperate();
        $this->assign('operate', $operate);
        $this->display();
    }

    public function form() {
        $order_id = (int)I('order_id');
        $order = D('OrderInfo')->getRow($order_id);
        $operateObj = new Operate($order, false);
        $result = $operateObj->checkOperate('back');
        if(!$result) $this->error(sprintf(L('CANNOT_OPERATE'), L('back')));

        $backInfoModel = new BackInfoModel();
        //判断是否已经创建退货单，不允许重复创建，只允许修改
        $back_info = $backInfoModel->where("order_id = '%d'", $order_id)->find();
        if($back_info) {
            $backOperateObj = new BackOperate($back_info);
            if(!$backOperateObj->checkOperate('edit')) $this->error(sprintf(L('CANNOT_OPERATE'), L('edit')));
            $this->assign('back_info', $back_info);
        }

        //查询订单商品
        $this->assign('order_id', $order_id);
        if($back_info) {
            $order_goods = M('OrderGoods')
                ->alias('AS og')
                ->join("__BACK_GOODS__ AS bg ON og.order_id = bg.order_id AND bg.goods_id = og.goods_id AND og.extension_code = bg.extension_code", 'LEFT')
                ->field('og.rec_id,og.order_id,og.goods_id,og.goods_name,og.goods_sn,sum(og.goods_number) as goods_number,og.shop_price,og.goods_price,og.extension_code,og.parent_id,og.is_gift, bg.goods_number as back_goods_number')
                ->where("og.order_id = '%d'", $order_id)
                ->group('og.goods_id, og.extension_code')
                ->select();
        }else {
            $order_goods = M('OrderGoods')->where("order_id = '%d'", $order_id)->group('goods_id')->field('rec_id,order_id,goods_id,goods_name,goods_sn,sum(goods_number) as goods_number,shop_price,goods_price,extension_code,parent_id,is_gift')->select();
        }
        $this->assign('order_goods', $order_goods);

        $this->display();
    }

    public function save() {
        $order_id = (int)I('order_id');
        $order = D('OrderInfo')->getRow($order_id);
        $operateObj = new Operate($order, false);
        $result = $operateObj->checkOperate('back');
        if(!$result) $this->error(sprintf(L('CANNOT_OPERATE'), L('back')));

        $backInfoModel = new BackInfoModel();
        //判断是否已经创建退货单，不允许重复创建，只允许修改
        $back_info = $backInfoModel->where("order_id = '%d'", $order_id)->find();
        if($back_info) {
            $backOperateObj = new BackOperate($back_info);
            if(!$backOperateObj->checkOperate('edit')) $this->error(sprintf(L('CANNOT_OPERATE'), L('edit')));
        }

        $params = I('post.');
        if(empty($params['shipping_name'])) $this->error(L('shipping_name_lost'));
        if(empty($params['back_invoice_no'])) $this->error(L('back_invoice_no_empty'));
        if(is_array($params['goods_number'])) {
            $back_goods = array();
            foreach($params['goods_number'] as $goods_id => $goods_number) {
                if(empty($goods_number)) continue;
                $back_goods[$goods_id] = $goods_number;
            }
            if(empty($back_goods)) $this->error(L('must_select_back_goods'));

            $goodsModel = D('OrderGoods');
            $order_goods = $goodsModel->where("order_id = '%d'", $order_id)->group('goods_id')->field('order_id,goods_id,goods_name,goods_sn,goods_number,shop_price,goods_price,extension_code,parent_id,is_gift')->select();
            $save_goods = array();
            foreach($order_goods as $key => $goods) {
                if($goods['parent_id'] == 0) {
                    if(in_array($goods['goods_id'], array_keys($back_goods))) {
                        $goods['goods_number'] = $back_goods[$goods['goods_id']];
                        $save_goods[] = $goods;
                        if($goods['extension_code'] == 'package_buy') {
                            foreach($order_goods as $skey => $sgoods) {
                                if($sgoods['extension_code'] == 'package_goods' && $goods['goods_id'] == $sgoods['parent_id']) {
                                    $save_goods[] = $sgoods;
                                }
                            }
                        }
                    }
                }
            }

            $today = date(C('DATE_FORMAT'));
            if(!$back_info) {
                $back_info = array(
                    'order_id' => $order_id,
                    'back_sn' => get_back_sn('BK'),
                    'order_sn' => $order['order_sn'],
                    'user_id'  => $order['user_id'],
                    'master_id' => USER_ID,
                    'shipping_name' => $params['shipping_name'],
                    'back_invoice_no' => $params['back_invoice_no'],
                    'back_status' => BK_WAIT_AUDIT,
                    'remark' => $params['remark'],
                    'create_time' => $today
                );
                $result = $backInfoModel->add($back_info);
                $backInfoModel->addLog($order_id, '创建退货单', $back_info['back_status'], USER_ID);
            }else {
                $back_info['back_status'] = BK_WAIT_AUDIT;
                $back_info['shipping_name'] = $params['shipping_name'];
                $back_info['back_invoice_no'] = $params['back_invoice_no'];
                $back_info['update_time'] = $today;
                $result = $backInfoModel->save($back_info);
                $backInfoModel->addLog($order_id, '修改退货单', $back_info['back_status'], USER_ID);
            }

            if(!$result) $this->error(L('OPERATE_ERROR'). $backInfoModel->getError());
            $backGoodsModel = D('BackGoods');
            $backGoodsModel->where("order_id = '%d'", $order_id)->delete();
            $backGoodsModel->addAll($save_goods);
            $this->success(L('SAVE_SUCCESS'));
        }
        $this->error(L('must_select_back_goods'));
    }

    public function do_operate() {
        $order_id = (int)I('order_id');
        $operate = trim(I('operate'));
        $remark = trim(I('remark'));
        $backInfoModel = new BackInfoModel();
        //判断是否已经创建退货单，不允许重复创建，只允许修改
        $back_info = $backInfoModel->where("order_id = '%d'", $order_id)->find();
        $operateObj = new BackOperate($back_info);
        $result = $operateObj->doOperate($operate, $remark);
        if($result) $this->success(sprintf(L('OPERATE_SUCCESS'), L($operate)));
        $this->error($operateObj->getError());
    }
}