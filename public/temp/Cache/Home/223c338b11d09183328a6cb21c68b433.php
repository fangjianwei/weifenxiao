<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta name="HandheldFriendly" content="true" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<title>我的收益</title>
<link type="text/css" rel="stylesheet" href="/public/stage/mobile/css/reset.css?1" />
<link type="text/css" rel="stylesheet" href="/public/stage/mobile/css/style.css?1" />
<script src="/public/stage/mobile/res/jquery/jquery-2.1.3.min.js"></script>
<script src="/public/stage/mobile/res/chat-js/exporting.js"></script>
<script src="/public/stage/mobile/res/chat-js/highcharts.js"></script>
<script type="text/javascript">
    $(function () {
        var scale_1 = "<?php echo ($data["scale_1"]); ?>";
        var scale_2 = "<?php echo ($data["scale_2"]); ?>";
        var scale_3 = "<?php echo ($data["scale_3"]); ?>";
        $('#container').highcharts({
            chart: {
                plotBackgroundColor: '#f0f0f0',
                plotBorderWidth: null,
                plotShadow: false
            },
            title: {
                text: ''
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        color: '#000000',
                        connectorColor: '#000000',
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                    }
                }
            },
            series: eval("[{type:'pie',name: '收益比例',data: [['三级收益',"+scale_3+"],['二级收益',"+scale_2+"],{name: '一级收益',y: "+scale_1+",sliced: true,selected: true}],colors: ['#e38e00','#eed202','#67bada']}]")
        });
    });
</script>
</head>
<body class="BG">
    <section>
        <div class="titleBox clearfix">
            <a href="javascript:history.go(-1)" class="f-left">
                <img src="/public/stage/mobile/images/back.png" /></a>
            <span> 我的收益</span>
            <a href="<?php echo U('finance/finance_detail');?>" class="f-right">收益明细</a>
        </div>
    </section>
    <section>
        <div id="container"></div>
        <div class="myIncomeBox">
            <dl class="clearfix">
                <dd>
                    <div class="myIncomeBoxCon myIncomeBoxConA">
                        <p class="myIncomeBoxConTitle"><span></span> 一级收益</p>
                        <p class="myIncomeBoxConNumber">￥<?php echo ($data["profit_1"]); ?></p>
                    </div>
                </dd> 
                <dd>
                    <div class="myIncomeBoxCon myIncomeBoxConB">
                        <p class="myIncomeBoxConTitle"><span></span>二级收益</p>
                        <p class="myIncomeBoxConNumber">￥<?php echo ($data["profit_2"]); ?></p>
                    </div>
                </dd>
                <dd>
                    <div class="myIncomeBoxCon myIncomeBoxConC myIncomeBoxConLast">
                        <p class="myIncomeBoxConTitle"><span></span>三级收益</p>
                        <p class="myIncomeBoxConNumber">￥<?php echo ($data["profit_3"]); ?></p>
                    </div>
                </dd>
                
            </dl>
        </div>
    </section>
<div class="hidHeight"></div>
<div class="navBottom">
    <dl class="clearfix">
        <dd class="navBottomFirst">
            <p class="navBottomItem">
                <!--<a href="stu_profile.html"><img src="/public/stage/mobile/images/tp8lz7ik.png" /></a>-->
                <?php if(defined('USER_ID')): ?><a href="<?php echo U('home/member/index');?>"><img src="<?php echo (HEADIMGURL); ?>" /></a>
                <?php else: ?>
                    <!--用户没设置头像或未获取到用户头像，显示默认头像-->
                    <a href="<?php echo U('home/member/index');?>"><img src="/public/stage/mobile/images/tp8lz7ik.png" /></a><?php endif; ?>
            </p>
        </dd>
        <dd>
            <p class="navBottomItem"><a href="<?php echo U('goods/index');?>">产品购买</a></p>
        </dd>

        <dd>
            <p class="navBottomItem"><a href="<?php echo U('member/index');?>">代理中心</a></p>
        </dd>

        <dd>
            <p class="navBottomItem"><a href="http://mp.weixin.qq.com/s?__biz=MzA4NTU1NjU2NQ==&mid=404012163&idx=1&sn=d0767f8bd157c37f25f63e70f55ff9af&scene=1&srcid=0314PVTY9tnDILY0LJunffqM#rd">客服中心</a></p>
        </dd>
    </dl>
</div>

<script src="/public/stage/mobile/js/WeixinTools.js"></script>
</body>
</html>