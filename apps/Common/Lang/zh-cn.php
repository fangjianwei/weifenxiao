<?php
return array(
    'COMPANY'                   => '广州瓷肌',
    'META_TITLE'                => '分销管理平台',
    'SYSTEM'                    => '系统',
    'CUSTOM'                    => '客户',
    'SEX'                       => array(
        1                       => '男',
        2                       => '女'
    ),

    //数据
    'DROP_ERROR'                => '删除失败！',
    'DROP_SUCCESS'              => '删除成功！',
    'NO_ALLOW_DROP'             => '不允许删除！',
    'SAVE_SUCCESS'              => '操作成功！',
    'SAVE_ERROR'                => '数据未更新！',
    'OPERATE_ERROR'           => '操作失败',
    'PARENT_NODE'               => '== 作为顶级 ==',
    'PARENT_ERROR'              => '所属上级信息不正确！',
    'SELECT_NODE'               => '请选择',
    'ALL'                       => '全部',
    'SAVE_TYPE_LOST'            => '请设置保存类型：添加(insert)，更改(update)',
    'PRIMARY_KEY_LOST'          => '请设置唯一主键值',
    'SEND_CODE_CONTENT'         => '您好，您的验证码是：%d，请妥善保管',
    'input_build_code'         => '请输入验证码！验证码通过扫描二维码获取！',
    'build_code_error'          => '验证码错误！',
    'build_code_success'        => '微信绑定成功！',
    'build_error'               => '该微信已被绑定到其他账号！',


    //登陆
    'VERIFY_ERROR'              => '您输入的验证码不正确，请重新输入！',
    'LOGIN_NAME_LOST'           => '请输入您的用户名！',
    'LOGIN_PASSWORD_LOST'       => '请输入您的登录密码！',
    'USER_NOT_EXISTS'           => '用户不存在！',
    'PASSWORD_ERROR'            => '密码不正确,请输入6位以上密码！',
    'NOT_ALLOW_LOGIN'           => '您的账号不允许登录！',

    //分类
    'CATEGORY_NAME_LOST'        => '类别名称不能为空！',
    'CATEGORY_NAME_EXISTS'      => '类别名称已存在！',



    //系统提示
    '_ERROR_SERVICE_'           => '服务接口不存在！',
    '_NOT_ACCESS_'              => '对不起，您没有权限使用此功能！',
    '_THIS_NOT_ACCESS_'         => '对不起，该员工没有权限管理代理商',
    '_PAGE_NO_EXISTS_'          => '很抱歉 您要访问的页面不存在',
    'payment_not_exists'        => '支付方式不存在！',
    '_IP_EXISTS__'              => 'IP地址已存在',


    //代理商申请 
    'CONTACT_LOST'              =>  '联系人不能为空！',
    'WECHA_NAME_LOST'           =>  '微信号不能为空！',
    'ADDRESS_LOST'              =>  '地址不能为空！',
    'PROVINCE_LOST'             =>  '请选择省份',
    'CITY_LOST'                 =>  '请选择城市',
    'district_lost'             => '请选择地区',
    'GRADE_ID_LOST'             =>  '请选择代理级别',
    'DONOT_APPLY_AGAIN'         =>  '请勿重复申请！',
    'ADVANTAGE'                 =>  '自身优势不能为空！',      
    'MOBILE_WRONG'              =>  '手机号码错误',
    'EMAIL_WRONG'               =>  '电子邮箱格式错误',
    'APPLY_SUCCESS'             =>  '提交成功！',
    'APPLY_FAIL'                =>  '提交失败！',
    'SAVE_ORDER'                =>  '添加订单',
    'insufficient_balance'      =>  '您的上级代理账户余额不足，请与招商上级代理联系！',
    'settlement_fail'           =>  '余额结算失败',
    'SAVE_ORDER_FAIL'           =>  '系统生成订单失败',
    'SAVE_ORDER_SUCCESS'        =>  '系统生成订单成功！',
    'FOLLOW_MASTER_ERROR'             =>  '您的帐号没有分配跟进，不允许下单',

    //注册
    'MEMBER_CLOSE_REGISTER'             => '已关闭注册',
    'EMAIL_NOT_EXISTS'                  => '邮箱不存在',
    'OLD_PASSWORD_ERROR'                => '旧密码不正确',
    'CONFIRM_PASSWORD_ERROR'            => '新密码与确认密码不一致',
    'EDIT_PASSWORD_SUCCESS'             => '密码修改成功！',
    'EDIT_PASSWORD_FAILED'              => '密码修改失败',
    'USER_NAME_LOST'					=> '请填写用户名！',
    'USER_NAME_EXISTS'					=> '用户名已存在！',
    'MOBILE_LOST'						=> '请填写手机号码！',
    'MOBILE_EXISTS'						=> '手机号码已存在！',
    'MOBILE_ERROR'						=> '手机号码错误！',
    'WEIXIN_LOST'                       => '请填写微信号！',
    'WEIXIN_ERROR'                      => '微信号错误',
    'WEIXIN_EXISTS'                     => '微信号已存在！',
    'EMAIL_ERROR'						=> '邮箱地址错误！',
    'EMAIL_EXISTS'						=> '邮箱地址已存在！',
    'PASSWORD_FORMAT_ERROR' 			=> '密码由6至16位字母、数字、下划线组成',
    'REGISTER_SUCCESS'					=> '注册成功！',
    'REGISTER_FAIL'						=> '注册失败！',
    'LOGIN_SUCCESS'						=> '登陆成功！',

    //商品购买
    'ONLY_TOP_AGENT_ADD'                    => '对不起，您的代理组别不允许下单！',
    'NOT_ALLOW_BUY'                         => '对不起，您没有权限购买此商品！',
    'GOODS_STOCK_LEST'                      =>  '商品 %s 库存不足，请检查购物车！',
    'GOODS_LIMIT_BUY'                       => '商品 %s 的起购数量为 %d，请检查购物车中相应的商品数量！',
    'CART_NOT_GOODS'                    => '请先选购商品',
    'AGENT_LOST'                        => '代理商信息错误！',
    'NUMBER_CONFINE'                    => '单件商品的数量上限为100件',

    'buy_index'                         => '购物车',
    'buy_buySuccess'                    => '购买成功',

    //分页
    'HOME'                              => '第一页',
    'LAST'                              => '最后页',
    'PREV'                              => '上一页',
    'NEXT'                              => '下一页',

    //安全中心
    'PASSWORD_LOST'             => '新密码不能为空！',
    'PASSWORD_NOT_SAME'         => '新密码不一致！',
    'PASSWORD_LENGTH_ERROR'     => '新密码长度不对',
    'PLEASE_GET_VERIFY'         => '请先获取验证码！',
    'VERIFY_OVERTIME'           => '验证码超时！',
    'SEND_MOBILE_SUCCESS'       => '验证码已发送到您手机！',
    'SEND_EMAIL_SUCCESS'        => '验证码已发送到您邮箱！',
    'SEND_FAIL'                 => '发送失败！',
    'EMAIL_ERORR'               => '邮箱不正确！',
    'VERIFY_EMAIL_TITLE'        => '修改邮箱验证码',
    'PLEASE_TRY_LATER'          => '验证码已发送，请于%d秒后再尝试操作！',
    'ACCOUNT_ERROR'             => '账户名错误！',
    'ACCOUNT_NOT_EXISTS'        => '账户名不存在！',
    'BUILD_SUCCESS'             => '绑定成功！请关闭窗口，然后在电脑上完成操作！',
    'SEND_SUCCESS'              => '验证码发送成功！',
    'MOBILE'                    => '手机号码',
    'EMAIL'                     => '邮箱',
    'ITEM_ERROR'                => '%s不正确',
    'ITEM_IS_THE_SAME'          => '新%s与旧%s一致',   

    //模板消息
    'WX_UNPAY_ORDER'            => '%s，您好，您的订单已通过审核，请及时支付！',
    'WX_PROCESS_APPLY'          => '%s发起了流程申请',
    'WX_UNPAY_AMOUNT'           => '订单总额：%s元',

    'CANNOT_CHECK'              => '无法查看该信息',
);