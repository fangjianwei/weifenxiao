<?php
/**
 * ====================================
 * 地区管理
 * ====================================
 * Author: 91336
 * Date: 2014/11/29 9:17
 * ====================================
 * File: RegionController.class.php
 * ====================================
 */
namespace Manage\Controller;

use Common\Controller\ManageController;
use Common\Library\Manage\Master;

class RegionController extends ManageController {
    protected $region;

    public function _after_save() {
        $params = I('post.');
        if(empty($params['region_id'])) {
            Master::log(USER_ID, L('add_region') . $params['region_name']);
        }else {
            Master::log(USER_ID, L('edit_region') . $params['region_name']);
        }
    }

    public function _before_delete() {
        $itemid = I('request.itemid');
        if(empty($itemid)) return;
        $this->region = $this->dbModel->find($itemid);
    }

    public function _after_delete() {
        Master::log(USER_ID, L('delete_region') . $this->region['region_name']);
    }
}