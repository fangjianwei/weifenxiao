<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title><?php echo (L("meta_title")); ?></title>
    <script type="text/javascript" src="/public/static/base.js"></script>
    <script type="text/javascript" src="/public/cpanel/js/easyui.js"></script>
    <script type="text/javascript" src="/public/cpanel/js/index.js"></script>
</head>
<body>
<div class="easyui-layout"  data-options="fit:true">
    <div data-options="region:'north', border: false" class="top-region">
        <div class="top-layout">
            <a href="javascript:;" class="top-left-a"></a>
            <div class="top-logo"><?php echo (L("meta_title")); ?></div>
            <ul class="top-menu">
                <li>欢迎您：<?php echo (REAL_NAME); ?></li>
                <li>|</li>
                <li><a href="<?php echo U('passport/logout');?>">安全退出</a></li>
                <li>|</li>
                <li><a href="javascript:void(0);" onclick="openTab(null, '个人信息', '<?php echo U('master/info');?>', true)">个人信息</a></li>
                <li>|</li>
                <li><a href="javascript:void(0);" onclick="reflashTab()">刷新当前页</a></li>
                <?php if(power('index-clear')): ?><li>|</li>
                <li><a href="javascript:void(0);" onclick="openTab(null, '清理缓存', '<?php echo U('index/clear');?>', true)">清理缓存</a></li><?php endif; ?>
            </ul>
        </div>
    </div>
    <div data-options="region:'west',split:true" iconCls="icon-accordion" style="width: 180px;">
        <div class="easyui-accordion" data-options="fit:true, border:false">
            <?php if(is_array($menuList)): $i = 0; $__LIST__ = $menuList;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$menu): $mod = ($i % 2 );++$i;?><div title="<?php echo ($menu["text"]); ?>">
                <ul class="left-menu">
                    <?php if(is_array($menu['children'])): $i = 0; $__LIST__ = $menu['children'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$nextMenu): $mod = ($i % 2 );++$i;?><li><a href="javascript:;" data-url="<?php echo ($nextMenu["href"]); ?>"><?php echo ($nextMenu["text"]); ?></a></li><?php endforeach; endif; else: echo "" ;endif; ?>
                </ul>
            </div><?php endforeach; endif; else: echo "" ;endif; ?>
        </div>
    </div>
    <div data-options="region:'center',border:false">
        <div id="rightTabs" class="easyui-tabs" data-options="fit:true,border:true,onContextMenu:onRightTabsContextMenu">
            <div title="工作台" href="<?php echo U('index/main');?>" class="content-layout"></div>
        </div>
    </div>
    <div data-options="region:'south', border:false" class="footer">
        版权所有 &copy; <a href="http://www.chinaskin.biz"><?php echo (L("company")); ?></a>
    </div>
</div>
<div id="rightTabsMenu" class="easyui-menu" data-options="onClick:onRightTabsHandler" style="width:120px;">
    <div data-options="name:'close', iconCls:'icon-no'">关闭当前标签页</div>
    <div data-options="name:'closeAll', iconCls:'icon-no'">关闭所有标签页</div>
    <div data-options="name:'reload',iconCls:'icon-save'">刷新</div>
</div>
</body>
</html>