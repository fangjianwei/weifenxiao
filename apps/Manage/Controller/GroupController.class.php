<?php
/**
 * ====================================
 * 管理员组别管理
 * ====================================
 * Author: Hugo
 * Date: 14-5-20 下午9:58
 * ====================================
 * File: GroupController.class.php
 * ====================================
 */
namespace Manage\Controller;
use Common\Controller\ManageController;

class GroupController extends ManageController {
    protected $allowAction = array('index');
}