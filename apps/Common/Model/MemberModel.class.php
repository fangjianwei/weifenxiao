<?php
namespace Common\Model;

class MemberModel extends CommonModel
{

    /**
     * 数据格式化
     */
    public function format($data) {
//        set_lang('agent');
//        $data_from = L('data_from');
//        $agent_type = L('agent_type');
        Vendor('phxcrypt.phxcrypt');
        $myCrypt = new \phxCrypt;
        foreach ($data['rows'] as $key => $row) {
            $row['mobile'] = $myCrypt->isDecrypted($row['mobile']);     //解密手机号
            $row['mobile'] = preg_replace('/(1[34578]{1}[0-9])[0-9]{4}([0-9]{4})/i','$1****$2',$row['mobile']);     //手机号码中间4位*表示
            $where_pid['user_id'] = array('eq',$row['pid']);
            $row_par_name = M('member')->field("concat(truename,'-',nickname) as par_name")->where($where_pid)->find();    //上级真实姓名
            $row['par_name'] = $row_par_name['par_name'];
            $alloc_parent_user_url = 'http://'.$_SERVER['HTTP_HOST'].U("member/form",array('user_id'=>$row['user_id']));    //分配上级
            $alloc_parent_user_url = 'OpenUrl("'.$alloc_parent_user_url.'")';
            $address_url = 'http://'.$_SERVER['HTTP_HOST'].U("member/address_list",array('user_id'=>$row['user_id']));      //收货地址
            $address_url = 'openWindow("'.$row['nickname'].'的收货地址","'.$address_url.'")';
            $finance_url = 'http://'.$_SERVER['HTTP_HOST'].U("member/finance",array('user_id'=>$row['user_id']));           //财务明细
            $finance_url = 'openWindow("'.$row['nickname'].'的财务明细","'.$finance_url.'")';
            $row['userinfo'] = "<a href='#' onclick='".$alloc_parent_user_url."'>修改资料</a> | <a href='#'onclick='".$address_url."'>收货地址</a> | <a href='#' onclick='".$finance_url."'>财务明细</a>";
            $data['rows'][$key] = $row;
        }
        return $data;
    }

    /**
     * 查询用户详细信息
     */
    public function getInfo($user_id){
        $row = $this->find($user_id);

        Vendor('phxcrypt.phxcrypt');
        $myCrypt = new \phxCrypt;
        $row['mobile'] = $myCrypt->isDecrypted($row['mobile']);     //解密手机号
        $row['mobile'] = preg_replace('/(1[34578]{1}[0-9])[0-9]{4}([0-9]{4})/i','$1****$2',$row['mobile']);     //手机号码中间4位*表示
        return $row;
    }

    /**
     * 查询会员列表
     */
    public function get_user_list($params=''){
        $where = array();
        if($params){
            $where['truename'] = array('like',"%".$params['keyword']."%");
            $where['nickname'] = array('like',"%".$params['keyword']."%");
            $where['_logic'] = 'or';
        }
        $data = M('member')->field("user_id,concat(truename,'-',nickname) as name")->where($where)->select();
        return $data;
    }

    /**
     * 保存数据
     */
    public function save(){
        $params = I("request.");
        $data['pid'] = $params['pid'];
        $data['truename'] = $params['truename'];
        $data['nickname'] = $params['nickname'];
        $data['locked'] = $params['locked'];
        $where['user_id'] = $params['user_id'];
        $result = M('member')->where($where)->save($data);

        if($result!==false){
            $return_arr = array('info'=>'保存成功','status'=>1,'url'=>'');
        }else{
            $return_arr = array('info'=>'保存失败','status'=>0,'url'=>'');
        }
        return $return_arr;
    }
}