<?php
/**
 * Created by PhpStorm.
 * User: 9009078
 * Date: 2016/5/20
 * Time: 15:22
 */
namespace Common\Model;

use Common\Library\Common;

class AddressModel extends CommonModel {
    /**
     * 加载省市区数据
     * @return array
     */
    public function load_address(){
        $params = I("request.");
        if($params['address_type']){
            $where['parent_id'] = $params['region_id'];
        }
        $data = M('region')->field('region_id,region_name')->where($where)->select();
        return $data;
    }

    /**
     * 新增/编辑收货地址
     * @return array
     */
    public function add_address(){
        $params = I("request.");
        //用户ID
        $data['user_id'] = 1;
        //查询用户保存的收货地址，每个用户最多填写5个收获地址
        $nums = M('member_address')->where($data)->count();
        if(empty($params['address_id']) && $nums==5){
            return ADDRESS_NUMS_LIMIT;
        }
        //收货人姓名
        if($params['consignee']){
            $data['consignee'] = trim($params['consignee']);
            if(strlen($data['consignee'])>10){
                return ADDRESS_CONSIGNEE_LENGTH;    //收货人姓名长度过长
            }
        }else{
            return ADDRESS_CONSIGNEE_EMPTY;     //收货人姓名为空
        }
        //手机号码
        if($params['mobile']){
            if(!preg_match("/^0?(13[0-9]|15[0123456789]|17[0123456789]|18[0123456789]|14[57])[0-9]{8}$/",$params['mobile'])){
                return ADDRESS_MOBILE_ERR;   //手机号格式有误
            }
            Vendor('phxcrypt.phxcrypt');
            $myCrypt = new \phxCrypt;
            $data['mobile'] = $myCrypt->phxEncrypt(trim($params['mobile']));    //手机号码加密
        }else{
            return ADDRESS_MOBILE_EMPTY;    //手机号为空
        }
        //省份
        if($params['province']){
            $data['province'] = intval($params['province']);
        }else{
            return ADDRESS_PROVINCE_EMPTY;    //省份为空
        }
        //城市
        if($params['city']){
            $data['city'] = intval($params['city']);
        }else{
            return ADDRESS_CITY_EMPTY;    //城市为空
        }
        //地区
        if($params['district']){
            $data['district'] = intval($params['district']);
        }else{
            return ADDRESS_DISTRICT_EMPTY;    //城市为空
        }
        //镇街
        if($params['town']){
            $data['town'] = intval($params['town']);
        }
        //详细地址
        if($params['address']){
            $data['address'] = trim($params['address']);
        }else{
            return ADDRESS_EMPTY;    //城市为空
        }
        //新增或保存操作
        if($params['address_id']){
            $address_id = $params['address_id'];
            M('member_address')->where("address_id = '$address_id'")->save($data);
        }else{
            M('member_address')->add($data);
        }
        return ADD_ADDRESS_SUC;     //添加成功
    }

    /**
     * 收货地址列表数据
     * @return array
     */
    public function address_list(){
        //用户ID
        $where['user_id'] = 1;
        $data = array();
        if($where['user_id']){
            $data = M('member_address')->where($where)->select();
            Vendor('phxcrypt.phxcrypt');
            $myCrypt = new \phxCrypt;
            foreach($data as $key => $val){
                empty($val['province']) ? '' : $arr[] = $val['province'];
                empty($val['city'])     ? '' : $arr[] = $val['city'];
                empty($val['district']) ? '' : $arr[] = $val['district'];
                empty($val['town'])     ? '' : $arr[] = $val['town'];   //某些地区没有镇街划分，有可能为空
                $where_region['region_id'] = array('in',$arr);
                $data_region = M('region')->field('region_name')->where($where_region)->select();
                $address_str = '';
                foreach($data_region as $k => $v){
                    $address_str .= $v['region_name']." ";
                }
                $data[$key]['address_str'] = $address_str;
                $data[$key]['mobile'] = $myCrypt->isDecrypted($val['mobile']);    //手机号码解密
            }
        }
        return $data;
    }

    /**
     * 查看收货地址信息
     * @return array
     */
    public function address_info(){
        $params = I("request.");
        $data = array();
        Vendor('phxcrypt.phxcrypt');
        $myCrypt = new \phxCrypt;
        if($params['address_id']){
            $where['address_id'] = $params['address_id'];
            $data = M('member_address')->where($where)->find();
            empty($val['province']) ? '' : $arr[] = $val['province'];
            empty($val['city'])     ? '' : $arr[] = $val['city'];
            empty($val['district']) ? '' : $arr[] = $val['district'];
            empty($val['town'])     ? '' : $arr[] = $val['town'];   //某些地区没有镇街划分，有可能为空
            $where_region['region_id'] = array('in',$arr);
            $data_region = M('region')->field('region_name')->where($where_region)->select();
            foreach($data_region as $k => $v){
                $address_str .= $v['region_name']." ";
            }
            $data['address_str'] = $address_str;
            $data['mobile'] = $myCrypt->isDecrypted($data['mobile']);    //手机号码解密
        }else{
            //防止新增地址时JS报错
            $data['address_id']=0;
            $data['province']=0;
            $data['city']=0;
            $data['district']=0;
            $data['town']=0;
        }
        return $data;
    }

    /**
     * 删除收货地址
     * @return array
     */
    public function delete_address(){
        $params = I("request.");
        $data = array();
        if($params['address_id']){
            $address_id = $params['address_id'];
            M('member_address')->where("address_id = '$address_id'")->delete();
            $data['status'] = 1;    //删除成功
            $data['result'] = '删除成功！';
        }else{
            $data['status'] = 0;    //删除失败
            $data['result'] = '参数有误，删除失败！';
        }
        return $data;
    }
}