<?php
/**
 * ====================================
 * 这里是说明
 * ====================================
 * Author: 91336
 * Date: 2014/11/24 16:17
 * ====================================
 * File: GoodsCategoryModel.class.php
 * ====================================
 */
namespace Common\Model;
use Common\Library\Common;

class GoodsCategoryModel extends CommonModel {
    protected $_validate = array(
        array('text','require','{%category_name_lost}'),
        array('text','','{%category_name_exists}', self::MUST_VALIDATE, 'unique', self::MODEL_BOTH),
        array('pid', 'validateEq', '{%parent_error}', self::EXISTS_VALIDATE, 'callback', self::MODEL_UPDATE)
    );

    /**
     * 判断父级ID正确性
     * @param $pid
     * @param $id
     * @return bool
     */
    protected function validateEq($pid){
        return $pid != I('post.id');
    }

    public function lists($params) {
        if($params['keyword']) {
            $this->where(array(
                'text' => array('LIKE', "%{$params['keyword']}%")
            ));
        }
        $data = $this
            ->field('*,id as cat_id')
            ->order("pid ASC, orderby DESC")
            ->select();

        Common::buildTree($data, $params['selected'], $params['type']);
        return $data;
    }
}