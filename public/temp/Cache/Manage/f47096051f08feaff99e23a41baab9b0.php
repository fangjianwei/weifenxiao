<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>后台管理系统</title>
    <script type="text/javascript" src="/public/static/base.js"></script>
    <script type="text/javascript" src="/public/cpanel/js/easyui.js"></script>
</head>
<body>
<form id="form" data-url="<?php echo U('save');?>" method="post" class="form-panel">
    <table>
        <tr>
            <th>配置标识：</th>
            <td>
                <input name="name" class="easyui-validatebox textbox" data-options="required:true" title="用于C函数调用，只能使用英文且不能重复">
            </td>
        </tr>
        <tr>
            <th>配置标题：</th>
            <td>
                <input name="title" class="easyui-validatebox textbox" data-options="required:true">
            </td>
        </tr>
        <tr>
            <th>排序：</th>
            <td>
                <input type="text" name="orderby" class="easyui-numberspinner" data-options="min:0,max:1000,required:true" value="50">
            </td>
        </tr>
        <tr>
            <th>配置类型：</th>
            <td>
                <select name="type" class="easyui-combobox" style="width: 100px;">
                    <?php if(is_array(C("CONFIG_TYPE_LIST"))): $i = 0; $__LIST__ = C("CONFIG_TYPE_LIST");if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$type): $mod = ($i % 2 );++$i;?><option value="<?php echo ($key); ?>"><?php echo ($type); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                </select>
            </td>
        </tr>
        <tr>
            <th>配置分组：</th>
            <td>
                <select name="group" class="easyui-combobox" style="width: 100px;">
                    <option value="0">不分组</option>
                    <?php if(is_array(C("CONFIG_GROUP_LIST"))): $i = 0; $__LIST__ = C("CONFIG_GROUP_LIST");if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$type): $mod = ($i % 2 );++$i;?><option value="<?php echo ($key); ?>"><?php echo ($type); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                </select>
            </td>
        </tr>
        <tr>
            <th>配置值：</th>
            <td>
                <input name="value" class="easyui-textbox" data-options="multiline:true,prompt:'数组类型需要填写配置项，格式: 0:关闭,1:开启'"style="width:300px;height:100px">
            </td>
        </tr>
        <tr>
            <th>配置项：</th>
            <td>
                <input name="extra" class="easyui-textbox" data-options="multiline:true,prompt:'枚举需要填写配置项，格式: 0:关闭,1:开启'"style="width:300px;height:100px">
            </td>
        </tr>
        <tr>
            <th>说明：</th>
            <td>
                <input name="remark" class="easyui-textbox" data-options="multiline:true"style="width:300px;height:100px">
            </td>
        </tr>
        <tr>
            <td>
                <input type="hidden" name="id">
            </td>
        </tr>
    </table>
</form>
</body>
</html>