<?php
/**
 * 退货数据操作
 * User: 91336
 * Date: 2015/3/19
 * Time: 16:39
 */
namespace Common\Model;
class BackInfoModel extends CommonModel {

    public function format($data){
        set_lang('order');
        foreach ($data['rows'] as $key => $row) {
            $row['status_text'] = get_lang('back_status', $row['back_status']);
            $data['rows'][$key] = $row;
        }
        return $data;
    }

    public function filter($params){
        $this->order('create_time desc');
        $where = array();
        if (!empty($params['begindate']) && !empty($params['enddate'])){
            $where['create_time'] = array('between',$params['begindate'].','.$params['enddate']);
        } else {
            $params['begindate'] && $where['create_time'] = array('gt',$params['begindate']);
            $params['enddate']   && $where['create_time'] = array('lt',$params['enddate']);
        }

        $params['keywords']   && $where['back_sn|order_sn'] = array('like','%'.$params['keywords'].'%');
        $this->where($where);
        return $where;
    }

    /**
     * 写入订单日志
     * @param integer $orderId        订单ID
     * @param string  $actionNote     操作备注
     * @param integer $orderStatus    订单状态
     * @param integer $shippingStatus 快递状态
     * @param integer $payStatus      支付状态
     * @param integer $masterId       管理员ID，前台下单时为空
     */
    public function addLog($orderId, $actionNote = '',$back_status, $masterId = 0){
        $data['order_id']    = $orderId;
        $data['action_note'] = $actionNote;
        $data['create_time'] = date(C('DATE_FORMAT'));

        $masterId       && $data['master_id']       = $masterId;
        $back_status    && $data['back_status']    = $back_status;

        return M('BackLog')->add($data);
    }

    /**
     * 获取订单日志
     * @param $orderId
     * @return mixed
     */
    public function getLog($orderId) {
        $data = $this->table('__BACK_LOG__')
            ->where(array('order_id' => $orderId))
            ->order('create_time desc')
            ->select();

        $masterModel = new MasterModel();
        if($data) {
            set_lang('order');
            foreach($data as $key => $row) {
                $row['back_status'] = get_lang('back_status', $row['back_status']);
                $row['master_name'] = empty($row['master_id']) ? L('CUSTOM') : $masterModel->where(array('user_id' => $row['master_id']))->getField('real_name');
                $data[$key] = $row;
            }
        }
        return $data;
    }
}