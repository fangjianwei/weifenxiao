<?php
/**
 * ====================================
 * member等级模型
 * ====================================
 * Author: weitao
 * Date: 14-11-20 下午6:09
 * ====================================
 * File: ArticleModel.class.php
 * ====================================
 */
namespace Common\Model;

class AgentGradeModel extends CommonModel{

    protected $_validate = array(
        array('grade_name', 'require', '{%grade_name_lost}'),
        array('grade_name', '','{%grade_name_is_exist}',self::MUST_VALIDATE,'unique'),
        array('orderby', 'number', '{%orderby_must_be_number}'),
        array('orderby', '','{%orderby_is_exist}',self::MUST_VALIDATE,'unique'),
        array('pay_money','number','{%pay_money_must_be_number}'),
    );

    public function filter($params) {
        $where = array();
        if($params['keywords']) {
            $where['grade_name'] = array('LIKE', "%{$params['keywords']}%");
        }

        return $this->order('orderby asc')->where($where);
    }


    /**
     * 获取下级代理商级别
     * @param $grade_id
     * @return mixed
     */
    public function getLowerGrade($grade_id){
        $grade_id = intval($grade_id);
        $orderBy = $this->getGrade($grade_id, 'orderby');
        $grade_info = $this->where(array('orderby' => array('lt', $orderBy)))->order('orderby DESC')->find();
        return $grade_info;
    }

    /**
     * 获取代理商最低级别
     * @param null $field
     * @return mixed
     */
    public function getMinGrade($field = null){
        $min_grade = $this->order('orderby ASC')->find();
        return empty($field) ? $min_grade : $min_grade[$field];
    }

    /**
     * 获得代理商最高级别
     * @param null $field
     * @return mixed
     */
    public function getMaxGrade($field = null){
        $max_grade = $this->order('orderby DESC')->find();
        return empty($field) ? $max_grade : $max_grade[$field];
    }

    /**
     * 获取代理商信息
     * @param $grade_id
     * @param null $field
     * @return mixed
     */
    public function getGrade($grade_id,$field = null){
        static $grade;
        if(empty($grade_id)) return false;
        if(!$grade[$grade_id]) {
            $grade[$grade_id] = $this->where('grade_id = '.$grade_id)->find();
        }
        return empty($field) ? $grade[$grade_id] : $grade[$grade_id][$field];
    }


}