<?php
/**
 * ====================================
 * 系统全局常量定义
 * ====================================
 * Author: 91336
 * Date: 2014/12/3 16:24
 * ====================================
 * File: define.php
 * ====================================
 */

//代理商状态
define('AG_NEW',                0); //意向资料库
define('AG_AUTH',               1); //已授权资料库
define('AG_CANCEL',             3); //已注销资料库

//退货状态
define('BK_WAIT_AUDIT',         1); //未审核
define('BK_AUDIT',              2); //审核通过
define('BK_AUDIT_FAIL',         3); //审核退回
define('BK_CANCEL',             4); //取消退货
define('BK_RECEIVED',           5); //退货入库
define('BK_END',                6); //退货完成

//订单状态
define('OS_WAIT_CONFIRM',       1); //待确认
define('OS_WAIT_AUDIT',         2); //待审核
define('OS_AUDIT',              3); //审核中
define('OS_AUDIT_BACK',         4); //审核退回
define('OS_AUDIT_PASS',         5); //审核通过
define('OS_CANCEL_AUDIT',       6); //取消审核中
define('OS_CANCEL',             7); //已取消
define('OS_RETURNED_AUDIT',     8); //退货审核中
define('OS_RETURNED',           9); //已退货

//配送状态
define('SS_UNSHIPPED',          0); // 未发货
define('SS_PREPARING',          3); // 配货中
define('SS_EXPRESSED',          4); //已打单
define('SS_BALE',               8); //已打包
define('SS_SHIPPED',            1); //已发货
define('SS_RECEIVED',           2); //已签收
define('SS_DEPOSTUNUSUAL',      20); //仓库返回异常
define('SS_RETURNED',           29); //退货已签收
define('SS_SPlIT',              28); //拆单

/* 支付状态 */
define('PS_UN_PAYED',           0); // 未付款
define('PS_WAIT_PAY',           1); // 等待付款
define('PS_PAYED',              2); // 已付款
define('PS_REFUND',             3); // 已退款
define('PS_UN_REFUND',          4); // 退款失败
define('PS_WAIT_CONFIRM',		5); // 等待支付确认

//代理来源
define('AGENT_TYPE_JINGBAI',    1); //京百
define('AGENT_TYPE_SKIN',       2); //瓷肌

//积分类型
define('INTEGRAL_GOODS',        'goods');           //商品
define('INTEGRAL_SIGN',         'sign');            //签到
define('INTEGRAL_REGISTER',     'register');        //注册

//短信接口
define('KEYSMSAPI','SMSAPI#*GO2821119');
define('URL',      'http://api.chinaskin.cn/apiSms/index');

define('QRCODE_IMG_PATH', APP_PATH.'../public/qrcode/'); //二维码的存放目录
define('FILE_NUMBER', 1000); //每个文件夹的文件个数

define('DEFAULT_SHIPPING_ID', 45);

/*售后类型*/
define('AFTER_RETURN_REFUND',1);  //退货退款
define('AFTER_EXCHANGE_GOODS',2);  //换货
define('AFTER_REPLENISH',3);  //补货
define('AFTER_REFUND',4);  //退款
define('AFTER_REJECT_RETURN',5);  //拒收
define('AFTER_TACKL_RETURN',6);  //快递拦截
/*处理结果*/
define('AUDITING',0); //待审核
define('AUDITPASS',1); //审核通过
define('AUDITNOTPASS',2); //审核未通过

//会员中心-绑定手机号状态
define('BOUND_SUC',1);   //绑定成功
define('BOUND_MOBILE_EXIST',2);   //手机号已注册
define('BOUND_MOBILE_ERR',3);   //手机号格式有误
define('BOUND_TRUENAME_EMPTY',4);   //姓名为空
define('BOUND_MOBILE_EMPTY',5);   //手机号为空
define('BOUND_WECHAT_EMPTY',6);   //微信号为空
//会员中心-新增收货地址
define('ADD_ADDRESS_SUC',1);   //新增成功
define('ADDRESS_CONSIGNEE_EMPTY',2);   //收货人姓名为空
define('ADDRESS_CONSIGNEE_LENGTH',3);   //收货人姓名长度过长
define('ADDRESS_MOBILE_EMPTY',4);   //手机号为空
define('ADDRESS_MOBILE_ERR',5);   //手机号格式有误
define('ADDRESS_PROVINCE_EMPTY',6);   //省份为空
define('ADDRESS_CITY_EMPTY',7);   //城市为空
define('ADDRESS_DISTRICT_EMPTY',8);   //地区为空
define('ADDRESS_EMPTY',9);   //详细地址为空
define('ADDRESS_NUMS_LIMIT',10);   //收货地址保存个数限制

//会员来源
define('USER_SOURCE_WEBSQ','1');    //用户来源，微信网页授权