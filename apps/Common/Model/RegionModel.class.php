<?php
/**
 * ====================================
 * 地区数据模型
 * ====================================
 * Author: 91336
 * Date: 2014/11/29 9:34
 * ====================================
 * File: RegionModel.class.php
 * ====================================
 */
namespace Common\Model;
use Common\Library\Common;

class RegionModel extends CommonModel {
    protected $_validate = array(
        array('region_name','require','{%region_name_lost}'),
        array('parent_id', 'validateEq', '{%parent_error}', self::EXISTS_VALIDATE, 'callback', self::MODEL_UPDATE),
        array('region_name','validateName','{%region_name_exists}', self::MUST_VALIDATE, 'callback', self::MODEL_BOTH),
    );

    /**
     * 验证地区地址是否存在
     * @param $region_name
     * @param $data
     * @return bool
     */
    protected function validateName($region_name, $data) {
        $data = I('post.');
        $where = array();

        //如果是新增地区，判断所选上级地区下是否存在新增的地区名称
        $where['parent_id'] = (int)$data['parent_id'];
        $where['region_name'] = trim($data['region_name']);

        //如果是编辑地区，判断所选上级地区是否存在除所编辑地区外的新地区名称
        if(!empty($data['region_id'])) {
            $where['region_id'] = array('neq', (int)$data['region_id']);
        }
        return $this->where($where)->count() > 0 ? false : true;
    }

    /**
     * 判断父级ID正确性
     * @param $pid
     * @param $id
     * @return bool
     */
    protected function validateEq($parent_id){
        return $parent_id != I('post.region_id');
    }

    public function lists($params) {
        $where = array();
        if($params['id']) {
            $where['b.parent_id'] = (int)$params['id'];
        }elseif($params['keyword']) {
            $where['b.region_name'] = array('LIKE', "%{$params['keyword']}%");
        }else{
            $where['b.parent_id'] = 0;
        }

        if(empty($params['type'])) {
            $data = $this->alias(' AS b')
                ->where($where)
                ->join("__REGION__ AS s ON s.parent_id = b.region_id", 'left')
                ->field("b.region_id as id, b.parent_id as pid, b.region_name as text, count(s.region_id) as have_children")
                ->group('b.region_id')
                ->order('b.parent_id ASC')
                ->select();

            foreach($data as $key => $row){
                if($row['have_children']) {
                    $row['state'] = 'closed';
                    $data[$key] = $row;
                }
            }
        }else {
            $data = $this->alias(' AS b')
                ->field("b.region_id as id, b.parent_id as pid, b.region_name as text")
                ->order('b.parent_id ASC')
                ->select();
            Common::buildTree($data, $params['selected']);
        }

        return $data;
    }


    function getRegion($parent_id){
        $data = $this->where(array('parent_id' => $parent_id))->select();
        return $data;
    }
}