<?php
/**
 * ====================================
 * 系统菜单公共类
 * ====================================
 * Author: Hugo
 * Date: 14-6-2 上午11:09
 * ====================================
 * File: MenuService.class.php
 * ====================================
 */
namespace Common\Library\Manage;
class Menu {
    public static function children($menuId = 0) {
        $data = M('Menu')->where("pid = %d", $menuId)->select();
        if($data){
            foreach($data as $key => $row){
                $query = str_replace(',', '&', str_replace(array(' ', '，'), array('', ','), $row['params']));
                parse_str($query, $params);
                $params['menuid'] = $row['id'];
                $action = empty($row['action']) ? C('DEFAULT_ACTION') : $row['action'];
                $row['href'] = U($row['module'] . '/' . $action, $params);
                $data[$key] = $row;
            }
        }
        return $data;
    }
}