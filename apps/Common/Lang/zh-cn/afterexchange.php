<?php
return array(
    'user_id_lost'           => '客户编号不能为空！',
    'mobile_lost'            => '手机号码不能为空！',
    'mobile_error'           => '手机号码不正确！',
    'order_sn_lost'          => '原订单号不能为空！',
    'exchange_lost'          => '退件类型不能为空!',
);