<?php
/**
 * ====================================
 * 公共数据模型类
 * ====================================
 * Author: Hugo
 * Date: 14-5-20 下午9:58
 * ====================================
 * File: CommonModel.class.php
 * ====================================
 */

namespace Common\Model;
use Common\Library\Common;
use Think\Model;

abstract class CommonModel extends Model {
    protected $_auto = array(
        array('create_time', 'date', self::MODEL_INSERT, 'function', 'Y-m-d H:i:s'),
        array('update_time', 'date', self::MODEL_UPDATE, 'function', 'Y-m-d H:i:s'),
    );
    /**
     * easyui分页处理
     * @param array $where
     * @param string $order_by
     * @param string $join
     * @param string $alias
     * @param int $pagesize
     * @return array
     */
    public function lists($params = array()) {
        list($orderBy, $page, $pageSize) = Common::pageInfo($params);

        //统计总记录数
        $options = $this->options;
        $total = $this->count();

        //排序并获取分页记录
        $options['order'] = empty($options['order']) ? $orderBy : $options['order'];
        $this->options = $options;
        $this->limit($pageSize)->page($page);
        $rows = $this->select();
        return array('total' => (int)$total, 'rows' => (empty($rows) ? false : $rows), 'pagecount' => ceil($total / $pageSize));
    }

    /**
     * 获取分页信息
     * @param array $params
     * @return array
     */
    public function getPage($params = array()) {
        $options = $this->options;
        //统计总记录数
        $total = $this->count();
        $params = array_merge(array(
            'total'     => $total,
            'page_size' => 10,
            'show_num'  => 5
        ), $params);
        $pager = Common::getPager($params);

        $this->options = $options;
        $this->limit($params['page_size'])->page($pager['page']);
        $rows = $this->select();
        return array('pager' => $pager, 'rows' => $rows, 'total'=>$total);
    }

    /**
     * 获取指定数据库的所有表名
     * @author huajie <banhuajie@163.com>
     */
    public function getTables(){
        $tables = M()->query('SHOW TABLES;');
        foreach ($tables as $key=>$value){
            $table_name = $value['Tables_in_'.C('DB_NAME')];
            $table_name = substr($table_name, strlen(C('DB_PREFIX')));
            $tables[$key] = $table_name;
        }
        return $tables;
    }
}