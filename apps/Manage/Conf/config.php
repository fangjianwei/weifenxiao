<?php
return array(
    'LAYOUT_ON'                 => true,
    'LAYOUT_NAME'               => 'layout',
    'TOKEN_ON'                  => false,
    'DEFAULT_THEME'             => '',
    'VIEW_PATH'                 => '',
    'THEME_LIST'                => '',
    'TMPL_ACTION_ERROR'         => 'Public:message',
    'TMPL_ACTION_SUCCESS'       => 'Public:message',
    'TMPL_PARSE_STRING'         => array(
        '__STATIC__' => __ROOT__ . '/public/static',
        '__IMG__'    => __ROOT__ . '/public/cpanel/img',
        '__CSS__'    => __ROOT__ . '/public/cpanel/css',
        '__JS__'     => __ROOT__ . '/public/cpanel/js',
    ),
);